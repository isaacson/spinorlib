#include <thrust/device_ptr.h>
#include <thrust/count.h>
#include <thrust/execution_policy.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/functional.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/random.h>
#include <thrust/execution_policy.h>

#include <chrono>
#include <cstdlib>
#include <iostream>
#include <random>

// #include "SpinorLib/BG_Amplitude.h"
#include "SpinorLib/Kleiss.cuh"
#include "SpinorLib/Color.cuh"
#include "SpinorLib/reduction.h"
#include "SpinorLib/Rambo.h"
#include "SpinorLib/Statistics.cuh"
#include "cuda/Interface.cuh"

// #include "common/errors.h"

#include "CLI11/CLI11.hpp"

using spinorlib::AllocateOnGpu;
using spinorlib::CopyToGpu;
using spinorlib::MemoryPair;
using Complex = thrust::complex<double>;

constexpr size_t factorial(size_t n) {
    return n == 0 ? 1 : n * factorial(n-1);
}

struct is_nonzero {
    __host__ __device__
    bool operator()(const double &x) {
        return x != 0.0;
    }
};

int main(int argc, char** argv) {
    CLI::App app{"Calculation of gluon amplitudes using a GPU"};

    // Run parameters
    size_t nevents = 1 << 10;
    size_t nexternal = 4;
    double ecm = 1500;
    app.add_option("-e,--nevents", nevents, "The number of events");
    app.add_option("-g,--ngluons", nexternal, "The number of gluons");
    app.add_option("-s,--ecm", ecm, "The center of mass energy");
    
    // Cuts
    double ptCut = 60;
    double etaCut = 2.0;
    double deltaRCut = 0.7;
    app.add_option("--ptCut", ptCut, "Minimum allowed pt");
    app.add_option("--etaCut", etaCut, "Maximum allowed eta");
    app.add_option("--deltaRCut", deltaRCut, "Minimum allowed DeltaR");


    CLI11_PARSE(app, argc, argv);

    std::cout << "Number of events: " << nevents << std::endl;
    auto devices = spinorlib::cuda::GetDevices();
    spinorlib::cuda::SetDevice(0);

    MemoryPair<spinorlib::Color> color(1);
    // MemoryPair<BG_Amplitude> bg(1, nexternal);
    MemoryPair<Kleiss> kleiss(1, nexternal);
    MemoryPair<Rambo> rambo(1, 2, nexternal-2, ptCut, etaCut, deltaRCut, color.device);

    MemoryPair<Vec4D> momentum((1 << nexternal)*nevents);
    CVec4D *devCur = AllocateOnGpu<CVec4D>((1 << nexternal)*nevents*9*sizeof(CVec4D));
    spinorlib::Tensor *devTen = AllocateOnGpu<spinorlib::Tensor>((1 << nexternal)*nevents*9*sizeof(spinorlib::Tensor));
    MemoryPair<double> weights(nevents);
    double *rans = AllocateOnGpu<double>((10*nexternal-8)*nevents*sizeof(double));

    MemoryPair<double> amps2(nevents);
    curandState *devStates = AllocateOnGpu<curandState>(nevents*sizeof(curandState));

    for(size_t i = 0; i < nevents; ++i) {
        momentum.host[i*(1 << (nexternal - 1))] = Vec4D(ecm/2, 0, 0, ecm/2);
        momentum.host[i*(1 << (nexternal - 1))+1] = Vec4D(ecm/2, 0, 0, -ecm/2);
    }

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    color.ToGpu();
    momentum.ToGpu();
    rambo.ToGpu();
    // bg.ToGpu();
    kleiss.ToGpu();

    cudaEventRecord(start);
    SeedRandom<<<nevents/512, 512>>>(devStates, 123456789);
    cudaDeviceSynchronize();
    CheckCudaCall(cudaPeekAtLastError());

    GenerateMomenta<<<nevents/512, 512>>>(devStates, rambo.device, momentum.device,
                                          devCur, weights.device, rans);
    cudaDeviceSynchronize();

    momentum.FromGpu();
    // weights.FromGpu();

    CheckCudaCall(cudaPeekAtLastError());
    EvaluateCDAmplitude<<<nevents/1024, 1024>>>(kleiss.device, amps2.device, momentum.device, devCur, devTen, weights.device);
    cudaDeviceSynchronize();
    CheckCudaCall(cudaPeekAtLastError());
    cudaEventRecord(stop);

    amps2.FromGpu();

    // size_t count = 0;
    // double sum = 0;
    // for(auto amp2 : amps2.host) {
    //     if(count++ < 10) fmt::print("Event {}: Amplitude^2 = {}\n", count, amp2);
    //     sum += amp2;
    // }
    // fmt::print("xsec = {}\n", sum/nevents/2/ecm);

    cudaEventSynchronize(stop);
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    fmt::print("For {} gluons it took {:.3e} ms for {} events => {:.3e} ms/event\n",
               nexternal, milliseconds, nevents, milliseconds/(nevents));

    // setup stats
    summary_stats_unary_op<double> unary_op;
    summary_stats_binary_op<double> binary_op;
    summary_stats_data<double> init;
    init.initialize();

    cudaEventRecord(start);
    auto result = thrust::transform_reduce(thrust::device, amps2.device, amps2.device + nevents,
                                           unary_op, init, binary_op);
    cudaDeviceSynchronize();
    CheckCudaCall(cudaPeekAtLastError());
    cudaEventRecord(stop);

    cudaEventSynchronize(stop);
    milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    constexpr double initialStateAvg = 16*16;
    constexpr double hbarc2 = 3.89379304e8;
    double nfactorial = factorial(nexternal-2);
    double flux = 2*ecm*ecm;
    double prefactors = hbarc2/flux/initialStateAvg/nfactorial;
    double xsec = result.mean*prefactors;
    double xsec_err = sqrt(result.variance()/nevents)*prefactors;
    fmt::print("Reduction took {:.3e} ms for {} events with a result of {:.3e} +/- {:.3e} pb\n", milliseconds, nevents, xsec, xsec_err);
    double cut_eff = thrust::count_if(thrust::device, amps2.device, amps2.device + nevents,
                                      is_nonzero());
    fmt::print("Cut efficiency: {:.3f}\n", cut_eff/nevents);
    fmt::print("Max weight: {:.3e}\n", result.max*prefactors);
}
