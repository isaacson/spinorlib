#include "SpinorLib/reduction.h"
#include <cooperative_groups.h>

#include "cuda/Interface.cuh"

namespace cg = cooperative_groups;

using spinorlib::MemoryPair;

unsigned int nextPow2(unsigned int x) {
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    return ++x;
}

void GetNumBlocksAndThreads(int n, int maxBlocks, int maxThreads, int &blocks, int &threads) {

    // get device capability, to avoid block/grid size exceed the upper bound
    cudaDeviceProp prop;
    int device;
    CheckCudaCall(cudaGetDevice(&device));
    CheckCudaCall(cudaGetDeviceProperties(&prop, device));

    threads = (n < maxThreads) ? nextPow2(n) : maxThreads;
    blocks = (n + threads - 1) / threads;

    if((float)threads * blocks > (float)prop.maxGridSize[0] * prop.maxThreadsPerBlock) {
        fmt::print("n is too large, please reduce the number of events\n");
    }

    if(blocks > prop.maxGridSize[0]) {
        fmt::print("Grid size <{}> exceeds the device capability <{}>, setting block size as "
                   "{} (original {})\n", blocks, prop.maxGridSize[0], threads * 2, threads);
        blocks /= 2;
        threads *= 2;
    }

    blocks = std::min(maxBlocks, blocks);
}

double ReduceAmplitude(int n, int numThreads, int numBlocks, int maxThreads,
                       int maxBlocks, bool cpuFinalReduction, int cpuFinalThreshold,
                       MemoryPair<double> idata, MemoryPair<double> odata) {
    double result = 0;
    bool needReadBack = true;

    double *intermediateSums;
    CheckCudaCall(cudaMalloc((void **)&intermediateSums, sizeof(double)*numBlocks));

    cudaDeviceSynchronize();
    Reduce(n, numThreads, numBlocks, idata.device, odata.device);
    CheckCudaCall(cudaPeekAtLastError());

    int s = numBlocks;
    while( s > cpuFinalThreshold) {
        int threads = 0, blocks = 0;
        GetNumBlocksAndThreads(s, maxBlocks, maxThreads, blocks, threads);
        CheckCudaCall(cudaMemcpy(intermediateSums, odata.device, s*sizeof(double),
                                 cudaMemcpyDeviceToDevice));
        Reduce(s, threads, blocks, intermediateSums, odata.device);

        s = (s + (threads * 2 - 1)) / (threads * 2);
    }

    if(s > 1) {
        // copy result from device to host
        odata.FromGpu();
        for(int i = 0; i < s; ++i) {
            result += odata.host[i];
        }

        needReadBack = false;
    }

    cudaDeviceSynchronize();
    if(needReadBack) {
        // copy final sum from device to host
        CheckCudaCall(cudaMemcpy(&result, odata.device, sizeof(double), cudaMemcpyDeviceToHost));
    }
    CheckCudaCall(cudaFree(intermediateSums));
    return result;
}

constexpr bool isPow2(unsigned int x) { return ((x & (x-1)) == 0); }

template<unsigned int blockSize, bool nIsPow2>
__global__ void ReduceKernel(double *idata, double *odata, unsigned int n) {
    // Handle to thread block group
    cg::thread_block cta = cg::this_thread_block();
    extern __shared__ double sdata[];

    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    unsigned int tid = threadIdx.x;
    unsigned int gridSize = blockSize * gridDim.x;

    double sum = 0;

    // we reduce multiple elements per thread. The number is determined by the
    // number of active thread blocks (via gridDim). More blocks will result
    // in a larger gridSize and therefore fewer elements per thread
    if(nIsPow2) {
        unsigned int i = blockIdx.x * blockSize * 2 + threadIdx.x;
        gridSize = gridSize << 1;

        while(i < n) {
            sum += idata[i];
            // ensure we don't read out of bounds -- this is optimized away for
            // powerOf2 sized arrays
            if((i + blockSize) < n) {
                sum += idata[i + blockSize];
            }
            i += gridSize;
        }
    } else {
        unsigned int i = blockIdx.x * blockSize + threadIdx.x;
        while(i < n) {
            sum += idata[i];
            i += gridSize;
        }
    }

    // each thread puts its local sum into shared memory
    sdata[tid] = sum;
    cg::sync(cta);

    // do reduction in shared mem
    if((blockSize >= 512) && (tid < 256)) {
        sdata[tid] = sum = sum + sdata[tid + 256];
    }
    cg::sync(cta);

    if((blockSize >= 256) && (tid < 128)) {
        sdata[tid] = sum = sum + sdata[tid + 128];
    }
    cg::sync(cta);

    if((blockSize >= 128) && (tid < 64)) {
        sdata[tid] = sum = sum + sdata[tid + 64];
    }
    cg::sync(cta);

    cg::thread_block_tile<32> tile32 = cg::tiled_partition<32>(cta);

    if(cta.thread_rank() < 32) {
        // Fetch final intermediate sum from 2nd wrap
        if(blockSize >= 64) sum += sdata[tid + 32];
        // Reduce final warp using shuffle
        for(int offset = tile32.size() / 2; offset > 0; offset /= 2)
            sum += tile32.shfl_down(sum, offset);
    }

    // write result for this block to global mem
    if(cta.thread_rank() == 0) odata[blockIdx.x] = sum;
}

void Reduce(int size, int threads, int blocks, double *idata, double *odata) {
    dim3 dimBlock(threads, 1, 1);
    dim3 dimGrid(blocks, 1, 1);

    int smemSize = (threads <= 32) ? 2 * threads * sizeof(double) : threads * sizeof(double);

    if(isPow2(size)) {
        switch(threads) {
            case 512:
                ReduceKernel<512, true><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 256:
                ReduceKernel<256, true><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 128:
                ReduceKernel<128, true><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 64:
                ReduceKernel<64, true><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 32:
                ReduceKernel<32, true><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 16:
                ReduceKernel<16, true><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 8:
                ReduceKernel<8, true><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 4:
                ReduceKernel<4, true><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 2:
                ReduceKernel<2, true><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 1:
                ReduceKernel<1, true><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
        }
    } else {
        switch(threads) {
            case 512:
                ReduceKernel<512, false><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 256:
                ReduceKernel<256, false><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 128:
                ReduceKernel<128, false><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 64:
                ReduceKernel<64, false><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 32:
                ReduceKernel<32, false><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 16:
                ReduceKernel<16, false><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 8:
                ReduceKernel<8, false><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 4:
                ReduceKernel<4, false><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 2:
                ReduceKernel<2, false><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
            case 1:
                ReduceKernel<1, false><<<dimGrid, dimBlock, smemSize>>>(idata, odata, size);
                break;
        }

    }
}
