#include "SpinorLib/Rambo.h"
#include "SpinorLib/Color.cuh"

Rambo::Rambo(int nin_, int nout_, double ptCut_, double etaCut_, double deltaRCut_, spinorlib::Color *color) 
        : nin(nin_), nout(nout_), ptCut(ptCut_), etaCut(etaCut_), deltaRCut(deltaRCut_), p_color(color) {
    double pi2log = log(M_PI/2.);
    double *Z = new double[nout+1];
    Z[2] = pi2log;
    for(size_t k=3; k<=nout; ++k) 
        Z[k] = Z[k-1]+pi2log-2.*log(double(k-2));
    for(size_t k=3; k<=nout; ++k) 
        Z[k] = Z[k]-log(double(k-1));
    Z_N = Z[nout];
    delete[] Z;

    Vec4D k(1.0, 1.0, 0.0, 0.0);
    m_kp = SpinorD::ConstructSpinor(1, k);
    m_km = SpinorD::ConstructSpinor(-1, k);
}

__device__
size_t Rambo::Cut(const Event &event) const {
    int pass = 1;
    if(threadIdx.x==0 && blockIdx.x==0) {
        printf("ptCut = %f, etaCut = %f\n", ptCut, etaCut);
    }
    for(int i = nin; i < nin+nout; ++i) {
        if(threadIdx.x==0 && blockIdx.x==0) {
            printf("event.mom[Cur(%d)].PT() = %f\n", i, event.mom[Cur(i)].PT());
        }
        pass *= event.mom[Cur(i)].PT() > ptCut;
        pass *= abs(event.mom[Cur(i)].Eta()) < etaCut;
    }
    for(int i = nin; i < nin+nout-1; ++i) {
        for(int j = i+1; j < nin+nout; ++j) {
            pass *= event.mom[Cur(i)].DeltaR(event.mom[Cur(j)]) > deltaRCut;
            if(threadIdx.x==0 && blockIdx.x==0)
                printf("DeltaR(%d, %d) = %f\n", i, j, event.mom[Cur(i)].DeltaR(event.mom[Cur(j)]));
        }
    }
    if(threadIdx.x==0 && blockIdx.x==0) {
        printf("Cut = %d\n", pass);
    }
    return pass;
}

__device__
void Rambo::GeneratePoint(Event &event, double *rans, double &weight) {
    Vec4D sump(0., 0., 0., 0.);
    for(size_t i = 0; i < nin; ++i) sump += event.mom[Cur(i)];
    double ET = sqrt(sump.Abs2());

    Vec4D R;
    for(size_t i = nin; i < nin+nout; ++i) {
        double ctheta = 2*rans[4*(i-nin)] - 1;
        double stheta = sqrt(1-ctheta*ctheta);
        double phi = 2*M_PI*rans[1+4*(i-nin)];
        double Q = -log(rans[2+4*(i-nin)]*rans[3+4*(i-nin)]); 
        event.mom[Cur(i)] = Vec4D(Q, Q*stheta*sin(phi), Q*stheta*cos(phi), Q*ctheta);
        R += event.mom[Cur(i)];
    }

    double RMAS = sqrt(R.Abs2());
    Vec3D B = -Vec3D(R)/RMAS;
    double G = R[0]/RMAS;
    double A = 1.0/(1.0+G);
    double X = ET/RMAS;

    for(size_t i = nin; i < nin+nout; ++i) {
        double e = event.mom[Cur(i)][0];
        double BQ = B*Vec3D(event.mom[Cur(i)]);
        event.mom[Cur(i)] = X*Vec4D((G*e+BQ), Vec3D(event.mom[Cur(i)])+B*(e+A*BQ));
    }
            
    weight = exp((2.*nout - 4.)*log(ET)+Z_N)/pow(2.*M_PI, nout*3. - 4);
    weight *= Cut(event);

    // Setup the polarization vectors and color matrices
    spinorlib::zArray z;
    thrust::complex<double> *colors = new thrust::complex<double>[9];
    for(int i = 0; i < nin+nout; ++i) {
        int idx = 4*nout+6*i;
        if(i < nin) event.mom[Cur(i)] = -event.mom[Cur(i)];
        if(threadIdx.x==0 && blockIdx.x==0) {
            printf("mom(%d) = {%f,%f,%f,%f}\n", i,
                   event.mom[Cur(i)][0],
                event.mom[Cur(i)][1],
                event.mom[Cur(i)][2],
                event.mom[Cur(i)][3]);
        }

        // Differential elements
        constexpr auto dphase = M_PI;
        constexpr auto dphi = 2*M_PI;
        constexpr auto dtheta = M_PI/2;
        constexpr auto dzeta = M_PI/2;

        // Polarization vector
        thrust::complex<double> phase = thrust::exp(thrust::complex<double>(0, dphase)*rans[idx]);
        CVec4D eps = (phase*EP(event.mom[Cur(i)]) + thrust::conj(phase)*EM(event.mom[Cur(i)]))*sqrttwo;
        // CVec4D eps;
        // if(i < nin) eps = EP(event.mom[Cur(i)]);
        // else eps = EM(event.mom[Cur(i)]);

        // angles for z
        double phi[3]{dphi*rans[idx+1], dphi*rans[idx+2], dphi*rans[idx+3]};
        double theta = dtheta*rans[idx+4];
        double zeta = dzeta*rans[idx+5];

        // Weight for sampling
        // weight *= dtheta*dzeta*pow(dphi, 3)*cos(theta)*pow(sin(theta), 3)*cos(zeta)*sin(zeta);
        // weight /= pow(M_PI, 3);

        p_color -> MakeZ(phi, theta, zeta, z);
        p_color -> MakeEtaIJ(z, colors);

        for(int j = 0; j < 3; ++j) {
            for(int k = 0; k < 3; ++k) {
                event.current[CurIdx((1 << i)-1, j, k)] = colors[3*j+k]*eps*sqrt(6.0);
                // if((i == 0 && j == 0 && k == 0) || (i == 1 && j == 0 && k == 2) || (i == 2 && j == 2 && k == 0) || (i == 3 && j == 2 && k == 0) || (i == 4 && j == 0 && k == 2)) {
                //     event.current[CurIdx((1 << i)-1, j, k)] = eps;//*sqrt(6.0);
                //     if(threadIdx.x==0 && blockIdx.x==0) {
                //         printf("current(%d, %d, %d) = {(%f,%f), (%f,%f), (%f,%f), (%f,%f)}\n", (1<<i)-1, j, k,
                //                event.current[CurIdx((1<<i)-1,j,k)][0].real(), event.current[CurIdx((1<<i)-1,j,k)][0].imag(),
                //                event.current[CurIdx((1<<i)-1,j,k)][1].real(), event.current[CurIdx((1<<i)-1,j,k)][1].imag(),
                //                event.current[CurIdx((1<<i)-1,j,k)][2].real(), event.current[CurIdx((1<<i)-1,j,k)][2].imag(),
                //                event.current[CurIdx((1<<i)-1,j,k)][3].real(), event.current[CurIdx((1<<i)-1,j,k)][3].imag());
                //     }
                // }
            }
        }
    }
    delete[] colors;
    auto idx = blockIdx.x*blockDim.x+threadIdx.x;
    if(idx==0)
        printf("rambo weight = %f\n", weight);
}

__global__ void SeedRandom(curandState *state, size_t seed) {
    int id = threadIdx.x+blockIdx.x*blockDim.x;
    curand_init(seed, id, 0, &state[id]);
}

__global__ void GenerateMomenta(curandState *state, Rambo *rambo,
                                Vec4D *mom, CVec4D *eps,
                                double *wgts, double *rans) {
    int id = threadIdx.x + blockIdx.x*blockDim.x;
    Event event;
    event.max_cur = 1 << (rambo->Npart() - 1);
    event.mom = &mom[id*event.max_cur];
    event.current = &eps[id*9*event.max_cur];
    double *thread_rans = rans+(10*rambo->Nout() + 6*rambo->Nin())*id;

    // Copy state to local memory for efficiency
    curandState localState = state[id];

    // Generate random numbers
    // double *rans = new double[5*rambo->Nout() + rambo->Nin()];
    for(int i = 0; i < 10*rambo->Nout() + 6*rambo->Nin(); ++i) {
        thread_rans[i] = curand_uniform_double(&localState);
    }

    // Generate momentum and get the event weight
    rambo->GeneratePoint(event, thread_rans, wgts[id]);  

    // Copy state back to global memory
    state[id] = localState;
}
