#include "SpinorLib/BG_Amplitude.h"

BG_Amplitude::BG_Amplitude(const int &n) : m_n(n) {
    Vec4D k(1.0, 1.0, 0.0, 0.0);
    m_kp = SpinorD::ConstructSpinor(1, k);
    m_km = SpinorD::ConstructSpinor(-1, k);
}

BG_Amplitude::BG_Amplitude(const BG_Amplitude &other) 
    : m_n(other.m_n), m_kp(other.m_kp), m_km(other.m_km) {}

BG_Amplitude::~BG_Amplitude() {}

__device__
double BG_Amplitude::CalcJL(Vec4D *ps, CVec4D *eps, CVec4D *pj) const {
    // Load initial states
    for(int d = 0; d < m_n; ++d)
        pj[d + m_n*d] = eps[d];

    // Calculate currents
    for(int d = 1; d < m_n; ++d) {
        for(int i = 1; i < m_n-d; ++i) {
            pj[i+m_n*(i+d)] = JL(i, i+d, ps, pj);
        }
    }

    // Return final result
    auto result = pj[0]*pj[1+m_n*(m_n-1)];
    return thrust::norm(result);
}

__device__
CVec4D BG_Amplitude::VL(const int &ei, const int &ej, Vec4D *ps, CVec4D *pj) const {
    ps[ei+m_n*ej] = ps[ei+m_n*ei]+ps[ei+1+m_n*ej];
    CVec4D sum(0.0, 0.0, 0.0, 0.0);
    for(int i = ei; i < ej; ++i) {
        if(i > ei) ps[ei+m_n*i] = ps[ei+m_n*ei]+ps[ei+1+m_n*i];
        if(ej > i+1) ps[i+1+m_n*ej] = ps[i+1+m_n*(i+1)]+ps[i+2+m_n*ej];
        sum += sqrt(0.118*4*M_PI)*V3L(ps[ei+m_n*i], ps[i+1+m_n*ej], pj[ei+m_n*i], pj[i+1+m_n*ej]);
        if(i < ej - 1) {
            for(int j = i+1; j < ej; ++j)
                sum += 0.118*4*M_PI*V4L(pj[ei+m_n*i], pj[i+1+m_n*j], pj[j+1+m_n*ej]);
        }
    }

    return sum;
}

__global__
void EvaluateCOAmplitude(BG_Amplitude *bg, Vec4D *mom, double *amps2,
                         CVec4D *devEps, CVec4D *devCur, Vec4D *devMom, int nexternal) {
    Vec4D *ps = &(devMom[nexternal*nexternal*(blockIdx.x*blockDim.x+threadIdx.x)]);
    CVec4D *pj = &(devCur[nexternal*nexternal*(blockIdx.x*blockDim.x+threadIdx.x)]);
    CVec4D *eps = &(devEps[nexternal*(blockIdx.x*blockDim.x+threadIdx.x)]);
    for(int i = 0; i < nexternal; ++i)
        ps[i+nexternal*i] = mom[i + threadIdx.x*nexternal];

    amps2[blockIdx.x*blockDim.x+threadIdx.x] = bg -> CalcJL(ps, eps, pj);
}
