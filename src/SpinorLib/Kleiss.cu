#include "SpinorLib/Kleiss.cuh"
#include "SpinorLib/Color.cuh"

#define MAX_PARTICLES 30

__device__
double Kleiss::CalcJL(Event &event) const {
    for(size_t i = 2; i < m_n; ++i)
        GenerateCurrents(i, event);

    thrust::complex<double> amp = 0;
    for(int i = 0; i < 3; ++i) {
        for(int j = 0; j < 3; ++j) {
            amp += event.current[CurIdx(event.max_cur-1,i,j)]*event.current[CurIdx(event.max_cur-2,j,i)]; 
        }
    }

    if(threadIdx.x==0 && blockIdx.x==0) {
        printf("amp = %f, %f\n", amp.real(), amp.imag());
        printf("amp2 = %f\n", thrust::norm(amp));
    }

    return thrust::norm(amp);
}

__device__
void Kleiss::GenerateCurrents(unsigned int m, Event &e) const {
    unsigned int cur = (1 << m) - 1;
    unsigned int set[MAX_PARTICLES];
    // Combine all currents but the last one
    while(cur < (1 << (m_n - 1))) {
        SetBits(cur, set, m_n-1);
        for(unsigned int iset = 1; iset < m; ++iset) {
            SubCurrent(cur, iset, m, set, e);
        }

        cur = NextPermutation(cur);
    }
}

__device__
void Kleiss::SubCurrent(unsigned int cur, unsigned int iset, unsigned int nset,
                        unsigned int *set, Event &event) const {
    unsigned int idx = (1 << iset) - 1;
    while(idx < (1 << (nset - 1))) {
        unsigned int subCur1 = 0;
        for(unsigned int i = 0; i < m_n; ++i) {
            subCur1 += set[i]*((idx >> i) & 1);
        }
        auto subCur2 = cur ^ subCur1;
        Vertex3(cur, subCur1, subCur2, event);
        idx = NextPermutation(idx);
    }
}

__device__
CVec4D Kleiss::V3L(const Vec4D &p1, const Vec4D &p2,
                   const CVec4D &j1, const CVec4D &j2) const {
    return invsqrttwo*(j1*j2)*(p1-p2) + sqrttwo*((j1*p2)*j2-(j2*p1)*j1);
}

__device__
void Kleiss::Vertex3(unsigned int cur, unsigned int sub1, unsigned int sub2,
                     Event &e) const {
    cur -= 1;
    sub1 -= 1;
    sub2 -= 1;

    double coupling = sqrt(0.118*4*M_PI);
    double coupling2 = sqrt(0.118*4*M_PI/2);
    e.mom[cur] = e.mom[sub1] + e.mom[sub2];
    double denom = cur == (e.max_cur-2) ? 1 : e.mom[cur].Abs2();
    // if(threadIdx.x==0 && blockIdx.x==0) {
    //     printf("denom = %f\n", denom);
    // }


    for(int i = 0; i < 3; ++i) {
        for(int j = 0; j < 3; ++j) {
            for(int k = 0; k < 3; ++k) {
                // Vector <- Vector-Vector vertex
                e.current[CurIdx(cur, i, j)] += (V3L(e.mom[sub1], e.mom[sub2],
                                                     e.current[CurIdx(sub1, i, k)],
                                                     e.current[CurIdx(sub2, k, j)])
                                                 - V3L(e.mom[sub1], e.mom[sub2],
                                                       e.current[CurIdx(sub1, k, j)],
                                                       e.current[CurIdx(sub2, i, k)])) * coupling/denom;

                // Vector <- Tensor-Vector vertex
                e.current[CurIdx(cur, i, j)] += (e.tensor[CurIdx(sub1, i, k)]*e.current[CurIdx(sub2, k, j)]
                                                -e.tensor[CurIdx(sub1, k, j)]*e.current[CurIdx(sub2, i, k)]) 
                                                * coupling2 / denom;
                e.current[CurIdx(cur, i, j)] += (e.tensor[CurIdx(sub2, i, k)]*e.current[CurIdx(sub1, k, j)]
                                                -e.tensor[CurIdx(sub2, k, j)]*e.current[CurIdx(sub1, i, k)]) 
                                                * coupling2 / denom;
                
                // Tensor <- Vector-Vector vertex
                e.tensor[CurIdx(cur, i, j)] += (spinorlib::Tensor(e.current[CurIdx(sub1, i, k)],
                                                                  e.current[CurIdx(sub2, k, j)])
                                                - spinorlib::Tensor(e.current[CurIdx(sub1, k, j)],
                                                                    e.current[CurIdx(sub2, i, k)])) * coupling2;
                
                // if(threadIdx.x==0 && blockIdx.x==0) {
                //     auto tmp = (e.tensor[CurIdx(sub2, i, k)]*e.current[CurIdx(sub1, k, j)]
                //                -e.tensor[CurIdx(sub2, k, j)]*e.current[CurIdx(sub1, i, k)]);
                //     printf("tmp = {(%f, %f), (%f, %f), (%f, %f), (%f, %f)}\n",
                //             tmp[0].real(), tmp[0].imag(),
                //             tmp[1].real(), tmp[1].imag(),
                //             tmp[2].real(), tmp[2].imag(),
                //             tmp[3].real(), tmp[3].imag());
                // }
            }
            // if(threadIdx.x==0 && blockIdx.x==0) {
            //     printf("tensor(%d, %d, %d) = {(%f,%f), (%f,%f), (%f,%f), (%f,%f), (%f, %f), (%f, %f)}\n", cur, i, j,
            //            e.tensor[CurIdx(cur,i,j)][0].real(), e.tensor[CurIdx(cur,i,j)][0].imag(),
            //            e.tensor[CurIdx(cur,i,j)][1].real(), e.tensor[CurIdx(cur,i,j)][1].imag(),
            //            e.tensor[CurIdx(cur,i,j)][2].real(), e.tensor[CurIdx(cur,i,j)][2].imag(),
            //            e.tensor[CurIdx(cur,i,j)][3].real(), e.tensor[CurIdx(cur,i,j)][3].imag(),
            //            e.tensor[CurIdx(cur,i,j)][4].real(), e.tensor[CurIdx(cur,i,j)][4].imag(),
            //            e.tensor[CurIdx(cur,i,j)][5].real(), e.tensor[CurIdx(cur,i,j)][5].imag());
            //     printf("current(%d, %d, %d) = {(%f,%f), (%f,%f), (%f,%f), (%f,%f)}\n", cur, i, j,
            //            e.current[CurIdx(cur,i,j)][0].real(), e.current[CurIdx(cur,i,j)][0].imag(),
            //            e.current[CurIdx(cur,i,j)][1].real(), e.current[CurIdx(cur,i,j)][1].imag(),
            //            e.current[CurIdx(cur,i,j)][2].real(), e.current[CurIdx(cur,i,j)][2].imag(),
            //            e.current[CurIdx(cur,i,j)][3].real(), e.current[CurIdx(cur,i,j)][3].imag());
            // }
        }
    }
}

__global__
void EvaluateCDAmplitude(Kleiss *kleiss, double *amps2, Vec4D *mom,
                         CVec4D *cur, spinorlib::Tensor *tensor, double *wgts) {
    // Load event
    auto idx = blockIdx.x*blockDim.x+threadIdx.x;
    Event event;
    event.max_cur = 1 << (kleiss->size() - 1); 
    event.mom = &mom[idx*event.max_cur];
    event.current = &cur[9*idx*event.max_cur];
    event.tensor = &tensor[9*idx*event.max_cur];

    amps2[idx] = kleiss -> CalcJL(event)*wgts[idx];

    // if(idx==0)
    //     printf("amp = %f, wgt = %f\n", amps2[idx], wgts[idx]);
}
