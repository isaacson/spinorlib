*
* validating eqs 3.4 against 3.1
*
      program validate
*
      use colorpolarization
*
      implicit none
      integer i,j,k,N,nevent,ntrial
      double precision wgt
      double precision result2,result3,result4,result5,result6
      double precision wgt2,wgt3,wgt4,wgt5,wgt6
      double precision avg2,avg3,avg4,avg5,avg6
      double precision sd2,sd3,sd4,sd5,sd6
      double precision yT2z,yT3z,yT4z,yT5z,yT6z
      double precision Tr1,Tr2,Tr3,Tr4,Tr5,Tr6
*     
      integer,parameter::seed=66453
      call srand(seed)  
*
      ntrial=10
      nevent=100000000
*
      N=6
      avg2=0d0
      avg3=0d0
      avg4=0d0
      avg5=0d0
      avg6=0d0
      sd2=0d0
      sd3=0d0
      sd4=0d0
      sd5=0d0
      sd6=0d0
      do i=1,ntrial
         write(*,*) "starting trial",i
         result2=0d0
         result3=0d0
         result4=0d0
         result5=0d0
         result6=0d0
         do j=1,nevent
            do k=1,N
               y(k,:)=colpol()
               z(k,:)=colpol()
            enddo
            wgt2=Tr2(1,2)**2
            wgt3=Tr3(1,2,3)**2/2d0
            wgt4=Tr4(1,2,3,4)**2/4d0
            wgt5=Tr5(1,2,3,4,5)*Tr5(1,2,4,3,5)/8d0
            wgt6=Tr6(1,2,3,4,5,6)*Tr6(6,2,5,3,1,4)/16d0
            result2=result2+wgt2
            result3=result3+wgt3
            result4=result4+wgt4
            result5=result5+wgt5
            result6=result6+wgt6
         enddo
         result2=result2/nevent
         result3=result3/nevent
         result4=result4/nevent
         result5=result5/nevent
         result6=result6/nevent
         avg2=avg2+result2
         avg3=avg3+result3
         avg4=avg4+result4
         avg5=avg5+result5
         avg6=avg6+result6
         sd2=sd2+result2**2
         sd3=sd3+result3**2
         sd4=sd4+result4**2
         sd5=sd5+result5**2
         sd6=sd6+result6**2
      enddo
      avg2=avg2/ntrial
      avg3=avg3/ntrial
      avg4=avg4/ntrial
      avg5=avg5/ntrial
      avg6=avg6/ntrial
      sd2=sd2/ntrial
      sd3=sd3/ntrial
      sd4=sd4/ntrial
      sd5=sd5/ntrial
      sd6=sd6/ntrial
      sd2=sqrt((sd2-avg2**2)/ntrial)
      sd3=sqrt((sd3-avg3**2)/ntrial)
      sd4=sqrt((sd4-avg4**2)/ntrial)
      sd5=sqrt((sd5-avg5**2)/ntrial)
      sd6=sqrt((sd6-avg6**2)/ntrial)

      write(*,*)
      write(*,*) "C2:",2d0,avg2,"+/-",sd2
      write(*,*) "C3:",7d0/3d0,avg3,"+/-",sd3
      write(*,*) "C4:",19d0/6d0,avg4,"+/-",sd4
      write(*,*) "C5:",-29d0/54d0,avg5,"+/-",sd5
      write(*,*) "C6:",5d0/648d0,avg6,"+/-",sd6
      end
*
*
*
      double precision function yT1z(i1,i2,j) result(T)
      use colorpolarization
      implicit none
      integer i1,i2,j
* 
      T=0.5d0*(bra(i1,i2)*bra(i2,j)-Ninv*bra(i2,i2)*bra(i1,j))
*
      end function
*
*
      double precision function yT2z(i1,i2,i3,j) result(T)
      use colorpolarization
      implicit none
      integer i1,i2,i3,j
      double precision yT1z
*
      T=bra(i1,i2)*yT1z(i2,i3,j)-Ninv*bra(i2,i2)*yT1z(i1,i3,j)
*
      end function

      double precision function yT3z(i1,i2,i3,i4,j) result(T)
      use colorpolarization
      implicit none
      integer i1,i2,i3,i4,j
      double precision yT2z
*
      T=bra(i1,i2)*yT2z(i2,i3,i4,j)-Ninv*bra(i2,i2)*yT2z(i1,i3,i4,j)
*
      end function

      double precision function yT4z(i1,i2,i3,i4,i5,j) result(T)
      use colorpolarization
      implicit none
      integer i1,i2,i3,i4,i5,j
      double precision yT3z
*
      T=     bra(i1,i2)*yT3z(i2,i3,i4,i5,j)
     . -Ninv*bra(i2,i2)*yT3z(i1,i3,i4,i5,j)
*
      end function

      double precision function yT5z(i1,i2,i3,i4,i5,i6,j) result(T)
      use colorpolarization
      implicit none
      integer i1,i2,i3,i4,i5,i6,j
      double precision yT4z
*
      T=     bra(i1,i2)*yT4z(i2,i3,i4,i5,i6,j)
     . -Ninv*bra(i2,i2)*yT4z(i1,i3,i4,i5,i6,j)
*
      end function

      double precision function Tr1(i1) result(Tr)
      use colorpolarization
      implicit none
      integer i1
*
      Tr=0d0
*     
      end function

      double precision function Tr2(i1,i2) result(Tr)
      use colorpolarization
      implicit none
      integer i1,i2
      double precision yT1z,Tr1
*
      Tr=yT1z(i1,i2,i1)-Ninv*bra(i1,i1)*Tr1(i2)
*     
      end function

      double precision function Tr3(i1,i2,i3) result(Tr)
      use colorpolarization
      implicit none
      integer i1,i2,i3
      double precision yT2z,Tr2
*
      Tr=yT2z(i1,i2,i3,i1)-Ninv*bra(i1,i1)*Tr2(i2,i3)
*     
      end function

      double precision function Tr4(i1,i2,i3,i4) result(Tr)
      use colorpolarization
      implicit none
      integer i1,i2,i3,i4
      double precision yT3z,Tr3
*
      Tr=yT3z(i1,i2,i3,i4,i1)-Ninv*bra(i1,i1)*Tr3(i2,i3,i4)
*     
      end function

      double precision function Tr5(i1,i2,i3,i4,i5) result(Tr)
      use colorpolarization
      implicit none
      integer i1,i2,i3,i4,i5
      double precision yT4z,Tr4
*
      Tr=yT4z(i1,i2,i3,i4,i5,i1)-Ninv*bra(i1,i1)*Tr4(i2,i3,i4,i5)
*     
      end function

      double precision function Tr6(i1,i2,i3,i4,i5,i6) result(Tr)
      use colorpolarization
      implicit none
      integer i1,i2,i3,i4,i5,i6
      double precision yT5z,Tr5
*
      Tr=yT5z(i1,i2,i3,i4,i5,i6,i1)-Ninv*bra(i1,i1)*Tr5(i2,i3,i4,i5,i6)
*     
      end function
