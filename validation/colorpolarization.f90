      module colorpolarization
        implicit none
        double precision y(6,3),z(6,3)
        double precision,parameter::Ninv=1d0/3d0
        double precision,parameter::pi=3.14159265d0

      contains

        double precision function bra(i,j)
          implicit none
          integer i,j
          
          bra=y(i,1)*z(j,1)+y(i,2)*z(j,2)+y(i,3)*z(j,3)
          
        end function bra

        function colpol() result(y)
          implicit none
          double precision cosphi,sinphi,theta
          double precision y(3)

          cosphi=2d0*rand()-1d0
          theta=2d0*pi*rand()
          sinphi=sqrt(1d0-cosphi)
          y(1)=sqrt(3d0)*cosphi
          y(2)=sqrt(2d0)*sinphi*cos(theta)
          y(3)=sqrt(2d0)*sinphi*sin(theta)
        end function colpol

      end module colorpolarization
         


