*
* validating eqs 3.4 against 3.1
*
      program validate
      implicit none
*
      interface
         function colpol() result(y)
           double precision y(3)
         end function
      end interface
*
      integer i,j,nevent
      integer,parameter::N=3
      double precision wgt
      double precision y11,y12,y13,y22,y23,y33
      double precision y1(3),z1(3),y2(3),z2(3)
      double precision,parameter::pi=3.14159265d0
*
      integer,parameter::seed=66453
      call srand(seed)  
*
      nevent=10000000
*
      write(*,*) "-------------------------------------------------"
      write(*,*) "checking eqs. (3.4) against (3.1):"
      write(*,*)
      y11=0d0
      y12=0d0
      y13=0d0
      y22=0d0
      y23=0d0
      y33=0d0

      do i=1,nevent
         y1=colpol()
         wgt=y1(1)*y1(1)
         y11=y11+wgt
         wgt=y1(1)*y1(2)
         y12=y12+wgt
         wgt=y1(1)*y1(3)
         y13=y13+wgt
         wgt=y1(2)*y1(2)
         y22=y22+wgt
         wgt=y1(2)*y1(3)
         y23=y23+wgt
         wgt=y1(3)*y1(3)
         y33=y33+wgt
      enddo

      write(*,*) "d_11,d_12,d_13=",y11/nevent,y12/nevent,y13/nevent
      write(*,*) "d_12,d_13=",y22/nevent,y23/nevent
      write(*,*) "d_33=",y33/nevent
      write(*,*) 
 
      end
*
*
*
      double precision function bra(y,z)
      implicit none
      double precision y(3),z(3)

      bra=y(1)*z(1)+y(2)*z(2)+y(3)*z(3)

      end function
*
*
*
      function colpol() result(y)
      implicit none
      double precision cosphi,sinphi,theta
      double precision y(3)
      double precision,parameter::pi=3.14159265d0
*     
      cosphi=2d0*rand()-1d0
      theta=2d0*pi*rand()
      sinphi=sqrt(1d0-cosphi)
      y(1)=sqrt(3d0)*cosphi
      y(2)=sqrt(2d0)*sinphi*cos(theta)
      y(3)=sqrt(2d0)*sinphi*sin(theta)
*
      end function
