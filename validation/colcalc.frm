symbol N;
Auto cfunction T;
Auto index a=8,i=N,j=N;

L E2=T(a1,i1,i2)*T(a2,i2,i1);
L E2t=T(a2,j2,j1)*T(a1,j1,j2);

L E3=T(a1,i1,i2)*T(a2,i2,i3)*T(a3,i3,i1);
L E3t=T(a3,j3,j2)*T(a2,j2,j1)*T(a1,j1,j3);

L E4=T(a1,i1,i2)*T(a2,i2,i3)*T(a3,i3,i4)*T(a4,i4,i1);
L E4t=T(a4,j4,j3)*T(a3,j3,j2)*T(a2,j2,j1)*T(a1,j1,j4);

L E5=T(a1,i1,i2)*T(a2,i2,i3)*T(a3,i3,i4)*T(a4,i4,i5)*T(a5,i5,i1);
L E5t=T(a5,j5,j4)*T(a3,j4,j3)*T(a4,j3,j2)*T(a2,j2,j1)*T(a1,j1,j5);

L E6=T(a1,i1,i2)*T(a2,i2,i3)*T(a3,i3,i4)*T(a4,i4,i5)*T(a5,i5,i6)*T(a6,i6,i1);
L E6t=T(a6,j6,j5)*T(a2,j5,j4)*T(a5,j4,j3)*T(a3,j3,j2)*T(a1,j2,j1)*T(a4,j1,j6);

L C2=E2*E2t;
L C3=E3*E3t;
L C4=E4*E4t;
L C5=E5*E5t;
L C6=E6*E6t;

Id T(a?,i1?,j1?)*T(a?,i2?,j2?)=(d_(i1,j2)*d_(i2,j1)-d_(i1,j1)*d_(i2,j2)/N)/2;

Id N=3;
Al N^-1=1/3;

Print;
.end