*
* validating eqs 5.1-5.3
*
      program validate
      implicit none
*
      interface
         function colpol() result(y)
           double precision y(3)
         end function
      end interface
*
      integer i,j,nevent,ntrials
      integer,parameter::N=3
      double precision anly,mcresult,wgt,avg,bra,sd
      double precision y11,y12,y13,y22,y23,y33
      double precision y1(3),z1(3),y2(3),z2(3)
      double precision,parameter::pi=3.14159265d0
*
      integer,parameter::seed=66453
      call srand(seed)  
*
      ntrials=10
      nevent=1000000
*
      write(*,*) "-------------------------------------------------"
      write(*,*) "checking eq. (5.3) against (5.1):"
      write(*,*)
      anly=(N**2-1d0)/4d0
      avg=0d0
      sd=0d0

      do j=1,ntrials
      mcresult=0d0
      do i=1,nevent
         y1=colpol()
         z1=colpol()
         y2=colpol()
         z2=colpol()
         wgt=(0.5d0*(bra(z2,y1)*bra(z1,y2)-bra(z1,y1)*bra(y2,z2)/N))**2
         mcresult=mcresult+wgt
      enddo
      wgt=mcresult/nevent
      write(*,*) j,wgt
      avg=avg+wgt
      sd=sd+wgt**2
      enddo
      write(*,*)
      avg=avg/ntrials
      sd=sd/ntrials
      write(*,*) anly,avg,sqrt((sd-avg**2)/ntrials)
*
      end
*
*
*
      double precision function bra(y,z)
      implicit none
      double precision y(3),z(3)

      bra=y(1)*z(1)+y(2)*z(2)+y(3)*z(3)

      end function
*
*
*
      function colpol() result(y)
      implicit none
      double precision cosphi,sinphi,theta
      double precision y(3)
      double precision,parameter::pi=3.14159265d0
*     
      cosphi=2d0*rand()-1d0
      theta=2d0*pi*rand()
      sinphi=sqrt(1d0-cosphi)
      y(1)=sqrt(3d0)*cosphi
      y(2)=sqrt(2d0)*sinphi*cos(theta)
      y(3)=sqrt(2d0)*sinphi*sin(theta)
*
      end function
