#pragma once

#include "SpinorLib/Vector.h"
#include "thrust/complex.h"

using Complex = thrust::complex<double>;

namespace spinorlib {

class Tensor {
    public:
        __host__ __device__
        Tensor() : m_x{0} {}

        __host__ __device__
        Tensor(const Tensor &t) {
            m_x[0] = t[0]; m_x[1] = t[1]; m_x[2] = t[2];
            m_x[3] = t[3]; m_x[4] = t[4]; m_x[5] = t[5];
        }

        __host__ __device__
        Tensor(const Complex x[6]) {
            m_x[0] = x[0]; m_x[1] = x[1]; m_x[2] = x[2];
            m_x[3] = x[3]; m_x[4] = x[4]; m_x[5] = x[5];
        }

        __host__ __device__
        Tensor(const Complex &x0, const Complex &x1, const Complex &x2,
               const Complex &x3, const Complex &x4, const Complex &x5) {
            m_x[0] = x0; m_x[1] = x1; m_x[2] = x2;
            m_x[3] = x3; m_x[4] = x4; m_x[5] = x5;
        }

        __host__ __device__
        Tensor(const CVec4D &v1, const CVec4D &v2) {
            m_x[0]=v1[0]*v2[1]-v1[1]*v2[0];// 0,1 
            m_x[1]=v1[1]*v2[2]-v1[2]*v2[1];// 1,2 
            m_x[2]=v1[2]*v2[3]-v1[3]*v2[2];// 2,3 
            m_x[3]=v1[0]*v2[2]-v1[2]*v2[0];// 0,2 
            m_x[4]=v1[1]*v2[3]-v1[3]*v2[1];// 1,3 
            m_x[5]=v1[0]*v2[3]-v1[3]*v2[0];// 0,3 
        }

        __host__ __device__
        Complex& operator[](const int i) { return m_x[i]; }
        __host__ __device__
        Complex operator[](const int i) const { return m_x[i]; }

        __host__ __device__
        Tensor operator+(const Tensor &other) const {
            return Tensor(m_x[0]+other[0], m_x[1]+other[1],m_x[2]+other[2],
                          m_x[3]+other[3], m_x[4]+other[4],m_x[5]+other[5]);
        }

        __host__ __device__
        Tensor operator-() const {
            return Tensor(-m_x[0], -m_x[1], -m_x[2],
                          -m_x[3], -m_x[4], -m_x[5]);
        }

        __host__ __device__
        Tensor operator-(const Tensor &other) const {
            return (*this) + (-other);
        }

        __host__ __device__
        Tensor operator+=(const Tensor &other) {
            (*this) = (*this) + other;
            return *this;
        }

        __host__ __device__
        Tensor operator-=(const Tensor &other) {
            (*this) = (*this) - other;
            return *this;
        }

        __host__ __device__
        Tensor operator*=(const Complex &c) {
            m_x[0] *= c; m_x[1] *= c; m_x[2] *= c;
            m_x[3] *= c; m_x[4] *= c; m_x[5] *= c;
            return *this;
        }

        __host__ __device__
        Tensor operator*(const Complex &c) const {
            return Tensor(m_x[0]*c, m_x[1]*c, m_x[2]*c,
                          m_x[3]*c, m_x[4]*c, m_x[5]*c);
        }

        __host__ __device__
        Tensor Conj() const {
            return Tensor(thrust::conj(m_x[0]), thrust::conj(m_x[1]), thrust::conj(m_x[2]),
                          thrust::conj(m_x[3]), thrust::conj(m_x[4]), thrust::conj(m_x[5]));
        }

    private:
        Complex m_x[6];
};

__host__ __device__
inline Tensor operator*(const Complex &c, const Tensor &t) {
    return t*c;
}

__host__ __device__
inline Tensor operator/(const Tensor &t, const Complex &c) {
    return t*1.0/c;
}

__host__ __device__
inline CVec4D operator*(const CVec4D &v, const Tensor &t) {
    CVec4D j;
    j[0]=-t[0]/*-0,1*/*v[1]-t[3]/*-0,2*/*v[2]-t[5]/*-0,3*/*v[3];
    j[1]=-t[0]/*-0,1*/*v[0]-t[1]/*-1,2*/*v[2]-t[4]/*-1,3*/*v[3];
    j[2]=-t[3]/*-0,2*/*v[0]+t[1]/* 1,2*/*v[1]-t[2]/*-2,3*/*v[3];
    j[3]=-t[5]/*-0,3*/*v[0]+t[4]/* 1,3*/*v[1]+t[2]/* 2,3*/*v[2];
    return j;
}

__host__ __device__
inline CVec4D operator*(const Tensor &t, const CVec4D &v) {
    return -(v*t);
}

}
