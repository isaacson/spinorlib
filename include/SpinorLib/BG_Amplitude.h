#pragma once

#include "thrust/complex.h"
#include "thrust/host_vector.h"
#include "thrust/device_vector.h"

#include "Vector.h"
#include "Spinor.h"

class Managed {
    public:
        void *operator new(size_t len) {
            void *ptr;
            cudaMallocManaged(&ptr, len);
            cudaDeviceSynchronize();
            return ptr;
        }

        void operator delete(void *ptr) {
            cudaDeviceSynchronize();
            cudaFree(ptr);
        }
};

class BG_Amplitude {
    public:
        BG_Amplitude(const int&);
        BG_Amplitude(const BG_Amplitude&);
        ~BG_Amplitude();

        __host__ __device__
        size_t GetMultiplicity() const { return m_n; }

        __host__ __device__
        static inline bool GetHelicity(const int &chir, const int &n) {
            return chir & (1 << n);
        }

        __device__
        double CalcJL(Vec4D *ps, CVec4D *eps, CVec4D *pj) const;

    private:
        __host__ __device__
        CVec4D V3L(const Vec4D &p1, const Vec4D &p2,
                   const CVec4D &j1, const CVec4D &j2) const {
            return invsqrttwo*(j1*j2)*(p1-p2) + sqrttwo*((j1*p2)*j2-(j2*p1)*j1);
        }

        __host__ __device__
        CVec4D V4L(const CVec4D &j1, const CVec4D &j2, const CVec4D &j3) const {
            return (j1*j3)*j2 - 0.5*((j2*j3)*j1 + (j2*j1)*j3);
        }

        __device__
        CVec4D JL(const int &i, const int &j, Vec4D *ps, CVec4D *pj) const {
            CVec4D vl(VL(i, j, ps, pj));
            return (i==1 && j==m_n-1) ? vl :  vl/ps[i+m_n*j].Abs2();
        }

        __device__
        CVec4D VL(const int &ei, const int &ej, Vec4D *ps, CVec4D *pj) const;

        int m_n;
        SpinorD m_kp, m_km;
};

__global__ 
void EvaluateAmplitude(BG_Amplitude *bg, Vec4D *mom, double *amps2,
                       CVec4D *devEps, CVec4D *devCur, Vec4D *devMom, int nexternal);
