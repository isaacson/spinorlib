#pragma once

#include "thrust/complex.h"

#include "Vector.h"

#define sqrttwo sqrt(2.0)
#define invsqrttwo 1.0/sqrttwo

template<typename T>
class Spinor {
    public:
        using TComplex = thrust::complex<T>;

        __host__ __device__
        Spinor() {}
        __host__ __device__
        Spinor(const int &r, const TComplex &u1, const TComplex &u2) 
            : m_r(r), m_u1(u1), m_u2(u2) {}
        __host__ __device__
        Spinor(const Spinor &s) : m_r(s.m_r), m_u1(s.m_u1), m_u2(s.m_u2) {}

        __host__ __device__
        TComplex U1() const { return m_u1; }
        __host__ __device__
        TComplex U2() const { return m_u2; }

        __host__ __device__
        inline TComplex operator*(const Spinor &s) const {
            return m_u1*s.m_u2 - m_u2*s.m_u1;
        }

        __host__ __device__
        static Spinor<T> ConstructSpinor(const int &r, const Vec4<T> &p) {
            using TComplex = thrust::complex<T>;

            TComplex rpp(sqrt(TComplex(p.PPlus()))), rpm(sqrt(TComplex(p.PMinus()))), pt(PT(p));
            if(pt != TComplex(0.0, 0.0))
                rpm = TComplex(pt.real(), r > 0 ? pt.imag() : -pt.imag()) / rpp;

            return Spinor<T>(r, rpp, rpm);
        }

    private:
        __host__ __device__
        static inline TComplex PT(const Vec4D &p) {
            return TComplex(p[1], p[2]);    
        }

        int m_r;
        TComplex m_u1, m_u2;
};
    
using SpinorD = Spinor<double>;
