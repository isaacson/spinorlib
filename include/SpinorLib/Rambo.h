#pragma once

#include "curand_kernel.h"
#include "thrust/complex.h"

#include "SpinorLib/Spinor.h"
#include "SpinorLib/Vector.h"
#include "SpinorLib/Color.cuh"
#include "SpinorLib/Event.cuh"

class Rambo {
    public:
        __host__ __device__
        Rambo(const Rambo& r) : nin(r.nin), nout(r.nout), ptCut(r.ptCut), etaCut(r.etaCut), Z_N(r.Z_N),
                                deltaRCut(r.deltaRCut), m_km(r.m_km), m_kp(r.m_kp), p_color(r.p_color) {}
        Rambo(int, int, double, double, double, spinorlib::Color*);
        __host__ __device__
        ~Rambo() {}

        __device__
        void GeneratePoint(Event&, double*, double&);
        __host__ __device__
        inline int Nin() const { return nin; }
        __host__ __device__
        inline int Nout() const { return nout; }
        __host__ __device__
        inline int Npart() const { return nin+nout; }

    private:
        __device__
        size_t Cut(const Event&) const;

        __host__ __device__
        CVec4D EP(const Vec4D &mom) const {
            SpinorD pm = SpinorD::ConstructSpinor(-1, mom);
            CVec4D e = EVec(m_kp, pm);
            return e/(sqrttwo*thrust::conj(m_km*pm));
        }

        __host__ __device__
        CVec4D EM(const Vec4D &mom) const {
            SpinorD pp = SpinorD::ConstructSpinor(1, mom);
            CVec4D e = EVec(pp, m_km);
            return e/(sqrttwo*thrust::conj(m_kp*pp));
        }

        __host__ __device__
        static CVec4D EVec(const SpinorD &sp, const SpinorD &sm) {
            CVec4D e;
            e[0] = sp.U1()*sm.U1() + sp.U2()*sm.U2();
            e[3] = sp.U1()*sm.U1() - sp.U2()*sm.U2();
            e[1] = sp.U1()*sm.U2() + sp.U2()*sm.U1();
            e[2] = thrust::complex<double>(0.0, 1.0)*(
                    sp.U1()*sm.U2() - sp.U2()*sm.U1());

            return e;
        }

        int nin, nout;
        double Z_N, ptCut, etaCut, deltaRCut;
        SpinorD m_kp, m_km;
        spinorlib::Color *p_color;
};

__global__ void SeedRandom(curandState*, size_t);
__global__ void GenerateMomenta(curandState*, Rambo*, Vec4D*, CVec4D*,
                                double*, double*);
