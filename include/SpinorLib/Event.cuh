#pragma once

#include "thrust/complex.h"
#include "SpinorLib/Vector.h"
#include "SpinorLib/Tensor.h"

struct Event {
    Vec4D *mom;
    CVec4D *current;
    spinorlib::Tensor *tensor;
    int max_cur, nexternal;
};

__device__
inline unsigned int Cur(unsigned int ipart) {
    return (1 << ipart) - 1;
}

__device__
inline unsigned int CurIdx(unsigned int cur, unsigned int i, unsigned int j) {
    return 9*cur + 3*i + j;
}
