#pragma once

#include "thrust/complex.h"

namespace spinorlib {

using zArray = thrust::complex<double>[3];
using etaA = thrust::complex<double>[8];
using etaIJ = thrust::complex<double>[9];

class Color {
    public:
        Color();
        __host__ __device__
        Color(const Color &c) {
            for(int i = 0; i < 72; ++i)
                colorOperator[i] = c.colorOperator[i];
        }

        __device__
        thrust::complex<double> Taij(unsigned int, unsigned int, unsigned int);

        __device__
        void MakeZ(double*, double, double, zArray&,
                   thrust::complex<double> pI = thrust::complex<double>(0,1));

        __device__
        void MakeEtaA(zArray, etaA&);

        __device__
        void MakeEtaIJ(zArray, thrust::complex<double>*);

    private:
        thrust::complex<double> colorOperator[72];
};

}
