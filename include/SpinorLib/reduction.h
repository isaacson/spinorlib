#ifndef REDUCTION_H
#define REDUCTION_H

namespace spinorlib {
template<typename T>
class MemoryPair;
}

double ReduceAmplitude(int, int, int, int, int, bool, int,
                       spinorlib::MemoryPair<double>,
                       spinorlib::MemoryPair<double>);
void Reduce(int, int, int, double*, double*);

#endif
