#pragma once 

#include "thrust/complex.h"


template<typename T>
class Vec4;

template<typename T>
class Vec3 {
    public:
        __host__ __device__
        Vec3() { m_x[0] = m_x[1] = m_x[2] = 0.0; }
        __host__ __device__
        Vec3(const double &x0, const double &x1, const double &x2) {
            m_x[0] = x0;
            m_x[1] = x1;
            m_x[2] = x2;
        }
        __host__ __device__
        Vec3(const T x[3]) : m_x(x) {}
        __host__ __device__
        Vec3(const Vec3& v) {
            m_x[0] = v.m_x[0]; 
            m_x[1] = v.m_x[1]; 
            m_x[2] = v.m_x[2]; 
        }
        __host__ __device__
        Vec3(const Vec4<T> &v);

        __host__ __device__
        inline T operator[](size_t i) { return m_x[i]; }
        __host__ __device__
        inline T operator[](size_t i) const { return m_x[i]; }

        __host__ __device__
        inline Vec3<T> operator*=(const T &t) { 
            m_x[0] *= t;
            m_x[1] *= t;
            m_x[2] *= t;
            return *this;
        }
        __host__ __device__
        inline Vec3<T> operator+=(const Vec3<T> &other) { 
            m_x[0] += other.m_x[0];
            m_x[1] += other.m_x[1];
            m_x[2] += other.m_x[2];
            return *this;
        }
        __host__ __device__
        inline Vec3<T> operator-=(const Vec3<T> &other) { 
            m_x[0] -= other.m_x[0];
            m_x[1] -= other.m_x[1];
            m_x[2] -= other.m_x[2];
            return *this;
        }

        __host__ __device__
        inline Vec3<T> operator*(const T &t) const { return Vec3<T>(*this) *= t; }
        __host__ __device__
        friend inline Vec3<T> operator*(const T &t, const Vec3<T> &v) {
            return v*t;
        }
        __host__ __device__
        inline Vec3<T> operator/(const T &t) const { return Vec3<T>(*this) * (1./t); }

        __host__ __device__
        inline Vec3<T> operator+(const Vec3<T> &other) const { 
            return Vec3<T>(*this) += other;
        }
        __host__ __device__
        inline Vec3<T> operator-(const Vec3<T> &other) const { 
            return Vec3<T>(*this) -= other;
        }
        __host__ __device__
        inline T operator*(const Vec3<T> &other) const { 
            return m_x[0]*other.m_x[0]+m_x[1]*other.m_x[1]+m_x[2]*other.m_x[2];
        }
        __host__ __device__
        inline Vec3<T> operator-() const { 
            return Vec3<T>(*this) *= -1.0;
        }

    private:
        T m_x[3];
};

template<typename T>
class Vec4 {
    public:
        __host__ __device__
        Vec4() { m_x[0] = m_x[1] = m_x[2] = m_x[3] = 0.0; }
        __host__ __device__
        Vec4(const T &x0, const T &x1, const T &x2, const T &x3) {
            m_x[0] = x0;
            m_x[1] = x1;
            m_x[2] = x2;
            m_x[3] = x3;
        }
        __host__ __device__
        Vec4(const T x[4]) : m_x(x) {}
        __host__ __device__
        Vec4(const Vec4& v) {
            m_x[0] = v.m_x[0]; 
            m_x[1] = v.m_x[1]; 
            m_x[2] = v.m_x[2]; 
            m_x[3] = v.m_x[3]; 
        }
        __host__ __device__
        Vec4(const double &e, const Vec3<T> &v) {
            m_x[0] = e;
            m_x[1] = v[0];
            m_x[2] = v[1];
            m_x[3] = v[2];
        }

        __host__ __device__
        inline T PPlus() const { return m_x[0] + m_x[3]; }
        __host__ __device__
        inline T PMinus() const { return m_x[0] - m_x[3]; }
        __host__ __device__
        inline T PT() const { return sqrt(m_x[1]*m_x[1] + m_x[2]*m_x[2]); }
        __host__ __device__
        inline T Abs2() const { return (*this) * (*this); }
        __host__ __device__
        T Eta() const { return 0.5*log(abs(PPlus()/PMinus())); }
        __host__ __device__
        T DeltaR(const Vec4<T> &other) const {
            double deta = 0.5*log(abs((PPlus()*other.PMinus())/PMinus()/other.PPlus())); 
            double d = (m_x[1]*other.m_x[1]+m_x[2]*other.m_x[2])/PT()/other.PT();
            d = min(max(d, -1.0), 1.0);
            double dphi = acos(d);
            return sqrt(deta*deta + dphi*dphi);
        }

        __host__ __device__
        inline T& operator[](size_t i) { return m_x[i]; }
        __host__ __device__
        inline T operator[](size_t i) const { return m_x[i]; }

        __host__ __device__
        inline Vec4<T> operator*=(const T &t) { 
            m_x[0] *= t;
            m_x[1] *= t;
            m_x[2] *= t;
            m_x[3] *= t;
            return *this;
        }
        __host__ __device__
        inline Vec4<T> operator+=(const Vec4<T> &other) { 
            m_x[0] += other.m_x[0];
            m_x[1] += other.m_x[1];
            m_x[2] += other.m_x[2];
            m_x[3] += other.m_x[3];
            return *this;
        }
        __host__ __device__
        inline Vec4<T> operator-=(const Vec4<T> &other) { 
            m_x[0] -= other.m_x[0];
            m_x[1] -= other.m_x[1];
            m_x[2] -= other.m_x[2];
            m_x[3] -= other.m_x[3];
            return *this;
        }

        __host__ __device__
        inline Vec4<T> operator*(const T &t) const { return Vec4<T>(*this) *= t; }
        __host__ __device__
        friend inline Vec4<T> operator*(const T &t, const Vec4<T> &v) {
            return v*t;
        }
        __host__ __device__
        inline Vec4<T> operator/(const T &t) const { return Vec4<T>(*this) * (1./t); }

        __host__ __device__
        inline Vec4<T> operator+(const Vec4<T> &other) const { 
            return Vec4<T>(*this) += other;
        }
        __host__ __device__
        inline Vec4<T> operator-(const Vec4<T> &other) const { 
            return Vec4<T>(*this) -= other;
        }
        __host__ __device__
        inline Vec4<T> operator-() const { 
            return Vec4<T>(-m_x[0], -m_x[1], -m_x[2], -m_x[3]);
        }
        __host__ __device__
        inline T operator*(const Vec4<T> &other) const { 
            return m_x[0]*other.m_x[0]-m_x[1]*other.m_x[1]-m_x[2]*other.m_x[2]-m_x[3]*other.m_x[3];
        }

    private:
        T m_x[4];
};


template<typename T> __host__ __device__
Vec3<T>::Vec3<T>(const Vec4<T>& v) {
    m_x[0] = v[1];
    m_x[1] = v[2];
    m_x[2] = v[3];
}

using Vec3D = Vec3<double>;
using Vec4D = Vec4<double>;
using CVec4D = Vec4<thrust::complex<double>>;

__host__ __device__
inline CVec4D operator*(const thrust::complex<double> &c, const Vec4D &v) {
    return c*CVec4D(v[0], v[1], v[2], v[3]);
}

__host__ __device__
inline thrust::complex<double> operator*(const CVec4D &v1, const Vec4D &v2) {
    return v1*CVec4D(v2[0], v2[1], v2[2], v2[3]);
}
