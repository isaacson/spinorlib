import numpy as np

Ta = np.array([
    # T1
    [[0, 1, 0],
     [1, 0, 0],
     [0, 0, 0]],
    # T2
    [[0, -1j, 0],
     [1j, 0, 0],
     [0, 0, 0]],
    # T3
    [[1, 0, 0],
     [0, -1, 0],
     [0, 0, 0]],
    # T4
    [[0, 0, 1],
     [0, 0, 0],
     [1, 0, 0]],
    # T5
    [[0, 0, -1j],
     [0, 0, 0],
     [1j, 0, 0]],
    # T6
    [[0, 0, 0],
     [0, 0, 1],
     [0, 1, 0]],
    # T7
    [[0, 0, 0],
     [0, 0, -1j],
     [0, 1j, 0]],
    # T8
    [[1/np.sqrt(3), 0, 0],
     [0, 1/np.sqrt(3), 0],
     [0, 0, -2/np.sqrt(3)]],
    ])/2


NC = 3


def make_z(phi, theta, zeta):
    return np.array([np.exp(1j*phi[:, 0])*np.cos(theta),
                     np.exp(1j*phi[:, 1])*np.sin(theta)*np.cos(zeta),
                     np.exp(1j*phi[:, 2])*np.sin(theta)*np.sin(zeta)]).T


def make_eta(zi):
    zistr = zi.conj()
    zitazj = np.einsum('ei,aij,ej->ea', zistr, Ta, zi)
    return zitazj*np.sqrt(24)


def make_eta2(zi):
    zistr = zi.conj()
    zizj = np.einsum('ei,ej -> eij', zistr, zi)
    return (zizj-1/NC*np.diag([1, 1, 1]))*np.sqrt(6)


def main():
    result = 0
    results = []
    ngluons = 1
    niterations = 10
    nevents = 1000000

    for i in range(niterations):
        print(f"Calculating iteration {i+1}")
        phi = np.random.random([nevents*ngluons, 3])*2*np.pi
        theta = np.random.random([nevents*ngluons])*np.pi/2
        zeta = np.random.random([nevents*ngluons])*np.pi/2
        dz = np.cos(theta)*np.sin(theta)**3*np.cos(zeta)*np.sin(zeta)
        dz *= (np.pi/2)**2
        z = make_z(phi, theta, zeta)
        eta = make_eta(z)
        term1 = np.einsum('ea,eb->eab', eta, eta)*dz[..., None, None]
        result += np.mean(term1.real, axis=0)
        # eta2 = make_eta2(z)
        # term2 = np.einsum('eij,eji->e', eta2, eta2)*dz
        # results.append(np.mean(term2.real))

    np.set_printoptions(precision=3, linewidth=100)
    print(f"Result for Eq. (30) for {nevents*niterations} points:")
    print(result)
    print(f"{np.mean(results)} +/- {np.std(results)}")
    print(f"Expected result: {3/4*(1-1/NC)}")


if __name__ == '__main__':
    main()
