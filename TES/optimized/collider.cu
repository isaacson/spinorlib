#include <cstdio>
#include <iostream>
#include "architecture.h"
#include "momentum.h"
#include "KahanAdder.h"
//#include "YODA/Histo1D.h"
//#include "YODA/Utils/Formatting.h"
//#include "YODA/WriterYODA.h"
//#include "TH1.h"
//#include "TH2.h"
//#include "TCanvas.h"
//#include "TROOT.h"
//#include "TApplication.h"
//#include "TPie.h"
//#include "TProfile.h"

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}


using namespace std;

extern void *g_weight,*g_cut;
extern void *g_observable0,*g_observable1,*g_observable2,*g_observable3,*g_observable4,*g_observable5,*g_observable6;
extern void init(int N,int M);
extern void cleanup();
extern void Event(int N,float Ecm,float Ptmin,float etamax,float Rmin,int M);


int main(void)
{
  //
  // run set-up
  //
  int N=12;
  int Iterations=100000000;
//  int Iterations=1000;
  float Ecm=14000.0;
//  float Ptmin=60.0;
  float Ptmin=20.0;
//  float etamax=2.0;
  float etamax=2.5;
  float Rmin=0.4;
  //
  // set-up
  //
  int events_limit_register=REGISTER_MEMORY/(2*MAX_REGISTER);
  int max_events_per_MP=(SHARED_MEMORY-16)/(sizeof(momentum<float>)*N*(N+1)/2);
  int events_per_MP=min(events_limit_register,max_events_per_MP);
  int total_events_per_iteration=events_per_MP*NUMBER_MP;
  cout<<endl<<endl<<endl;
  cout<<"Running configuration: gg ---> "<<N-2<<" gluons"<<endl;
  cout<<"                       number of MP's used                 = "
      <<NUMBER_MP<<endl;
  cout<<"                       events/MP limit from used registers = "
      <<events_limit_register<<endl;
  cout<<"                       events/MP limit from shared memory  = "
      <<max_events_per_MP<<endl;
  cout<<"                       events/MP                           = "
      <<events_per_MP<<endl;
  cout<<"                       threads used/MP (out of 1024)       = "
      <<events_per_MP*(N-1)<<endl;
  cout<<"                       events per iteration                = "
      <<total_events_per_iteration<<endl;
  cout<<"                       number of iterations                = "
      <<Iterations<<endl;
  cout<<"                       total generated event in this run   = "
      <<(double) Iterations*(double) total_events_per_iteration<<endl;
  cout<<endl<<endl<<endl;
  //
/*  gROOT->Reset();
  TApplication myapp("myapp",0,0);
  TCanvas *screen = new TCanvas("screen","Observables",0,0,800,800);
  screen->Divide(2,2);
  screen->cd(1);
  gPad->SetLogy();
*/
//  TH1F *histHt=new TH1F("Run Stats","Ht (in Gev)",140,0.0,14000.0);
//  Histo1D histHt(140,0.0,14000.0,path="Run Stats",title="Ht (in Gev)");
  /*  histHt->SetFillColor(kGreen); 
  histHt->Draw();
  screen->cd(2);
  TH1F *histR=new TH1F("Run Stats","<R_ij>",150,0.0,3.1416);
  histR->SetFillColor(kGreen); 
  histR->Draw();
  screen->cd(3);
  TH1F *histPhi=new TH1F("Run Stats","<Phi>",150,0.0,3.1416);
  histPhi->SetFillColor(kGreen); 
  histPhi->Draw();
  screen->cd(4);
  TH1F *histEta1=new TH1F("Run Stats","<|eta|>",100,0.0,4.0);
  histEta1->SetFillColor(kGreen); 
  histEta1->Draw();
  TH1F *histEta2=new TH1F("Run Stats","<|eta|>",100,0.0,4.0);
  histEta2->SetFillColor(kBlue); 
  histEta2->Draw();

  TCanvas *control = new TCanvas("control","Control",820,0,800,800);
  control->Divide(2,2);
  control->cd(1);
  TH2F *scatterGauge=new TH2F("Run Stats","Gauge Invariance",1000,-12.0,0.0,1000,-7.0,7.0);
  scatterGauge->SetMarkerColor(kRed) ;
  control->cd(2);
  TH2F *scatterRelGauge=new TH2F("Run Stats","Relative Gauge Invariance",1000,-12.0,0.0,1000,-7.0,7.0);
  scatterRelGauge->SetMarkerColor(kRed) ;
  control->cd(3);
  TProfile *profileRelGauge=new TProfile("Run Stats","Relative Gauge Invariance profile",100,-7.0,7.0,-12.0,0.0);
  profileRelGauge->SetMarkerColor(kBlue);
  profileRelGauge->SetMarkerSize(0.25);
  profileRelGauge->SetMarkerStyle(21);


  control->cd(4);
  TH1F *weight=new TH1F("Run Stats","Weight",1000,-25,10);
  weight->SetFillColor(kBlue); */
  //
  srand(53);
  cudaEvent_t start,stop,event;
  float init_time=0.0,event_time=0.0,histogram_time=0.0;
  float time;
  cudaEventCreate(&start);cudaEventCreate(&stop);cudaEventCreate(&event);
  //
  // Initialize run
  //
  cudaEventRecord(start, 0);
  init(N,total_events_per_iteration);
  cudaEventRecord(stop, 0);cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time, start, stop);
  init_time+=time;
  //
  // start calculating the events
  //
  int update=10000;
  KahanAdder<double> xsection,x2section;
  KahanAdder<long int> Naccepted;
  //float maxwgt=0.0;
  for (int eventsblocks=0;eventsblocks<Iterations;++eventsblocks)
    {
      if (eventsblocks!=0)
	{
	  float wgt[total_events_per_iteration],Ht[total_events_per_iteration],R[total_events_per_iteration];
	  //	  float Phi[total_events_per_iteration],Eta[total_events_per_iteration];
	  //	  float M[total_events_per_iteration],G[total_events_per_iteration],PS[total_events_per_iteration];
	  bool cut[total_events_per_iteration];
	  gpuErrchk( cudaMemcpy(wgt,g_weight,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost) );
/*	  gpuErrchk( cudaMemcpy(PS,g_observable0,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost) );
	  gpuErrchk( cudaMemcpy(M,g_observable1,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost) );
	  gpuErrchk( cudaMemcpy(G,g_observable2,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost) );
	  gpuErrchk( cudaMemcpy(Ht,g_observable3,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost) );
	  gpuErrchk( cudaMemcpy(R,g_observable4,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost) );
	  gpuErrchk( cudaMemcpy(Phi,g_observable5,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost) );
	  gpuErrchk( cudaMemcpy(Eta,g_observable6,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost) );*/
	  gpuErrchk( cudaMemcpy(cut,g_cut,sizeof(bool)*total_events_per_iteration,cudaMemcpyDeviceToHost) );
	  for (int i=0;i<total_events_per_iteration;i++) {
	      Naccepted+=1;
	      if (cut[i]==true) {
		xsection+=wgt[i];
		x2section+=wgt[i]*wgt[i];
		// histHt.fill(Ht[i],wgt[i]);
		/*		histR->Fill(R[i],wgt[i]);
		histPhi->Fill(Phi[i],wgt[i]);
		histEta1->Fill(Phi[i],wgt[i]);
		histEta2->Fill(Eta[i],wgt[i]);
		scatterGauge->Fill(log10(G[i]),log10(M[i]));
		scatterRelGauge->Fill(log10(G[i]/M[i]),log10(M[i]));
		profileRelGauge->Fill(log10(M[i]),log10(G[i]/M[i]));
		weight->Fill(log10(wgt[i]));*/
	      }}
	  if (eventsblocks==update) 
	    {
/*	      screen->cd(1);
	      TH1F ht=(*histHt)*(1.0/((double) eventsblocks*(double) total_events_per_iteration));
	      ht.Draw();
	      screen->cd(2);
	      TH1F r=(*histR)*(1.0/((double) eventsblocks*(double) total_events_per_iteration));
	      r.Draw();
	      screen->cd(3);
	      TH1F phi=(*histPhi)*(1.0/((double) eventsblocks*(double) total_events_per_iteration));
	      phi.Draw();
	      screen->cd(4);
	      TH1F eta1=(*histEta1)*(1.0/((double) eventsblocks*(double) total_events_per_iteration));
	      TH1F eta2=(*histEta2)*(1.0/((double) eventsblocks*(double) total_events_per_iteration));
	      eta2.Draw();
	      eta1.Draw("same");
	      screen->Modified();
	      screen->Update();
	      control->cd(1);
	      scatterGauge->Draw();
	      control->cd(2);
	      scatterRelGauge->Draw();
	      control->cd(3);
	      profileRelGauge->Draw();
	      control->cd(4);
	      TH1F w=(*weight)*(1.0/((double) eventsblocks*(double) total_events_per_iteration));
	      w.Draw();
	      control->Modified();
	      control->Update();*/
	      cout<<"Accepted events:"<<Naccepted();
	      double avg=xsection()/(double) Naccepted();
	      double avg2=x2section()/(double) Naccepted();
	      double sigma_mean=sqrt((avg2-avg*avg)/(double) Naccepted());
	      cout<<"   cross section: ("<<avg<<" +/- "<<sigma_mean<<" pb)"<<endl;
	      update+=10000;
	    }
	  cudaEventRecord(stop, 0);
	  cudaEventSynchronize(stop);
	  cudaEventElapsedTime(&time,start,event);
	  event_time+=time;
	  cudaEventElapsedTime(&time,event,stop);
	  histogram_time+=time;
	}
      cudaEventRecord(start, 0);
      Event(N,Ecm,Ptmin,etamax,Rmin,events_per_MP);
      cudaEventRecord(event, 0);
    }
  float wgt[total_events_per_iteration],Ht[total_events_per_iteration],R[total_events_per_iteration];
  //  float Phi[total_events_per_iteration],Eta[total_events_per_iteration];
  //  float M[total_events_per_iteration],G[total_events_per_iteration],PS[total_events_per_iteration];
  bool cut[total_events_per_iteration];
 gpuErrchk( cudaMemcpy(wgt,g_weight,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost) );
 /* gpuErrchk( cudaMemcpy(PS,g_observable0,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost) );
 gpuErrchk( cudaMemcpy(M,g_observable1,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost) );
 gpuErrchk( cudaMemcpy(G,g_observable2,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost) );
 gpuErrchk( cudaMemcpy(Ht,g_observable3,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost) );
 gpuErrchk( cudaMemcpy(R,g_observable4,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost) );
 gpuErrchk( cudaMemcpy(Phi,g_observable5,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost) );
 gpuErrchk( cudaMemcpy(Eta,g_observable6,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost) );*/
 gpuErrchk( cudaMemcpy(cut,g_cut,sizeof(bool)*total_events_per_iteration,cudaMemcpyDeviceToHost) );
  for (int i=0;i<total_events_per_iteration;i++) {
    if (cut[i]==true) {
      xsection+=wgt[i];
      x2section+=wgt[i]*wgt[i];
      //histHt.fill(Ht[i],wgt[i]);
      /*      histR->Fill(R[i],wgt[i]);
      histPhi->Fill(Phi[i],wgt[i]);
      histEta1->Fill(Phi[i],wgt[i]);
      histEta2->Fill(Eta[i],wgt[i]);
      scatterGauge->Fill(log10(G[i]),log10(M[i]));
      scatterRelGauge->Fill(log10(G[i]/M[i]),log10(M[i]));
      profileRelGauge->Fill(log10(M[i]),log10(G[i]/M[i]));
      weight->Fill(log10(wgt[i]));*/
      Naccepted+=1;
    }}
/*  screen->cd(1);
  TH1F ht=(*histHt)*(1.0/((double) Iterations*(double) total_events_per_iteration));
  ht.Draw();
  screen->cd(2);
  TH1F r=(*histR)*(1.0/((double) Iterations*(double) total_events_per_iteration));
  r.Draw();
  screen->cd(3);
  TH1F phi=(*histPhi)*(1.0/((double) Iterations*(double) total_events_per_iteration));
  phi.Draw();
  screen->cd(4);
  TH1F eta1=(*histEta1)*(1.0/((double) Iterations*(double) total_events_per_iteration));
  TH1F eta2=(*histEta2)*(1.0/((double) Iterations*(double) total_events_per_iteration));
  eta2.Draw();
  eta1.Draw("same");
  screen->Modified();
  screen->Update();
  control->cd(1);
  scatterGauge->Draw();
  control->cd(2);
  scatterRelGauge->Draw();
  control->cd(3);
  profileRelGauge->Draw();
  control->cd(4);
  TH1F w=(*weight)*(1.0/((double) Iterations*(double) total_events_per_iteration));
  w.Draw();
  control->Modified();
  control->Update();
*/
  cudaEventRecord(stop, 0);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,event);
  event_time+=time;
  cudaEventElapsedTime(&time,event,stop);
  histogram_time+=time;
  // Output Histograms
  // WriterYODA::write("gluon_output.yoda", histHt);
  //
  float total_time=init_time+event_time+histogram_time;
  cout<<"Accepted events:"<<Naccepted();
  double avg=xsection()/(double) Naccepted();
  double avg2=x2section()/(double) Naccepted();
  double sigma_mean=sqrt((avg2-avg*avg)/(double) Naccepted());
  cout<<"   cross section: ("<<avg<<" +/- "<<sigma_mean<<" pb)"<<endl;
  cout<<endl<<"Timing:"<<endl;
  cout<<"       Initialization:"<<init_time/1000.0<<" seconds ("
      <<100.0*init_time/total_time<<"%)"<<endl;
  cout<<"       Event         :"<<event_time/1000.0<<" seconds ("
      <<100.0*event_time/total_time<<"%)"<<endl;
  cout<<"       Histogramming :"<<histogram_time/1000.0<<" seconds ("
      <<100.0*histogram_time/total_time<<"%)"<<endl;
  cout<<endl;
  cout<<"      Total event time/events   :"
      <<event_time/1000.0/((double) Iterations*(double) events_per_MP*(double) NUMBER_MP)<<" seconds"<<endl;
  cout<<"       Total/event   :"
      <<total_time/1000.0/((double) Iterations*(double) events_per_MP*(double) NUMBER_MP)<<" seconds"<<endl;
  cout<<"       Total         :"
      <<total_time/1000.0<<" seconds"<<endl;

  cleanup();

/*  TCanvas *pie=new TCanvas("pie","Timing",820,0,200,200);
  int nvals=3;
  float vals[3];
  const char *labels[3];
  vals[0]=init_time/1000.0;labels[0]="init";
  vals[1]=event_time/1000.0;labels[1]="event";
  vals[2]=histogram_time/1000.0;labels[2]="histogramming";
  int colors[]={1,2,3};
  TPie *piechart = new TPie("piechart","Timing",nvals,vals,colors,labels);
  piechart->SetRadius(.2);
  piechart->SetLabelsOffset(.01);
  piechart->SetLabelFormat("#splitline{%val (%perc)}{%txt}");
  piechart->Draw("nol <");
  piechart->Draw();
  pie->Update();
  cout << endl << "Enter q to terminate..." << endl;
  char tmp;cin>>tmp;*/
}
