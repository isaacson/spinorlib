      subroutine BGttbar(O,kmom,pol,flav,Nlegs,weight)
      implicit none
      include 'constants.f'
      include 'fermionmass.f'
      logical loglk(Nlegs,Nlegs)
      integer Nlegs,l,k,n,m,h,f1,f2,fv,nflav,i
      integer pol(mxpart),flav(mxpart),O(mxpart),flavlk(Nlegs,Nlegs)
      double precision dotpr,mm,cdotpr,sprod
      double precision P(4),kmom(mxpart,4),qmom(Nlegs,4)
      double complex weight
      double complex Jlk(Nlegs,Nlegs,4),K1(4),K2(4),Ktemp(4)
      double complex J(4),J0(4),J1(4),J2(4),J3(4),Jtemp(4)


      Jlk(:,:,:)=czip
      loglk(:,:)=.false.
C-----create cumulative sums of momenta
      P(:)=kmom(O(1),:)
      qmom(1,:)=P(:)
      do i=1,Nlegs-1
         P(:)=P(:)+kmom(O(1+i),:)
         qmom(1+i,:)=P(:)  
      enddo

C---construct the partonic currents
C---setup polarization vectors for Nlegs-1 particles
C---diagonal elements of matrix
      do k=2,Nlegs
C ---partonic wavefunction
         P(:)=kmom(O(k),:)
         h=pol(O(k)) 
         nflav=flav(O(k)) 
         if (abs(nflav).le.6) then
            loglk(k-1,k-1)=.true.
            flavlk(k-1,k-1)=nflav
            if (h.eq.0) then
               if (nflav.ne.0) then
                  write(*,"(' Particle ',i1,' has flavor ',i1)") k,nflav 
                  write(*,*) 'Check aborted'
                  stop
               else
                  Jlk(k-1,k-1,:)=dcmplx(P(:))/50d0
               endif
            else
               call wavefunction(P,nflav,h,J)
               Jlk(k-1,k-1,:)=J(:)
           endif
        endif
      enddo
!
! Make partonic currents
!
      do l=1,Nlegs-2
         do k=1,Nlegs-l-1
            J(:)=0d0
            do n=k,k+l-1
               Jtemp(:)=dcmplx(0d0,0d0)
               if (loglk(k,n) .and. loglk(n+1,k+l)) then
                  K1(:)=dcmplx(qmom(n+1,:)-qmom(k,:))
                  K2(:)=dcmplx(qmom(k+l+1,:)-qmom(n+1,:))
                  J1(:)=Jlk(k,n,:)
                  J2(:)=Jlk(n+1,k+l,:)
                  f1=flavlk(k,n)
                  f2=flavlk(n+1,k+l)
                  if ((f1.eq.0).and.(f2.eq.0)) then
                     loglk(k,k+l)=.true.
                     Jtemp(:)=+2d0*(cdotpr(J1,K2)*J2(:)
     &                        -cdotpr(J2,K1)*J1(:))
     &                        +cdotpr(J1,J2)*(K1(:)-K2(:))
                  elseif ((f1.eq.0).and.(f2.ne.0)) then 
                     loglk(k,k+l)=.true.
                     if (f2.gt.0) then
                        call UbkslashRL(J2,J1,Jtemp,1d0,1d0)
                     else
                        call kslashURL(J1,J2,Jtemp,1d0,1d0)
                        Jtemp=-Jtemp
                     endif
                  elseif ((f1.ne.0).and.(f2.eq.0)) then 
                     loglk(k,k+l)=.true.
                     if (f1.gt.0) then
                        call UbkslashRL(J1,J2,Jtemp,1d0,1d0)
                        Jtemp=-Jtemp
                     else
                        call kslashURL(J2,J1,Jtemp,1d0,1d0)
                     endif
                  elseif (f1.eq.-f2) then
                     loglk(k,k+l)=.true.
                     if (f1.gt.0) then
                        call UbGammaURL(J2,J1,Jtemp,1d0,1d0)
                     else
                        call UbGammaURL(J1,J2,Jtemp,1d0,1d0)
                     endif
                  endif
               endif
               J(:)=J(:)+Jtemp(:)
            enddo               ! endloop n
            
            f1=(f1+f2)
            do n=k,k+l-2
               do m=n+1,k+l-1
                  jtemp(:)=0d0
                if (loglk(k,n).and.loglk(n+1,m).and.loglk(m+1,k+l)) then
                     if ((flavlk(k,n)==0)
     &                    .and.(flavlk(n+1,m)==0)
     &                    .and.(flavlk(m+1,k+l)==0)) then
                        loglk(k,k+l)=.true.
                        J1(:)=Jlk(k,n,:)
                        J2(:)=Jlk(n+1,m,:)
                        J3(:)=Jlk(m+1,k+l,:)
                        Jtemp(:)=
     &                       +2d0*cdotpr(J1,J3)*J2(:)
     &                       -cdotpr(J1,J2)*J3(:)
     &                       -cdotpr(J2,J3)*J1(:)
                        f1=0
                     endif
                  endif
                  J(:)=J(:)+Jtemp(:)
               enddo            ! endloop m
            enddo               ! endloop n

C---Divide by propagator
            if (l < Nlegs-2) then
               P(:)=qmom(k+l+1,:)-qmom(k,:)
               if (loglk(k,k+l)) then
                  if (f1.eq.0) then
                     J(:)=J(:)/dotpr(P,P)
                  else
                     Ktemp(:)=dcmplx(P(:))
                     mm=mass(f1)
                     if (f1.gt.0) then
                        call UbkslashRL(J,Ktemp,Jtemp,1d0,1d0)
                        J(:)=(Jtemp(:)+mm*J(:))/(dotpr(P,P)-mm**2)
                     else
                        call kslashURL(-Ktemp,J,Jtemp,1d0,1d0)
                        J(:)=(Jtemp(:)+mm*J(:))/(dotpr(P,P)-mm**2)
                     endif
                  endif
               endif
            endif
            Jlk(k,k+l,:)=J(:)
            flavlk(k,k+l)=f1
         enddo
      enddo
      

      P(:)=kmom(O(1),:)
      h=pol(O(1)) 
      nflav=flav(O(1)) 
      if (abs(nflav).le.6) then
         if (h.eq.0) then
            if (nflav.ne.0) then
               write(*,"(' Particle ',i1,' has flavor ',i1)") k,nflav 
               write(*,*) 'Check aborted'
               stop
            else
               J0(:)=dcmplx(P(:))/50d0
            endif
         else
            call wavefunction(P,nflav,h,J0)
         endif
      endif
      J(:)=Jlk(1,Nlegs-1,:)
      fv=flavlk(1,Nlegs-1)
      if (nflav.ne.-fv) then
         write(*,*) "flavor mismatch", fv,nflav
         stop
      endif
      if (nflav .eq. 0) then
         weight=cdotpr(J0,J)
      else
         weight=sprod(J0,J)
      endif
      return
      end

