      subroutine ubarmass(p,eta,m,i,f)
C     Spinor in Dirac representation as a compound of p and eta
      implicit none
      integer i
      double precision m
      double complex p(4),eta(4),pf(4),norm
      double complex f(4),spe(4),spp(4)

      if (m .eq. 0d0) then
      call ubarspinor0(p,i,f)
      return
      else
      pf(:)=p(:)-0.5d0*m**2*eta(:)
     & /(eta(1)*p(1)-eta(2)*p(2)-eta(3)*p(3)-eta(4)*p(4))
      call ubarspinor0(eta,-i,spe)
      call Ubkslash(spe,p,f)
      call vspinor0(pf,i,spp)
      norm=-(spe(1)*spp(1)+spe(2)*spp(2)+spe(3)*spp(3)+spe(4)*spp(4))
      f(:)=(f(:)+spe(:)*m)/norm
      endif
      return
      end


c      subroutine for anti-quark spinor
      subroutine vmass(p,eta,m,i,f)
      implicit none
      integer i
      double precision m
      double complex p(4),eta(4),pf(4),norm
      double complex f(4),spe(4),spp(4)

      if (m .eq. 0d0) then
      call vspinor0(p,i,f)
      return
      else
      pf(:)=p(:)-0.5d0*m**2*eta(:)
     & /(eta(1)*p(1)-eta(2)*p(2)-eta(3)*p(3)-eta(4)*p(4))
      call vspinor0(eta,-i,spe)
      call kslashU(p,spe,f)
      call ubarspinor0(pf,i,spp)
      norm=spe(1)*spp(1)+spe(2)*spp(2)+spe(3)*spp(3)+spe(4)*spp(4)
      f(:)=(f(:)-spe(:)*m)/norm
      endif
      return
      end

