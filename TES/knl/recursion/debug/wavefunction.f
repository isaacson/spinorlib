      subroutine wavefunction(p,nflav,hi,f)
c-----setup external wavefunction
      implicit none
      include 'fermionmass.f'
      include 'constants.f'
      integer hi,nflav
      double precision p(4),b(4),m
      double precision pmcfm(mxpart,4)
      double complex etaQ(4),etaQb(4)
      data b/1.732050808,1d0,1d0,1d0/
      data etaQ /(1.732050808,0d0),(1d0,0d0),(1d0,0d0),(-1d0,0d0)/
      data etaQb/(1.732050808,0d0),(1d0,0d0),(-1d0,0d0),(1d0,0d0)/
      common/mcfm/pmcfm
      double complex q(4)
      double complex f(4)

      if (nflav .eq. 0) then
         call helix(p,b,hi,f)
      elseif (nflav .gt. 0) then
         m=mass(nflav)
         q(:)=dcmplx(p(:))
c     call ubarspinor(q,m,-hi,f)
         call ubarmass(q,etaQ,m,hi,f)
      elseif (nflav .lt. 0) then
         m=mass(nflav)
         q(:)=dcmplx(p(:))
C     call vspinor(q,m,hi,f)
         call vmass(q,etaQb,m,-hi,f)
      endif

      return
      end


