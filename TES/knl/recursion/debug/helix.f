      subroutine helix(p,b,h,e)
c-----massless vector polarization subroutine
C ----p massless vector
C ----b massless auxiliary vector
C ----h =+1 or -1 integer helicity
C ----e complex helicity vector
      implicit none
      include 'constants.f'
      include 'swapxz.f'
      integer h,nu
      double precision p(4),b(4)
      double complex e(4),Ubk(4),Vb(4),norm,tmp

      call ubarspinor0(dcmplx(p),h,Ubk)
      call vspinor0(dcmplx(b),h,Vb)
      e(4)=+(Ubk(2)*Vb(4)+Ubk(1)*Vb(3)+Ubk(4)*Vb(2)+Ubk(3)*Vb(1))
      e(1)=(-Ubk(1)*Vb(4)-Ubk(2)*Vb(3)+Ubk(4)*Vb(1)+Ubk(3)*Vb(2))
      e(2)=im*(Ubk(1)*Vb(4)-Ubk(2)*Vb(3)-Ubk(3)*Vb(2)+Ubk(4)*Vb(1))
      e(3)=(Ubk(2)*Vb(4)-Ubk(4)*Vb(2)-Ubk(1)*Vb(3)+Ubk(3)*Vb(1))
      if (swapxz) then
      tmp=e(3)
      e(3)=e(1)
      e(1)=tmp
      e(2)=-e(2)
      endif
      
      call ubarspinor0(dcmplx(p),-h,Ubk)
      norm=(Ubk(1)*Vb(1)+Ubk(2)*Vb(2)+Ubk(3)*Vb(3)+Ubk(4)*Vb(4))
     & *dcmplx(rt2)

      if (h .eq. 1) norm=-norm ! -<kb> for +ve, +[kb] for -ve
      do nu=1,4
       e(nu)=e(nu)/norm
      enddo
    
      return
      end

