      implicit none
      include 'constants.f'
      include 'swapxz.f'
      include 'fermionmass.f'
      integer i,j,nrambo,lw,Nlegs,njets
      integer count,gauge,h
      integer Order(mxpart),pol(mxpart),flav(mxpart)
      double precision Rn,pswt,Et
      double precision p0(4),pl(4),pmcfm(mxpart,4)
      double precision kmom(mxpart,4),xm(mxpart),p(4,mxpart)
      double complex amp,Gamp
      common/mcfm/pmcfm

      swapxz=.true.

      do i=1,mxpart
         Order(i)=i
         flav(i)=0
         pol(i)=-1+2*int(2d0*rn(1.))
      enddo
      do count=1,10000000
      if (mod(count,100000).eq.1) write(*,*) count
      call tester(Nlegs,gauge,flav,pol)
      nrambo=Nlegs-2

      lw=0

      p0(1)=+71.5344542606618d0
      p0(2)=0d0
      p0(3)=0d0
      p0(4)=-71.5344542606618d0

      pl(1)=-71.5344542606618d0
      pl(2)=0d0
      pl(3)=0d0
      pl(4)=-71.5344542606618d0
      ET=-p0(4)-pl(4)

      do j=1,nrambo
         xm(j)=mass(flav(j+2))
      enddo
      kmom(1,:)=p0(:)
      kmom(2,:)=pl(:)

      call RAMBO(nrambo,ET,XM,P,PSWT,LW)
      do j=1,nrambo
         kmom(j+2,:)=p(:,j)
      enddo

      do j=1,nrambo+2
         pmcfm(j,:)=kmom(j,:)
      enddo

      call bgttbar(Order,Kmom,pol,flav,Nlegs,amp)
      h=pol(gauge)
      pol(gauge)=0
      call bgttbar(Order,Kmom,pol,flav,Nlegs,Gamp)
      pol(gauge)=h
      if ((cdabs(Gamp)/cdabs(amp).gt.1d-8).and.
     &     (cdabs(amp).gt.1e-13)) then
         write(*,*) cdabs(Gamp),cdabs(amp),cdabs(Gamp)/cdabs(amp)
         write(*,*)
         write(*,"(3i3)") nlegs,gauge
         write(*,"(20i3)") (flav(i),i=1,nlegs)
         write(*,"(20i3)") (pol(i),i=1,nlegs)
         write(*,*)
         write(*,*) 'partons momenta'
         do j=1,nlegs
            write(*,*) j,kmom(j,:),
     .           sqrt(abs(kmom(j,4)**2-kmom(j,3)**2
     .           -kmom(j,2)**2-kmom(j,1)**2))
         enddo
         write(*,*) '--------------------------------------------'
         call bgdebug(Order,Kmom,pol,flav,Nlegs,amp)
         write(*,*) '--------------------------------------------'
      endif
      enddo
      end


