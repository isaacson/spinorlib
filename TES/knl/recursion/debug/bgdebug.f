      subroutine BGdebug(O,kmom,pol,flav,Nlegs,weight)
      implicit none
      include 'constants.f'
      include 'fermionmass.f'
      logical loglk(Nlegs,Nlegs)
      integer Nlegs,l,k,n,m,h,f1,f2,fv,nflav,i
      integer pol(mxpart),flav(mxpart),O(mxpart),flavlk(Nlegs,Nlegs)
      double precision dotpr,mm,add,cdotpr,sprod
      double precision P(4),kmom(mxpart,4),qmom(Nlegs,4)
      double complex weight
      double complex Jlk(Nlegs,Nlegs,4),K1(4),K2(4),Ktemp(4)
      double complex J(4),J0(4),J1(4),J2(4),J3(4),Jtemp(4)

      write(*,*) '*******************************'
      Jlk(:,:,:)=czip
      loglk(:,:)=.false.
C-----create cumulative sums of momenta
      P(:)=kmom(O(1),:)
      qmom(1,:)=P(:)
      do i=1,Nlegs-1
         P(:)=P(:)+kmom(O(1+i),:)
         qmom(1+i,:)=P(:)  
      enddo

C---construct the partonic currents
C---setup polarization vectors for Nlegs-1 particles
C---diagonal elements of matrix
      do k=2,Nlegs
C ---partonic wavefunction
         P(:)=kmom(O(k),:)
         h=pol(O(k)) 
         nflav=flav(O(k)) 
         if (abs(nflav).le.6) then
            loglk(k-1,k-1)=.true.
            flavlk(k-1,k-1)=nflav
            if (h.eq.0) then
               write(*,*) 'Gauge invariance check initiated'
               if (nflav.ne.0) then
                  write(*,"(' Particle ',i1,' has flavor ',i1)") k,nflav 
                  write(*,*) 'Check aborted'
                  stop
               else
                  Jlk(k-1,k-1,:)=dcmplx(P(:))/50d0
                  write(*,"('J(',2i1,')=K(',i1,')->',d16.8)")
     &                 ,k-1,k-1,k-1,add(Jlk(k-1,k-1,:),kmom,O,pol,flav)
               endif
            else
               call wavefunction(P,nflav,h,J)
               Jlk(k-1,k-1,:)=J(:)
           if (nflav.gt.0) write(*,"('ub(',2i1,')=ub(',i2,')->',d16.8)") 
     &           k-1,k-1,h,add(J,kmom,O,pol,flav)
            if (nflav.eq.0) write(*,"('J(',2i1,')=e(',i2,')->',d16.8)") 
     &           k-1,k-1,h,add(J,kmom,O,pol,flav)
            if (nflav.lt.0) write(*,"('u(',2i1,')=u(',i2,')->',d16.8)") 
     &           k-1,k-1,h,add(J,kmom,O,pol,flav)
           endif
        endif
      enddo
!
! Make partonic currents
!
      do l=1,Nlegs-2
         do k=1,Nlegs-l-1
            J(:)=0d0
            write(*,"('J(',i1,i1,')=0')"),k,k+l
            do n=k,k+l-1
               Jtemp(:)=dcmplx(0d0,0d0)
               if (loglk(k,n) .and. loglk(n+1,k+l)) then
                  K1(:)=dcmplx(qmom(n+1,:)-qmom(k,:))
                  K2(:)=dcmplx(qmom(k+l+1,:)-qmom(n+1,:))
                  J1(:)=Jlk(k,n,:)
                  J2(:)=Jlk(n+1,k+l,:)
                  f1=flavlk(k,n)
                  f2=flavlk(n+1,k+l)
                  if ((f1.eq.0).and.(f2.eq.0)) then
                     loglk(k,k+l)=.true.
                     Jtemp(:)=+2d0*(cdotpr(J1,K2)*J2(:)
     &                        -cdotpr(J2,K1)*J1(:))
     &                        +cdotpr(J1,J2)*(K1(:)-K2(:))
              write(*,"('J(',2i1,')+=[J(',2i1,'),J(',2i1,')]->',d16.8)")
     &                    ,k,k+l,k,n,n+1,k+l,add(Jtemp,kmom,O,pol,flav)
                  elseif ((f1.eq.0).and.(f2.ne.0)) then 
                     loglk(k,k+l)=.true.
                     if (f2.gt.0) then
                        call UbkslashRL(J2,J1,Jtemp,1d0,1d0)
               write(*,"('ub(',2i1,')+=Ub(',2i1,')*J('2i1')->',d16.8)")
     &                    ,k,k+l,n+1,k+l,k,n,add(Jtemp,kmom,O,pol,flav)
                     else
                        call kslashURL(J1,J2,Jtemp,1d0,1d0)
                        Jtemp=-Jtemp
               write(*,"('u(',2i1,')+=J(',2i1,')*u('2i1')->',d16.8)")
     &                    ,k,k+l,k,n,n+1,k+l,add(Jtemp,kmom,O,pol,flav)
                     endif
                  elseif ((f1.ne.0).and.(f2.eq.0)) then 
                     loglk(k,k+l)=.true.
                     if (f1.gt.0) then
                        call UbkslashRL(J1,J2,Jtemp,1d0,1d0)
                        Jtemp=-Jtemp
               write(*,"('ub(',2i1,')-=ub(',2i1,')*J('2i1')->',d16.8)")
     &                    ,k,k+l,k,n,n+1,k+l,add(Jtemp,kmom,O,pol,flav)
                     else
                        call kslashURL(J2,J1,Jtemp,1d0,1d0)
               write(*,"('u(',2i1,')+=J(',2i1,')*u('2i1')->',d16.8)")
     &                    ,k,k+l,n+1,k+l,k,n,add(Jtemp,kmom,O,pol,flav)
                     endif
                  elseif (f1.eq.-f2) then
                     loglk(k,k+l)=.true.
                     if (f1.gt.0) then
                        call UbGammaURL(J2,J1,Jtemp,1d0,1d0)
               write(*,"('J(',2i1,')+=u(',2i1,')*ub('2i1')->',d16.8)")
     &                    ,k,k+l,n+1,k+l,k,n,add(Jtemp,kmom,O,pol,flav)
                     else
                        call UbGammaURL(J1,J2,Jtemp,1d0,1d0)
               write(*,"('J(',2i1,')+=u(',2i1,')*ub('2i1')->',d16.8)")
     &                    ,k,k+l,k,n,n+1,k+l,add(Jtemp,kmom,O,pol,flav)
                     endif
                  endif
               endif
               J(:)=J(:)+Jtemp(:)
            enddo               ! endloop n
            
            f1=(f1+f2)
            do n=k,k+l-2
               do m=n+1,k+l-1
                  jtemp(:)=0d0
                if (loglk(k,n).and.loglk(n+1,m).and.loglk(m+1,k+l)) then
                     if ((flavlk(k,n)==0)
     &                    .and.(flavlk(n+1,m)==0)
     &                    .and.(flavlk(m+1,k+l)==0)) then
                        loglk(k,k+l)=.true.
                        J1(:)=Jlk(k,n,:)
                        J2(:)=Jlk(n+1,m,:)
                        J3(:)=Jlk(m+1,k+l,:)
                        Jtemp(:)=
     &                       +2d0*cdotpr(J1,J3)*J2(:)
     &                       -cdotpr(J1,J2)*J3(:)
     &                       -cdotpr(J2,J3)*J1(:)
                        f1=0
             write(*,"('J(',2i1,')+=[J(',2i1,')J(',2i1,')J(',2i1,')]->'
     &     ,d16.8)"),k,k+l,k,n,n+1,m,m+1,k+l,add(Jtemp,kmom,O,pol,flav)
                     endif
                  endif
                  J(:)=J(:)+Jtemp(:)
               enddo            ! endloop m
            enddo               ! endloop n

C---Divide by propagator
            if (l < Nlegs-2) then
               P(:)=qmom(k+l+1,:)-qmom(k,:)
               if (loglk(k,k+l)) then
                  if (f1.eq.0) then
                     J(:)=J(:)/dotpr(P,P)
               write(*,"('J(',2i1,')*=Pg(K(',2i1,'))->',d16.8)")
     &              ,k,k+l,k,k+l,add(J,kmom,O,pol,flav)
                  else
                     Ktemp(:)=dcmplx(P(:))
                     mm=mass(f1)
                     if (f1.gt.0) then
                        call UbkslashRL(J,Ktemp,Jtemp,1d0,1d0)
                        J(:)=(Jtemp(:)+mm*J(:))/(dotpr(P,P)-mm**2)
                  J(:)=(Jtemp(:)+mm*J(:))/(dotpr(P,P)-mm**2)
               write(*,"('ub(',2i1,')*=Pub(K(',2i1,'))->',d16.8)")
     &                 ,k,k+l,k,k+l,add(Jtemp,kmom,O,pol,flav)
                     else
                        call kslashURL(-Ktemp,J,Jtemp,1d0,1d0)
                        J(:)=(Jtemp(:)+mm*J(:))/(dotpr(P,P)-mm**2)
               write(*,"('J(',2i1,')*=Pu(K(',2i1,'))->',d16.8)")
     &                 ,k,k+l,k,k+l,add(Jtemp,kmom,O,pol,flav)
                     endif
                  endif
               endif
            endif
            Jlk(k,k+l,:)=J(:)
            flavlk(k,k+l)=f1
         enddo
      enddo
      

      P(:)=kmom(O(1),:)
      h=pol(O(1)) 
      nflav=flav(O(1)) 
      if (abs(nflav).le.6) then
         if (h.eq.0) then
            write(*,*) 'Gauge invariance check initiated'
            if (nflav.ne.0) then
               write(*,"(' Particle ',i1,' has flavor ',i1)") k,nflav 
               write(*,*) 'Check aborted'
               stop
            else
               J0(:)=dcmplx(P(:))/50d0
                  write(*,"('J(',2i1,')=K(',i1,')->',d16.8)")
     &               ,nlegs-1,nlegs-1,nlegs-1,add(J0(:),kmom,O,pol,flav)
            endif
         else
            call wavefunction(P,nflav,h,J0)
           if (nflav.gt.0) write(*,"('ub(',2i1,')=ub(',i2,')->',d16.8)") 
     &           nlegs-1,nlegs-1,h,add(J0,kmom,O,pol,flav)
            if (nflav.eq.0) write(*,"('J(',2i1,')=e(',i2,')->',d16.8)") 
     &           nlegs-1,nlegs-1,h,add(J0,kmom,O,pol,flav)
            if (nflav.lt.0) write(*,"('u(',2i1,')=u(',i2,')->',d16.8)") 
     &           nlegs-1,nlegs-1,h,add(J0,kmom,O,pol,flav)
         endif
      endif
      J(:)=Jlk(1,Nlegs-1,:)
      fv=flavlk(1,Nlegs-1)
      if (nflav.ne.-fv) then
         write(*,*) "flavor mismatch", fv,nflav
         stop
      endif
      if (nflav .eq. 0) then
         weight=cdotpr(J0,J)
         write(*,"('A=J(',i1,i1,')*J(',i1,i1,')->',d16.8)")
     &        ,1,Nlegs-1,Nlegs,Nlegs,cdabs(weight)
      else
         weight=sprod(J0,J)
         if (nflav.gt.0) then
         write(*,"('A=ub(',i1,i1,')*u(',i1,i1,')->',d16.8)")
     &        ,Nlegs,Nlegs,1,Nlegs-1,cdabs(weight)
         endif
         if (nflav.lt.0) then
         write(*,"('A=ub(',i1,i1,')*u(',i1,i1,')->',d16.8)")
     &        ,1,Nlegs-1,Nlegs,Nlegs,cdabs(weight)
         endif
      endif
      return
      end

      function add(J,kmom,O,pol,flav)
      implicit none
      integer i
      double complex weight,cdotpr,sprod,J(4),J0(4)
      double precision add

      integer h,nflav,pol(7),flav(7),O(7)
      double precision P(4),kmom(7,4)

      P(:)=kmom(O(1),:)
      h=pol(O(1)) 
      nflav=flav(O(1)) 
      if (h.eq.0) then
         J0(:)=dcmplx(P(:))/50d0
      else
         call wavefunction(P,nflav,h,J0)
      endif
      if (nflav .eq. 0) then
         weight=cdotpr(J0,J)
      else
         weight=sprod(J0,J)
      endif
!      add=dreal(weight)
!      add=dimag(weight)
      add=0d0
      do i=1,4
         add=add+dimag(J(i))
         add=add+cdabs(J(i))
      enddo
      return
      end
