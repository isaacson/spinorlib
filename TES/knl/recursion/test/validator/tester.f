      subroutine tester(nlegs,gauge,flav,pol)
      implicit none
      integer ngluons,nquarks,nlegs,i,j,q,p,l,gauge
      double precision rn
      integer m(20),flav(20),pol(20) 

      ngluons=1+int(10d0*rn(1.))
      gauge=1+int(ngluons*rn(1.))
      nquarks=1+int(5d0*rn(1.))
      nlegs=ngluons+2*nquarks
      m(:)=0
      do i=1,nquarks
         m(2*i-1)=i
         m(2*i)=-i
      enddo
      do i=nquarks-1,1,-1
         p=1+int(10*rn(1.))
         if (p.gt.6) then
            j=2*i
            q=m(j)
            do while (q.ne.-(i+1)) 
               q=m(j+1)
               m(j)=q
               j=j+1
            enddo
            m(j)=-i
         endif
      enddo
      l=2*nquarks
      do i=1,ngluons
         p=1+int((l+i)*rn(1.))
         do j=nlegs-1,p,-1
            m(j+1)=m(j)
         enddo
         m(p)=0
      enddo

      p=1+int(nlegs*rn(1.))
      do i=1,nlegs
         q=p+i
         flav(i)=m(mod(q,nlegs)+1)
      enddo

      do i=1,nquarks
         m(i)=-1+2*int(2*rn(1.))
      enddo
      p=0
      do i=1, nlegs
         if (flav(i).gt.0) pol(i)=m(flav(i))
         if (flav(i).lt.0) pol(i)=-m(-flav(i))
         if (flav(i).eq.0) then
            p=p+1
            if (p.eq.gauge) gauge=i
            pol(i)=-1+2*int(2*rn(1.))
         endif
      enddo
      do i=1,nquarks
         m(i)=1+int(6*rn(1.))
      enddo
      do i=1,nlegs
         if ((i.le.2).and.m(abs(flav(i))).ge.5) then
            m(abs(flav(i)))=1+int(4*rn(1.))
         endif
         if (flav(i).gt.0) flav(i)=m(flav(i))
         if (flav(i).lt.0) flav(i)=-m(-flav(i))
      enddo
!      write(*,"(20i2)") (flav(i),i=1,nlegs)
!      write(*,"(20i2)") (pol(i),i=1,nlegs)
!      write(*,*) gauge

      return
      end
