      subroutine BG(kmom,pol,flav,Nlegs,weight)
      implicit none
      include 'constants.f'
      include 'fermionmass.f'
      
      integer Nlegs,i,l,k,n,m,h,f1,f2
      double precision P(4),kmom(Nlegs,4),qmom(Nlegs,4),dotpr,mm
      double complex Jlk(Nlegs,Nlegs,4),
     & K1(4),K2(4),Ktemp(4),cdotpr,sprod,weight,
     & J(4),J0(4),J1(4),J2(4),J3(4),Jtemp(4)
      integer pol(Nlegs),flav(Nlegs),flavlk(Nlegs,Nlegs),nflav
      logical loglk(Nlegs,Nlegs),flag


      Jlk(:,:,:)=czip
C---by default set all branchings true
      loglk(:,:)=.true.

C---setup polarization vectors for Nlegs-1 particles
C---diagonal elements of matrix
C---also diagonal elements of flavour matrix
      do k=2,Nlegs
      P(:)=kmom(k,:)
      h=pol(k) 
      nflav=flav(k) 
      flavlk(k-1,k-1)=flav(k)
      call wavefunction(P,nflav,h,J)
C---initialize diagonal elements
      Jlk(k-1,k-1,:)=J(:)
      Ktemp(:)=dcmplx(P(:))
      if (nflav .eq. 0) then
c      write(6,*) 'cdotpr(Ktemp,J)',nflav,cdotpr(Ktemp,J)
      elseif (nflav .gt. 0) then
      call Ubkslash(J,Ktemp,Jtemp)
      write(6,*) k,k,J
      write(6,*) nflav,Jtemp-mass(flav(k))*J(:)
      else
c--debug
c      call testV(P,nflav)
c--debug
      call kslashU(Ktemp,J,Jtemp)
c      write(6,*) k,k,J
c      write(6,*) nflav,Jtemp(:)+mass(flav(k))*J(:)
      endif
!      write(*,*) k-1,k-1
!      write(*,*) J
!      write(*,*) '+++++++++++++++++++++++++++++'
      enddo

C-----create cumulative sums of momenta
      P(:)=kmom(1,:)
      qmom(1,:)=P(:)
      do i=1,Nlegs-1
      P(:)=P(:)+kmom(1+i,:)
      qmom(1+i,:)=P(:)      
      enddo

      do l=1,Nlegs-2
         flag =.false.
         do k=1,Nlegs-l-1
!            write(*,*) k,k+l
            J(:)=0d0
            do n=k,k+l-1
               if (loglk(k,n) .and. loglk(n+1,k+l)) then
                  K1(:)=dcmplx(qmom(n+1,:)-qmom(k,:))
                  K2(:)=dcmplx(qmom(k+l+1,:)-qmom(n+1,:))
                  J1(:)=Jlk(k,n,:)
                  J2(:)=Jlk(n+1,k+l,:)
                  f1=flavlk(k,n)
                  f2=flavlk(n+1,k+l)
                  if ((f1.eq.0).and.(f2.eq.0)) then
                     flag=.true.
                     Jtemp(:)=+2d0*(cdotpr(J1,K2)*J2(:)
     &                        -cdotpr(J2,K1)*J1(:))
     &                        +cdotpr(J1,J2)*(K1(:)-K2(:))
                  elseif ((f1.eq.0).and.(f2.ne.0)) then 
                     flag=.true.
                     if (f2.gt.0) then
                        call Ubkslash(J2,J1,Jtemp)
                     else
                        call kslashU(J1,J2,Jtemp)
                     endif
                  elseif ((f1.ne.0).and.(f2.eq.0)) then 
                     flag=.true.
                     if (f1.gt.0) then
                        call Ubkslash(J1,J2,Jtemp)
                     else
                        call kslashU(J2,J1,Jtemp)
                     endif
                  elseif (f1.eq.-f2) then
                     flag=.true.
                     if (f1.gt.0) then
                        call UbGammaU(J1,J2,Jtemp)
                     else
                        call UbGammaU(J2,J1,Jtemp)
                     endif
                  endif
               else
C-----NOT POSSIBLE
                  loglk(k,k+l)=.false.
               endif
               J(:)=J(:)+Jtemp(:)
            enddo               ! endloop n

            f1=(f1+f2)
      do n=k,k+l-2
        do m=n+1,k+l-1
           if (loglk(k,n).and.loglk(n+1,m).and.loglk(m+1,k+l)) then
          if ((flavlk(k,n)==0)
     &   .and.(flavlk(n+1,m)==0)
     &   .and.(flavlk(m+1,k+l)==0)) then
          J1(:)=Jlk(k,n,:)
          J2(:)=Jlk(n+1,m,:)
          J3(:)=Jlk(m+1,k+l,:);
          J(:)=J(:)
     &    +2d0*cdotpr(J1,J3)*J2(:)
     &        -cdotpr(J1,J2)*J3(:)
     &        -cdotpr(J2,J3)*J1(:)
          f1=0
          endif
          endif
      enddo ! endloop m
      enddo ! endloop n

C---Divide by propagator for all except the last
      if (l < Nlegs-2) then
!         write(*,*) 'Adding propagator to '
!         write(*,*) J
         P(:)=qmom(k+l+1,:)-qmom(k,:)
         if (f1.eq.0) then
            J(:)=J(:)/dotpr(P,P)
         else
            Ktemp(:)=dcmplx(P(:))
            mm=mass(f1)
            if (f1.gt.0) then
!               write(*,*) f1
               call Ubkslash(J,Ktemp,Jtemp)
               J(:)=(Jtemp(:)+mm*J(:))/(dotpr(P,P)-mm**2)
            else
!               write(*,*) f1,mm
               call kslashU(Ktemp,J,Jtemp)
               J(:)=(Jtemp(:)-mm*J(:))/(dotpr(P,P)-mm**2)
            endif
         endif
      endif
      Jlk(k,k+l,:)=J(:)
      flavlk(k,k+l)=f1
C---check
!        write(*,*) 'current',J
        Jtemp(:)=J(:)
        Ktemp(:)=dcmplx(qmom(k+l+1,:)-qmom(k,:))
!      write(6,*) 'l,k,k+l,flav,dotpr(Jtemp,ktemp)',
!     &  l,k,k+l,flavlk(k,k+l),cdotpr(Jtemp,ktemp)
!      write(*,*) '---------------------------'
      if (flag .eqv. .false.) loglk(k,k+l)=.false.
      enddo  ! endloop k
      enddo  ! endloop l
      P(:)=qmom(1,:)
      nflav=flav(1)
      h=pol(1)
!      if (nflav.ne.0) h=-h 
!      write(6,*) 'nflav',nflav
      call wavefunction(P,nflav,h,J0)
c--debug
c      call testUb(P,nflav)
c--debug
      J(:)=Jlk(1,Nlegs-1,:) 

!      write(*,*) '+++++++++++++++++++++++++++++'
!      write(*,*) 0,0
!      write(*,*) J0

      if (nflav .eq. 0) then 
      weight=cdotpr(J0,J)
      else
      weight=sprod(J0,J)
      endif
      return
      end

