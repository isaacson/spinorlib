      subroutine orthogonal(J,Pmom) 
      implicit none
      include 'constants.f'
      double precision phi,s,z,x,y,ran2,Pmom(4),cosphi,sinphi,norm
      double complex J1(4),J(4)
      z=2d0*ran2()-1d0
      s=sqrt(abs(1d0-z*z))
      phi=2d0*pi*ran2()
      sinphi=sin(phi)
      cosphi=cos(phi)
      x=s*sinphi
      y=s*cosphi
      J1(4)=czip 
      J1(1)=dcmplx(y*Pmom(3)-z*Pmom(2))
      J1(2)=dcmplx(z*Pmom(1)-x*Pmom(3))
      J1(3)=dcmplx(x*Pmom(2)-y*Pmom(1))
      norm=sqrt(abs(J1(1)**2+J1(2)**2+J1(3)**2))

      if (norm > 0.0d0) then
         J1(1:3)=J1(1:3)/norm
      else 
         J1(:)=czip
      endif

      J(:)=J1(:)
      return
      end
