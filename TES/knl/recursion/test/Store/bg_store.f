      subroutine BG(kmom,Nlegs,weight)
      implicit none
      integer Nm1,Nlegs,i,l,k,n,m
      double precision Jlk(Nlegs,Nlegs,4),
     & P(4),kmom(0:Nlegs-1,4),K1(4),K2(4),J(4),J0(4),J1(4),J2(4),J3(4),
     & dotpr,Jtemp(4),Ktemp(4),weight
      Nm1=Nlegs-1

      Jlk(:,:,:)=0d0
      write(6,*) 'Nlegs',Nlegs
      write(6,*) 'incoming k0',kmom(0,1),kmom(0,2),kmom(0,3),kmom(0,4)      
      write(6,*) 'incoming k1',kmom(1,1),kmom(1,2),kmom(1,3),kmom(1,4)      
      write(6,*) 'incoming k2',kmom(2,1),kmom(2,2),kmom(2,3),kmom(2,4)      
      write(6,*) 'incoming k3',kmom(3,1),kmom(3,2),kmom(3,3),kmom(3,4)      
      write(6,*) 'incoming k0+k1',
     & kmom(0,1)+kmom(1,1),
     & kmom(0,2)+kmom(1,2),
     & kmom(0,3)+kmom(1,3),
     & kmom(0,4)+kmom(1,4)
      write(6,*) 'incoming k0+k1+k2',
     & kmom(0,1)+kmom(1,1)+kmom(2,1),
     & kmom(0,2)+kmom(1,2)+kmom(2,2),
     & kmom(0,3)+kmom(1,3)+kmom(2,3),
     & kmom(0,4)+kmom(1,4)+kmom(2,4)
      write(6,*) 'incoming k0+k1+k2+k3',
     & kmom(0,1)+kmom(1,1)+kmom(2,1)+kmom(3,1),
     & kmom(0,2)+kmom(1,2)+kmom(2,2)+kmom(3,2),
     & kmom(0,3)+kmom(1,3)+kmom(2,3)+kmom(3,3),
     & kmom(0,4)+kmom(1,4)+kmom(2,4)+kmom(3,4)

C---setup polarization vectors for Nm1 particles
      do k=1,Nm1
      P(:)=kmom(k,:)
      call orthogonal(J,P)
C---initialize diagonal
      Jlk(k,k,:)=J(:)
      enddo

C-----create cumulative sums of momenta
      P(:)=kmom(0,:)
      do i=1,Nm1
      P(:)=P(:)+kmom(i,:)
      kmom(i,:)=P(:)      
      enddo

      write(6,*) 'Nm1',Nm1
      do l=1,Nm1-1
        do k=1,Nm1-l
            J(:)=0d0
            do n=k,k+l-1
              K1(:)=kmom(n,:)-kmom(k-1,:)
              K2(:)=kmom(k+l,:)-kmom(n,:)
              J1(:)=Jlk(k,n,:)
              J2(:)=Jlk(n+1,k+l,:)
          J(:)=J(:)
     &        +2d0*dotpr(J1,K2)*J2(:)
     &        -2d0*dotpr(J2,K1)*J1(:)
     &        +dotpr(J1,J2)*(K1(:)-K2(:))
            enddo  ! endloop n

      do n=k,k+l-2
        do m=n+1,k+l-1
          J1(:)=Jlk(k,n,:)
          J2(:)=Jlk(n+1,m,:)
          J3(:)=Jlk(m+1,k+l,:);
          J(:)=J(:)
     &    +2d0*dotpr(J1,J3)*J2(:)
     &        -dotpr(J1,J2)*J3(:)
     &        -dotpr(J2,J3)*J1(:)
      enddo ! endloop m
      enddo ! endloop n

C---Divide by propagator for all except the last
        if (l < Nm1-1) then
        P(:)=kmom(k+l,:)-kmom(k-1,:)
        J(:)=J(:)/dotpr(P,P)
      endif
        Jlk(k,k+l,:)=J(:)
C---check
        Jtemp(:)=J(:)
        Ktemp(:)=kmom(k+l,:)-kmom(k-1,:)
      write(6,*) 'l,k,k+l,dotpr(Jtemp,ktemp)',l,k,k+l,dotpr(Jtemp,ktemp)
      enddo  ! endloop k
      enddo  ! endloop l
      P(:)=kmom(0,:)
      call orthogonal(J0,P)
      write(6,*) 'taking element',1,Nm1,Jlk(1,Nm1,:)
      J(:)=Jlk(1,Nm1,:) 
      weight=dotpr(J0,J)**2
      return
      end

