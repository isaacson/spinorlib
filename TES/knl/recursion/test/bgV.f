      subroutine BGv(O,kmom,klep,pol,pollep,flav,Nlegs,weight)
      implicit none
      include 'constants.f'
      include 'fermionmass.f'
      
      integer Nlegs,i,l,k,n,m,h,f1,f2,fv,fvout,fvin,h1,h2,nflav
      integer pol(10),flav(10),flavlk(Nlegs,Nlegs)
      integer flavV(Nlegs, Nlegs),pollep(2),O(10)
      double precision P(4),Pv(4),kmom(Nlegs,4),qmom(Nlegs,4),klep(2,4)
      double precision Rlep,Llep,Rquark,Lquark,dotpr,mm,Mv,Pv2,Gv
      double precision add
      double complex Jlk(Nlegs,Nlegs,4),Jv(Nlegs,Nlegs,4),
     & K1(4),K2(4),Ktemp(4),cdotpr,sprod,weight,
     & J(4),J0(4),J1(4),J2(4),J3(4),Jtemp(4),Lcur(4),Pc(4),
     & JV1(4),JV2(4),JV3(4),JV4(4)
      logical loglk(Nlegs,Nlegs),logv(Nlegs,Nlegs)

      Rlep=1d0
      Llep=1d0
      Rquark=1.0d0
      Lquark=1.0d0
      Mv=25d0
      Gv=1d0
      fvin=1
      fvout=1

      Jlk(:,:,:)=czip
      Jv(:,:,:)=czip
C---by default set all branchings true
      loglk(:,:)=.false.
      logv(:,:)=.false.
C-- Make lepton current
      Pv(:)=klep(1,:)+klep(2,:)
      Pv2=Pv(4)**2-Pv(1)**2-Pv(2)**2-Pv(3)**2
      h1=pollep(1)
      h2=pollep(2)
      call wavefunction(Klep(1,:),11,h1,J1)
      call wavefunction(Klep(2,:),-11,h2,J2)
      call UbGammaURL(J2,J1,Lcur,Rlep,Llep)
! -----> Add vector boson propagator to the lepton current
!      Lcur(:)=Lcur(:)/(Pv2-Mv*Mv+im*Gv*Mv)
      Pc(:)=dcmplx(Pv(:))
      write(*,*)
      write(*,*) 'Lepton current conservation check (Pv*Jlep)'
     .     ,cdotpr(Lcur,Pc)
      write(*,*)
C-----create cumulative sums of momenta
      P(:)=kmom(O(1),:)
      qmom(1,:)=P(:)
      do i=1,Nlegs-1
         P(:)=P(:)+kmom(O(1+i),:)
         qmom(1+i,:)=P(:)  
      enddo

C---construct the partonic currents
C---setup polarization vectors for Nlegs-1 particles
C---diagonal elements of matrix
C---also diagonal elements of flavour matrix
      write(*,*) "----- Making sources -----"
      do k=2,Nlegs-1
C ---partonic wavefunction
         P(:)=kmom(O(k),:)
         h=pol(O(k)) 
         nflav=flav(O(k)) 
         if (abs(nflav).le.6) then
            loglk(k-1,k-1)=.true.
            flavlk(k-1,k-1)=nflav
            if (h.eq.0) then
               write(*,*) 'Gauge invariance check initiated'
               if (nflav.ne.0) then
                  write(*,"(' Particle ',i1,' has flavor ',i1)") k,nflav 
                  write(*,*) 'Check aborted'
                  stop
               else
                  Jlk(k-1,k-1,:)=dcmplx(P(:))/50d0
                  write(*,"('J(',2i1,')=K(',i1,')->',d16.8)")
     &                 ,k-1,k-1,k-1,add(Jlk(k-1,k-1,:),kmom,O,pol,flav)
               endif
            else
               call wavefunction(P,nflav,h,J)
               Jlk(k-1,k-1,:)=J(:)
               write(*,*) '1'
           if (nflav.gt.0) write(*,"('ub(',2i1,')=ub(',i2,')->',d16.8)") 
     &           k-1,k-1,h,add(J,kmom,O,pol,flav)
            if (nflav.eq.0) write(*,"('J(',2i1,')=e(',i2,')->',d16.8)") 
     &           k-1,k-1,h,add(J,kmom,O,pol,flav)
            if (nflav.lt.0) write(*,"('u(',2i1,')=u(',i2,')->',d16.8)") 
     &           k-1,k-1,h,add(J,kmom,O,pol,flav)
           endif
         endif
C ---add vectorboson if correct flavor
         Ktemp(:)=dcmplx(P(:)+Pv(:)) 
         mm=mass(nflav)
         Jtemp(:)=dcmplx(0d0,0d0)
         if (abs(nflav).gt.0) then
            if (nflav.gt.0) then
               call UbkslashRL(J,Lcur,Jtemp,Rquark,Lquark)
               if (Nlegs.gt.3) then
                 call UbkslashRL(Jtemp,Ktemp,J,1d0,1d0)
                 Jtemp(:)=(J(:)+mm*Jtemp)/(cdotpr(Ktemp,Ktemp)-mm**2)
               endif
               logV(k-1,k-1)=.true.
               flavV(k-1,k-1)=nflav
           write(*,"('ubV(',2i1,')=ub(',2i1,')*V*Pub(Pv+K(',i1,'))->'
     &     ,d16.8)"),k-1,k-1,k-1,k-1,k-1,add(Jtemp,kmom,O,pol,flav)
           endif
            if (nflav.lt.0) then
               call kslashURL(Lcur,J,Jtemp,Rquark,Lquark)
               if (Nlegs.gt.3) then
                 call kslashURL(-Ktemp,Jtemp,J,1d0,1d0)
                Jtemp(:)=(J(:)+mm*Jtemp(:))/(cdotpr(Ktemp,Ktemp)-mm**2)
               endif
               logV(k-1,k-1)=.true.
               flavV(k-1,k-1)=nflav
             write(*,"('uV(',2i1,')=Pu(Pv+K(',i1,')*V*u(',2i1,')->'
     &       ,d16.8)"),k-1,k-1,k-1,k-1,k-1,add(J,kmom,O,pol,flav)
            endif
         endif
         Jv(k-1,k-1,:)=Jtemp(:)
      enddo
!
! Make partonic currents
!
      write(*,*) "----- Making partonic currents -----"
      do l=1,Nlegs-3
         do k=1,Nlegs-l-2
            J(:)=0d0
            write(*,"('J(',i1,i1,')=0')"),k,k+l
            do n=k,k+l-1
               Jtemp(:)=dcmplx(0d0,0d0)
               if (loglk(k,n) .and. loglk(n+1,k+l)) then
                  K1(:)=dcmplx(qmom(n+1,:)-qmom(k,:))
                  K2(:)=dcmplx(qmom(k+l+1,:)-qmom(n+1,:))
                  J1(:)=Jlk(k,n,:)
                  J2(:)=Jlk(n+1,k+l,:)
                  f1=flavlk(k,n)
                  f2=flavlk(n+1,k+l)
                  if ((f1.eq.0).and.(f2.eq.0)) then
                     loglk(k,k+l)=.true.
                     Jtemp(:)=+2d0*(cdotpr(J1,K2)*J2(:)
     &                        -cdotpr(J2,K1)*J1(:))
     &                        +cdotpr(J1,J2)*(K1(:)-K2(:))
              write(*,"('J(',2i1,')+=[J(',2i1,'),J(',2i1,')]->',d16.8)")
     &                    ,k,k+l,k,n,n+1,k+l,add(Jtemp,kmom,O,pol,flav)
                  elseif ((f1.eq.0).and.(f2.ne.0)) then 
                     loglk(k,k+l)=.true.
                     if (f2.gt.0) then
                        call UbkslashRL(J2,J1,Jtemp,1d0,1d0)
               write(*,"('ub(',2i1,')+=Ub(',2i1,')*J('2i1')->',d16.8)")
     &                    ,k,k+l,n+1,k+l,k,n,add(Jtemp,kmom,O,pol,flav)
                     else
                        call kslashURL(J1,J2,Jtemp,1d0,1d0)
               write(*,"('u(',2i1,')+=J(',2i1,')*u('2i1')->',d16.8)")
     &                    ,k,k+l,k,n,n+1,k+l,add(Jtemp,kmom,O,pol,flav)
                        Jtemp=-Jtemp
                     endif
                  elseif ((f1.ne.0).and.(f2.eq.0)) then 
                     loglk(k,k+l)=.true.
                     if (f1.gt.0) then
                        call UbkslashRL(J1,J2,Jtemp,1d0,1d0)
                        Jtemp=-Jtemp
               write(*,"('ub(',2i1,')-=ub(',2i1,')*J('2i1')->',d16.8)")
     &                    ,k,k+l,k,n,n+1,k+l,add(Jtemp,kmom,O,pol,flav)
                     else
                        call kslashURL(J2,J1,Jtemp,1d0,1d0)
               write(*,"('u(',2i1,')+=J(',2i1,')*u('2i1')->',d16.8)")
     &                    ,k,k+l,n+1,k+l,k,n,add(Jtemp,kmom,O,pol,flav)
                     endif
                  elseif (f1.eq.-f2) then
                     loglk(k,k+l)=.true.
                     if (f1.gt.0) then
                        call UbGammaURL(J2,J1,Jtemp,1d0,1d0)
               write(*,"('J(',2i1,')+=u(',2i1,')*ub('2i1')->',d16.8)")
     &                    ,k,k+l,n+1,k+l,k,n,add(Jtemp,kmom,O,pol,flav)
!                        Jtemp=-Jtemp
                     else
                        call UbGammaURL(J1,J2,Jtemp,1d0,1d0)
               write(*,"('J(',2i1,')+=u(',2i1,')*ub('2i1')->',d16.8)")
     &                    ,k,k+l,k,n,n+1,k+l,add(Jtemp,kmom,O,pol,flav)
!                        Jtemp=-Jtemp
                     endif
                  endif
               endif
               J(:)=J(:)+Jtemp(:)
            enddo               ! endloop n
            
            f1=(f1+f2)
            do n=k,k+l-2
               do m=n+1,k+l-1
                  jtemp(:)=0d0
                if (loglk(k,n).and.loglk(n+1,m).and.loglk(m+1,k+l)) then
                     if ((flavlk(k,n)==0)
     &                    .and.(flavlk(n+1,m)==0)
     &                    .and.(flavlk(m+1,k+l)==0)) then
                        loglk(k,k+l)=.true.
                        J1(:)=Jlk(k,n,:)
                        J2(:)=Jlk(n+1,m,:)
                        J3(:)=Jlk(m+1,k+l,:)
                        Jtemp(:)=
     &                       +2d0*cdotpr(J1,J3)*J2(:)
     &                       -cdotpr(J1,J2)*J3(:)
     &                       -cdotpr(J2,J3)*J1(:)
             write(*,"('J(',2i1,')+=[J(',2i1,')J(',2i1,')J(',2i1,')]->'
     &     ,d16.8)"),k,k+l,k,n,n+1,m,m+1,k+l,add(Jtemp,kmom,O,pol,flav)

                        f1=0
                     endif
                  endif
                  J(:)=J(:)+Jtemp(:)
               enddo            ! endloop m
            enddo               ! endloop n

C---Divide by propagator
            P(:)=qmom(k+l+1,:)-qmom(k,:)
            if (loglk(k,k+l)) then
            if (f1.eq.0) then
               J(:)=J(:)/dotpr(P,P)
               write(*,"('J(',2i1,')*=Pg(K(',2i1,'))->',d16.8)")
     &              ,k,k+l,k,k+l,add(J,kmom,O,pol,flav)
            else
               Ktemp(:)=dcmplx(P(:))
               mm=mass(f1)
               if (f1.gt.0) then
                  call UbkslashRL(J,Ktemp,Jtemp,1d0,1d0)
                  J(:)=(Jtemp(:)+mm*J(:))/(dotpr(P,P)-mm**2)
               write(*,"('ub(',2i1,')*=Pub(K(',2i1,'))->',d16.8)")
     &                 ,k,k+l,k,k+l,add(Jtemp,kmom,O,pol,flav)
               else
                  call kslashURL(-Ktemp,J,Jtemp,1d0,1d0)
                  J(:)=(Jtemp(:)+mm*J(:))/(dotpr(P,P)-mm**2)
               write(*,"('J(',2i1,')*=Pu(K(',2i1,'))->',d16.8)")
     &                 ,k,k+l,k,k+l,add(Jtemp,kmom,O,pol,flav)
               endif
            endif
            endif
            Jlk(k,k+l,:)=J(:)
            flavlk(k,k+l)=f1
         enddo
      enddo
      

      write(*,*) "----- Adding vector boson to partonic currents -----"
      do l=1,Nlegs-3
         do k=1,Nlegs-l-2
! 
! Construct the vector currents by adding vector boson to partonic current
!
            fv=0
            logV(k,k+l)=.false.
            JV1(:)=dcmplx(0d0,0d0)
!            write(*,"('Jv(',i1,i1,')=0')"),k,k+l
            if (loglk(k,k+l)) then
               Jtemp(:)=Jlk(k,k+l,:)
               f1=flavlk(k,k+l)
               if (f1.gt.0) then
                  call UbkslashRL(Jtemp,Lcur,JV1,Rquark,Lquark)
                  logV(k,k+l)=.true.
                  fv=f1
                  write(*,"('ubV(',2i1,')=ub(',2i1,')*V->',d16.8)")
     &            ,k,k+l,k,k+l,add(JV1,kmom,O,pol,flav)
               elseif (f1.lt.0) then
                  call kslashURL(Lcur,Jtemp,JV1,Rquark, Lquark)
                  logV(k,k+l)=.true.
                  fv=f1
                  write(*,"('uV(',2i1,')=V*u(',2i1,')->',d16.8)")
     &            ,k,k+l,k,k+l,add(JV1,kmom,O,pol,flav)
               endif
            endif
            flavV(k,k+l)=fv


!     merge vector current with partonic current
            JV2(:)=dcmplx(0d0,0d0)
            do n=k,k+l-1
               Jtemp(:)=dcmplx(0d0,0d0)
               if (logV(k,n) .and. loglk(n+1,k+l)) then
                  logV(k,k+l)=.true.
                  K1(:)=dcmplx(Pv(:)+qmom(n+1,:)-qmom(k,:))
                  K2(:)=dcmplx(qmom(k+l+1,:)-qmom(n+1,:))
                  J1(:)=JV(k,n,:)
                  J2(:)=Jlk(n+1,k+l,:)
                  f1=flavV(k,n)
                  f2=flavlk(n+1,k+l)
                  if ((f1.eq.0).and.(f2.eq.0)) then
                     logV(k,k+l)=.true.
                     Jtemp(:)=+2d0*(cdotpr(J1,K2)*J2(:)
     &                    -cdotpr(J2,K1)*J1(:))
     &                    +cdotpr(J1,J2)*(K1(:)-K2(:))
            write(*,"('Jv(',2i1,')+=[Jv(',2i1,'),J(',2i1,')]->',d16.8)")
     &                    ,k,k+l,k,n,n+1,k+l,add(Jtemp,kmom,O,pol,flav)                     
                  elseif ((f1.eq.0).and.(f2.ne.0)) then 
                     logV(k,k+l)=.true.
                     if (f2.gt.0) then
                        call UbkslashRL(J2,J1,Jtemp,1d0,1d0)
                        fv=f2
              write(*,"('ubV(',2i1,')+=Ub(',2i1,')*Jv('2i1')->',d16.8)")
     &                    ,k,k+l,n+1,k+l,k,n,add(Jtemp,kmom,O,pol,flav)
                     else
                        call kslashURL(J1,J2,Jtemp,1d0,1d0)
                        Jtemp=-Jtemp
                        fv=f2
               write(*,"('uV(',2i1,')+=Jv(',2i1,')*u('2i1')->',d16.8)")
     &                    ,k,k+l,k,n,n+1,k+l,add(Jtemp,kmom,O,pol,flav)
                     endif
                  elseif ((f1.ne.0).and.(f2.eq.0)) then 
                     logV(k,k+l)=.true.
                     if (f1.gt.0) then
                        call UbkslashRL(J1,J2,Jtemp,1d0,1d0)
                        fv=f1
              write(*,"('ubV(',2i1,')-=ubV(',2i1,')*J('2i1')->',d16.8)")
     &                    ,k,k+l,k,n,n+1,k+l,add(Jtemp,kmom,O,pol,flav)
                        Jtemp=-Jtemp
                     else
                        call kslashURL(J2,J1,Jtemp,1d0,1d0)
                        fv=f1
               write(*,"('uV(',2i1,')+=J(',2i1,')*uV('2i1')->',d16.8)")
     &                    ,k,k+l,n+1,k+l,k,n,add(Jtemp,kmom,O,pol,flav)
                     endif
                  elseif (f1.eq.-f2) then
                     logV(k,k+l)=.true.
                     fv=0
                     if (f1.gt.0) then
                        call UbGammaURL(J2,J1,Jtemp,1d0,1d0)
              write(*,"('Jv(',2i1,')+=u(',2i1,')*ubV('2i1')->',d16.8)")
     &                    ,k,k+l,n+1,k+l,k,n,add(Jtemp,kmom,O,pol,flav)
 !                       Jtemp=-Jtemp
                    else
                        call UbGammaURL(J1,J2,Jtemp,1d0,1d0)
               write(*,"('Jv(',2i1,')+=uV(',2i1,')*ub('2i1')->',d16.8)")
     &                    ,k,k+l,k,n,n+1,k+l,add(Jtemp,kmom,O,pol,flav)
 !                           Jtemp=-Jtemp
                      endif
                  else
                     logV(k,k+l)=.false.
                  endif
                  JV2(:)=JV2(:)+Jtemp(:)
               endif
            enddo               ! endloop n

            JV3(:)=dcmplx(0d0,0d0)
            do n=k,k+l-1
               Jtemp(:)=dcmplx(0d0,0d0)
               if (loglk(k,n) .and. logV(n+1,k+l)) then
                  logV(k,k+l)=.true.
                  K1(:)=dcmplx(qmom(n+1,:)-qmom(k,:))
                  K2(:)=dcmplx(Pv(:)+qmom(k+l+1,:)-qmom(n+1,:))
                  J1(:)=Jlk(k,n,:)
                  J2(:)=JV(n+1,k+l,:)
                  f1=flavlk(k,n)
                  f2=flavV(n+1,k+l)
                  if ((f1.eq.0).and.(f2.eq.0)) then
                     logV(k,k+l)=.true.
                     Jtemp(:)=+2d0*(cdotpr(J1,K2)*J2(:)
     &                    -cdotpr(J2,K1)*J1(:))
     &                    +cdotpr(J1,J2)*(K1(:)-K2(:))
            write(*,"('Jv(',2i1,')+=[J(',2i1,'),Jv(',2i1,')]->',d16.8)")
     &                    ,k,k+l,k,n,n+1,k+l,add(Jtemp,kmom,O,pol,flav)                     
                  elseif ((f1.eq.0).and.(f2.ne.0)) then 
                     logV(k,k+l)=.true.
                     if (f2.gt.0) then
                        call UbkslashRL(J2,J1,Jtemp,1d0,1d0)
                        fv=f2
              write(*,"('ubV(',2i1,')+=ubV(',2i1,')*J('2i1')->',d16.8)")
     &                    ,k,k+l,n+1,k+l,k,n,add(Jtemp,kmom,O,pol,flav)
                     else
                        call kslashURL(J1,J2,Jtemp,1d0,1d0)
                        Jtemp=-Jtemp
                        fv=f2
               write(*,"('uV(',2i1,')-=J(',2i1,')*uV('2i1')->',d16.8)")
     &                    ,k,k+l,k,n,n+1,k+l,add(Jtemp,kmom,O,pol,flav)
                     endif
                  elseif ((f1.ne.0).and.(f2.eq.0)) then 
                     logV(k,k+l)=.true.
                     if (f1.gt.0) then
                        call UbkslashRL(J1,J2,Jtemp,1d0,1d0)
                        fv=f1
                        Jtemp=-Jtemp
              write(*,"('ubV(',2i1,')-=ub(',2i1,')*Jv('2i1')->',d16.8)")
     &                    ,k,k+l,k,n,n+1,k+l,add(Jtemp,kmom,O,pol,flav)
                     else
                        call kslashURL(J2,J1,Jtemp,1d0,1d0)
                        fv=f1
                     endif
               write(*,"('uV(',2i1,')+=JV(',2i1,')*u('2i1')->',d16.8)")
     &                    ,k,k+l,n+1,k+l,k,n,add(Jtemp,kmom,O,pol,flav)
                  elseif (f1.eq.-f2) then
                     logV(k,k+l)=.true.
                     fv=0
                     if (f1.gt.0) then
                        call UbGammaURL(J2,J1,Jtemp,1d0,1d0)
               write(*,"('Jv(',2i1,')+=uV(',2i1,')*ub('2i1')->',d16.8)")
     &                    ,k,k+l,n+1,k+l,k,n,add(Jtemp,kmom,O,pol,flav)
!                        Jtemp=-Jtemp
                     else
                        call UbGammaURL(J1,J2,Jtemp,1d0,1d0)
               write(*,"('Jv(',2i1,')+=u(',2i1,')*ubV('2i1')->',d16.8)")
     &                  ,k,k+l,k,n,n+1,k+l,add(Jtemp,kmom,O,pol,flav)
!                        Jtemp=-Jtemp
                     endif
                  else
                     logV(k,k+l)=.false.
                   endif
                   JV3(:)=JV3(:)+Jtemp(:)
               endif
            enddo               ! endloop n

            JV4(:)=dcmplx(0d0,0d0)
           do n=k,k+l-2
               do m=n+1,k+l-1
                  Jtemp(:)=dcmplx(0d0,0d0)
               if (logV(k,n).and.loglk(n+1,m).and.loglk(m+1,k+l)) then
                     if ((flavV(k,n)==0)
     &                    .and.(flavlk(n+1,m)==0)
     &                    .and.(flavlk(m+1,k+l)==0)) then
                        logV(k,k+l)=.true.
                        J1(:)=Jv(k,n,:)
                        J2(:)=Jlk(n+1,m,:)
                        J3(:)=Jlk(m+1,k+l,:)
                        Jtemp(:)=
     &                       +2d0*cdotpr(J1,J3)*J2(:)
     &                       -cdotpr(J1,J2)*J3(:)
     &                       -cdotpr(J2,J3)*J1(:)
            write(*,"('Jv(',2i1,')+=[Jv(',2i1,')J(',2i1,')J(',2i1,')]->'
     &      ,d16.8)"),k,k+l,k,n,n+1,m,m+1,k+l,add(Jtemp,kmom,O,pol,flav)
                        fv=0
                        JV4(:)=JV4(:)+Jtemp(:)
                     endif
                  endif
                if (loglk(k,n).and.logV(n+1,m).and.loglk(m+1,k+l)) then
                     if ((flavlk(k,n)==0)
     &                    .and.(flavV(n+1,m)==0)
     &                    .and.(flavlk(m+1,k+l)==0)) then
                        logV(k,k+l)=.true.
                        J1(:)=Jlk(k,n,:)
                        J2(:)=JV(n+1,m,:)
                        J3(:)=Jlk(m+1,k+l,:)
                        Jtemp(:)=
     &                       +2d0*cdotpr(J1,J3)*J2(:)
     &                       -cdotpr(J1,J2)*J3(:)
     &                       -cdotpr(J2,J3)*J1(:)
            write(*,"('Jv(',2i1,')+=[J(',2i1,')Jv(',2i1,')J(',2i1,')]->'
     &      ,d16.8)"),k,k+l,k,n,n+1,m,m+1,k+l,add(Jtemp,kmom,O,pol,flav)
                        fv=0
                        JV4(:)=JV4(:)+Jtemp(:)
                     endif
                  endif
                if (loglk(k,n).and.loglk(n+1,m).and.logV(m+1,k+l)) then
                     if ((flavlk(k,n)==0)
     &                    .and.(flavlk(n+1,m)==0)
     &                    .and.(flavV(m+1,k+l)==0)) then
                        logV(k,k+l)=.true.
                        J1(:)=Jlk(k,n,:)
                        J2(:)=Jlk(n+1,m,:)
                        J3(:)=JV(m+1,k+l,:)
                        Jtemp(:)=
     &                       +2d0*cdotpr(J1,J3)*J2(:)
     &                       -cdotpr(J1,J2)*J3(:)
     &                       -cdotpr(J2,J3)*J1(:)
            write(*,"('Jv(',2i1,')+=[J(',2i1,')J(',2i1,')J(',2i1,')]->'
     &      ,d16.8)"),k,k+l,k,n,n+1,m,m+1,k+l,add(Jtemp,kmom,O,pol,flav)
                        fv=0
                        JV4(:)=JV4(:)+Jtemp(:)
                     endif
                  endif
               enddo            ! endloop m
            enddo               ! endloop n



C---Divide by propagator for all except the last
            J(:)=JV1(:)+JV2(:)+JV3(:)+JV4(:)
            f1=fv
           if ((l < Nlegs-3).and.(logV(k,k+l))) then
               P(:)=Pv(:)+qmom(k+l+1,:)-qmom(k,:)
               if (f1.eq.0) then
                  J(:)=J(:)/dotpr(P,P)
               write(*,"('Jv(',2i1,')*=Pg(K(',2i1,'))->',d16.8)")
     &              ,k,k+l,k,k+l,add(J,kmom,O,pol,flav)
               else
                  Ktemp(:)=dcmplx(P(:))
                  mm=mass(f1)
                  if (f1.gt.0) then
                     call UbkslashRL(J,Ktemp,Jtemp,1d0,1d0)
                     J(:)=(Jtemp(:)+mm*J(:))/(dotpr(P,P)-mm**2)
               write(*,"('ub(',2i1,')*=Pub(Pv+K(',2i1,'))->',d16.8)")
     &                 ,k,k+l,k,k+l,add(Jtemp,kmom,O,pol,flav)
                  else
                     call kslashURL(-Ktemp,J,Jtemp,1d0,1d0)
                     J(:)=(Jtemp(:)+mm*J(:))/(dotpr(P,P)-mm**2)
               write(*,"('J(',2i1,')*=Pu(Pv+K(',2i1,'))->',d16.8)")
     &                 ,k,k+l,k,k+l,add(Jtemp,kmom,O,pol,flav)
                  endif
               endif
            endif
            Jv(k,k+l,:)=J(:)
            flavV(k,k+l)=fv
         enddo                  ! endloop k
      enddo                     ! endloop l


      write(*,*) "----- Put final parton on shell  -----"

      P(:)=kmom(O(1),:)
      h=pol(O(1)) 
      nflav=flav(O(1)) 
      if (abs(nflav).le.6) then
         if (h.eq.0) then
            write(*,*) 'Gauge invariance check initiated'
            if (nflav.ne.0) then
               write(*,"(' Particle ',i1,' has flavor ',i1)") k,nflav 
               write(*,*) 'Check aborted'
               stop
            else
               J0(:)=dcmplx(P(:))/50d0
                  write(*,"('J(',2i1,')=K(',i1,')->',d16.8)")
     &               ,nlegs-1,nlegs-1,nlegs-1,add(J0(:),kmom,O,pol,flav)
            endif
         else
               call wavefunction(P,nflav,h,J0)
           if (nflav.gt.0) write(*,"('ub(',2i1,')=ub(',i2,')->',d16.8)") 
     &           nlegs-1,nlegs-1,h,add(J0,kmom,O,pol,flav)
            if (nflav.eq.0) write(*,"('J(',2i1,')=e(',i2,')->',d16.8)") 
     &           nlegs-1,nlegs-1,h,add(J0,kmom,O,pol,flav)
            if (nflav.lt.0) write(*,"('u(',2i1,')=u(',i2,')->',d16.8)") 
     &           nlegs-1,nlegs-1,h,add(J0,kmom,O,pol,flav)
         endif
      endif
      J(:)=Jv(1,Nlegs-2,:)
      fv=flavV(1,Nlegs-2)
      if (nflav.ne.-fv) then
         write(*,*) "flavor mismatch", fv,nflav
         stop
      endif
      if (nflav .eq. 0) then
         weight=cdotpr(J0,J)
         write(*,"('A=Jv(',i1,i1,')*J(',i1,i1,')->',d16.8)")
     &        ,1,Nlegs-2,Nlegs-1,Nlegs-1,cdabs(weight)
      else
         weight=sprod(J0,J)
         if (nflav.gt.0) then
         write(*,"('A=ub(',i1,i1,')*uV(',i1,i1,')->',d16.8)")
     &        ,Nlegs-1,Nlegs-1,1,Nlegs-2,cdabs(weight)
         endif
         if (nflav.lt.0) then
         write(*,"('A=ubV(',i1,i1,')*u(',i1,i1,')->',d16.8)")
     &        ,1,Nlegs-2,Nlegs-1,Nlegs-1,cdabs(weight)
         endif
      endif
      return
      end


      function add(J,kmom,O,pol,flav)
      implicit none
      integer i
      double complex weight,cdotpr,sprod,J(4),J0(4)
      double precision add

      integer h,nflav,pol(7),flav(7),O(7)
      double precision P(4),kmom(7,4)

      P(:)=kmom(O(1),:)
      h=pol(O(1)) 
      nflav=flav(O(1)) 
      if (h.eq.0) then
         J0(:)=dcmplx(P(:))/50d0
      else
         call wavefunction(P,nflav,h,J0)
      endif
      if (nflav .eq. 0) then
         weight=cdotpr(J0,J)
      else
         weight=sprod(J0,J)
      endif
!      add=dreal(weight)
!      add=dimag(weight)
      add=0d0
      do i=1,4
         add=add+dimag(J(i))
         add=add+cdabs(J(i))
      enddo
      return
      end
