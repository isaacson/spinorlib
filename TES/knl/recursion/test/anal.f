      subroutine anal3(p,ap,am)
      include 'constants.f'
      include 'zprods_com.f'
      include 'sprods_com.f'
c      integer j,k
      double precision p(mxpart,4)
      double complex am,ap,sump,summ
      call spinorz(5,p,za,zb)

      ap=sump(1,3,2,4,5,2,1,1)       
      am=summ(1,3,2,4,5,2,1,1)       

      return
      end

      subroutine anal4(p,app,apm,amp,amm)
      include 'constants.f'
      include 'zprods_com.f'
      include 'sprods_com.f'
c      integer j,k
      double precision p(mxpart,4)
      double complex amm,amp,apm,app,summm,summp,sumpm,sumpp
      call spinorz(6,p,za,zb)


      app=4d0*sumpp(1,3,4,2,5,6,2,2,1,1,1,1,1)       
      write(6,*) 'app',app, cdabs(app)
!      app=sumpp(3,1,2,4,5,6,5,5,1,1,1,1,1)       
!      write(6,*) 'app',app  
      write(6,*) 

      apm=4d0*sumpm(1,3,4,2,5,6,2,2,1,1,1,1,1)       
      write(6,*) 'apm',apm,cdabs(apm)
!      apm=sumpm(3,1,2,4,5,6,5,5,1,1,1,1,1)       
!      write(6,*) 'apm',apm  
      write(6,*) 

      amp=4d0*summp(1,3,4,2,5,6,2,2,1,1,1,1,1)       
      write(6,*) 'amp',amp,cdabs(amp)
!      amp=summp(3,1,2,4,5,6,5,5,1,1,1,1,1)       
!      write(6,*) 'amp',amp  
      write(6,*) 

      amm=4d0*summm(1,3,4,2,5,6,2,2,1,1,1,1,1)       
      write(6,*) 'amm',amm,cdabs(amm)
!      amm=summm(2,1,3,4,5,6,5,5,1,1,1,1,1)       
!      write(6,*) 'amm',amm  
      write(6,*) 
!      pause
      return
      end
