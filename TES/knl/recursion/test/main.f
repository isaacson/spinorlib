      implicit none
      include 'constants.f'
      include 'swapxz.f'
      include 'fermionmass.f'
      include 'zprods_com.f'
      integer nu,j,nrambo,lw,Nlegs,ivec
      parameter(Nlegs=4)
      parameter(nrambo=Nlegs-2)
      double complex ap,am,ampl,amm,amp,apm,app,anly
      double precision Mr,Mv,Gv,Rn,p0(4),pl(4),pw(4),
     & kmom(Nlegs,4),klep(2,4),xm(nrambo+1),p(4,nrambo+1),pswt,Et
     & ,pmcfm(mxpart,4),dotpr
      integer Order(10),pol(10),pollep(2),flav(10)
      common/mcfm/pmcfm



      Mv=25d0
      Gv=1d0
      ivec=1
      pollep(1)=1
      pollep(2)=-1
      swapxz=.true.

      do j=1,Nlegs
         pol(j)=+1
         flav(j)=0
      enddo

c index refers to order in amplitude; value to particle id.
c we calculate m(O(1),O(2),...) where O(i)=m means i-th particle in ordered amplitude has momenum kmom(m,:), flavor flav(m) and polarization pol(m)
      Order(1)=1
      Order(2)=2
      Order(3)=3
      Order(4)=4
      Order(5)=5
      Order(6)=6
      Order(7)=7
      Order(8)=8
      Order(9)=9

c the particle associated with momenum kmom(i,:) has flavor flav(i) and polarization pol(i). momentum 1 and 2 are the incoming particles.
c flavor follows PDG numbering scheme convention, polarization is +1,-1 (self expanatory) or 0 (means external spin 1 is remplaced by its momentum, for gauge check)
      flav(1)=2
      flav(2)=-2
      flav(3)=0
      flav(4)=1
      flav(5)=1
      flav(6)=-1
      flav(7)=0
      flav(8)=0
      flav(9)=0

      pol(1)=1
      pol(2)=-1
      pol(3)=-1
      pol(4)=1
      pol(5)=1
      pol(6)=-1
      pol(7)=1
      pol(8)=-1
      pol(9)=1

      lw=0
 
      p0(1)=+71.5344542606618d0
      p0(2)=0d0
      p0(3)=0d0
      p0(4)=-71.5344542606618d0

      pl(1)=-71.5344542606618d0
      pl(2)=0d0
      pl(3)=0d0
      pl(4)=-71.5344542606618d0
      ET=-p0(4)-pl(4)

      do j=1,Nlegs-3
         xm(j)=mass(flav(j+2))
      enddo
      Mr=Mv+rn()*3d0*Gv
      kmom(1,:)=p0(:)
      kmom(2,:)=pl(:)
 20   continue
      xm(nrambo)=Mr
      if (ivec.eq.1) then
         if (nrambo.gt.1) then
            call RAMBO(nrambo,ET,XM,P,PSWT,LW)
            pw(:)=p(:,nrambo)
         else
            pw(:)=-p0(:)-pl(:)
            p(:,1)=pw(:)
            Mr=sqrt(dotpr(pw,pw))
         endif
         do j=1,nrambo
            kmom(j+2,:)=p(:,j)
         enddo
         write(*,*) 'partons+ V momenta'
         do j=1,nrambo+2
            write(*,*) j,kmom(j,:),
     .        sqrt(abs(kmom(j,4)**2-kmom(j,3)**2
     .                -kmom(j,2)**2-kmom(j,1)**2))
         enddo
         xm(1)=0d0
         xm(2)=0d0
         call RAMBOO(2,Mr,pw,xm,p,pswt,lw)
         do j=1,2
            klep(j,:)=p(:,j)
         enddo
         write(*,*) 'V+decay lepton momenta'
         write(*,*) 1,pw(:),sqrt(pw(4)**2-pw(1)**2-pw(2)**2-pw(3)**2)
         write(*,*) 2,klep(1,:),
     .        klep(1,4)**2-klep(1,1)**2-klep(1,2)**2-klep(1,3)**2
         write(*,*) 3,klep(2,:),
     .        klep(2,4)**2-klep(2,1)**2-klep(2,2)**2-klep(2,3)**2
      else
         call RAMBO(nrambo,ET,XM,P,PSWT,LW)
         kmom(2,:)=p0(:)
         kmom(3,:)=pl(:)
         do nu=1,4
            kmom(1,nu)=p(nu,1)
            do j=2,nrambo
               kmom(j+2,nu)=p(nu,j)
            enddo
         enddo
      endif

c---identify momenta for mcfm
      do j=1,nrambo+1
      pmcfm(j,:)=kmom(j,:)
!      write(6,*) j,pmcfm(j,:)
      enddo
      pmcfm(nrambo+2,:)=klep(1,:)
      pmcfm(nrambo+3,:)=klep(2,:)
!      write(6,*) pmcfm
!      call anal(pmcfm,alpp,alpm,almp,almm)

      call bgV(Order,Kmom,Klep,pol,pollep,flav,Nlegs,ampl)

      if (Nlegs.eq.3) then
         write(*,*) 'amplitude recursive: ',ampl,cdabs(ampl)
         call spinorz(Nlegs+1,pmcfm,za,zb)
         anly=2d0*za(1,3)*zb(2,4)
         write(*,*) 'amplitude analytic: ',anly,cdabs(anly)
         write(*,*) "ratio's :",ampl/anly, cdabs(ampl)/cdabs(anly)

      endif

      if (Nlegs.eq.4) then
         call anal3(pmcfm,ap,am)
         am=sqrt(8d0)*am
         ap=sqrt(8d0)*ap
         write(*,*) '-----------------------------------------------'
         write(*,*)
         write(*,*) "sum:"
         write(*,*) "recu",ampl,cdabs(ampl)
         write(*,*) "anly",ap,cdabs(ap)
         write(*,*) "rat ",cdabs(ampl)/cdabs(ap)
         write(*,*)
         write(*,*) '-----------------------------------------------'
      endif

      if (Nlegs.eq.5) then
         call anal4(pmcfm,app,apm,amp,amm)
         write(*,*) '-----------------------------------------------'
         write(*,*)
         write(*,*) "sum:"
         write(*,*) "recu",ampl,cdabs(ampl)
         write(*,*) "anly",app,cdabs(app)
         write(*,*) "rat ",cdabs(ampl)/cdabs(app)
         write(*,*)
         write(*,*) '-----------------------------------------------'
      endif

      if (Nlegs.ge.6) then
         write(*,*) '-----------------------------------------------'
         write(*,*)
         write(*,*) "recu",ampl,cdabs(ampl)
         write(*,*)
         write(*,*) '-----------------------------------------------'
      endif
      pause
      go to 20
      stop
      end


