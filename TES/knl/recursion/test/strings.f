      double complex function string1(Ub,p1,V)
      implicit none
      double complex Ub(4),p1(4),tmp1(4),V(4),sprod   
      call Ubkslash(Ub,p1,tmp1)
      string1=sprod(tmp1,V)
      return
      end

      double complex function string2(Ub,p1,p2,V)
      implicit none
      double complex Ub(4),p1(4),p2(4),tmp1(4),tmp2(4),V(4),sprod 
      call Ubkslash(Ub,p1,tmp1)
      call Ubkslash(tmp1,p2,tmp2)
      string2=sprod(tmp2,V)
      return
      end

      double complex function string3(Ub,p1,p2,p3,V)
      implicit none
      double complex Ub(4),p1(4),p2(4),p3(4),tmp1(4),tmp2(4),V(4),sprod 
      call Ubkslash(Ub,p1,tmp1)
      call Ubkslash(tmp1,p2,tmp2)
      call Ubkslash(tmp2,p3,tmp1)
      string3=sprod(tmp1,V)
      return
      end
