      subroutine BG(kmom,pol,Nlegs,weight)
      implicit none
      include 'constants.f'
      integer Nm1,Nlegs,i,l,k,n,m,h
      double precision P(4),kmom(Nlegs,4),dotpr,weight,pt
      double complex Jlk(Nlegs,Nlegs,4),K1(4),K2(4),Ktemp(4),
     & J(4),J0(4),J1(4),J2(4),J3(4),Jtemp(4),cdotpr
      integer pol(Nlegs)

      Nm1=Nlegs-1

      Jlk(:,:,:)=czip

C---setup polarization vectors for Nm1 particles
      do k=2,Nlegs
      P(:)=kmom(k,:)
      h=pol(k) 
      call helicity(P,h,J)
C---initialize diagonal elements
      Ktemp(:)=dcmplx(P(:))
      write(6,*) 'k,cdotpr(J,Ktemp)',k,cdotpr(J,Ktemp)
      Jlk(k-1,k-1,:)=J(:)
      enddo
      pause
C-----create cumulative sums of momenta
      pt=4d0*dotpr(kmom(1,:),kmom(Nlegs,:))**4
      do i=1,nlegs-1
         pt=pt/dotpr(kmom(i,:),kmom(i+1,:))
      enddo
      pt=pt/dotpr(kmom(Nlegs,:),kmom(1,:))

      P(:)=kmom(1,:)
      do i=1,Nm1
      P(:)=P(:)+kmom(1+i,:)
      kmom(1+i,:)=P(:)      
      enddo

      write(6,*) 'Nm1',Nm1
      do l=1,Nm1-1
        do k=1,Nm1-l
            J(:)=0d0
            do n=k,k+l-1
              K1(:)=dcmplx(kmom(n+1,:)-kmom(k,:))
              K2(:)=dcmplx(kmom(k+l+1,:)-kmom(n+1,:))
              J1(:)=Jlk(k,n,:)
              J2(:)=Jlk(n+1,k+l,:)
          J(:)=J(:)
     &        +2d0*cdotpr(J1,K2)*J2(:)
     &        -2d0*cdotpr(J2,K1)*J1(:)
     &        +cdotpr(J1,J2)*(K1(:)-K2(:))
            enddo  ! endloop n

      do n=k,k+l-2
        do m=n+1,k+l-1
          J1(:)=Jlk(k,n,:)
          J2(:)=Jlk(n+1,m,:)
          J3(:)=Jlk(m+1,k+l,:);
          J(:)=J(:)
     &    +2d0*cdotpr(J1,J3)*J2(:)
     &        -cdotpr(J1,J2)*J3(:)
     &        -cdotpr(J2,J3)*J1(:)
      enddo ! endloop m
      enddo ! endloop n

C---Divide by propagator for all except the last
        if (l < Nm1-1) then
        P(:)=kmom(k+l+1,:)-kmom(k,:)
        J(:)=J(:)/dotpr(P,P)
      endif
        Jlk(k,k+l,:)=J(:)
C---check
        Jtemp(:)=J(:)
        Ktemp(:)=dcmplx(kmom(k+l+1,:)-kmom(k,:))
      write(6,*) 'l,k,k+l,dotpr(Jtemp,ktemp)',
     &  l,k,k+l,cdotpr(Jtemp,ktemp)
      enddo  ! endloop k
      enddo  ! endloop l
      P(:)=kmom(1,:)
      call helicity(P,-1,J0)
      J(:)=Jlk(1,Nm1,:) 
      weight=cdabs(cdotpr(J0,J))**2
      write(6,*) 'weight',weight,pt,weight/pt
      return
      end

