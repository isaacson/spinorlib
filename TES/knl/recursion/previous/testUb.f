      subroutine testUb(k,nflav)
      implicit none
      include 'constants.f'
      include 'fermionmass.f'
      include 'swapxz.f'
      double complex Ubp(4),Ubm(4),Updag(4),Umdag(4),n(4),kslash(4,4),
     & Up(4),Um(4),f(4)
      double precision k(4),E,kx,ky,kz,m,mmatrix(4,4)
      integer nflav,i,j
      m=mass(nflav)
     
      mmatrix(:,:)=0d0
      do j=1,4
      mmatrix(j,j)=m
      enddo
      write(6,*) 'm',m,nflav

      n(:)=czip 
      n(4)=cone 
      write(6,*) 'nflav',nflav
      call wavefunction(k,nflav,+1,Ubp)
      call wavefunction(k,nflav,-1,Ubm)
      call Ubkslash(Ubp,n,Updag)
      call Ubkslash(Ubm,n,Umdag)
      Up(:)=dconjg(Updag(:))
      Um(:)=dconjg(Umdag(:))

C----Dirac
      write(6,*) 'dirac test'
      call Ubkslash(Ubp,dcmplx(k),f)
      do j=1,4
      write(6,*) f(j)-m*Ubp(j) 
      enddo

      call Ubkslash(Ubm,dcmplx(k),f)
      do j=1,4
      write(6,*) f(j)-m*Ubm(j) 
      enddo

      call kslashU(dcmplx(k),Up,f)
      do j=1,4
      write(6,*) f(j)-m*Up(j) 
      enddo

      call kslashU(dcmplx(k),Um,f)
      do j=1,4
      write(6,*) f(j)-m*Um(j) 
      enddo

      if (swapxz) then
C----create kslash after performing the swap (x<->z),(y->-y)
      E=k(4)
      kx=+k(3)
      ky=-k(2)
      kz=+k(1)
      else
      E=k(4)
      kx=+k(1)
      ky=+k(2)
      kz=+k(3)
      endif

      kslash(1,1)=czip
      kslash(1,2)=czip
      kslash(1,3)=E+kz
      kslash(1,4)=kx-im*ky

      kslash(2,1)=czip
      kslash(2,2)=czip
      kslash(2,3)=kx+im*ky
      kslash(2,4)=E-kz

      kslash(3,1)=E-kz
      kslash(3,2)=-kx+im*ky
      kslash(3,3)=czip
      kslash(3,4)=czip

      kslash(4,1)=-kx-im*ky
      kslash(4,2)=E+kz
      kslash(4,3)=czip
      kslash(4,4)=czip

      do i=1,4
      do j=1,4
      write(6,*) Up(i)*Ubp(j)+Um(i)*Ubm(j)-(kslash(i,j)+mmatrix(i,j))
      enddo
      enddo
      pause 'End of testUb'
      return
      end
