      implicit none
      include 'swapxz.f'
      include 'fermionmass.f'
      include 'hvqvectors.f'
      integer nu,j,nrambo,lw,Nlegs,h1,h2,h3,h4
      parameter(Nlegs=4)
      parameter(nrambo=Nlegs-2)
      double complex AB,BA,amp,XAB(2,2,2,2),XBA(2,2,2,2)
      double precision wtQggQ,wtBG,p0(4),pl(4),
     & kmom(Nlegs,4),xm(nrambo),p(4,nrambo),wtanly,pswt,Et
      integer pol(Nlegs),flav(Nlegs)

      swapxz=.true.

      do j=1,Nlegs
         pol(j)=+1
         flav(j)=0
      enddo
      flav(1)=6
      flav(4)=-6

      pol(1)=-1  
      pol(2)=-1
      pol(3)=-1
      pol(4)=-1
!      pol(Nlegs)=1
      lw=0
 
      p0(1)=+71.5344542606618d0
      p0(2)=0d0
      p0(3)=0d0
      p0(4)=-71.5344542606618d0

      pl(1)=-71.5344542606618d0
      pl(2)=0d0
      pl(3)=0d0
      pl(4)=-71.5344542606618d0

      ET=-p0(4)-pl(4)
      xm(:)=0d0
      xm(1)=mass(flav(1))
      xm(Nlegs-2)=mass(flav(4))
      write(*,*) xm(1),xm(2)
 20   continue
      call RAMBO(nrambo,ET,XM,P,PSWT,LW)
      kmom(2,:)=p0(:)
      kmom(3,:)=pl(:)
      do nu=1,4
         kmom(1,nu)=p(nu,1)
         do j=2,nrambo
            kmom(j+2,nu)=p(nu,j)
         enddo
      enddo
C---  set auxiliary vectors for definition of massive spinors 
C---  passed in common hvqvectors
      etaQ(:)=dcmplx(kmom(2,:))
      etaQb(:)=dcmplx(kmom(2,:))

      do j=1,4
         write(*,*) j,kmom(j,:)
 1    enddo

      call anly(Kmom,pol,flav,Nlegs,wtanly)

      wtQggQ=0d0
      do h1=1,2
      do h2=1,2
      do h3=1,2
      do h4=1,2
      pol(1)=2*h1-3
      pol(2)=2*h2-3
      pol(3)=2*h3-3
      pol(4)=2*h4-3
      call QggQbar(Kmom,pol,flav,Nlegs,AB,BA)
      wtQggQ=wtQggQ+
     & cdabs(AB)**2+cdabs(BA)**2-1d0/9d0*cdabs(AB+BA)**2
      call BG(Kmom,pol,flav,Nlegs,amp)
      XAB(h1,h2,h3,h4)=amp
      enddo
      enddo
      enddo
      enddo


C---exchange 2 and 3
      p0(:)=Kmom(2,:)
      Kmom(2,:)=Kmom(3,:)
      Kmom(3,:)=p0(:)

      do h1=1,2
      do h2=1,2
      do h3=1,2
      do h4=1,2
      pol(1)=2*h1-3
      pol(2)=2*h2-3
      pol(3)=2*h3-3
      pol(4)=2*h4-3
      call BG(Kmom,pol,flav,Nlegs,amp)
      XBA(h1,h3,h2,h4)=amp
      enddo
      enddo
      enddo
      enddo

      wtBG=0d0
      do h1=1,2
      do h2=1,2
      do h3=1,2
      do h4=1,2
      wtBG=wtBG
     & +cdabs(XAB(h1,h2,h3,h4))**2
     & +cdabs(XBA(h1,h2,h3,h4))**2
     & -1d0/9d0*cdabs(XAB(h1,h2,h3,h4)+XBA(h1,h2,h3,h4))**2
      enddo
      enddo
      enddo
      enddo


      write(*,*) "recur, anly, (2*wtanly/WtBG)",
     & wtBG/2d0,wtanly,2d0*wtanly/wtBG
      write(*,*) "QggQ, anly, wtanly/(2*wtQggQ)",
     & 2d0*wtQggQ,wtanly,wtanly/(2d0*wtQggQ)
      pause
      go to 20
      stop
      end

