      subroutine wavefunction(p,nflav,hi,f)
c-----setup external wavefunction
      implicit none
      include 'fermionmass.f'
      include 'hvqvectors.f'
      integer hi,nflav
      double precision p(4),m
      double complex q(4)
      double complex f(4)
      if (nflav .eq. 0) then
      call helicity(p,hi,f)
      elseif (nflav .gt. 0) then
      m=mass(nflav)
      q(:)=dcmplx(p(:))
c      call ubarspinor(q,m,-hi,f)
      call ubarmass(q,etaQ,m,-hi,f)
      elseif (nflav .lt. 0) then
      m=mass(nflav)
      q(:)=dcmplx(p(:))
C      call vspinor(q,m,hi,f)
      call vmass(q,etaQb,m,hi,f)
      endif
      return
      end


