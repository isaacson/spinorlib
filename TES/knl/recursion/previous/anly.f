      subroutine anly(kmom,pol,flav,Nlegs,weight)
      implicit none
      include 'fermionmass.f'
      integer Nlegs,i,j,qp,mhv,l1,l2,l3,f
      double precision kmom(Nlegs,4),k1(4),k2(4),k3(4),dotpr,weight
      double precision s,t1,t2,ro,m,m2
      integer pol(Nlegs),flav(Nlegs)
      logical Fmass

      qp=0
      mhv=0
      Fmass=.false.
      write(*,*) pol
      do i=1,Nlegs
         qp=qp+min(abs(flav(i)),1)
         mhv=mhv+pol(i)
         if (abs(flav(i)).eq.6) Fmass=.true.
      enddo
      weight=0d0
      qp=qp/2
      if (.true.) then
         write(*,*) 'mass,qp,Nlegs = ',mass(6),qp,Nlegs
         weight=0
         if (Nlegs.eq.4) then
            m=mass(flav(1))
            m2=m**2
            k1(:)=kmom(2,:)
            k2(:)=kmom(3,:)
            k3(:)=kmom(4,:)
            s=2d0*dotpr(k1,k2)
            t1=-2d0*dotpr(k1,k3)/s
            t2=-2d0*dotpr(k2,k3)/s
            ro=4d0*m2/s
            if (qp.eq.1) then
               weight=4d0/9d0*(8d0/t1/t2-18d0)
c               weight=4d0*(1d0/t1/t2-2d0)
               weight=weight*(t1**2+t2**2+ro-ro**2/(4d0*t1*t2))
!               weight=t1*t2*(t1**2+t2**2)*(1d0/t1**2+1d0/t2**2)
               write(*,*) 'anly:mass anly',m
               write(*,*) 'anly:s,t1,t2',s,t1,t2
               write(*,*) 'anly:weight',weight
            endif
            if (qp.eq.2) then
               weight=4d0/9d0*(t1**2+t2**2+ro/2)
            endif
         endif
      else
         if (qp.eq.0) then
            if (abs(mhv).eq.Nlegs) weight=0d0
            if (abs(mhv).eq.Nlegs-2) weight=0d0
            if (abs(mhv).eq.Nlegs-4) then
               f=1
               if (mhv.gt.0) f=-f
               do j=1,Nlegs
                  if (pol(j).eq.f) then
                     l2=l1
                     l1=j
                  endif
               enddo
               k1(:)=kmom(l1,:)
               k2(:)=kmom(l2,:)
               weight=4d0*dotpr(k1,k2)**4
               k1(:)=kmom(1,:)
               k2(:)=kmom(Nlegs,:)
               weight=weight/dotpr(k2,k1)
               do i=1,Nlegs-1
                  k1(:)=kmom(i,:)
                  k2(:)=kmom(i+1,:)
                  weight=weight/dotpr(k2,k1)
               enddo
            endif
         endif
         if (qp.eq.1) then
            write(*,*) mhv,Nlegs 
            if (abs(mhv).eq.Nlegs-2) weight=0d0
            if (abs(mhv).eq.Nlegs-4) then
               s=1
               if (mhv.gt.0) f=-f
               do j=2,Nlegs-1
                  if (pol(j).eq.f) then
                     if (flav(j).eq.0) then
                        l1=j
                     else
                        l2=j
                     endif
                  else
                     if (flav(j).ne.0) l3=j
                  endif
               enddo
               k1(:)=kmom(l1,:)
               k2(:)=kmom(l3,:)
               k3(:)=kmom(l2,:)
               weight=4d0*dotpr(k1,k2)**3*dotpr(k1,k3)
               k1(:)=kmom(1,:)
               k2(:)=kmom(Nlegs,:)
               weight=weight/dotpr(k2,k1)
               do i=1,Nlegs-1
                  k1(:)=kmom(i,:)
                  k2(:)=kmom(i+1,:)
                  weight=weight/dotpr(k2,k1)
               enddo
            endif
         endif
      endif
      return
      end
