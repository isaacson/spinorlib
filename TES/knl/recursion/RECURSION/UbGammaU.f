      subroutine UbGammaU(Ub,U,f) 
      implicit none
      include 'constants.f'
c-----Calculation of Ub*Gamma(1,2,3,4)*U
C-----Weyl representation
C     Energy component in MCFM notation = k(4)
      include 'swapxz.f'
      double complex Ub(4),U(4),f(4)
      double precision R,L
      parameter(R=1d0,L=1d0)
      logical,save::first
      data first/.true./
     
      if (first) then
      write(6,*) 'UbGammaU:swapxz=',swapxz
      first=.false.
      endif

      if (swapxz) then
C----perform the swap (x<->z),(y->-y)
      f(4)=+L*(Ub(2)*U(4)+Ub(1)*U(3))+R*(Ub(4)*U(2)+Ub(3)*U(1))
      f(3)=-L*(Ub(1)*U(4)+Ub(2)*U(3))+R*(Ub(3)*U(2)+Ub(4)*U(1))
      f(2)=-L*(Ub(1)*U(4)-Ub(2)*U(3))+R*(Ub(3)*U(2)-Ub(4)*U(1))
      f(1)=+L*(Ub(2)*U(4)-Ub(1)*U(3))-R*(Ub(4)*U(2)-Ub(3)*U(1))
      else
      f(4)=+L*(Ub(2)*U(4)+Ub(1)*U(3))+R*(Ub(4)*U(2)+Ub(3)*U(1))
      f(1)=-L*(Ub(1)*U(4)+Ub(2)*U(3))+R*(Ub(3)*U(2)+Ub(4)*U(1))
      f(2)=+L*(Ub(1)*U(4)-Ub(2)*U(3))-R*(Ub(3)*U(2)-Ub(4)*U(1))
      f(3)=+L*(Ub(2)*U(4)-Ub(1)*U(3))-R*(Ub(4)*U(2)-Ub(3)*U(1))


      endif
      f(2)=f(2)*im
      return
      end



