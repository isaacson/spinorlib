#include <math.h>
#include <stdlib.h>
#include "momentum.cc"
#include "random.h"
#include "/home/giele/lhapdf/include/LHAPDF/LHAPDF.h"

using namespace std;
using namespace LHAPDF;

float Z;

void init(int N)
{
  int Gamma=1,Nc=3;
  float Cgev2pb=0.389379304e9;
  float pi=3.1415926536;
  for (int n=1;n<N-2;n++) Gamma*=n;
  Z=Cgev2pb*pow(4.0*pi,3)*pow(Nc/8.0/pi,N-2)*(Nc^2-1)*(N-1)*(N-3)/4.0/Gamma/Gamma;
  Z*=4.0*pow(2.0,N)/pow(2.0*(Nc*Nc-1.0),2);
  //
  // set-up alpha_S and PDF's
  //
  setVerbosity(SILENT);
  const int SUBSET=0;
  initPDFSet("CTEQ6L1", LHPDF,SUBSET);
}

float xgluon(float x,float Q) { return (float) xfx(x,Q,0)/x; }

float alfas(float Q) { return (float) alphasPDF(Q);}

void ramboo(int N,momentum<float> *K)
{
  for (int i=2;i<N;i++) {
    float Eb=-(K[0].p[0]+K[1].p[0]);
    float pz=-(K[0].p[1]+K[1].p[1]);
    float f=K[i].p[0];
    K[i].p[0]=(Eb*f+pz*K[i].p[1]);
    f+=K[i].p[0];f*=pz/(1.0+Eb);
    K[i].p[1]+=f;
  }
}

bool cutter(int Nparticle,float Ecm,momentum<float> *K,float Ptmin,float etamax,float Rmin) {
  bool pass=true;
  momentum<float> S=K[0]+K[1];
  for (int i=2;i<Nparticle;i++)
  {
    float pe=K[i].p[0],px=K[i].p[2],py=K[i].p[3],pz=K[i].p[1];
    float Pt=sqrtf(px*px+py*py);
    float eta=0.5*logf(fabs((pe+pz)/(pe-pz)));
    if (Pt*Ecm<Ptmin) pass=false;
    if (fabs(eta)>etamax) pass=false;
    S+=K[i];
  }
  if ((S.p[0]*S.p[0]+S.p[1]*S.p[1]+S.p[2]*S.p[2]+S.p[3]*S.p[3])>1e-6) pass=false;
  if (pass) {
    for (int i=2;i<Nparticle-1;i++)
      {
	for (int j=i+1;j<Nparticle;j++)
	  {
	    float pe=K[i].p[0],px=K[i].p[2],py=K[i].p[3],pz=K[i].p[1];
	    float qe=K[j].p[0],qx=K[j].p[2],qy=K[j].p[3],qz=K[j].p[1];
	    float Deta=0.5*logf(fabs((pe+pz)*(qe-qz)/(pe-pz)/(qe+qz)));
	    float d=(px*qx+py*qy)/(sqrtf(px*px+py*py)*sqrtf(qx*qx+qy*qy));
	    d=fmax(d,-1.0);d=fmin(d,1.0);
	    float Dphi=acos(d);
	    float R=sqrtf(Deta*Deta+Dphi*Dphi);
	    if (R<Rmin) pass=false;
	  }
      }
  }
  return pass;
}

float scale(int Nparticle,float Ecm,momentum<float> *K)
{
  /*
  float mu=0.0;
  for (int i=2;i<Nparticle;i++)
    {
      float px=K[i].p[2],py=K[i].p[3];
      mu+=sqrtf(px*px+py*py);
    }
  return mu*Ecm;
  */
  return 91.188f;
}

void orthogonal(momentum<float> &J, const momentum<float> P) {
  momentum<float> J1;
  float pi=3.1415926536;
  float z=uniform(-1.0f,1.0f);
  float s=sqrtf(fabs(1.0f-z*z));
  float phi=uniform(0.0f,2.0*pi);
  float x,y;
  sincosf(phi,&x,&y);
  x*=s;
  y*=s;
  J1.p[0]=0.0f;
  J1.p[1]=y*P.p[3]-z*P.p[2];
  J1.p[2]=z*P.p[1]-x*P.p[3];
  J1.p[3]=x*P.p[2]-y*P.p[1];
  float norm=sqrtf(J1.p[1]*J1.p[1]+J1.p[2]*J1.p[2]+J1.p[3]*J1.p[3]);
  if (norm>0.0f) {
    J1.p[1]/=norm;
    J1.p[2]/=norm;
    J1.p[3]/=norm;
  }
  else J1=momentum_<float>(0.0,0.0,0.0,0.0);
  J=J1;
}

bool event(int Nparticle,float Ecm,float Ptmin,float etamax,float Rmin, float& weight) {
  bool pass=false;
  momentum<float> K[Nparticle];
  momentum<float> R=momentum_<float>(0.0, 0.0, 0.0, 0.0);
  for (int i=2;i<Nparticle;i++) {
    float c=uniform(-1.0f,1.0f);
    float s=sqrtf(fabs(1.0f-c*c));
    float pi=3.1415926536;
    float phi=uniform(0.0f,2.0*pi);
    float x,y;
    sincosf(phi,&x,&y);
    K[i]=-logf(uniform(0.0f,1.0f)*uniform(0.0f,1.0f))*momentum_<float>(1.0f, c, x*s, y*s);
    R+=K[i];
  }
  
  float Rmass=R*R;
  Rmass=sqrt(Rmass);
  R=-R/Rmass;
  float a=1.0f/(1.0f-R.p[0]);
  float x=1.0f/Rmass;
  for (int i=2;i<Nparticle;i++) {
    float bq=R.p[1]*K[i].p[1]+R.p[2]*K[i].p[2]+R.p[3]*K[i].p[3];
    float xq=K[i].p[0]+a*bq;
    K[i]=x*momentum_<float>(-R.p[0]*K[i].p[0]+bq, K[i].p[1]+R.p[1]*xq, 
			     K[i].p[2]+R.p[2]*xq, K[i].p[3]+R.p[3]*xq);
  }

  float eps=pow((Nparticle-2.0)*Ptmin/Ecm,2);
  float r1=uniform();
  float r2=uniform();
  float x1=pow(eps,r1*r2);
  float x2=pow(eps,r2)/x1;
  weight=Z*r2*x1*x2*pow(log(eps),2);
  float Epar=sqrtf(x1*x2);
  K[0]=-(x1/Epar)*momentum_<float>(0.5f, 0.5f, 0.0, 0.0);
  K[1]=-(x2/Epar)*momentum_<float>(0.5f,-0.5f, 0.0, 0.0);
  Epar*=Ecm;
  weight=weight/(Epar*Epar);
  ramboo(Nparticle,K);
  pass=cutter(Nparticle,Epar,K,Ptmin,etamax,Rmin);
  float mu=scale(Nparticle,Epar,K);
  weight*=xgluon(x1,mu)*xgluon(x2,mu);
  float as=alfas(mu);
  as=0.130f;
  weight*=pow(as,Nparticle-2);
  int r=1+uniform()*(Nparticle-1);
  momentum<float> Kt=K[r];
  K[r]=K[1];
  K[1]=Kt;

  int N=Nparticle-1;
  momentum<float> Jlk[N+1][N+1];
  for (int k=1;k<=N;k++){
    orthogonal(Jlk[k][k],K[k]); }
  momentum<float> P=K[0];
  for(int i=1; i<=N; i++) {
    P+=K[i];
    K[i]=P;
  }
  for(int l=1; l<N; l++) {
    for (int k=1;k<=N-l;k++){
      momentum<float> J=momentum_<float>(0.0,0.0,0.0,0.0);
      for (int n=k; n<k+l; n++) {
        // 2 J1.K2 J2 - 2 J2.K1 J1 + J1.J2 (K1-K2)
        momentum<float> K1=K[n]-K[k-1];
        momentum<float> K2=K[k+l]-K[n];
        momentum<float> J1=Jlk[k][n], J2=Jlk[n+1][k+l];
        J+=(2.0f*(J1*K2))*J2-(2.0f*(J2*K1))*J1+(J1*J2)*(K1-K2);
      }
      for (int n=k; n<k+l-1; n++) {
	for (int m=n+1;m<k+l;m++) {
	  momentum<float> J1=Jlk[k][n],J2=Jlk[n+1][m],J3=Jlk[m+1][k+l];
	  J+=(2.0f*(J1*J3))*J2-(J1*J2)*J3-(J2*J3)*J1;
	}
      }
      if (l<N-1) {
	momentum<float> P=K[k+l]-K[k-1];
	J/=P*P;
      }
      Jlk[k][k+l]=J;
    }
  }
  momentum<float> J;
  orthogonal(J,K[0]);
  weight*=pow(J*Jlk[1][N],2);
  
  return pass;
}


