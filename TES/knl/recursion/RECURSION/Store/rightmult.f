      subroutine rightmult(v,p,u)
C     rightmultiply by pslash in dirac notation.
      double complex v(4),p(4),u(4),slashp(4,4),im
      parameter(im=(0d0,1d0))
      integer j
c            [     p1         0          - p4      %i p3 - p2 ]
c            [                                           ]
c            [     0          p1      - %i p3 - p2      p4     ]
c            [                                           ]
c            [     p4      p2 - %i p3      - p1         0      ]
c            [                                           ]
c            [ %i p3 + p2     - p4         0          - p1    ]

      slashp(1,1)=p(1)
      slashp(1,2)=dcmplx(0d0,0d0)
      slashp(1,3)=-p(4)
      slashp(1,4)=im*p(3)-p(2)

      slashp(2,1)=dcmplx(0d0,0d0)
      slashp(2,2)=p(1)
      slashp(2,3)=-im*p(3)-p(2)
      slashp(2,4)=p(4)

      slashp(3,1)=p(4)
      slashp(3,2)=-im*p(3)+p(2)
      slashp(3,3)=-p(1)
      slashp(3,4)=dcmplx(0d0,0d0)

      slashp(4,1)=im*p(3)+p(2)
      slashp(4,2)=-p(4)
      slashp(4,3)=dcmplx(0d0,0d0)
      slashp(4,4)=-p(1)

      do j=1,4
      u(j)=
     & +v(1)*slashp(1,j)
     & +v(2)*slashp(2,j)
     & +v(3)*slashp(3,j)
     & +v(4)*slashp(4,j)
      enddo
      return 
      end
