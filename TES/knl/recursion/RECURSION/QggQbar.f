      subroutine QggQbar(Kmom,pol,flav,Nlegs,AB,BA)
      implicit none
      double precision Kmom(nlegs,4),p1(4),p2(4),p3(4),p4(4)
      double complex zp1(4),zp2(4),zp3(4),Ub(4),V(4),e2(4),e3(4),
     & AB,BA
      integer Nlegs,pol(Nlegs),flav(Nlegs)
      p1(:)=Kmom(1,:)
      p2(:)=Kmom(2,:)
      p3(:)=Kmom(3,:)
      p4(:)=Kmom(4,:)
      call wavefunction(p1,flav(1),pol(1),Ub)     
      call wavefunction(p2,flav(2),pol(2),e2)     
      call wavefunction(p3,flav(3),pol(3),e3)     
      call wavefunction(p4,flav(4),pol(4),V)     
      zp1(:)=dcmplx(p1(:))
      zp2(:)=dcmplx(p2(:))
      zp3(:)=dcmplx(p3(:))
      call amps(zp1,zp2,zp3,Ub,e2,e3,V,AB,BA)
      return
      end
