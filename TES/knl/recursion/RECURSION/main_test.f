      implicit none
      include 'constants.f'
      include 'swapxz.f'
      include 'fermionmass.f'
      include 'hvqvectors.f'
      include 'zprods_com.f'
      integer Nlegs,j,lw,Nevent
      parameter(Nlegs=6)
      double precision ET,pswt
      double precision p0(4),pl(4),kmom(Nlegs,4),pmcfm(mxpart,4),
     .     p(4,Nlegs-2),xm(Nlegs-2)
      double complex sprod,anly,num1,num2
      double complex J1(4),J2(4),Jt(4),p3(4),p4(4)
      common/mcfm/pmcfm

      Nevent=3
      lw=0
      swapxz=.true.
      p0(1)=+71.5344542606618d0
      p0(2)=0d0
      p0(3)=0d0
      p0(4)=-71.5344542606618d0

      pl(1)=-71.5344542606618d0
      pl(2)=0d0
      pl(3)=0d0
      pl(4)=-71.5344542606618d0
      ET=-p0(4)-pl(4)

      xm(:)=0d0
      kmom(1,:)=p0(:)
      kmom(2,:)=pl(:)
 20   continue
      call RAMBO(Nlegs-2,ET,XM,P,PSWT,LW)
      do j=1,Nlegs-2
         kmom(j+2,:)=p(:,j)
      enddo
      do j=1,Nlegs
         write(*,*) j,kmom(j,:),
     .        sqrt(abs(kmom(j,4)**2-kmom(j,3)**2
     .                -kmom(j,2)**2-kmom(j,1)**2))
      enddo
c---identify momenta for mcfm
      do j=1,Nlegs
         pmcfm(j,:)=kmom(j,:)
         write(6,*) pmcfm(j,:)
      enddo
      etaQ(:)=dcmplx(kmom(2,:))
      etaQb(:)=dcmplx(kmom(2,:))

      call spinorz(Nlegs,pmcfm,za,zb)

      call wavefunction(Kmom(3,:),1,-1,J1)
      call wavefunction(Kmom(6,:),-1,-1,J2)

      anly=za(3,6)
      num1=sprod(J1,J2)
      write(*,*)
      write(*,*) anly,num1,cdabs(anly)/cdabs(num1)

      p3(:)=dcmplx(kmom(4,:))
      call wavefunction(Kmom(6,:),-1,1,J2)
!      call kslashURL(p3,J2,Jt,1d0,1d0)
!      num1=sprod(J1,Jt)
      call UbkslashRL(J1,p3,Jt,1d0,1d0)
      num1=sprod(J2,Jt)
      anly=za(3,4)*zb(4,6)
      write(*,*)
      write(*,*) anly,num1,cdabs(anly)/cdabs(num1)
      p4(:)=dcmplx(kmom(5,:))
      call UbkslashRL(Jt,p4,Jt,1d0,1d0)
      call wavefunction(Kmom(6,:),-1,-1,J2)
      write(*,*) Jt
      num1=sprod(J2,Jt)
      anly=za(3,4)*zb(4,5)*za(5,6)
      write(*,*) anly,num1,cdabs(anly)/cdabs(num1)


      pause
      go to 20
      stop
      end

