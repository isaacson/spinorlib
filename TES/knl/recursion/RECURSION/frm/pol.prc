#procedure pol
#+ 

Id,e1p.v1?=bra(p1,+1)*g_(j,v1)*ket(b1,+1)*rt2/2*iza(b1,p1);
Id,e1m.v1?=bra(p1,-1)*g_(j,v1)*ket(b1,-1)*rt2/2*izb(p1,b1);

Id,e2p.v1?=bra(p2,+1)*g_(j,v1)*ket(b2,+1)*rt2/2*iza(b2,p2);
Id,e2m.v1?=bra(p2,-1)*g_(j,v1)*ket(b2,-1)*rt2/2*izb(p2,b2);

Id,e3p.v1?=bra(p3,+1)*g_(j,v1)*ket(b3,+1)*rt2/2*iza(b3,p3);
Id,e3m.v1?=bra(p3,-1)*g_(j,v1)*ket(b3,-1)*rt2/2*izb(p3,b3);

Id,e4p.v1?=bra(p4,+1)*g_(j,v1)*ket(b4,+1)*rt2/2*iza(b4,p4);
Id,e4m.v1?=bra(p4,-1)*g_(j,v1)*ket(b4,-1)*rt2/2*izb(p4,b4);

Id,e5p.v1?=bra(p5,+1)*g_(j,v1)*ket(b5,+1)*rt2/2*iza(b5,p5);
Id,e5m.v1?=bra(p5,-1)*g_(j,v1)*ket(b5,-1)*rt2/2*izb(p5,b5);

Id,g_(j?,e1p)=rt2*iza(b1,p1)*(+ket(p1,-1)*bra(b1,-1)+ket(b1,+1)*bra(p1,+1));
Id,g_(j?,e1m)=rt2*izb(p1,b1)*(+ket(p1,+1)*bra(b1,+1)+ket(b1,-1)*bra(p1,-1));

Id,g_(j?,e2p)=rt2*iza(b2,p2)*(+ket(p2,-1)*bra(b2,-1)+ket(b2,+1)*bra(p2,+1));
Id,g_(j?,e2m)=rt2*izb(p2,b2)*(+ket(p2,+1)*bra(b2,+1)+ket(b2,-1)*bra(p2,-1));

Id,g_(j?,e3p)=rt2*iza(b3,p3)*(+ket(p3,-1)*bra(b3,-1)+ket(b3,+1)*bra(p3,+1));
Id,g_(j?,e3m)=rt2*izb(p3,b3)*(+ket(p3,+1)*bra(b3,+1)+ket(b3,-1)*bra(p3,-1));

Id,g_(j?,e4p)=rt2*iza(b4,p4)*(+ket(p4,-1)*bra(b4,-1)+ket(b4,+1)*bra(p4,+1));
Id,g_(j?,e4m)=rt2*izb(p4,b4)*(+ket(p4,+1)*bra(b4,+1)+ket(b4,-1)*bra(p4,-1));

Id,g_(j?,e4p)=rt2*iza(b5,p4)*(+ket(p4,-1)*bra(b5,-1)+ket(b5,+1)*bra(p5,+1));
Id,g_(j?,e5m)=rt2*izb(p5,b5)*(+ket(p5,+1)*bra(b5,+1)+ket(b5,-1)*bra(p5,-1));
Id,rt2^2=2;
#endprocedure


