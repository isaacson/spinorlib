V p1,p2,p3,p4,pl,pa,e2,e3;
Autodeclare V p,e,b,v,k;
Autodeclare S a,d,h,s;
Autodeclare I mu,MU;
F bra,ket,prop,V3;
I j,j1,si,ro,ta;
CF iza,izb,za,zb,zab2,zba2,zab2,zba3;
S rt2;
L x1=d1;
L x2=d2;
L sump=(d1+d2)*bra(pl,-1)*g_(j1,si)*ket(pa,-1)/2/rt2;
L summ=(d1+d2)*bra(pl,-1)*g_(j1,si)*ket(pa,-1)/2/rt2;
Id,d1=a1*bra(p1,-1)*g_(j,si)*prop(j,-p23)*g_(j,e2)*ket(p3,-1);
Id,d2=a2*bra(p1,-1)*g_(j,e2)*prop(j,p12)*g_(j,si)*ket(p3,-1);
Id,prop(j,p1?)=g_(j,p1)/p1.p1;
*#call ward
Inexpression sump;
Id,e2=e2p;
Endinexpression;
Inexpression summ;
Id,e2=e2m;
Endinexpression;
#call pol
#call fierz(j,pl,j1,si,pa)
#call zfix
Id,bra(p2,h1?)*g_(j,p12)=bra(p2,h1)*g_(j,p1);
Id,g_(j,p23)*ket(p2,1)=g_(j,p3)*ket(p2,1);
Id,g_(j,p2?{p1,p2,p3})=+ket(p2,-1)*bra(p2,-1)+ket(p2,+1)*bra(p2,+1);
Id,bra(v1?,1)*g_(j,p12)*ket(v2?,1)=zba2(v1,p1,p2,v2);
Id,bra(v1?,1)*g_(j,p23)*ket(v2?,1)=zba2(v1,p2,p3,v2);
#call zfix
#call zcom
Id,p12.p12^-1=1/s12;
Id,p23.p23^-1=1/s23;
Print +s;
Format DoubleFortran;
.sort 
#write <sum1.f>"      double complex function"
#write <sum1.f>"     & sump(p1,p2,p3,pl,pa,b2,a1,a2)"
#write <sum1.f>"      implicit none"
#write <sum1.f>"C---gauge vectors b2 for gluon"
#write <sum1.f>"      integer p1,p2,p3,p4,pl,pa,b2,a1,a2"
#write <sum1.f>"      double precision s12,s23"
#write <sum1.f>"      double complex iza,izb,zba2"
#write <sum1.f>"      include \'constants.f\'"
#write <sum1.f>"      include \'zprods_com.f\'"
#write <sum1.f>"      include \'sprods_com.f\'"
#write <sum1.f>"C*****Statement functions****************************"
#write <sum1.f>"      iza(p1,p2)=cone/za(p1,p2)"
#write <sum1.f>"      izb(p1,p2)=cone/zb(p1,p2)"
#write <sum1.f>"      zba2(p1,p2,p3,p4)=zb(p1,p2)*za(p2,p4)+zb(p1,p3)*za(p3,p4)"
#write <sum1.f>"C***************************************************"
#write <sum1.f>"      s12=s(p1,p2)"
#write <sum1.f>"      s23=s(p2,p3)"
#write <sum1.f>"      sump=%e",sump(sump)
#write <sum1.f>"      return"
#write <sum1.f>"      end"
#write <sum1.f>"      "
#write <sum1.f>"      "
#write <sum1.f>"      double complex function"
#write <sum1.f>"     & summ(p1,p2,p3,pl,pa,b2,a1,a2)"
#write <sum1.f>"      implicit none"
#write <sum1.f>"C---gauge vectors b2,b3 for gluons"
#write <sum1.f>"      integer p1,p2,p3,p4,pl,pa,b2,a1,a2"
#write <sum1.f>"      double precision s3,s12,s23"
#write <sum1.f>"      double complex iza,izb,zba2"
#write <sum1.f>"      include \'constants.f\'"
#write <sum1.f>"      include \'zprods_com.f\'"
#write <sum1.f>"      include \'sprods_com.f\'"
#write <sum1.f>"C*****Statement functions****************************"
#write <sum1.f>"      iza(p1,p2)=cone/za(p1,p2)"
#write <sum1.f>"      izb(p1,p2)=cone/zb(p1,p2)"
#write <sum1.f>"      zba2(p1,p2,p3,p4)=zb(p1,p2)*za(p2,p4)+zb(p1,p3)*za(p3,p4)"
#write <sum1.f>"C***************************************************"
#write <sum1.f>"      s12=s(p1,p2)"
#write <sum1.f>"      s23=s(p2,p3)"
#write <sum1.f>"      summ=%e",summ(summ)
#write <sum1.f>"      return"
#write <sum1.f>"      end"
#write <sum1.f>"      "
#write <sum1.f>"      "
Print +s;
.end
