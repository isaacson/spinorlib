V p1,p2,p3,p4,pl,pa,e2,e3;
Autodeclare V p,e,b,v,k;
Autodeclare S a,d,h,s;
Autodeclare I mu,MU;
F bra,ket,prop,V3;
I j,j1,si,ro,ta;
CF iza,izb,za,zb,zab2,zba2,zab2,zba3;
S rt2;
L x1=d1;
L x2=d2;
L x3=d3;
L x4=d4;
L x5=d5;
L sumpp=(d1+d2+d3+d4+d5)*bra(pl,-1)*g_(j1,si)*ket(pa,-1)/4;
L summp=(d1+d2+d3+d4+d5)*bra(pl,-1)*g_(j1,si)*ket(pa,-1)/4;
L sumpm=(d1+d2+d3+d4+d5)*bra(pl,-1)*g_(j1,si)*ket(pa,-1)/4;
L summm=(d1+d2+d3+d4+d5)*bra(pl,-1)*g_(j1,si)*ket(pa,-1)/4;
Id,d1=a1*bra(p1,-1)*g_(j,si)*prop(j,-p234)*g_(j,e2)*prop(j,-p34)*g_(j,e3)*ket(p4,-1);
Id,d2=a2*bra(p1,-1)*g_(j,e2)*prop(j,p12)*g_(j,e3)*prop(j,p123)*g_(j,si)*ket(p4,-1);
Id,d3=a3*bra(p1,-1)*g_(j,e2)*prop(j,p12)*g_(j,si)*prop(j,-p34)*g_(j,e3)*ket(p4,-1);
Id,d4=a4*bra(p1,-1)*g_(j,si)*prop(j,-p234)*g_(j,ro)*ket(p4,-1)*V3(ro,e2,e3,-p23,p2,p3)/p23.p23;
Id,d5=a5*bra(p1,-1)*g_(j,ro)*prop(j,+p123)*g_(j,si)*ket(p4,-1)*V3(ro,e2,e3,-p23,p2,p3)/p23.p23;
Id,prop(j,p1?)=g_(j,p1)/p1.p1;
Id,V3(ro?,si?,ta?,p1?,p2?,p3?)=
 -d_(ro,si)*(-p3(ta)-2*p2(ta))
 -d_(si,ta)*(p2(ro)-p3(ro))
 -d_(ta,ro)*(2*p3(si)+p2(si));
*#call ward
Inexpression sumpp;
Id,e2=e2p;
Id,e3=e3p;
Endinexpression;
Inexpression summp;
Id,e2=e2m;
Id,e3=e3p;
Endinexpression;
Inexpression sumpm;
Id,e2=e2p;
Id,e3=e3m;
Endinexpression;
Inexpression summm;
Id,e2=e2m;
Id,e3=e3m;
Endinexpression;
#call pol
#call fierz(j,pl,j1,si,pa)
#call zfix
Id,bra(p2,h1?)*g_(j,p12)=bra(p2,h1)*g_(j,p1);
Id,g_(j,p234)*ket(p2,h1?)=g_(j,p34)*ket(p2,h1);
Id,g_(j,p3,p123)=g_(j,p3,p12);
Id,g_(j,p234,p3)=g_(j,p24,p3);
Id,g_(j,p2?{p1,p2,p3})=+ket(p2,-1)*bra(p2,-1)+ket(p2,+1)*bra(p2,+1);
#call zfix
*Id,bra(p1,-1)*g_(j,p123)=bra(p1,-1)*g_(j,p123)
Id,bra(p2,h1?)*g_(j,p123)=bra(p2,h1)*g_(j,p13);
Id,bra(p3,h1?)*g_(j,p123)=bra(p3,h1)*g_(j,p12);
Id,bra(v1?,1)*g_(j,p24)*ket(v2?,1)=zba2(v1,p2,p4,v2);
Id,bra(v1?,1)*g_(j,p34)*ket(v2?,1)=zba2(v1,p3,p4,v2);
Id,bra(v1?,1)*g_(j,p12)*ket(v2?,1)=zba2(v1,p1,p2,v2);
Id,bra(v1?,1)*g_(j,p13)*ket(v2?,1)=zba2(v1,p1,p3,v2);
Id,bra(v1?,1)*g_(j,p123)*ket(v2?,1)=zba3(v1,p1,p2,p3,v2);
Id,bra(v1?,1)*g_(j,p234)*ket(v2?,1)=zba3(v1,p2,p3,p4,v2);
#call zcom
Id,p12.p12^-1=1/s12;
Id,p23.p23^-1=1/s23;
Id,p34.p34^-1=1/s34;
Id,p123.p123^-1=1/s123;
Id,p234.p234^-1=1/s234;
Print +s;
Format DoubleFortran;
.sort
#write <sum.f>"      double complex function"
#write <sum.f>"     & sumpp(p1,p2,p3,p4,pl,pa,b2,b3,a1,a2,a3,a4,a5)"
#write <sum.f>"      implicit none"
#write <sum.f>"C---gauge vectors b2,b3 for gluons"
#write <sum.f>"      integer p1,p2,p3,p4,p5,pl,pa,b2,b3,a1,a2,a3,a4,a5"
#write <sum.f>"      double precision s3,s12,s23,s34,s123,s234"
#write <sum.f>"      double complex iza,izb,zba2,zba3"
#write <sum.f>"      include \'constants.f\'"
#write <sum.f>"      include \'zprods_com.f\'"
#write <sum.f>"      include \'sprods_com.f\'"
#write <sum.f>"C*****Statement functions****************************"
#write <sum.f>"      s3(p1,p2,p3)=s(p1,p2)+s(p2,p3)+s(p3,p1)"
#write <sum.f>"      iza(p1,p2)=cone/za(p1,p2)"
#write <sum.f>"      izb(p1,p2)=cone/zb(p1,p2)"
#write <sum.f>"      zba2(p1,p2,p3,p4)=zb(p1,p2)*za(p2,p4)+zb(p1,p3)*za(p3,p4)"
#write <sum.f>"      zba3(p1,p2,p3,p4,p5)="
#write <sum.f>"     & +zb(p1,p2)*za(p2,p5)+zb(p1,p3)*za(p3,p5)+zb(p1,p4)*za(p4,p5)"
#write <sum.f>"C***************************************************"
#write <sum.f>"      s12=s(p1,p2)"
#write <sum.f>"      s23=s(p2,p3)"
#write <sum.f>"      s34=s(p3,p4)"
#write <sum.f>"      s123=s3(p1,p2,p3)"
#write <sum.f>"      s234=s3(p2,p3,p4)"
#write <sum.f>"      sumpp=%e",sumpp(sumpp)
#write <sum.f>"      return"
#write <sum.f>"      end"
#write <sum.f>"      "
#write <sum.f>"      "
#write <sum.f>"      double complex function"
#write <sum.f>"     & summp(p1,p2,p3,p4,pl,pa,b2,b3,a1,a2,a3,a4,a5)"
#write <sum.f>"      implicit none"
#write <sum.f>"C---gauge vectors b2,b3 for gluons"
#write <sum.f>"      integer p1,p2,p3,p4,p5,pl,pa,b2,b3,a1,a2,a3,a4,a5"
#write <sum.f>"      double precision s3,s12,s23,s34,s123,s234"
#write <sum.f>"      double complex iza,izb,zba2,zba3"
#write <sum.f>"      include \'constants.f\'"
#write <sum.f>"      include \'zprods_com.f\'"
#write <sum.f>"      include \'sprods_com.f\'"
#write <sum.f>"C*****Statement functions****************************"
#write <sum.f>"      s3(p1,p2,p3)=s(p1,p2)+s(p2,p3)+s(p3,p1)"
#write <sum.f>"      iza(p1,p2)=cone/za(p1,p2)"
#write <sum.f>"      izb(p1,p2)=cone/zb(p1,p2)"
#write <sum.f>"      zba2(p1,p2,p3,p4)=zb(p1,p2)*za(p2,p4)+zb(p1,p3)*za(p3,p4)"
#write <sum.f>"      zba3(p1,p2,p3,p4,p5)="
#write <sum.f>"     & +zb(p1,p2)*za(p2,p5)+zb(p1,p3)*za(p3,p5)+zb(p1,p4)*za(p4,p5)"
#write <sum.f>"C***************************************************"
#write <sum.f>"      s12=s(p1,p2)"
#write <sum.f>"      s23=s(p2,p3)"
#write <sum.f>"      s34=s(p3,p4)"
#write <sum.f>"      s123=s3(p1,p2,p3)"
#write <sum.f>"      s234=s3(p2,p3,p4)"
#write <sum.f>"      summp=%e",summp(summp)
#write <sum.f>"      return"
#write <sum.f>"      end"
#write <sum.f>"      "
#write <sum.f>"      "
#write <sum.f>"      double complex function"
#write <sum.f>"     & sumpm(p1,p2,p3,p4,pl,pa,b2,b3,a1,a2,a3,a4,a5)"
#write <sum.f>"      implicit none"
#write <sum.f>"C---gauge vectors b2,b3 for gluons"
#write <sum.f>"      integer p1,p2,p3,p4,p5,pl,pa,b2,b3,a1,a2,a3,a4,a5"
#write <sum.f>"      double precision s3,s12,s23,s34,s123,s234"
#write <sum.f>"      double complex iza,izb,zba2,zba3"
#write <sum.f>"      include \'constants.f\'"
#write <sum.f>"      include \'zprods_com.f\'"
#write <sum.f>"      include \'sprods_com.f\'"
#write <sum.f>"C*****Statement functions****************************"
#write <sum.f>"      s3(p1,p2,p3)=s(p1,p2)+s(p2,p3)+s(p3,p1)"
#write <sum.f>"      iza(p1,p2)=cone/za(p1,p2)"
#write <sum.f>"      izb(p1,p2)=cone/zb(p1,p2)"
#write <sum.f>"      zba2(p1,p2,p3,p4)=zb(p1,p2)*za(p2,p4)+zb(p1,p3)*za(p3,p4)"
#write <sum.f>"      zba3(p1,p2,p3,p4,p5)="
#write <sum.f>"     & +zb(p1,p2)*za(p2,p5)+zb(p1,p3)*za(p3,p5)+zb(p1,p4)*za(p4,p5)"
#write <sum.f>"C***************************************************"
#write <sum.f>"      s12=s(p1,p2)"
#write <sum.f>"      s23=s(p2,p3)"
#write <sum.f>"      s34=s(p3,p4)"
#write <sum.f>"      s123=s3(p1,p2,p3)"
#write <sum.f>"      s234=s3(p2,p3,p4)"
#write <sum.f>"      sumpm=%e",sumpm(sumpm)
#write <sum.f>"      return"
#write <sum.f>"      end"
#write <sum.f>"      "
#write <sum.f>"      "
#write <sum.f>"      double complex function"
#write <sum.f>"     & summm(p1,p2,p3,p4,pl,pa,b2,b3,a1,a2,a3,a4,a5)"
#write <sum.f>"      implicit none"
#write <sum.f>"C---gauge vectors b2,b3 for gluons"
#write <sum.f>"      integer p1,p2,p3,p4,p5,pl,pa,b2,b3,a1,a2,a3,a4,a5"
#write <sum.f>"      double precision s3,s12,s23,s34,s123,s234"
#write <sum.f>"      double complex iza,izb,zba2,zba3"
#write <sum.f>"      include \'constants.f\'"
#write <sum.f>"      include \'zprods_com.f\'"
#write <sum.f>"      include \'sprods_com.f\'"
#write <sum.f>"C*****Statement functions****************************"
#write <sum.f>"      s3(p1,p2,p3)=s(p1,p2)+s(p2,p3)+s(p3,p1)"
#write <sum.f>"      iza(p1,p2)=cone/za(p1,p2)"
#write <sum.f>"      izb(p1,p2)=cone/zb(p1,p2)"
#write <sum.f>"      zba2(p1,p2,p3,p4)=zb(p1,p2)*za(p2,p4)+zb(p1,p3)*za(p3,p4)"
#write <sum.f>"      zba3(p1,p2,p3,p4,p5)="
#write <sum.f>"     & +zb(p1,p2)*za(p2,p5)+zb(p1,p3)*za(p3,p5)+zb(p1,p4)*za(p4,p5)"
#write <sum.f>"C***************************************************"
#write <sum.f>"      s12=s(p1,p2)"
#write <sum.f>"      s23=s(p2,p3)"
#write <sum.f>"      s34=s(p3,p4)"
#write <sum.f>"      s123=s3(p1,p2,p3)"
#write <sum.f>"      s234=s3(p2,p3,p4)"
#write <sum.f>"      summm=%e",summm(summm)
#write <sum.f>"      return"
#write <sum.f>"      end"
Print +s;
.end
