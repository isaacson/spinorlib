#procedure fierz(j,k1,j1,mu,k2)

#+
Id,bra(k3?,+1)*g_(j?,mu?)*ket(k4?,-1)=0;
Id,bra(k3?,-1)*g_(j?,mu?)*ket(k4?,+1)=0;
Id,bra(k3?,h5?)*g_(j?,mu?)*ket(k3?,h5?)=2*k3(mu);
.sort
IF ((Match(g_('j','mu'))) && (Match(bra('k1',-1)*g_('j1','mu')*ket('k2',-1))));
Id,once,bra('k1',-1)*g_('j1','mu')*ket('k2',-1)=1;
Id,g_('j','mu')=2*(ket('k1',+1)*bra('k2',+1)+ket('k2',-1)*bra('k1',-1));
ENDIF;
.sort
IF ((Match(g_('j','mu'))) && (Match(bra('k1',+1)*g_('j1','mu')*ket('k2',+1))));
Id,once,bra('k1',+1)*g_('j1','mu')*ket('k2',+1)=1;
Id,g_('j','mu')=2*(ket('k2',+1)*bra('k1',+1)+ket('k1',-1)*bra('k2',-1));
ENDIF;
.sort
#-
#endprocedure
