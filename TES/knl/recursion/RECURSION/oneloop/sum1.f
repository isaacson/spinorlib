      double complex function
     & sump(p1,p2,p3,pl,pa,b2,a1,a2)
      implicit none
C---gauge vectors b2 for gluon
      integer p1,p2,p3,p4,pl,pa,b2,a1,a2
      double precision s12,s23
      double complex iza,izb,zba2
      include 'constants.f'
      include 'zprods_com.f'
      include 'sprods_com.f'
C*****Statement functions****************************
      iza(p1,p2)=cone/za(p1,p2)
      izb(p1,p2)=cone/zb(p1,p2)
      zba2(p1,p2,p3,p4)=zb(p1,p2)*za(p2,p4)+zb(p1,p3)*za(p3,p4)
C***************************************************
      s12=s(p1,p2)
      s23=s(p2,p3)
      sump= - iza(p2,b2)*za(p1,pl)*za(p1,b2)*zb(p1,p2)*zb(p3,pa)*a2*
     & s12**(-1) + iza(p2,b2)*za(p1,pl)*zb(p2,p3)*zba2(pa,p2,p3,b2)*a1*
     & s23**(-1)

      return
      end
      
      
      double complex function
     & summ(p1,p2,p3,pl,pa,b2,a1,a2)
      implicit none
C---gauge vectors b2,b3 for gluons
      integer p1,p2,p3,p4,pl,pa,b2,a1,a2
      double precision s12,s23
      double complex iza,izb,zba2
      include 'constants.f'
      include 'zprods_com.f'
      include 'sprods_com.f'
C*****Statement functions****************************
      iza(p1,p2)=cone/za(p1,p2)
      izb(p1,p2)=cone/zb(p1,p2)
      zba2(p1,p2,p3,p4)=zb(p1,p2)*za(p2,p4)+zb(p1,p3)*za(p3,p4)
C***************************************************
      s12=s(p1,p2)
      s23=s(p2,p3)
      summ= - izb(p2,b2)*za(p1,p2)*zb(p3,pa)*zba2(b2,p1,p2,pl)*a2*
     & s12**(-1) + izb(p2,b2)*za(p1,pl)*za(p2,p3)*zb(p3,pa)*zb(p3,b2)*
     & a1*s23**(-1)

      return
      end
      
      
