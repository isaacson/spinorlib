      subroutine amps(p1,p2,p3,Ub,e2,e3,V,AB,BA)
      implicit none
      double complex string1,string2,string3,cdotpr,AB,BA,
     & p1(4),p2(4),p3(4),Ub(4),e2(4),e3(4),V(4)
      double precision mt
      mt=sqrt(abs(dble(p1(4)**2-p1(1)**2-p1(2)**2-p1(3)**2)))
      AB=
     & -0.25d0/(cdotpr(p1,p2))*string2(Ub,e2,e3,V)*mt
     & -0.25d0/(cdotpr(p1,p2))*string3(Ub,e2,p1,e3,V)
     & -0.25d0/(cdotpr(p1,p2))*string3(Ub,e2,p2,e3,V)
     & -0.5D0/(cdotpr(p2,p3))*string1(Ub,e2,V)*cdotpr(e3,p2)
     & +0.5D0/(cdotpr(p2,p3))*string1(Ub,e3,V)*cdotpr(e2,p3)
     & +0.25d0/(cdotpr(p2,p3))*string1(Ub,p2,V)*cdotpr(e2,e3)
     & -0.25d0/(cdotpr(p2,p3))*string1(Ub,p3,V)*cdotpr(e2,e3)
      BA=
     & -0.25d0/(cdotpr(p1,p3))*string2(Ub,e3,e2,V)*mt
     & -0.25d0/(cdotpr(p1,p3))*string3(Ub,e3,p1,e2,V)
     & -0.25d0/(cdotpr(p1,p3))*string3(Ub,e3,p3,e2,V)
     & +0.5D0/(cdotpr(p2,p3))*string1(Ub,e2,V)*cdotpr(e3,p2)
     & -0.5D0/(cdotpr(p2,p3))*string1(Ub,e3,V)*cdotpr(e2,p3)
     & -0.25d0/(cdotpr(p2,p3))*string1(Ub,p2,V)*cdotpr(e2,e3)
     & +0.25d0/(cdotpr(p2,p3))*string1(Ub,p3,V)*cdotpr(e2,e3)
      return
      end
