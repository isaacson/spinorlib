      subroutine helicity(p,i,f)
c-----massless vector polarization subroutine
      implicit none
      integer i,pol
      double precision p(4),p0,px,py,pz,pv,ct,st,cphi,sphi
      double complex f(4)

      p0=p(4)
      px=p(1)
      py=p(2)
      pz=p(3)

      pv=dsqrt(dabs(p0**2))
      ct=pz/pv
      st=dsqrt(dabs(1d0-ct**2))

      if (st.lt.1d-8) then 
         cphi=1d0
         sphi=0d0
      else
         cphi= px/pv/st
         sphi= py/pv/st
      endif

c      the following ifstatement distinguishes between 
c      positive and negative energies
      if ( p0.gt.0d0) then  
      pol=i
      else
      pol=-i
      endif

      f(4)=dcmplx(0d0,0d0)
      f(1)=dcmplx(ct*cphi/dsqrt(2d0),+pol*sphi/dsqrt(2d0))
      f(2)=dcmplx(ct*sphi/dsqrt(2d0),-pol*cphi/dsqrt(2d0))
      f(3)=dcmplx(-st/dsqrt(2d0),0d0)

      return
      end

