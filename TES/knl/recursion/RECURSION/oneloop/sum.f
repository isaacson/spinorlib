      double complex function
     & sumpp(p1,p2,p3,p4,pl,pa,b2,b3,a1,a2,a3,a4,a5)
      implicit none
C---gauge vectors b2,b3 for gluons
      integer p1,p2,p3,p4,p5,pl,pa,b2,b3,a1,a2,a3,a4,a5
      double precision s3,s12,s23,s34,s123,s234
      double complex iza,izb,zba2,zba3
      include 'constants.f'
      include 'zprods_com.f'
      include 'sprods_com.f'
C*****Statement functions****************************
      s3(p1,p2,p3)=s(p1,p2)+s(p2,p3)+s(p3,p1)
      iza(p1,p2)=cone/za(p1,p2)
      izb(p1,p2)=cone/zb(p1,p2)
      zba2(p1,p2,p3,p4)=zb(p1,p2)*za(p2,p4)+zb(p1,p3)*za(p3,p4)
      zba3(p1,p2,p3,p4,p5)=
     & +zb(p1,p2)*za(p2,p5)+zb(p1,p3)*za(p3,p5)+zb(p1,p4)*za(p4,p5)
C***************************************************
      s12=s(p1,p2)
      s23=s(p2,p3)
      s34=s(p3,p4)
      s123=s3(p1,p2,p3)
      s234=s3(p2,p3,p4)
      sumpp= - 1.D0/2.D0*iza(p2,b2)*iza(p3,b3)*za(p1,p2)*za(b2,b3)*zb(
     & p2,p3)*zb(p4,pa)*zba2(p2,p1,p3,pl)*a5*s23**(-1)*s123**(-1) + 1.D0
     & /2.D0*iza(p2,b2)*iza(p3,b3)*za(p1,p3)*za(b2,b3)*zb(p2,p3)*zb(p4,
     & pa)*zba2(p3,p1,p2,pl)*a5*s23**(-1)*s123**(-1) + iza(p2,b2)*iza(
     & p3,b3)*za(p1,pl)*za(p1,b2)*zb(p1,p2)*zb(p3,p4)*zba2(pa,p3,p4,b3)
     & *a3*s12**(-1)*s34**(-1) + iza(p2,b2)*iza(p3,b3)*za(p1,pl)*za(p2,
     & b3)*zb(p2,p3)*zb(p2,p4)*zba3(pa,p2,p3,p4,b2)*a4*s23**(-1)*
     & s234**(-1) + iza(p2,b2)*iza(p3,b3)*za(p1,pl)*za(p3,b2)*zb(p2,p3)
     & *zb(p3,p4)*zba3(pa,p2,p3,p4,b3)*a4*s23**(-1)*s234**(-1) - 1.D0/2.
     & D0*iza(p2,b2)*iza(p3,b3)*za(p1,pl)*za(b2,b3)*zb(p2,p3)*zb(p2,p4)
     & *zba3(pa,p2,p3,p4,p2)*a4*s23**(-1)*s234**(-1) + 1.D0/2.D0*iza(p2
     & ,b2)*iza(p3,b3)*za(p1,pl)*za(b2,b3)*zb(p2,p3)*zb(p3,p4)*zba2(pa,
     & p2,p4,p3)*a4*s23**(-1)*s234**(-1) + iza(p2,b2)*iza(p3,b3)*za(p1,
     & pl)*zb(p3,p4)*zba2(p2,p3,p4,b3)*zba3(pa,p2,p3,p4,b2)*a1*
     & s34**(-1)*s234**(-1) + iza(p2,b2)*iza(p3,b3)*za(p1,b2)*za(p1,b3)
     & *zb(p1,p2)*zb(p4,pa)*zba2(p3,p1,p2,pl)*a2*s12**(-1)*s123**(-1)
      sumpp = sumpp + iza(p2,b2)*iza(p3,b3)*za(p1,b2)*za(p2,b3)*zb(p2,
     & p3)*zb(p4,pa)*zba2(p2,p1,p3,pl)*a5*s23**(-1)*s123**(-1) + iza(p2
     & ,b2)*iza(p3,b3)*za(p1,b3)*za(p3,b2)*zb(p2,p3)*zb(p4,pa)*zba2(p3,
     & p1,p2,pl)*a5*s23**(-1)*s123**(-1)

      return
      end
      
      
      double complex function
     & summp(p1,p2,p3,p4,pl,pa,b2,b3,a1,a2,a3,a4,a5)
      implicit none
C---gauge vectors b2,b3 for gluons
      integer p1,p2,p3,p4,p5,pl,pa,b2,b3,a1,a2,a3,a4,a5
      double precision s3,s12,s23,s34,s123,s234
      double complex iza,izb,zba2,zba3
      include 'constants.f'
      include 'zprods_com.f'
      include 'sprods_com.f'
C*****Statement functions****************************
      s3(p1,p2,p3)=s(p1,p2)+s(p2,p3)+s(p3,p1)
      iza(p1,p2)=cone/za(p1,p2)
      izb(p1,p2)=cone/zb(p1,p2)
      zba2(p1,p2,p3,p4)=zb(p1,p2)*za(p2,p4)+zb(p1,p3)*za(p3,p4)
      zba3(p1,p2,p3,p4,p5)=
     & +zb(p1,p2)*za(p2,p5)+zb(p1,p3)*za(p3,p5)+zb(p1,p4)*za(p4,p5)
C***************************************************
      s12=s(p1,p2)
      s23=s(p2,p3)
      s34=s(p3,p4)
      s123=s3(p1,p2,p3)
      s234=s3(p2,p3,p4)
      summp= - iza(p3,b3)*izb(p2,b2)*za(p1,p2)*za(p2,b3)*zb(p2,p3)*zb(
     & p4,pa)*zba3(b2,p1,p2,p3,pl)*a5*s23**(-1)*s123**(-1) - 1.D0/2.D0*
     & iza(p3,b3)*izb(p2,b2)*za(p1,p2)*za(p2,b3)*zb(p3,b2)*zb(p4,pa)*
     & zba2(p2,p1,p3,pl)*a5*s23**(-1)*s123**(-1) + iza(p3,b3)*izb(p2,b2
     & )*za(p1,p2)*zb(p3,p4)*zba2(pa,p3,p4,b3)*zba2(b2,p1,p2,pl)*a3*
     & s12**(-1)*s34**(-1) + iza(p3,b3)*izb(p2,b2)*za(p1,p2)*zb(p4,pa)*
     & zba2(p3,p1,p2,pl)*zba2(b2,p1,p2,b3)*a2*s12**(-1)*s123**(-1) + 1.D
     & 0/2.D0*iza(p3,b3)*izb(p2,b2)*za(p1,p3)*za(p2,b3)*zb(p3,b2)*zb(p4
     & ,pa)*zba2(p3,p1,p2,pl)*a5*s23**(-1)*s123**(-1) - iza(p3,b3)*izb(
     & p2,b2)*za(p1,pl)*za(p2,p3)*zb(p3,p4)*zb(p3,b2)*zba3(pa,p2,p3,p4,
     & b3)*a4*s23**(-1)*s234**(-1) + iza(p3,b3)*izb(p2,b2)*za(p1,pl)*
     & za(p2,b3)*zb(p2,p3)*zb(p4,b2)*zba2(pa,p3,p4,p2)*a4*s23**(-1)*
     & s234**(-1) - 1.D0/2.D0*iza(p3,b3)*izb(p2,b2)*za(p1,pl)*za(p2,b3)
     & *zb(p2,p4)*zb(p3,b2)*zba3(pa,p2,p3,p4,p2)*a4*s23**(-1)*
     & s234**(-1) + 1.D0/2.D0*iza(p3,b3)*izb(p2,b2)*za(p1,pl)*za(p2,b3)
     & *zb(p3,p4)*zb(p3,b2)*zba2(pa,p2,p4,p3)*a4*s23**(-1)*s234**(-1)
      summp = summp - iza(p3,b3)*izb(p2,b2)*za(p1,pl)*zb(p3,p4)*zba2(pa
     & ,p3,p4,p2)*zba2(b2,p3,p4,b3)*a1*s34**(-1)*s234**(-1) - iza(p3,b3
     & )*izb(p2,b2)*za(p1,b3)*za(p2,p3)*zb(p3,b2)*zb(p4,pa)*zba2(p3,p1,
     & p2,pl)*a5*s23**(-1)*s123**(-1)

      return
      end
      
      
      double complex function
     & sumpm(p1,p2,p3,p4,pl,pa,b2,b3,a1,a2,a3,a4,a5)
      implicit none
C---gauge vectors b2,b3 for gluons
      integer p1,p2,p3,p4,p5,pl,pa,b2,b3,a1,a2,a3,a4,a5
      double precision s3,s12,s23,s34,s123,s234
      double complex iza,izb,zba2,zba3
      include 'constants.f'
      include 'zprods_com.f'
      include 'sprods_com.f'
C*****Statement functions****************************
      s3(p1,p2,p3)=s(p1,p2)+s(p2,p3)+s(p3,p1)
      iza(p1,p2)=cone/za(p1,p2)
      izb(p1,p2)=cone/zb(p1,p2)
      zba2(p1,p2,p3,p4)=zb(p1,p2)*za(p2,p4)+zb(p1,p3)*za(p3,p4)
      zba3(p1,p2,p3,p4,p5)=
     & +zb(p1,p2)*za(p2,p5)+zb(p1,p3)*za(p3,p5)+zb(p1,p4)*za(p4,p5)
C***************************************************
      s12=s(p1,p2)
      s23=s(p2,p3)
      s34=s(p3,p4)
      s123=s3(p1,p2,p3)
      s234=s3(p2,p3,p4)
      sumpm= - 1.D0/2.D0*iza(p2,b2)*izb(p3,b3)*za(p1,p2)*za(p3,b2)*zb(
     & p2,b3)*zb(p4,pa)*zba2(p2,p1,p3,pl)*a5*s23**(-1)*s123**(-1) - 
     & iza(p2,b2)*izb(p3,b3)*za(p1,p3)*za(p1,b2)*zb(p1,p2)*zb(p4,pa)*
     & zba3(b3,p1,p2,p3,pl)*a2*s12**(-1)*s123**(-1) - iza(p2,b2)*izb(p3
     & ,b3)*za(p1,p3)*za(p3,b2)*zb(p2,p3)*zb(p4,pa)*zba3(b3,p1,p2,p3,pl
     & )*a5*s23**(-1)*s123**(-1) + 1.D0/2.D0*iza(p2,b2)*izb(p3,b3)*za(
     & p1,p3)*za(p3,b2)*zb(p2,b3)*zb(p4,pa)*zba2(p3,p1,p2,pl)*a5*
     & s23**(-1)*s123**(-1) + iza(p2,b2)*izb(p3,b3)*za(p1,pl)*za(p1,b2)
     & *zb(p1,p2)*zb(p4,b3)*zba2(pa,p3,p4,p3)*a3*s12**(-1)*s34**(-1) - 
     & iza(p2,b2)*izb(p3,b3)*za(p1,pl)*za(p2,p3)*zb(p2,p4)*zb(p2,b3)*
     & zba3(pa,p2,p3,p4,b2)*a4*s23**(-1)*s234**(-1) + iza(p2,b2)*izb(p3
     & ,b3)*za(p1,pl)*za(p3,b2)*zb(p2,p3)*zb(p4,b3)*zba3(pa,p2,p3,p4,p3
     & )*a4*s23**(-1)*s234**(-1) - 1.D0/2.D0*iza(p2,b2)*izb(p3,b3)*za(
     & p1,pl)*za(p3,b2)*zb(p2,p4)*zb(p2,b3)*zba3(pa,p2,p3,p4,p2)*a4*
     & s23**(-1)*s234**(-1) + 1.D0/2.D0*iza(p2,b2)*izb(p3,b3)*za(p1,pl)
     & *za(p3,b2)*zb(p2,b3)*zb(p3,p4)*zba2(pa,p2,p4,p3)*a4*s23**(-1)*
     & s234**(-1)
      sumpm = sumpm + iza(p2,b2)*izb(p3,b3)*za(p1,pl)*zb(p4,b3)*zba2(p2
     & ,p3,p4,p3)*zba3(pa,p2,p3,p4,b2)*a1*s34**(-1)*s234**(-1) - iza(p2
     & ,b2)*izb(p3,b3)*za(p1,b2)*za(p2,p3)*zb(p2,b3)*zb(p4,pa)*zba2(p2,
     & p1,p3,pl)*a5*s23**(-1)*s123**(-1)

      return
      end
      
      
      double complex function
     & summm(p1,p2,p3,p4,pl,pa,b2,b3,a1,a2,a3,a4,a5)
      implicit none
C---gauge vectors b2,b3 for gluons
      integer p1,p2,p3,p4,p5,pl,pa,b2,b3,a1,a2,a3,a4,a5
      double precision s3,s12,s23,s34,s123,s234
      double complex iza,izb,zba2,zba3
      include 'constants.f'
      include 'zprods_com.f'
      include 'sprods_com.f'
C*****Statement functions****************************
      s3(p1,p2,p3)=s(p1,p2)+s(p2,p3)+s(p3,p1)
      iza(p1,p2)=cone/za(p1,p2)
      izb(p1,p2)=cone/zb(p1,p2)
      zba2(p1,p2,p3,p4)=zb(p1,p2)*za(p2,p4)+zb(p1,p3)*za(p3,p4)
      zba3(p1,p2,p3,p4,p5)=
     & +zb(p1,p2)*za(p2,p5)+zb(p1,p3)*za(p3,p5)+zb(p1,p4)*za(p4,p5)
C***************************************************
      s12=s(p1,p2)
      s23=s(p2,p3)
      s34=s(p3,p4)
      s123=s3(p1,p2,p3)
      s234=s3(p2,p3,p4)
      summm=izb(p2,b2)*izb(p3,b3)*za(p1,p2)*za(p2,p3)*zb(p2,b3)*zb(p4,
     & pa)*zba3(b2,p1,p2,p3,pl)*a5*s23**(-1)*s123**(-1) - 1.D0/2.D0*
     & izb(p2,b2)*izb(p3,b3)*za(p1,p2)*za(p2,p3)*zb(p4,pa)*zb(b2,b3)*
     & zba2(p2,p1,p3,pl)*a5*s23**(-1)*s123**(-1) - izb(p2,b2)*izb(p3,b3
     & )*za(p1,p2)*zb(p4,pa)*zba2(b2,p1,p2,p3)*zba3(b3,p1,p2,p3,pl)*a2*
     & s12**(-1)*s123**(-1) + izb(p2,b2)*izb(p3,b3)*za(p1,p2)*zb(p4,b3)
     & *zba2(pa,p3,p4,p3)*zba2(b2,p1,p2,pl)*a3*s12**(-1)*s34**(-1) + 
     & izb(p2,b2)*izb(p3,b3)*za(p1,p3)*za(p2,p3)*zb(p3,b2)*zb(p4,pa)*
     & zba3(b3,p1,p2,p3,pl)*a5*s23**(-1)*s123**(-1) + 1.D0/2.D0*izb(p2,
     & b2)*izb(p3,b3)*za(p1,p3)*za(p2,p3)*zb(p4,pa)*zb(b2,b3)*zba2(p3,
     & p1,p2,pl)*a5*s23**(-1)*s123**(-1) - 1.D0/2.D0*izb(p2,b2)*izb(p3,
     & b3)*za(p1,pl)*za(p2,p3)*zb(p2,p4)*zb(b2,b3)*zba3(pa,p2,p3,p4,p2)
     & *a4*s23**(-1)*s234**(-1) - izb(p2,b2)*izb(p3,b3)*za(p1,pl)*za(p2
     & ,p3)*zb(p2,b3)*zb(p4,b2)*zba2(pa,p3,p4,p2)*a4*s23**(-1)*
     & s234**(-1) + 1.D0/2.D0*izb(p2,b2)*izb(p3,b3)*za(p1,pl)*za(p2,p3)
     & *zb(p3,p4)*zb(b2,b3)*zba2(pa,p2,p4,p3)*a4*s23**(-1)*s234**(-1)
      summm = summm - izb(p2,b2)*izb(p3,b3)*za(p1,pl)*za(p2,p3)*zb(p3,
     & b2)*zb(p4,b3)*zba3(pa,p2,p3,p4,p3)*a4*s23**(-1)*s234**(-1) - 
     & izb(p2,b2)*izb(p3,b3)*za(p1,pl)*zb(p4,b3)*zba2(pa,p3,p4,p2)*
     & zba2(b2,p3,p4,p3)*a1*s34**(-1)*s234**(-1)

      return
      end
