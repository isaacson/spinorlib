      double precision function dotpr(K1,K2)
      implicit none
      double precision K1(4),K2(4)
      dotpr=K1(4)*K2(4)-K1(1)*K2(1)-K1(2)*K2(2)-K1(3)*K2(3)
      return
      end
