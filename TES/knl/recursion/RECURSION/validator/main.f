      implicit none
      include 'constants.f'
      include 'swapxz.f'
      include 'fermionmass.f'
      integer nu,i,j,nrambo,lw,Nlegs,ivec
      integer gauge,count
      double complex amp,Gamp
      double precision Mr,Mv,Gv,Rn,p0(4),pl(4),pw(4),
     & kmom(mxpart,4),klep(2,4),xm(mxpart-1),p(4,mxpart-1),pswt,Et
     & ,pmcfm(mxpart,4),dotpr
      integer Order(nmax),pol(nmax),pollep(2),flav(nmax)
      common/mcfm/pmcfm

      Mv=25d0
      Gv=1d0
      ivec=1
      pollep(1)=1
      pollep(2)=-1
      swapxz=.true.

      do i=1,mxpart
         Order(i)=i
      enddo

      do count=1,10000000
      if (mod(count,100000).eq.1) write(*,*) count
      call tester(Nlegs,gauge,flav,pol)
      Nlegs=Nlegs+1 !add vector boson
      nrambo=Nlegs-2

      lw=0
 
      p0(1)=+71.5344542606618d0
      p0(2)=0d0
      p0(3)=0d0
      p0(4)=-71.5344542606618d0

      pl(1)=-71.5344542606618d0
      pl(2)=0d0
      pl(3)=0d0
      pl(4)=-71.5344542606618d0
      ET=-p0(4)-pl(4)

      do j=1,Nlegs-3
         xm(j)=mass(flav(j+2))
      enddo
      Mr=Mv+rn()*3d0*Gv
      kmom(1,:)=p0(:)
      kmom(2,:)=pl(:)

      xm(nrambo)=Mr
      if (ivec.eq.1) then
         if (nrambo.gt.1) then
            call RAMBO(nrambo,ET,XM,P,PSWT,LW)
            pw(:)=p(:,nrambo)
         else
            pw(:)=-p0(:)-pl(:)
            p(:,1)=pw(:)
            Mr=sqrt(dotpr(pw,pw))
         endif
         do j=1,nrambo
            kmom(j+2,:)=p(:,j)
         enddo
         xm(1)=0d0
         xm(2)=0d0
         call RAMBOO(2,Mr,pw,xm,p,pswt,lw)
         do j=1,2
            klep(j,:)=p(:,j)
         enddo
      else
         call RAMBO(nrambo,ET,XM,P,PSWT,LW)
         kmom(2,:)=p0(:)
         kmom(3,:)=pl(:)
         do nu=1,4
            kmom(1,nu)=p(nu,1)
            do j=2,nrambo
               kmom(j+2,nu)=p(nu,j)
            enddo
         enddo
      endif

      do j=1,nrambo+1
         pmcfm(j,:)=kmom(j,:)
      enddo
      pmcfm(nrambo+2,:)=klep(1,:)
      pmcfm(nrambo+3,:)=klep(2,:)

      call bgV(Order,Kmom,Klep,pol,pollep,flav,Nlegs,amp)
      pol(gauge)=0
      call bgV(Order,Kmom,Klep,pol,pollep,flav,Nlegs,Gamp)
      if (cdabs(Gamp)/cdabs(amp).gt.1d-80) then
         write(*,*) cdabs(Gamp),cdabs(amp),cdabs(Gamp)/cdabs(amp)
         write(*,*)
         write(*,*) nlegs,gauge
         write(*,"(20i2)") (flav(i),i=1,nlegs)
         write(*,"(20i2)") (pol(i),i=1,nlegs)
         write(*,*)
         write(*,*) 'partons+ V momenta'
         do j=1,nrambo+2
            write(*,*) j,kmom(j,:),
     .        sqrt(abs(kmom(j,4)**2-kmom(j,3)**2
     .                -kmom(j,2)**2-kmom(j,1)**2))
         enddo
         write(*,*) 'V+decay lepton momenta'
         write(*,*) 1,pw(:),sqrt(pw(4)**2-pw(1)**2-pw(2)**2-pw(3)**2)
         write(*,*) 2,klep(1,:),
     .        klep(1,4)**2-klep(1,1)**2-klep(1,2)**2-klep(1,3)**2
         write(*,*) 3,klep(2,:),
     .        klep(2,4)**2-klep(2,1)**2-klep(2,2)**2-klep(2,3)**2
      endif
      enddo
      end


