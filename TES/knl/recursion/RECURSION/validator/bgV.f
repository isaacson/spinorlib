      subroutine BGv(O,kmom,klep,pol,pollep,flav,Nlegs,weight)
      implicit none
      include 'constants.f'
      include 'fermionmass.f'
      
      integer Nlegs,l,k,n,m,h,f1,f2,fv,fvout,fvin,h1,h2,nflav,i
      integer Nmax
      parameter(Nmax=22)
      integer pol(Nmax),flav(Nmax),flavlk(Nlegs,Nlegs)
      integer flavV(Nlegs, Nlegs),pollep(2),O(Nmax)
      double precision P(4),Pv(4),kmom(Nmax,4),qmom(Nlegs,4),klep(2,4)
      double precision Rlep,Llep,Rquark,Lquark,dotpr,mm,Mv,Pv2,Gv
      double complex Jlk(Nlegs,Nlegs,4),Jv(Nlegs,Nlegs,4),
     & K1(4),K2(4),Ktemp(4),cdotpr,sprod,weight,
     & J(4),J0(4),J1(4),J2(4),J3(4),Jtemp(4),Lcur(4),Pc(4),
     & JV1(4),JV2(4),JV3(4),JV4(4)
      logical loglk(Nlegs,Nlegs),logv(Nlegs,Nlegs)

      Rlep=1d0
      Llep=1d0
      Rquark=1.0d0
      Lquark=1.0d0
      Mv=25d0
      Gv=1d0
      fvin=1
      fvout=1

      Jlk(:,:,:)=czip
      Jv(:,:,:)=czip
C---by default set all branchings true
      loglk(:,:)=.false.
      logv(:,:)=.false.
C-- Make lepton current
      Pv(:)=klep(1,:)+klep(2,:)
      Pv2=Pv(4)**2-Pv(1)**2-Pv(2)**2-Pv(3)**2
      h1=pollep(1)
      h2=pollep(2)
      call wavefunction(Klep(1,:),11,h1,J1)
      call wavefunction(Klep(2,:),-11,h2,J2)
      call UbGammaURL(J2,J1,Lcur,Rlep,Llep)
! -----> Add vector boson propagator to the lepton current
!      Lcur(:)=Lcur(:)/(Pv2-Mv*Mv+im*Gv*Mv)
      Pc(:)=dcmplx(Pv(:))
C-----create cumulative sums of momenta
      P(:)=kmom(O(1),:)
      qmom(1,:)=P(:)
      do i=1,Nlegs-1
         P(:)=P(:)+kmom(O(1+i),:)
         qmom(1+i,:)=P(:)  
      enddo

C---construct the partonic currents
C---setup polarization vectors for Nlegs-1 particles
C---diagonal elements of matrix
C---also diagonal elements of flavour matrix
      do k=2,Nlegs-1
C ---partonic wavefunction
         P(:)=kmom(O(k),:)
         h=pol(O(k)) 
         nflav=flav(O(k)) 
         if (abs(nflav).le.6) then
            loglk(k-1,k-1)=.true.
            flavlk(k-1,k-1)=nflav
            if (h.eq.0) then
               if (nflav.ne.0) then
                  write(*,"(' Particle ',i1,' has flavor ',i1)") k,nflav 
                  write(*,*) 'Check aborted'
                  stop
               else
                  Jlk(k-1,k-1,:)=dcmplx(P(:))/50d0
               endif
            else
               call wavefunction(P,nflav,h,J)
               Jlk(k-1,k-1,:)=J(:)
           endif
         endif
C ---add vectorboson if correct flavor
         Ktemp(:)=dcmplx(P(:)+Pv(:)) 
         mm=mass(nflav)
         Jtemp(:)=dcmplx(0d0,0d0)
         if (abs(nflav).gt.0) then
            if (nflav.gt.0) then
               call UbkslashRL(J,Lcur,Jtemp,Rquark,Lquark)
               if (Nlegs.gt.3) then
                 call UbkslashRL(Jtemp,Ktemp,J,1d0,1d0)
                 Jtemp(:)=(J(:)+mm*Jtemp)/(cdotpr(Ktemp,Ktemp)-mm**2)
               endif
               logV(k-1,k-1)=.true.
               flavV(k-1,k-1)=nflav
           endif
            if (nflav.lt.0) then
               call kslashURL(Lcur,J,Jtemp,Rquark,Lquark)
               if (Nlegs.gt.3) then
                 call kslashURL(-Ktemp,Jtemp,J,1d0,1d0)
                Jtemp(:)=(J(:)+mm*Jtemp(:))/(cdotpr(Ktemp,Ktemp)-mm**2)
               endif
               logV(k-1,k-1)=.true.
               flavV(k-1,k-1)=nflav
            endif
         endif
         Jv(k-1,k-1,:)=Jtemp(:)
      enddo
!
! Make partonic currents
!
      do l=1,Nlegs-3
         do k=1,Nlegs-l-2
            J(:)=0d0
            do n=k,k+l-1
               Jtemp(:)=dcmplx(0d0,0d0)
               if (loglk(k,n) .and. loglk(n+1,k+l)) then
                  K1(:)=dcmplx(qmom(n+1,:)-qmom(k,:))
                  K2(:)=dcmplx(qmom(k+l+1,:)-qmom(n+1,:))
                  J1(:)=Jlk(k,n,:)
                  J2(:)=Jlk(n+1,k+l,:)
                  f1=flavlk(k,n)
                  f2=flavlk(n+1,k+l)
                  if ((f1.eq.0).and.(f2.eq.0)) then
                     loglk(k,k+l)=.true.
                     Jtemp(:)=+2d0*(cdotpr(J1,K2)*J2(:)
     &                        -cdotpr(J2,K1)*J1(:))
     &                        +cdotpr(J1,J2)*(K1(:)-K2(:))
                  elseif ((f1.eq.0).and.(f2.ne.0)) then 
                     loglk(k,k+l)=.true.
                     if (f2.gt.0) then
                        call UbkslashRL(J2,J1,Jtemp,1d0,1d0)
                     else
                        call kslashURL(J1,J2,Jtemp,1d0,1d0)
                        Jtemp=-Jtemp
                     endif
                  elseif ((f1.ne.0).and.(f2.eq.0)) then 
                     loglk(k,k+l)=.true.
                     if (f1.gt.0) then
                        call UbkslashRL(J1,J2,Jtemp,1d0,1d0)
                        Jtemp=-Jtemp
                     else
                        call kslashURL(J2,J1,Jtemp,1d0,1d0)
                     endif
                  elseif (f1.eq.-f2) then
                     loglk(k,k+l)=.true.
                     if (f1.gt.0) then
                        call UbGammaURL(J2,J1,Jtemp,1d0,1d0)
                     else
                        call UbGammaURL(J1,J2,Jtemp,1d0,1d0)
                     endif
                  endif
               endif
               J(:)=J(:)+Jtemp(:)
            enddo               ! endloop n
            
            f1=(f1+f2)
            do n=k,k+l-2
               do m=n+1,k+l-1
                  jtemp(:)=0d0
                if (loglk(k,n).and.loglk(n+1,m).and.loglk(m+1,k+l)) then
                     if ((flavlk(k,n)==0)
     &                    .and.(flavlk(n+1,m)==0)
     &                    .and.(flavlk(m+1,k+l)==0)) then
                        loglk(k,k+l)=.true.
                        J1(:)=Jlk(k,n,:)
                        J2(:)=Jlk(n+1,m,:)
                        J3(:)=Jlk(m+1,k+l,:)
                        Jtemp(:)=
     &                       +2d0*cdotpr(J1,J3)*J2(:)
     &                       -cdotpr(J1,J2)*J3(:)
     &                       -cdotpr(J2,J3)*J1(:)
                        f1=0
                     endif
                  endif
                  J(:)=J(:)+Jtemp(:)
               enddo            ! endloop m
            enddo               ! endloop n

C---Divide by propagator
            P(:)=qmom(k+l+1,:)-qmom(k,:)
            if (loglk(k,k+l)) then
            if (f1.eq.0) then
               J(:)=J(:)/dotpr(P,P)
            else
               Ktemp(:)=dcmplx(P(:))
               mm=mass(f1)
               if (f1.gt.0) then
                  call UbkslashRL(J,Ktemp,Jtemp,1d0,1d0)
                  J(:)=(Jtemp(:)+mm*J(:))/(dotpr(P,P)-mm**2)
               else
                  call kslashURL(-Ktemp,J,Jtemp,1d0,1d0)
                  J(:)=(Jtemp(:)+mm*J(:))/(dotpr(P,P)-mm**2)
               endif
            endif
            endif
            Jlk(k,k+l,:)=J(:)
            flavlk(k,k+l)=f1
         enddo
      enddo
      

      do l=1,Nlegs-3
         do k=1,Nlegs-l-2
! 
! Construct the vector currents by adding vector boson to partonic current
!
            fv=0
            logV(k,k+l)=.false.
            JV1(:)=dcmplx(0d0,0d0)
            if (loglk(k,k+l)) then
               Jtemp(:)=Jlk(k,k+l,:)
               f1=flavlk(k,k+l)
               if (f1.gt.0) then
                  call UbkslashRL(Jtemp,Lcur,JV1,Rquark,Lquark)
                  logV(k,k+l)=.true.
                  fv=f1
               elseif (f1.lt.0) then
                  call kslashURL(Lcur,Jtemp,JV1,Rquark, Lquark)
                  logV(k,k+l)=.true.
                  fv=f1
               endif
            endif
            flavV(k,k+l)=fv


!     merge vector current with partonic current
            JV2(:)=dcmplx(0d0,0d0)
            do n=k,k+l-1
               Jtemp(:)=dcmplx(0d0,0d0)
               if (logV(k,n) .and. loglk(n+1,k+l)) then
                  logV(k,k+l)=.true.
                  K1(:)=dcmplx(Pv(:)+qmom(n+1,:)-qmom(k,:))
                  K2(:)=dcmplx(qmom(k+l+1,:)-qmom(n+1,:))
                  J1(:)=JV(k,n,:)
                  J2(:)=Jlk(n+1,k+l,:)
                  f1=flavV(k,n)
                  f2=flavlk(n+1,k+l)
                  if ((f1.eq.0).and.(f2.eq.0)) then
                     logV(k,k+l)=.true.
                     Jtemp(:)=+2d0*(cdotpr(J1,K2)*J2(:)
     &                    -cdotpr(J2,K1)*J1(:))
     &                    +cdotpr(J1,J2)*(K1(:)-K2(:))
                  elseif ((f1.eq.0).and.(f2.ne.0)) then 
                     logV(k,k+l)=.true.
                     if (f2.gt.0) then
                        call UbkslashRL(J2,J1,Jtemp,1d0,1d0)
                        fv=f2
                     else
                        call kslashURL(J1,J2,Jtemp,1d0,1d0)
                        Jtemp=-Jtemp
                        fv=f2
                     endif
                  elseif ((f1.ne.0).and.(f2.eq.0)) then 
                     logV(k,k+l)=.true.
                     if (f1.gt.0) then
                        call UbkslashRL(J1,J2,Jtemp,1d0,1d0)
                        fv=f1
                        Jtemp=-Jtemp
                     else
                        call kslashURL(J2,J1,Jtemp,1d0,1d0)
                        fv=f1
                     endif
                  elseif (f1.eq.-f2) then
                     logV(k,k+l)=.true.
                     fv=0
                     if (f1.gt.0) then
                        call UbGammaURL(J2,J1,Jtemp,1d0,1d0)
                    else
                        call UbGammaURL(J1,J2,Jtemp,1d0,1d0)
                      endif
                  else
                     logV(k,k+l)=.false.
                  endif
                  JV2(:)=JV2(:)+Jtemp(:)
               endif
            enddo               ! endloop n

            JV3(:)=dcmplx(0d0,0d0)
            do n=k,k+l-1
               Jtemp(:)=dcmplx(0d0,0d0)
               if (loglk(k,n) .and. logV(n+1,k+l)) then
                  logV(k,k+l)=.true.
                  K1(:)=dcmplx(qmom(n+1,:)-qmom(k,:))
                  K2(:)=dcmplx(Pv(:)+qmom(k+l+1,:)-qmom(n+1,:))
                  J1(:)=Jlk(k,n,:)
                  J2(:)=JV(n+1,k+l,:)
                  f1=flavlk(k,n)
                  f2=flavV(n+1,k+l)
                  if ((f1.eq.0).and.(f2.eq.0)) then
                     logV(k,k+l)=.true.
                     Jtemp(:)=+2d0*(cdotpr(J1,K2)*J2(:)
     &                    -cdotpr(J2,K1)*J1(:))
     &                    +cdotpr(J1,J2)*(K1(:)-K2(:))
                  elseif ((f1.eq.0).and.(f2.ne.0)) then 
                     logV(k,k+l)=.true.
                     if (f2.gt.0) then
                        call UbkslashRL(J2,J1,Jtemp,1d0,1d0)
                        fv=f2
                     else
                        call kslashURL(J1,J2,Jtemp,1d0,1d0)
                        Jtemp=-Jtemp
                        fv=f2
                     endif
                  elseif ((f1.ne.0).and.(f2.eq.0)) then 
                     logV(k,k+l)=.true.
                     if (f1.gt.0) then
                        call UbkslashRL(J1,J2,Jtemp,1d0,1d0)
                        fv=f1
                        Jtemp=-Jtemp
                     else
                        call kslashURL(J2,J1,Jtemp,1d0,1d0)
                        fv=f1
                     endif
                  elseif (f1.eq.-f2) then
                     logV(k,k+l)=.true.
                     fv=0
                     if (f1.gt.0) then
                        call UbGammaURL(J2,J1,Jtemp,1d0,1d0)
                     else
                        call UbGammaURL(J1,J2,Jtemp,1d0,1d0)
                     endif
                  else
                     logV(k,k+l)=.false.
                   endif
                   JV3(:)=JV3(:)+Jtemp(:)
               endif
            enddo               ! endloop n

            JV4(:)=dcmplx(0d0,0d0)
           do n=k,k+l-2
               do m=n+1,k+l-1
                  Jtemp(:)=dcmplx(0d0,0d0)
               if (logV(k,n).and.loglk(n+1,m).and.loglk(m+1,k+l)) then
                     if ((flavV(k,n)==0)
     &                    .and.(flavlk(n+1,m)==0)
     &                    .and.(flavlk(m+1,k+l)==0)) then
                        logV(k,k+l)=.true.
                        J1(:)=Jv(k,n,:)
                        J2(:)=Jlk(n+1,m,:)
                        J3(:)=Jlk(m+1,k+l,:)
                        Jtemp(:)=
     &                       +2d0*cdotpr(J1,J3)*J2(:)
     &                       -cdotpr(J1,J2)*J3(:)
     &                       -cdotpr(J2,J3)*J1(:)
                        fv=0
                        JV4(:)=JV4(:)+Jtemp(:)
                     endif
                  endif
                if (loglk(k,n).and.logV(n+1,m).and.loglk(m+1,k+l)) then
                     if ((flavlk(k,n)==0)
     &                    .and.(flavV(n+1,m)==0)
     &                    .and.(flavlk(m+1,k+l)==0)) then
                        logV(k,k+l)=.true.
                        J1(:)=Jlk(k,n,:)
                        J2(:)=JV(n+1,m,:)
                        J3(:)=Jlk(m+1,k+l,:)
                        Jtemp(:)=
     &                       +2d0*cdotpr(J1,J3)*J2(:)
     &                       -cdotpr(J1,J2)*J3(:)
     &                       -cdotpr(J2,J3)*J1(:)
                        fv=0
                        JV4(:)=JV4(:)+Jtemp(:)
                     endif
                  endif
                if (loglk(k,n).and.loglk(n+1,m).and.logV(m+1,k+l)) then
                     if ((flavlk(k,n)==0)
     &                    .and.(flavlk(n+1,m)==0)
     &                    .and.(flavV(m+1,k+l)==0)) then
                        logV(k,k+l)=.true.
                        J1(:)=Jlk(k,n,:)
                        J2(:)=Jlk(n+1,m,:)
                        J3(:)=JV(m+1,k+l,:)
                        Jtemp(:)=
     &                       +2d0*cdotpr(J1,J3)*J2(:)
     &                       -cdotpr(J1,J2)*J3(:)
     &                       -cdotpr(J2,J3)*J1(:)
                        fv=0
                        JV4(:)=JV4(:)+Jtemp(:)
                     endif
                  endif
               enddo            ! endloop m
            enddo               ! endloop n



C---Divide by propagator for all except the last
            J(:)=JV1(:)+JV2(:)+JV3(:)+JV4(:)
            f1=fv
           if ((l < Nlegs-3).and.(logV(k,k+l))) then
               P(:)=Pv(:)+qmom(k+l+1,:)-qmom(k,:)
               if (f1.eq.0) then
                  J(:)=J(:)/dotpr(P,P)
               else
                  Ktemp(:)=dcmplx(P(:))
                  mm=mass(f1)
                  if (f1.gt.0) then
                     call UbkslashRL(J,Ktemp,Jtemp,1d0,1d0)
                     J(:)=(Jtemp(:)+mm*J(:))/(dotpr(P,P)-mm**2)
                  else
                     call kslashURL(-Ktemp,J,Jtemp,1d0,1d0)
                     J(:)=(Jtemp(:)+mm*J(:))/(dotpr(P,P)-mm**2)
                  endif
               endif
            endif
            Jv(k,k+l,:)=J(:)
            flavV(k,k+l)=fv
         enddo                  ! endloop k
      enddo                     ! endloop l



      P(:)=kmom(O(1),:)
      h=pol(O(1)) 
      nflav=flav(O(1)) 
      if (abs(nflav).le.6) then
         if (h.eq.0) then
            if (nflav.ne.0) then
               write(*,"(' Particle ',i1,' has flavor ',i1)") k,nflav 
               write(*,*) 'Check aborted'
               stop
            else
               J0(:)=dcmplx(P(:))/50d0
            endif
         else
               call wavefunction(P,nflav,h,J0)
         endif
      endif
      J(:)=Jv(1,Nlegs-2,:)
      fv=flavV(1,Nlegs-2)
      if (nflav.ne.-fv) then
         write(*,*) "flavor mismatch", fv,nflav
         stop
      endif
      if (nflav .eq. 0) then
         weight=cdotpr(J0,J)
      else
         weight=sprod(J0,J)
      endif
      return
      end

