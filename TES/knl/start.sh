qsub -q knl -l nodes=1:knl -A genMC -I 

export KMP_AFFINITY=compact, granularity=thread
export KMP_PLACE_THREADS=1s,64c,4t
