	subroutine Vegas(result,absacc,relacc,ndim,ncall,
     .                 maxiter,init)
! Uses kahan summation to guarantee identical results independent of number of threads
!	USE IFPORT
	
	implicit none
	integer::mxdim
	parameter(mxdim=26)
	integer:: ndim, ncall, maxiter, neval,init,i
	double precision fun, sfun, sfun2,ranmp
	double precision sint,sint2,sweight
	double precision fun2, weight
	double precision r, dr, xo, xn, err
	double precision Ingrid,Incall
	double precision result,absacc,relacc,integrand
	integer:: iter, calls, dim, grid, g
	integer:: ngrid
	parameter (ngrid = 10)
	double precision xi(ngrid, MXDIM), d(ngrid, MXDIM)
	double precision x(mxdim), imp(ngrid), tmp(ngrid - 1)
	integer:: pos(NDIM)
	double precision t,dt,cfun,cfun2,cd(ngrid,MXDIM)

	integer:: nlength
	common/nlength/nlength
        save xi
!	integer njets
!	double precision sqrtS,ptmin,etamax,Rmin
!	common /collider/ njets,sqrtS,ptmin,etamax,Rmin

	if(init<=0)then
	   do i=1,ndim
	      xi(1,i)=1d0
           enddo
*       define the initial distribution of intervals
	   Ingrid=1d0/real(ngrid)
	   do dim = 1, ndim
	      do grid = 1, ngrid
	         r = real(grid)*Ingrid
	         xi(grid, dim) = r
	      enddo
	   enddo
	endif
	if (init<=1)then
	   neval = 0
	   sint=0d0
	   sweight=0d0
	   sint2=0d0
	   iter=0
	endif
*       iterations loop
 1	continue
	iter = iter + 1
*       initialize iteration variables
	sfun = 0d0
	sfun2 = 0d0
	d(:,:)=0d0
	cfun = 0d0
	cfun2 = 0d0
	cd(:,:)=0d0
	Incall=1d0/real(ncall)
!$omp  parallel do
!$omp& schedule(dynamic)
!$omp& default(private)
!$omp& shared(incall,xi,ncall,ndim,sfun,sfun2,d,cfun,cfun2,cd)

	do calls = 1, ncall
	   weight = Incall
!$omp critical
	   call ranset()
!$omp end critical	   
           do i=1,ndim
! Vegas uses the random numbers 1,..., ndim. From ndim+1,...,mxdim are random numbers generatedd for non-vegas use	      
              x(i)=ranmp()
           enddo
!!$omp end critical
	   do dim = 1, ndim
	      r = x(dim)*ngrid + 1
	      grid = int(r)
	      xo = 0
	      if( grid > 1 ) xo = xi(grid - 1, dim)
	      xn = xi(grid, dim) - xo
	      x(dim) = xo + (r - grid)*xn
	      pos(dim) = grid
	      weight = weight*xn*ngrid
	   enddo
*       compute the integrand
	   fun=Integrand(x,weight)
	   fun = fun*weight
	   fun2 = fun**2
!$omp critical
           t=sfun+fun
	   if (abs(sfun)>=abs(fun)) then
	      cfun=cfun+((sfun-t)+fun)
	   else
	      cfun=cfun+((fun-t)+sfun)
	   endif
	   sfun=t
           t=sfun2+fun2
	   if (abs(sfun2)>=abs(fun2)) then
	      cfun2=cfun2+((sfun2-t)+fun2)
	   else
	      cfun2=cfun2+((fun2-t)+sfun2)
	   endif
	   sfun2=t
	   do dim = 1, ndim
	      i=pos(dim)
	      dt=d(i,dim)
	      t=dt+fun2
	      if (abs(dt)>=abs(fun2)) then
		 cd(i,dim)=cd(i,dim)+((dt-t)+fun2)
	      else
		 cd(i,dim)=cd(i,dim)+((fun2-t)+dt)
	      endif
	      d(i,dim)=t
	   enddo
!$omp end critical
	enddo
!$omp end parallel do
	sfun=sfun+cfun
	sfun2=sfun2+cfun2
	d(:,:)=d(:,:)+cd(:,:)
	neval = neval + ncall
*       compute the integral and error values
	err = 0
	fun2 = sfun**2
	sint2 = sint2 + fun2
	r = sfun2*ncall - fun2
	if( r .ne. 0 ) then
	   weight = fun2/abs(r)*(ncall - 1)
	   sweight = sweight + weight
	   sint = sint + sfun*weight
	endif
	if( sweight == 0 ) then
	   result = 0
	else
	   r = sint/sweight
	   result = r
*       if the integrand is very close to zero, it is pointless (and costly)
*       to insist on a certain relative accuracy
	   if( abs(r) > absacc ) then
              r = sint2/(sint*r)
	      if( r > err ) then
		 err = r
	      endif
	   endif
	endif
	err = sqrt(err/iter)

!        print *, "iteration ", iter, ":",result,"+/-",result*err
        if (abs(result) < 1d-9) then
          print *, "integral is zero, exiting"
          return
        endif
	if( err <= relacc ) then
	   print *, "iteration ", iter, ":",result,"+/-",result*err
	   return
	endif

*       redefine the grid (importance sampling)
*       - smooth the f^2 value stored for each interval
	do dim = 1, ndim
	   xo = d(1, dim)
	   xn = d(2, dim)
	   d(1, dim) = .5D0*(xo + xn)
	   x(dim) = d(1, dim)
	   do grid = 2, ngrid - 1
	      r = xo + xn
	      xo = xn
	      xn = d(grid + 1, dim)
	      d(grid, dim) = (r + xn)/3D0
	      x(dim) = x(dim) + d(grid, dim)
	   enddo
	   d(ngrid, dim) = .5D0*(xo + xn)
	   x(dim) = x(dim) + d(ngrid, dim)
	enddo

*       - compute the importance function of each interval
	do dim = 1, ndim
	   r = 0
	   do grid = 1, ngrid
	      imp(grid) = 0
	      if( d(grid, dim) > 0 ) then
		 xo = x(dim)/d(grid, dim)
		 imp(grid) = ((xo - 1)/xo/log(xo))**1.5D0
	      endif
	      r = r + imp(grid)
	   enddo
	   r = r/ngrid

*       - redefine the size of each interval
	   dr = 0
	   xn = 0
	   g = 0
	   do grid = 1, ngrid - 1
	      do while( dr < r )
		 g = g + 1
		 dr = dr + imp(g)
		 xo = xn
		 xn = xi(g, dim)
	      enddo
	      dr = dr - r
	      tmp(grid) = xn - (xn - xo)*dr/imp(g)
	   enddo
	   do grid = 1, ngrid - 1
	      xi(grid, dim) = tmp(grid)
	   enddo
	   xi(ngrid, dim) = 1
	enddo

	if( iter >= maxiter ) then
	   print *,"iterations reached set maximum of ",maxiter
	   print *, "iteration ", iter, ":",result,"+/-",result*err
	   return
	endif

	goto 1

	end


	subroutine ranset()
	implicit none
	integer i,mxrnd,rpoint
	parameter(mxrnd=50)
	double precision ran2
	double precision r(mxrnd)
	common /ranarr/ r,rpoint
!$omp threadprivate(/ranarr/)
	
	do i=1,mxrnd
	   r(i)=ran2()
	enddo
	rpoint=0

	return
	end
	
	double precision function ranmp()
	implicit none
	integer mxrnd,rpoint
	parameter(mxrnd=50)
	double precision r(mxrnd)
	common /ranarr/ r,rpoint
!$omp threadprivate(/ranarr/)

	rpoint=rpoint+1
	if (rpoint.gt.mxrnd) then
	   write(*,*) 'exceeded maximim # of random point',rpoint,mxrnd
	   ranmp=0d0
	else
	   ranmp=r(rpoint)
	endif
	return
	end
	



