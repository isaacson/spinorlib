      program TES

c      USE IFPORT

      implicit none
      include 'omp_lib.h'

      character pdfgrid*40
      integer:: i,t,threadmin,threadmax
      integer:: mth,njets,idum,Nc
      integer ndim,ncall,maxiter,init
      double precision result,pi,gam
      parameter (mth=256)
      parameter (pi=3.141592654d0)
      double precision rone,r(mth)
      double precision tone,t0,t1,time(mth)
      double precision absacc,relacc,Z
      double precision sqrtS,ptmin,etamax,Rmin
      common /collider/ sqrtS,ptmin,etamax,Rmin,njets
      COMMON /ranno/ idum

      njets=7
      sqrtS=14000d0
      ptmin=20d0
      etamax=2.5
      Rmin=0.4d0
      relacc=1d-6
      pdfgrid='CT14llo.pds'
      Nc=3
      ndim=2
      ncall=100000
      maxiter=10
      init=0

      call SetCT14(pdfgrid)
      absacc=1d-8

      gam=1d0
      do i=1,njets-1
         gam=gam*i
      enddo
      Z=0.389379304E9 ! GeV^2 --> pb
      Z=Z*(4d0*pi)**3
      Z=Z*(Nc/8d0)**njets
      Z=Z*(Nc**2-1d0)
      Z=Z*(njets+1)
!      Z=Z*(njets+1)*(njets-1)/4d0/gam/gam
      Z=Z*4d0*2.0**(njets+2)/(2.0*(Nc**2-1))**2
!      Z=Z/2d0
!      if (njets.ge.2) Z=Z*4d0
!     if (njets.ge.3) Z=Z*5d0
!      if (njets.ge.4) Z=Z*6d0
!      if (njets.ge.5) Z=Z*7d0
!     if (njets.ge.6) Z=Z*8d0
      Z=Z*(2d0*pi)**(njets-2)*2d0
!      Z=0.389379304E9*(4d0*pi)**3/4/(4*pi)**(njets-1)/2d0
!      Z=0.389379304E9
!      Z=Z*(2d0*pi)**(4-3*njets)
!      Z=Z*Nc**njets*(Nc**2-1d0)
!      Z=Z*(njets+1)
!      Z=Z/4d0/(Nc**2-1)**2
      
      threadmax=omp_get_max_threads()
      threadmax=1
!set threadmin=threadmax to run at maximum number of threads only
      threadmin=threadmax
      threadmin=1
      if (threadmax>mth) then
         write(*,*) 'increase parameter mth to at least ',threadmax
         stop
      endif
      write(*,*)
      write(*,*)
      write(*,*)
      write(*,*) 'maximum number of threads available: ',threadmax
      
      do t=threadmin,threadmax
         idum=-1
         write(*,*) 
         write(*,*) '>>>>>>>>',t,' threads <<<<<<<<'
         write(*,*)
         call omp_set_num_threads(t)
         t0=omp_get_wtime()
         call vegas(result,absacc,relacc,ndim,ncall,maxiter,init)
         t1=omp_get_wtime()
         if (t==1) then 
            tone=t1-t0
            rone=result*Z/ncall
         endif
         time(t)=t1-t0
         r(t)=result*Z/ncall
      enddo
      write(*,*)
      write(*,*) 'timing results:'
      write(*,3) 100d0*relacc
      do t=threadmin,threadmax
         if (threadmin .ne. threadmax) then
            write(*,1) t,time(t),tone/time(t),r(t)
         else
            write(*,2) t,time(t),r(t)
         endif
      enddo
      
      if (threadmin .ne. threadmax) then
         write(*,*)
         write(*,*) "set 'threadmin=threadmax'"
         write(*,*) "for normal running with max number of threads only" 
         write(*,*)
      endif

 1    format('threads: ',i3,' time: ',f8.2,'  ratio: ',f8.2,
     &     ' x-section: ',e18.12,' fb')
 2    format('threads: ',i3,' time: ',f8.2,
     &     '    x-section: ',e18.12,' fb')
 3     format('(relative error',e18.12,'%)')
      
      end

