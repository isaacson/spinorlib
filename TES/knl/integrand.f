      double precision function integrand(x,weight)
      implicit none
      logical pass
      integer mxdim,mxpart,i,j,n,m
      parameter (mxdim=26,mxpart=12)
      double precision weight,wgt,taumin,r1,r2,x1,x2,fx1,fx2,as
      double precision pt,eta,Rsep,ranmp
      double precision CT14Pdf,CT14Alphas
      double precision sqrtS,ptmin,etamax,Rmin,shat,Q
      double precision x(mxdim)
      double precision pa(4),pb(4),psys(4),p(mxpart,4),ptemp(4)
      double precision ps(4,mxpart-2),xm(mxpart-2)
      common /collider/ sqrtS,ptmin,etamax,Rmin,n

      taumin=(n*ptmin/sqrtS)**2
      xm=0d0
      ps=0d0
      r1=x(1)
      r2=x(2)
      call genpapb(taumin,r1,r2,x1,x2,wgt)
      weight=wgt

      shat=sqrt(x1*x2)*sqrtS

      pa(1)=0d0
      pa(2)=0d0
      pa(3)=0.5d0*sqrtS*x1
      pa(4)=0.5d0*sqrtS*x1
      pb(1)=0d0
      pb(2)=0d0
      pb(3)=-0.5d0*sqrtS*x2
      pb(4)= 0.5d0*sqrtS*x2
      psys(:)=pa(:)+pb(:)

      call ramboo(n,shat,psys,xm,ps,wgt)
      weight=weight*wgt

      p(1,:)=-pa(:)
      p(2,:)=-pb(:)
      do i=1,n
         p(i+2,:)=ps(:,i)
      enddo

      pass=.true.
      do i=3,n+2
         if (pt(p(i,:)).lt.ptmin) pass=.false.
         if (abs(eta(p(i,:))).gt.etamax) pass=.false.
      enddo
      do i=3,n+1
         do j=i+1,n+2
            if (Rsep(p(i,:),p(j,:)).lt.Rmin) pass=.false.
         enddo
      enddo
      
!      do i=1,n+2
!         write(*,*) i,p(i,:)
!     enddo
      if (pass) then
         m=2+ranmp()*(n+1)
         ptemp(:)=p(2,:)
         p(2,:)=p(m,:)
         p(m,:)=ptemp(:)
         call BG(p,n+2,wgt)
         Q=shat
         Q=91.188d0
         fx1=CT14Pdf(0,x1,Q)
         fx2=CT14Pdf(0,x2,Q)
         as=CT14Alphas(Q)
         wgt=as**n*fx1*fx2
         integrand=wgt*weight/shat**2
      else
         integrand=0d0
      endif
      
      return
      end

      subroutine BG(kmom,Nlegs,weight)
C      subroutine BG(kmom,pol,Nlegs,weight)
      implicit none

      integer mxpart
      parameter (mxpart=12)
      integer Nm1,Nlegs,i,l,k,n,m
      double precision P(4),kmom(mxpart,4),dotpr,weight,pt
      double complex Jlk(Nlegs,Nlegs,4),K1(4),K2(4),Ktemp(4),
     & J(4),J0(4),J1(4),J2(4),J3(4),Jtemp(4),cdotpr
C      integer pol(Nlegs)

      Nm1=Nlegs-1

      Jlk(:,:,:)=0d0

C---setup polarization vectors for Nm1 particles
      do k=2,Nlegs
         P(:)=kmom(k,:)
c      h=pol(k) 
c     call helicity(P,h,J)
         call orthogonal(P,J)
C---initialize diagonal elements
         Ktemp(:)=dcmplx(P(:))
!         write(6,*) 'k,cdotpr(J,Ktemp)',k,cdotpr(J,Ktemp)
         Jlk(k-1,k-1,:)=J(:)
      enddo

C-----create cumulative sums of momenta
      pt=4d0*dotpr(kmom(1,:),kmom(Nlegs,:))**4
      do i=1,nlegs-1
         pt=pt/dotpr(kmom(i,:),kmom(i+1,:))
      enddo
      pt=pt/dotpr(kmom(Nlegs,:),kmom(1,:))

      P(:)=kmom(1,:)
      do i=1,Nm1
      P(:)=P(:)+kmom(1+i,:)
      kmom(1+i,:)=P(:)      
      enddo

!      write(6,*) 'Nm1',Nm1
      do l=1,Nm1-1
        do k=1,Nm1-l
            J(:)=0d0
            do n=k,k+l-1
              K1(:)=dcmplx(kmom(n+1,:)-kmom(k,:))
              K2(:)=dcmplx(kmom(k+l+1,:)-kmom(n+1,:))
              J1(:)=Jlk(k,n,:)
              J2(:)=Jlk(n+1,k+l,:)
          J(:)=J(:)
     &        +2d0*cdotpr(J1,K2)*J2(:)
     &        -2d0*cdotpr(J2,K1)*J1(:)
     &        +cdotpr(J1,J2)*(K1(:)-K2(:))
            enddo  ! endloop n

      do n=k,k+l-2
        do m=n+1,k+l-1
          J1(:)=Jlk(k,n,:)
          J2(:)=Jlk(n+1,m,:)
          J3(:)=Jlk(m+1,k+l,:);
          J(:)=J(:)
     &    +2d0*cdotpr(J1,J3)*J2(:)
     &        -cdotpr(J1,J2)*J3(:)
     &        -cdotpr(J2,J3)*J1(:)
      enddo ! endloop m
      enddo ! endloop n

C---Divide by propagator for all except the last
        if (l < Nm1-1) then
        P(:)=kmom(k+l+1,:)-kmom(k,:)
        J(:)=J(:)/dotpr(P,P)
      endif
        Jlk(k,k+l,:)=J(:)
C---check
        Jtemp(:)=J(:)
        Ktemp(:)=dcmplx(kmom(k+l+1,:)-kmom(k,:))
!      write(6,*) 'l,k,k+l,dotpr(Jtemp,ktemp)',
!     &  l,k,k+l,cdotpr(Jtemp,ktemp)
      enddo  ! endloop k
      enddo  ! endloop l
      P(:)=kmom(1,:)
c     call helicity(P,-1,J0)
      call orthogonal(P,J0)
      J(:)=Jlk(1,Nm1,:)
      weight=cdabs(cdotpr(J0,J))**2
c      write(6,*) 'weight',weight,pt,weight/pt
      return
      end

      subroutine genpapb(taumin,r1,r2,x1,x2,wgt)
      implicit none
      double precision taumin,r1,r2,x1,x2,wgt

      x1=taumin**(r2*r1)
      x2=taumin**(r2*(1d0-r1))
      wgt=r2*taumin**r2*log(taumin)**2

      return
      end

!--- RAMBOO decays a vector PSYS into N particles ( PSYS(i)=PS(i,1)+...+PS(i,N) ) 
!--- Particle i has momentum PS(4,i) and a mass of AM(i)
!--- WT is the event weight.
!--- Input:  N  = number of final state particles
!---         Et = PSYS^2
!---         PSYS(4) is vector to be decayed
!---         AM(100) contains the particle masses
!--- Output: PS(4,100) contains the final state 4 vectors
!---         WT is the phase space weight
!---
      SUBROUTINE RAMBOO(N,ET,PSYS,AM,PS,WT)
      IMPLICIT REAL*8(A-H,O-Z)
      integer mxpart
      parameter (mxpart=12)
      DIMENSION PSYS(4),AM(mxpart-2),PS(4,mxpart-2),PL(4),PC(4)
      dimension P(4,mxpart-2)
      CALL RAMBO(N,ET,AM,P,WT,1)
      DO 10 I=1,N
        DO 20 J=1,4
          PC(J)=P(J,I)
   20   CONTINUE
        CALL BOOST_R(ET,PSYS,PC,PL)
        DO 30 J=1,4
          PS(J,I)=PL(J)
   30   CONTINUE
   10 CONTINUE
      END
*
      SUBROUTINE BOOST_R(Q,PBOO,PCM,PLB)
      REAL*8 PBOO(4),PCM(4),PLB(4),Q,FACT
         PLB(4)=(PBOO(4)*PCM(4)+PBOO(3)*PCM(3)
     &             +PBOO(2)*PCM(2)+PBOO(1)*PCM(1))/Q
         FACT=(PLB(4)+PCM(4))/(Q+PBOO(4))
         DO 10 J=1,3
  10     PLB(J)=PCM(J)+FACT*PBOO(J)
      RETURN
      END

      SUBROUTINE RAMBO(N,ET,XM,P,WT,LW)                            
C------------------------------------------------------            
C                                                                  
C                       RAMBO                                      
C                                                                  
C             RA(NDOM)  M(OMENTA)  BO(OSTER)                       
C                                                                  
C    A DEMOCRATIC MULTI-PARTICLE PHASE SPACE GENERATOR             
C    AUTHORS:  S.D. ELLIS,  R. KLEISS,  W.J. STIRLING              
C                                                                  
C    N  = NUMBER OF PARTICLES (>1, IN THIS VERSION <101)           
C    ET = TOTAL CENTRE-OF-MASS ENERGY                              
C    XM = PARTICLE MASSES ( DIM=N )                                
C    P  = PARTICLE MOMENTA ( DIM=(4,N) )                           
C    WT = WEIGHT OF THE EVENT                                      
C    LW = FLAG FOR EVENT WEIGHTING:                                
C         LW = 0 WEIGHTED EVENTS                                   
C         LW = 1 UNWEIGHTED EVENTS ( FLAT PHASE SPACE )            
C------------------------------------------------------            
      IMPLICIT REAL*8(A-H,O-Z)                                     
      integer mxpart,mxdim
      parameter (mxpart=12,mxdim=26)
      DIMENSION XM(mxpart-2),P(4,mxpart-2),Q(4,100),Z(100),R(4),                 
     .   B(3),P2(100),XM2(100),E(100),V(100),IWARN(5)              
      DATA ACC/1.D-14/,ITMAX/6/,IBEGIN/0/,IWARN/5*0/               
      save ACC,ITMAX,IWARN               
      SAVE IBEGIN,TWOPI,PO2LOG,Z
      
!$omp threadprivate(acc,itmax,ibegin,iwarn)     
C                                                                  
C INITIALIZATION STEP: FACTORIALS FOR THE PHASE SPACE WEIGHT       
      IF(IBEGIN.NE.0) GOTO 103                                     
      IBEGIN=1                                                     
      TWOPI=8.*DATAN(1.D0)                                         
      PO2LOG=DLOG(TWOPI/4.)                                        
      Z(2)=PO2LOG                                                  
      DO 101 K=3,100                                               
  101 Z(K)=Z(K-1)+PO2LOG-2.*DLOG(DFLOAT(K-2))                      
      DO 102 K=3,100                                               
  102 Z(K)=(Z(K)-DLOG(DFLOAT(K-1)))                                
C                                                                  
C CHECK ON THE NUMBER OF PARTICLES                                 
  103 IF(N.GT.1.AND.N.LT.101) GOTO 104                             
      PRINT 1001,N                                                 
      STOP                                                         
C                                                                  
C CHECK WHETHER TOTAL ENERGY IS SUFFICIENT; COUNT NONZERO MASSES   
  104 XMT=0.                                                       
      NM=0                                                         
      DO 105 I=1,N                                                 
      IF(XM(I).NE.0.D0) NM=NM+1                                    
  105 XMT=XMT+DABS(XM(I))                                          
      IF(XMT.LE.ET) GOTO 106                                       
      PRINT 1002,XMT,ET                                            
      STOP                                                         
C                                                                  
C CHECK ON THE WEIGHTING OPTION                                    
  106 IF(LW.EQ.1.OR.LW.EQ.0) GOTO 201                              
      PRINT 1003,LW                                                
      STOP                                                         
C                                                                  
C THE PARAMETER VALUES ARE NOW ACCEPTED                            
C                                                                  
C GENERATE N MASSLESS MOMENTA IN INFINITE PHASE SPACE              
  201 DO 202 I=1,N                                                 
      C=2.*ranmp()-1.                                                
      S=DSQRT(1.-C*C)                                              
      F=TWOPI*ranmp()                                                
      Q(4,I)=-DLOG(ranmp()*ranmp())                                    
      Q(3,I)=Q(4,I)*C                                              
      Q(2,I)=Q(4,I)*S*DCOS(F)                                      
  202 Q(1,I)=Q(4,I)*S*DSIN(F)                                      
C                                                                  
C CALCULATE THE PARAMETERS OF THE CONFORMAL TRANSFORMATION         
      DO 203 I=1,4                                                 
  203 R(I)=0.                                                      
      DO 204 I=1,N                                                 
      DO 204 K=1,4                                                 
  204 R(K)=R(K)+Q(K,I)                                             
      RMAS=DSQRT(R(4)**2-R(3)**2-R(2)**2-R(1)**2)                  
      DO 205 K=1,3                                                 
  205 B(K)=-R(K)/RMAS                                              
      G=R(4)/RMAS                                                  
      A=1./(1.+G)                                                  
      X=ET/RMAS                                                    
C                                                                  
C TRANSFORM THE Q'S CONFORMALLY INTO THE P'S                       
      DO 207 I=1,N                                                 
      BQ=B(1)*Q(1,I)+B(2)*Q(2,I)+B(3)*Q(3,I)                       
      DO 206 K=1,3                                                 
  206 P(K,I)=X*(Q(K,I)+B(K)*(Q(4,I)+A*BQ))                         
  207 P(4,I)=X*(G*Q(4,I)+BQ)                                       
C                                                                  
C RETURN FOR UNWEIGHTED MASSLESS MOMENTA                           
      WT=1.D0                                                      
      IF(NM.EQ.0.AND.LW.EQ.1) RETURN                               
C                                                                  
C CALCULATE WEIGHT AND POSSIBLE WARNINGS                           
      WT=PO2LOG                                                    
      IF(N.NE.2) WT=(2.*N-4.)*DLOG(ET)+Z(N)                        
      IF(WT.GE.-180.D0) GOTO 208                                   
      IF(IWARN(1).LE.5) PRINT 1004,WT                              
      IWARN(1)=IWARN(1)+1                                          
  208 IF(WT.LE. 174.D0) GOTO 209                                   
      IF(IWARN(2).LE.5) PRINT 1005,WT                              
      IWARN(2)=IWARN(2)+1                                          
C                                                                  
C RETURN FOR WEIGHTED MASSLESS MOMENTA                             
  209 IF(NM.NE.0) GOTO 210                                         
      WT=DEXP(WT)                                                  
      RETURN                                                       
C                                                                  
C MASSIVE PARTICLES: RESCALE THE MOMENTA BY A FACTOR X             
  210 XMAX=DSQRT(1.-(XMT/ET)**2)                                   
      DO 301 I=1,N                                                 
      XM2(I)=XM(I)**2                                              
  301 P2(I)=P(4,I)**2                                              
      ITER=0                                                       
      X=XMAX                                                       
      ACCU=ET*ACC                                                  
  302 F0=-ET                                                       
      G0=0.                                                        
      X2=X*X                                                       
      DO 303 I=1,N                                                 
      E(I)=DSQRT(XM2(I)+X2*P2(I))                                  
      F0=F0+E(I)                                                   
  303 G0=G0+P2(I)/E(I)                                             
      IF(DABS(F0).LE.ACCU) GOTO 305                                
      ITER=ITER+1                                                  
      IF(ITER.LE.ITMAX) GOTO 304                                   
      PRINT 1006,ITMAX                                             
      GOTO 305                                                     
  304 X=X-F0/(X*G0)                                                
      GOTO 302                                                     
  305 DO 307 I=1,N                                                 
      V(I)=X*P(4,I)                                                
      DO 306 K=1,3                                                 
  306 P(K,I)=X*P(K,I)                                              
  307 P(4,I)=E(I)                                                  
C                                                                  
C CALCULATE THE MASS-EFFECT WEIGHT FACTOR                          
      WT2=1.                                                       
      WT3=0.                                                       
      DO 308 I=1,N                                                 
      WT2=WT2*V(I)/E(I)                                            
  308 WT3=WT3+V(I)**2/E(I)                                         
      WTM=(2.*N-3.)*DLOG(X)+DLOG(WT2/WT3*ET)                       
      IF(LW.EQ.1) GOTO 401                                         
C                                                                  
C RETURN FOR  WEIGHTED MASSIVE MOMENTA                             
      WT=WT+WTM                                                    
      IF(WT.GE.-180.D0) GOTO 309                                   
      IF(IWARN(3).LE.5) PRINT 1004,WT                              
      IWARN(3)=IWARN(3)+1                                          
  309 IF(WT.LE. 174.D0) GOTO 310                                   
      IF(IWARN(4).LE.5) PRINT 1005,WT                              
      IWARN(4)=IWARN(4)+1                                          
  310 WT=DEXP(WT)                                                  
      RETURN                                                       
C                                                                  
C UNWEIGHTED MASSIVE MOMENTA REQUIRED: ESTIMATE MAXIMUM WEIGHT     
  401 WT=DEXP(WTM)                                                 
      IF(NM.GT.1) GOTO 402                                         
C                                                                  
C ONE MASSIVE PARTICLE                                             
      WTMAX=XMAX**(4*N-6)                                          
      GOTO 405                                                     
  402 IF(NM.GT.2) GOTO 404                                         
C                                                                  
C TWO MASSIVE PARTICLES                                            
      SM2=0.                                                       
      PM2=0.                                                       
      DO 403 I=1,N                                                 
      IF(XM(I).EQ.0.D0) GOTO 403                                   
      SM2=SM2+XM2(I)                                               
      PM2=PM2*XM2(I)                                               
  403 CONTINUE                                                     
      WTMAX=((1.-SM2/(ET**2))**2-4.*PM2/ET**4)**(N-1.5)            
      GOTO 405                                                     
C                                                                  
C MORE THAN TWO MASSIVE PARTICLES: AN ESTIMATE ONLY                
  404 WTMAX=XMAX**(2*N-5+NM)                                       
C                                                                  
C DETERMINE WHETHER OR NOT TO ACCEPT THIS EVENT                    
  405 W=WT/WTMAX                                                   
      IF(W.LE.1.D0) GOTO 406                                       
      IF(IWARN(5).LE.5) PRINT 1007,WTMAX,W                         
      IWARN(5)=IWARN(5)+1                                          
  406 CONTINUE                                                     
      IF(W.LT.ranmp()) GOTO 201                                      
      WT=1.D0                                                      
      RETURN                                                       
 1001 FORMAT(' RAMBO FAILS: # OF PARTICLES =',I5,' IS NOT ALLOWED')
 1002 FORMAT(' RAMBO FAILS: TOTAL MASS =',D15.6,' IS NOT',         
     . ' SMALLER THAN TOTAL ENERGY =',D15.6)                       
 1003 FORMAT(' RAMBO FAILS: LW=',I3,' IS NOT AN ALLOWED OPTION')   
 1004 FORMAT(' RAMBO WARNS: WEIGHT = EXP(',F20.9,') MAY UNDERFLOW')
 1005 FORMAT(' RAMBO WARNS: WEIGHT = EXP(',F20.9,') MAY  OVERFLOW')
 1006 FORMAT(' RAMBO WARNS:',I3,' ITERATIONS DID NOT GIVE THE',    
     . ' DESIRED ACCURACY =',D15.6)                                
 1007 FORMAT(' RAMBO WARNS: ESTIMATE FOR MAXIMUM WEIGHT =',D15.6,  
     . '     EXCEEDED BY A FACTOR ',D15.6)                         
      END                                                          

      
      subroutine orthogonal(Pmom,J) 
      implicit none

      double precision phi,s,z,x,y,Pmom(4),cosphi,sinphi,norm,pi,ranmp
      double complex J1(4),J(4)
      parameter (pi=3.141592654d0)

      z=2d0*ranmp()-1d0
      s=sqrt(abs(1d0-z*z))
      phi=2d0*pi*ranmp()
      sinphi=sin(phi)
      cosphi=cos(phi)
      x=s*sinphi
      y=s*cosphi
      J1(4)=0d0 
      J1(1)=dcmplx(y*Pmom(3)-z*Pmom(2))
      J1(2)=dcmplx(z*Pmom(1)-x*Pmom(3))
      J1(3)=dcmplx(x*Pmom(2)-y*Pmom(1))
      norm=sqrt(abs(J1(1)**2+J1(2)**2+J1(3)**2))

      if (norm > 0.0d0) then
         J1(1:3)=J1(1:3)/norm
      else 
         J1(:)=0d0
      endif

      J(:)=J1(:)
      return
      end

      double precision function dotpr(K1,K2)
      implicit none
      double precision K1(4),K2(4)
      dotpr=K1(4)*K2(4)-K1(1)*K2(1)-K1(2)*K2(2)-K1(3)*K2(3)
      return
      end

      double complex function cdotpr(J1,J2)
      implicit none
      double complex J1(4),J2(4)
      cdotpr=J1(4)*J2(4)-J1(1)*J2(1)-J1(2)*J2(2)-J1(3)*J2(3)
      return
      end

C  (C) Copr. 1986-92 Numerical Recipes Software ]2w.1,r1..

C--- Version where idum is passed via common block
      double precision FUNCTION ran2()
      implicit none
      INTEGER idum,IA,IM,IQ,IR,NTAB,NDIV
      double precision AM,EPS,RNMX
      PARAMETER (IA=16807,IM=2147483647,AM=1d0/IM,IQ=127773,
     .IR=2836,NTAB=32,NDIV=1+(IM-1)/NTAB,EPS=3d-16,RNMX=1d0-EPS)
      INTEGER j,k,iv(NTAB),iy
      COMMON /ranno/ idum
      DATA iv /NTAB*0/, iy /0/
      SAVE iv,iy


      if (idum.le.0.or.iy.eq.0) then
        idum=max(-idum,1)
        do 11 j=NTAB+8,1,-1
          k=idum/IQ
          idum=IA*(idum-k*IQ)-IR*k
          if (idum.lt.0) idum=idum+IM
          if (j.le.NTAB) iv(j)=idum
11      continue
        iy=iv(1)
      endif
      k=idum/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum=idum+IM
      j=1+iy/NDIV
      iy=iv(j)
      iv(j)=idum
      ran2=min(AM*dble(iy),RNMX)

c      write(6,*) 'idum',idum
c      write(6,*) 'AM=',AM
c      write(6,*) 'iy=',iy
c      write(6,*) 'AM*dble(iy)',AM*dble(iy)
c      write(6,*) 'ran2',ran2

      return
      END

      double precision function pt(p)
      implicit none
      double precision p(4)

      pt=sqrt(p(1)**2+p(2)**2)

      return
      end

      double precision function eta(p)
      implicit none
      double precision p(4)

      eta=0.5d0*log((p(4)+p(3))/(p(4)-p(3)))

      return
      end

      double precision function dphi(p1,p2)
      implicit none
      double precision np1,np2
      double precision p1(4),p2(4)

      np1=sqrt(p1(1)*p1(1)+p1(2)*p1(2))
      np2=sqrt(p2(1)*p2(1)+p2(2)*p2(2))
      dphi=acos((p1(1)*p2(1)+p1(2)*p2(2))/np1/np2)

      return
      end

      double precision function Rsep(p1,p2)
      implicit none
      double precision eta,dphi
      double precision p1(4),p2(4)

      Rsep=sqrt((eta(p1)-eta(p2))**2+dphi(p1,p2)**2)

      return
      end

      
