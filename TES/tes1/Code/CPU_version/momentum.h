template<typename T>
struct momentum {
  T p[4];
  momentum<T> &operator+=(const momentum<T> &);
};

template<typename T> momentum<T> momentum_(T t, T x, T y, T z);
template<typename T> momentum<T> operator+(const momentum<T> P1, const momentum<T> P2);
template<typename T> momentum<T> operator-(const momentum<T> P1, const momentum<T> P2);
template<typename T> momentum<T> operator-(const momentum<T> P1);
template<typename T> T operator*(momentum<T> P1, momentum<T> P2);
template<typename T> momentum<T> operator*(const T x, const momentum<T> P1);
template<typename T> momentum<T> operator*(const momentum<T> P1, const T x);
template<typename T> momentum<T> operator/(const momentum<T> P1,const T x);
