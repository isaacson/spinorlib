#include <cstdio>
#include <stdlib.h>
#include <iostream>
#include<math.h>
#include "KahanAdder.h"

using namespace std;

extern void init(int N);
extern bool event(int N,float Ecm,float Ptmin,float etamax,float Rmin, float& wgt);

int main(void)
{
  //
  // run set-up
  //
  int N=11;
  int events=10000000;
  float Ecm=14000.0;
  float Ptmin=20.0;
  float etamax=2.5;
  float Rmin=0.4;
  //
  init(N);
  srand(1);
  KahanAdder<double> xsection,x2section;
  clock_t start=clock();
  for (int i=0;i<events;i++) {
    float wgt;
    bool pass=event(N,Ecm,Ptmin,etamax,Rmin,wgt);
    //cout<<wgt<<endl;
      if (pass) {
	xsection+=wgt;
	x2section+=wgt*wgt;
      }
  }
  clock_t stop=clock();
  double time=(double) (stop-start)/(double) CLOCKS_PER_SEC;
  double avg=xsection()/(double) events;
  double avg2=x2section()/(double) events;
  double sigma_mean=sqrt(fabs((avg2-avg*avg)/events));
  cout<<endl<<"Number of gluons: "<<N<<endl;
  cout<<endl<<endl<<"Cross section: ("<<avg<<" +/- "<<sigma_mean<<" pb)"<<endl;
  cout<<endl<<"Timing:"<<endl;
  cout      <<"          Total: "<<time<<" seconds"<<endl;
  cout      <<"      per Event: "<<time/(double) events<<" seconds"<<endl;
}
