//
// initializing device memory
//

void *g_entropy,*g_weight,*g_cut;

extern void event_init(int N,int N);

void init(int N,int total_events) 
{
  //
  // mapping device memory
  //
  cudaMalloc(&g_cut,total_events*sizeof(bool));
  cudaMalloc(&g_entropy,total_events*(N-1)*sizeof(uint2));
  cudaMalloc(&g_weight,total_events*sizeof(float));
  //
  // Kernel initializations
  //
  event_init(N,total_events);
}  
