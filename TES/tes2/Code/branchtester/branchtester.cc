#include <cstdio>
#include <iostream>
#include <math.h>
#include "momentum.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"
#include "TGaxis.h"
#include "TLegend.h"
#include "TLatex.h"

using namespace std;

extern momentum<double> *Rambo(double,int,double&);
extern double Ht(int,momentum<double>*);
extern double Aveta(int,momentum<double>*);
extern double Azimuth(int,momentum<double>*);
extern bool cuts(int,momentum<double>*,double,double,double);
extern momentum<double> *Brancher(const int,momentum<double>*,bool&,double&,const double,const double);
extern bool JetCuts(int,momentum<double>*,double,double,double);
extern momentum<double> *Cluster(int,momentum<double>*);
extern int fac(int);

int main(void)
{
  int Jets=2;
  int Iterations=1000;
  int Events=100000;
  int nev=10;
  float Ecm=7000.0/7000.0;
  float Ptmin=70.0/7000.0;
  float etamax=3.0;
  float Rmin=0.4;
  bool plots=true;
  double Smax=Ecm*Ecm;
  //
  // set-up
  //
  int N=Jets+2;
  // Rambo(N) vs Rambo(N-1)*Brancher --> plot N particle observables for comparison
  // root set-up mumbo jumbo
  gROOT->Reset();
  gROOT->SetStyle("Plain");
  TGaxis::SetMaxDigits(3);
  //gROOT->ForceStyle();
  TApplication myapp("myapp",0,0);
  // set-up root screens
  TCanvas *screen = new TCanvas("screen","Phase space comparisons",1200,600);
  gStyle->SetOptStat(0);
  gStyle->SetTitleOffset(1.0,"y");
  gStyle->SetTitleOffset(1.0,"x");
  gStyle->SetTitleSize(0.10,"x");
  gStyle->SetPadBottomMargin(0.25);
  TPad* s1=new TPad("s1","s1",0,0.33,0.5,1.0);
  s1->SetBottomMargin(0.015);
  //s1->SetLogy();
  s1->Draw();
  s1->cd();
  screen->cd(0);
  TPad* s2=new TPad("s2","s2",0,0,0.5,0.33);
  s2->SetTopMargin(0.05);
  s2->SetGridy();
  s2->SetTicks(1,0);
  s2->Draw();
  s2->cd();
  screen->cd();
  TPad* s3=new TPad("s3","s3",0.5,0.33,1.0,1.0);
  s3->SetBottomMargin(0.015);
  s3->Draw();
  s3->cd();
  screen->cd();
  TPad* s4=new TPad("s4","s4",0.5,0,1.0,0.33);
  s4->SetTopMargin(0.05);
  s4->SetGridy();
  s4->SetTicks(1,0);
  s4->Draw();
  s4->cd();
  TLegend *legend1=new TLegend(0.58,0.70,0.75,0.88);
  TLegend *legend2=new TLegend(0.13,0.70,0.30,0.88);
  TLatex *header=new TLatex(0.2,0.91,"Flat phase space: 6 jet inclusive");
  header->SetNDC();
  header->SetTextSize(0.06);
  legend1->SetFillColor(0);
  legend1->SetBorderSize(0);
  legend1->SetTextSize(0.045);
  legend2->SetFillColor(0);
  legend2->SetBorderSize(0);
  legend2->SetTextSize(0.045);
  TH1D *h1=new TH1D("","",100,0.0,0.7);
  h1->SetLineColor(kBlue);
  h1->GetXaxis()->SetNdivisions(50700,false);
  h1->GetYaxis()->SetTitle("number of events");
  h1->GetYaxis()->CenterTitle();
  h1->GetYaxis()->SetNdivisions(1003,true);
  h1->GetXaxis()->SetLabelFont(63);
  h1->GetXaxis()->SetLabelSize(16);
  h1->GetYaxis()->SetLabelFont(63);
  h1->GetYaxis()->SetLabelSize(16);
  legend1->AddEntry(h1,"Rambo + Clustering","l");
  TH1D *h2=new TH1D("","",100,0.0,0.7);
  h2->SetLineColor(kRed);
  legend1->AddEntry(h2,"Rambo + Branching","l");
  TH1D *h3=new TH1D("","",100,0.0,0.7);
  legend1->AddEntry(h3,"Rambo","l");

  TH1D *h4=new TH1D("","",100,0.0,M_PI);
  h4->SetLineColor(kBlue);
  h4->GetXaxis()->SetNdivisions(50700,false);
  h4->GetYaxis()->SetTitle("number of events");
  h4->GetYaxis()->CenterTitle();
  h4->GetYaxis()->SetNdivisions(1003,true);
  h4->GetXaxis()->SetLabelFont(63);
  h4->GetXaxis()->SetLabelSize(16);
  h4->GetYaxis()->SetLabelFont(63);
  h4->GetYaxis()->SetLabelSize(16);
  legend2->AddEntry(h4,"Rambo + Clustering","l");
  TH1D *h5=new TH1D("","",100,0.0,M_PI);
  h5->SetLineColor(kRed);
  legend2->AddEntry(h5,"Rambo + Branching","l");
  TH1D *h6=new TH1D("","",100,0.0,M_PI);
  legend2->AddEntry(h6,"Rambo","l");
  h6->GetXaxis()->SetNdivisions(50700,false);
  h6->GetYaxis()->SetTitle("number of events");
  h6->GetYaxis()->CenterTitle();
  h6->GetYaxis()->SetNdivisions(1003,true);
  h6->GetXaxis()->SetLabelFont(63);
  h6->GetXaxis()->SetLabelSize(16);
  h6->GetYaxis()->SetLabelFont(63);
  h6->GetYaxis()->SetLabelSize(16);

  TH1D hr1,hr2;
  cout<<endl<<"Number of jets = "<<Jets<<endl;
  int Njets=0,Nbranch=0;
  for (int i=0;i<Iterations;i++)
    {
      cout<<"Iteration "<<i<<endl;
      for (int j=0;j<Events;j++)
	{
	  //cout<<"   Event "<<j<<endl;
	  // Generate N+1 momenta using rambo and cluster to N jet momenta
	  double Rwgt;
	  {
	    momentum<double> *K=Rambo(Ecm,N+1,Rwgt);
	    momentum<double> *Kc=Cluster(N,K);
	    if (JetCuts(N,Kc,Ptmin,etamax,Rmin)) 
	      { Njets++;
		h1->Fill(Ht(N,Kc)/sqrt(Smax),Rwgt);
		h4->Fill(Azimuth(N,Kc),Rwgt);
	      }
	    delete K;
	    delete Kc;
	  }
	  // Generate N jet momenta, use the brancher to generate the brem phase space (i.e. calculate
	  // volume through vetoes.
	  bool pass=false;
	  double wgt;
	  double Smin=1e-8;
	  momentum<double> *K1=Rambo(Ecm,N,Rwgt);
	  if (JetCuts(N,K1,Ptmin,etamax,Rmin))
	    { double o1=Ht(N,K1),o2=Azimuth(N,K1);
	      {
		h3->Fill(o1/sqrt(Smax),Rwgt);
		h6->Fill(o2,Rwgt);
		for (int l=0;l<nev;l++) {
		  momentum<double> *Kb=Brancher(N,K1,pass,wgt,Smax,Smin);
		  if (pass) 
		    {
		      Nbranch++;
		      h2->Fill(o1/sqrt(Smax),Rwgt*wgt/nev);
		      h5->Fill(o2,Rwgt*wgt/nev);
		    } 
		  delete Kb;
		}}}
	  delete K1;
	}
      if (plots)
	{
	  screen->cd();
	  s1->cd();
	  h1->DrawCopy();
	  h2->Draw("same");
	  h3->Draw("same");
	  legend1->Draw();
	  header->Draw();
	  screen->cd();
	  s2->cd();
	  hr1=((*h1)-(*h2))/(*h1);
	  hr1.SetLineColor(kBlue);
	  hr1.SetTitle("");
	  hr1.GetYaxis()->SetTitle("");
	  hr1.GetXaxis()->SetTitle("H_{T}");
	  hr1.SetMaximum(0.10);
	  hr1.SetMinimum(-0.10);
	  hr1.GetXaxis()->SetNdivisions(510,true);
	  hr1.GetYaxis()->SetNdivisions(204,false);
	  hr1.Draw("");
	  screen->cd();
	  s3->cd();
	  h4->Draw();
	  h5->Draw("same");
	  h6->Draw("same");
	  legend2->Draw();
	  header->Draw();
	  screen->cd();
	  s4->cd();
	  hr2=((*h4)-(*h5))/(*h4);
	  hr2.SetLineColor(kBlue);
	  hr2.SetTitle("");
	  hr2.GetYaxis()->SetTitle("");
	  hr2.GetXaxis()->SetTitle("#Delta#phi_{12}");
	  hr2.SetMaximum(0.10);
	  hr2.SetMinimum(-0.10);
	  hr2.GetXaxis()->SetNdivisions(510,true);
	  hr2.GetYaxis()->SetNdivisions(204,false);
	  hr2.Draw("");
	  screen->cd();
	  screen->Update();
	}
    }
  screen->SaveAs("PS6_i1000_n100000_b10.ps");
  delete h1,h2,screen;
  double Nevents=(double) (Iterations*Events);
  cout<<"Generated events:    "<<Nevents<<endl;
  cout<<"Rambo efficiency:    "<<Njets/Nevents<<endl;
  cout<<"Brancher efficiency: "<<Nbranch/Nevents/nev<<endl;
  cout << endl << "Enter q to terminate..." << endl;
  char tmp;cin>>tmp;
}  

