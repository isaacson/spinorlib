#include <cstdio>
#include <cmath>
#include <algorithm>
#include <iostream>
#include "momentum.cc"

using namespace std;

double pi=3.14159265;
bool samplingFF=true;
bool samplingIF=true;

double uniform() 
{
  double random,s,t;
  int m,seed1=1429,seed2=9343;
  int i=(seed1/177)%177+2,j=(seed1%177)+2,k=(seed2/169)%178+1,l=seed2%169;
  static bool init=false;
  static int iranmr=96,jranmr=32;
  static double ranc=362436.0/16777216.0,rancd=7654321.0/16777216.0,
    rancm=16777213.0/16777216.0,ranu[97];
  if (init==false) 
    { init=true;
      for (int il=0;il<97;++il)
	{ s=0;t=0.5;
	  for (int jl=0;jl<24;++jl)
	    { m=(((i*j%179)*k)%179);i=j;j=k;k=m;l=(53*l+1)%169;
	      if (((l*m)%64)>=32) s+=t;
	      t*=0.5;
	    };
	  ranu[il]=s;
	};
    };
  random=ranu[iranmr]-ranu[jranmr];
  if (random<0.0) ++random;
  ranu[iranmr--]=random;
  jranmr--;
  if (iranmr<0) iranmr=96;
  if (jranmr<0) jranmr=96;
  ranc-=rancd;
  if (ranc<0.0) ranc+=rancm;
  random-=ranc;
  if (random<0.0) random++;
  return random;  
}

double uniform(double min,double max) {return min+(max-min)*uniform();}

int fac(int n)
{
  int r=1;
  for (int i=2;i<n+1;i++) r*=i;
  return r;
}

double Ht(int n,momentum<double> *k)
{
  double ht=0.0;
  for (int i=2;i<n;i++)
    { double px=k[i].p[2],py=k[i].p[3];
      ht+=px*px+py*py;
    }
  return sqrt(ht);
}

double Aveta(int n,momentum<double> *k)
{
  double avg=0.0,sumPt=0.0;
  for (int i=2;i<n-1;i++)
    { double pe=k[i].p[0],px=k[i].p[2],py=k[i].p[3],pz=k[i].p[1];
      double pt=sqrt(px*px+py*py);
      double peta=0.5*log((pe+pz)/(pe-pz));
      for (int j=i+1;j<n;j++)
	{ double qe=k[j].p[0],qx=k[j].p[2],qy=k[j].p[3],qz=k[j].p[1];
	  double qt=sqrt(qx*qx+qy*qy);
	  double qeta=0.5*log((qe+qz)/(qe-qz));
	  avg+=max(pt,qt)*abs(peta-qeta);sumPt+=max(pt,qt);
	}
    }
  avg/=sumPt;
  return avg;
}

double Azimuth(int n,momentum<double> *k)
{
  int m1=0,m2=0;
  double Ptmax=0.0;
  for (int i=2;i<n;i++)
    { double pe=k[i].p[0],px=k[i].p[2],py=k[i].p[3],pz=k[i].p[1];
      double pt=sqrt(px*px+py*py);
      if (pt>Ptmax) { m1=i; Ptmax=pt;}
    }
  Ptmax=0.0;
  for (int i=2;i<n;i++)
    { double pe=k[i].p[0],px=k[i].p[2],py=k[i].p[3],pz=k[i].p[1];
      double pt=sqrt(px*px+py*py);
      if (pt>Ptmax and i!=m1) { m2=i; Ptmax=pt;}
    }
  double pe=k[m1].p[0],px=k[m1].p[2],py=k[m1].p[3],pz=k[m1].p[1];
  double qe=k[m2].p[0],qx=k[m2].p[2],qy=k[m2].p[3],qz=k[m2].p[1];
  double pt=sqrt(px*px+py*py);
  double qt=sqrt(qx*qx+qy*qy);
  double d=(px*qx+py*qy)/pt/qt;
  d=max(d,-1.0);d=min(d,1.0);
  double phi=acos(d);
  return phi;
}

void ramboo(double Ec,int N,momentum<double> *K)
{
  for (int i=2;i<N;i++) 
    { double Eb=-(K[0].p[0]+K[1].p[0])/Ec;
      double pz=-(K[0].p[1]+K[1].p[1])/Ec;
      double f=K[i].p[0];
      K[i].p[0]=(Eb*f+pz*K[i].p[1]);
      f+=K[i].p[0];f*=pz/(1.0+Eb);
      K[i].p[1]+=f;
    }
}

momentum<double> *Rambo(double Elab,int Nparticle,double& wgt)
{
  momentum<double> *P;
  P=new momentum<double>[Nparticle];
  double xmin=1e-8;
  double x1=uniform(xmin,1.0);
  double x2=uniform(xmin,1.0);
  P[0]=-Elab*x1*momentum_<double>(0.5, 0.5, 0.0, 0.0);
  P[1]=-Elab*x2*momentum_<double>(0.5,-0.5, 0.0, 0.0);
  double Ecm=Elab*sqrt(x1*x2);
  momentum<double> R=momentum_(0.0,0.0,0.0,0.0);
  for (int i=2;i<Nparticle;i++) 
    { double c=uniform(-1.0,1.0);
      double s=sqrt(1.0-c*c);
      double f=2.0*pi*uniform(0.0,1.0);
      P[i]=-log(uniform(0.0,1.0)*uniform(0.0,1.0))*momentum_(1.0,c,s*cos(f),s*sin(f));
      R=R+P[i]; 
    }
  double Rmass=sqrt(R*R);
  R=-R/Rmass;
  double a=1.0/(1.0-R.p[0]);
  double x=Ecm/Rmass;
  for (int i=2;i<Nparticle;i++) 
    { double bq=R.p[1]*P[i].p[1]+R.p[2]*P[i].p[2]+R.p[3]*P[i].p[3];
      double xq=P[i].p[0]+a*bq;
      P[i]=x*momentum_(-R.p[0]*P[i].p[0]+bq,P[i].p[1]+R.p[1]*xq,P[i].p[2]+R.p[2]*xq,P[i].p[3]+R.p[3]*xq);
    }
  int N=Nparticle-2;
  wgt=1.0;
  ramboo(Ecm,Nparticle,P);
  return P;
};

bool JetCuts(int Np,momentum<double> *k,double Ptmin,double etamax,double Rmin) {
  bool pass=true;
  for (int i=2;i<Np;i++) 
    { double pe=k[i].p[0],px=k[i].p[2],py=k[i].p[3],pz=k[i].p[1];
      if (sqrt(px*px+py*py)<Ptmin) pass=false;
      if (abs(0.5*log((pe+pz)/(pe-pz)))>etamax) pass=false;
    }
  if (pass) 
    { for (int i=2;i<Np-1;i++) 
	{ double pe1=k[i].p[0],px1=k[i].p[2],py1=k[i].p[3],pz1=k[i].p[1];
	  for (int j=i+1;i<Np;i++) 
	    { double pe2=k[j].p[0],px2=k[j].p[2],py2=k[j].p[3],pz2=k[j].p[1];
	      double Deta=0.5*log(abs((pe1+pz1)*(pe2-pz2)/(pe1-pz1)/(pe2+pz2)));
	      double d=(px1*px2+py1*py2)/(sqrt(px1*px1+py1*py1)*sqrt(px2*px2+py2*py2));
	      d=max(d,-1.0);d=min(d,1.0);
	      double Dphi=acos(d);
	      double R=sqrt(Deta*Deta+Dphi*Dphi);
	      if (R<Rmin) pass=false;
	    }
	}
    }
  return pass;
}

void boost(int lflag,momentum<double> q,momentum<double> &ph,momentum<double> &p)
{
  /*
                                        _                       
   Boost of a 4-vector ( relative speed q/q(0) ):               
                                                                
   ph is the 4-vector in the rest frame of q                    
   p is the corresponding 4-vector in the lab frame             
                                                                  
              INPUT                               OUTPUT         
                                                            
  lflag= 0:   q, ph                               p             
                                                                
  lflag= 1:   q, p                                ph            

  */

  double rsq = sqrt(abs(q*q));
  if (lflag==0) {
    double dp = (q.p[0]*ph.p[0]+q.p[1]*ph.p[1]+q.p[2]*ph.p[2]+q.p[3]*ph.p[3])/rsq;
    double c1 = (ph.p[0]+dp)/(rsq+q.p[0]);
    p=ph+c1*q;
    p.p[0]=dp;
  }
  else {
    double dp = (q*p)/rsq;
    double c1 = (p.p[0]+dp)/(rsq+q.p[0]);
    ph =p-c1*q;
    ph.p[0]=dp;
  }
}
double SequentialBrancherIF(momentum<double> P1,momentum<double> Ka,momentum<double> &p1,momentum<double> &kr,momentum<double> &ka,
			    const double Smax,const double smin)
{
  double x=2.0*P1.p[0]/sqrt(Smax);
  double Kmax=pow(Ka.p[0]+0.5*(1.0-x)*sqrt(Smax),2);
  double Ecm=sqrt(Smax);
  double wgt;
  
  if (samplingIF)
    { double r1=abs(uniform(0.0,1.0));
      double r2=abs(uniform(0.0,1.0));
      double r3=abs(uniform(0.0,1.0));
      double ymin=abs(0.5f*smin/Ecm/sqrt(Kmax));
      double y=pow(ymin,r1);
      double ct=1.0-2.0*y;
      double st=sqrt(abs(1.0-ct*ct));
      wgt=2.0*pi*abs(y*log(ymin));
      double Kmin=pow(smin/Ecm/y,2.0);
      double kr2=pow(Kmax,r2)*pow(Kmin,(1.0-r2));
      wgt*=abs(kr2*log(abs(Kmax/Kmin)));
      double phi=2.0*pi*r3;
      double cp=cos(phi);
      double sp=sin(phi);
      if (P1.p[1]>0.0)
	kr=sqrt(abs(kr2))*momentum_(1.0,ct,cp*st,sp*st);
      else
	kr=sqrt(abs(kr2))*momentum_(1.0,-ct,cp*st,sp*st);
    }
  else
    { double r1=abs(uniform(0.0,1.0));
      double r2=abs(uniform(0.0,1.0));
      double r3=abs(uniform(0.0,1.0));
      double kr2=Kmax*r1;
      wgt=2.0*pi*Kmax;
      double ct=2.0*r2-1.0;
      double st=sqrt(abs(1.0-ct*ct));
      double phi=2.0*pi*r3; 
      double cp=cos(phi);
      double sp=sin(phi);
      if (P1.p[1]>0.0)
	kr=sqrt(abs(kr2))*momentum_(1.0,ct,cp*st,sp*st);
      else
	kr=sqrt(abs(kr2))*momentum_(1.0,-ct,cp*st,sp*st);
    }
  double d=abs(P1*Ka-P1*kr);
  double z=1.0+(kr*Ka)/d;
  if (z*x>1.0) wgt=0.0;
  wgt*=0.5f*(P1*Ka)/d/pi;
  ka=Ka-kr+(z-1.0)*P1;
  if (ka.p[0]<0.0) wgt=0.0;
  p1=z*P1;
   if ((2.0*(p1*kr)<smin) or (2.0*(kr*ka)<smin)) wgt=0.0;
  return wgt;
}

double SequentialBrancherFF(momentum<double> K1,momentum<double> K2,momentum<double> &Pa,momentum<double> &Pr,momentum<double> &Pb,
			    const double smin)
{
  double s12=2.0*(K1*K2);
  double ymin=smin/s12;
  double sar,srb,wgt;
  if (samplingFF)
    { double r1=abs(uniform(0.0,1.0));
      double r2=abs(uniform(0.0,1.0));
      double yar=pow(ymin,r1);
      double yrb=pow(ymin,r2);
      if (yar<yrb) { sar=yar*s12;srb=yrb*s12; }
      else { sar=yrb*s12;srb=yar*s12; }
      if (yar+yrb>1.0) wgt=0.0;
      else wgt=0.5*yar*yrb*pow(log(ymin),2.0);
    }
  else
    { double yar=abs(uniform(0.0,1.0));
      double yrb=abs(uniform(0.0,1.0));
      if (yar<yrb) { sar=yar*s12;srb=yrb*s12; }
      else { sar=yrb*s12;srb=yar*s12; }
      if (yar+yrb>1) {yar=s12-srb;yrb=s12-sar;sar=yar;srb=yrb; }
      wgt=0.25;
    }
  double sab=s12-sar-srb;
  double gamma=sar/s12;
  Pb=(1.0-gamma)*K2;
  // Goto rest frame of Q
  momentum<double> Q=K1+gamma*K2;
  momentum<double> P;
  boost(1,Q,P,Pb);
  // Calculate angles wrt positive z-axis, so we can rotate system such that Pb is along positive z-axis
  double dp=sqrt(abs(P.p[1]*P.p[1]+P.p[2]*P.p[2]));
  double sphi=P.p[2]/dp;
  double cphi=P.p[1]/dp;
  P.p[1]=dp;
  dp=sqrt(abs(P.p[1]*P.p[1]+P.p[3]*P.p[3]));
  double sthe=P.p[1]/dp;
  double cthe=P.p[3]/dp;
  // Calculate decay of Q in its rest system aK[0]*K[1])/(K[1]*Pb)nd Pb is along the pozitive z-axis.
  // The decay is subject to the constraint 2*(Pr*Pb)=srb wich fixes the z-components. 
  double Ea=sqrt(abs(Q*Q));
  double c=1.0-2.0*srb/(s12-sar);
  double s=sqrt(abs(1.0-c*c));
  double f=pi*abs(uniform(-1.0,1.0));
  Pr=0.5*Ea*momentum_(1.0,s*cos(f),s*sin(f),c);
  Pa=0.5*Ea*momentum_(1.0,-s*cos(f),-s*sin(f),-c);
  // Rotate the system such that Pb is along its original direction and boost back to lab.
  double px=Pa.p[3]*cthe-Pa.p[1]*sthe;
  double py=Pa.p[3]*sthe+Pa.p[1]*cthe;
  Pa.p[3]=px;
  Pa.p[1]=py;
  px=Pa.p[1]*cphi-Pa.p[2]*sphi;
  py=Pa.p[1]*sphi+Pa.p[2]*cphi;
  Pa.p[1]=px;
  Pa.p[2]=py;
  boost(0,Q,Pa,Pa);
  px=Pr.p[3]*cthe-Pr.p[1]*sthe;
  py=Pr.p[3]*sthe+Pr.p[1]*cthe;
  Pr.p[3]=px;
  Pr.p[1]=py;
  px=Pr.p[1]*cphi-Pr.p[2]*sphi;
  py=Pr.p[1]*sphi+Pr.p[2]*cphi;
  Pr.p[1]=px;
  Pr.p[2]=py;
  boost(0,Q,Pr,Pr);
  return wgt;
}

double Rkt(momentum<double> P1,momentum<double> P2) 
{
  double R12;
  double E1 =P1.p[0],E2 =P2.p[0];
  double px1=P1.p[1],px2=P2.p[1];
  double py1=P1.p[2],py2=P2.p[2];
  double pz1=P1.p[3],pz2=P2.p[3];
  double Ets1=px1*px1+py1*py1,Ets2=px2*px2+py2*py2;
  if (E1<0.0 and E2>0.0) R12=Ets2;
  else if (E1>0.0 and E2<0.0) R12=Ets1;
  else if (E1<0.0 and E2<0.0) R12=sqrt(2.0*P1*P2);
  else 
    { double Deta=0.5*log((E1+pz1)*(E2-pz2)/((E1-pz1)*(E2+pz2)));
      double d=(px1*px2+py1*py2)/(sqrt(Ets1)*sqrt(Ets2));
      d=max(d,-1.0);d=min(d,1.0);
      double Dphi=acos(d);
      double R2=Deta*Deta+Dphi*Dphi;
      R12=R2*min(Ets1,Ets2);
    }
  return R12;
}

double R(momentum<double> P1,momentum<double> P2) 
{
  double s=2.0*abs(P1*P2);
  return s;
}

double R(momentum<double> P1,momentum<double> P2,momentum<double> P3) 
{
  return min(R(P1,P3),R(P3,P2));
}

momentum<double> *Brancher(const int N, momentum<double> *K,bool &pass,double &wgt,const double Smax,const double Smin)
{
  momentum<double> *Kbranched;
  Kbranched=new momentum<double>[N+1];
  for (int i=0;i<N;i++) Kbranched[i]=K[i];
  int a,b;
  int n=N+1;
  double nff=(double) (n-4)/(double) (n-2);
  if (uniform(0.0,1.0)<nff)
    { do 
	{
	  a=(int) (uniform(2.0,(double) N));
	  b=(int) (uniform(2.0,(double) N));
	} while (a==b); 
    }
  else
    { a=(int) (uniform(0.0,2.0));
      b=(int) (uniform(2.0,(double) N));
    }
  momentum<double> Ka=K[a];
  momentum<double> Kb=K[b];
  momentum<double> Pa,Pr,Pb;
  if (a>1) 
    { wgt=SequentialBrancherFF(Ka,Kb,Pa,Pr,Pb,Smin);
      wgt*=0.5*(N-2.0)*(N-3.0)*(Ka*Kb)/(K[0]*K[1]);
    }
  else if (a==0)
    { wgt=SequentialBrancherIF(-Ka,Kb,Pa,Pr,Pb,Smax,Smin);
      Pa=-Pa;
      if (N>4)
	wgt*=(N-2.0)*(N-3.0)*pow((K[0]*K[1])/(Pa*K[1]),N-4.0)/(Pa*K[1]);
      else
	wgt*=2.0/(Pa*K[1]);
    }
  else
    { wgt=SequentialBrancherIF(-Ka,Kb,Pa,Pr,Pb,Smax,Smin);
      Pa=-Pa;
      if (N>4)
	wgt*=(N-2.0)*(N-3.0)*pow((K[0]*K[1])/(Pa*K[0]),N-4.0)/(Pa*K[0]);
      else
	wgt*=2.0/(Pa*K[0]);
    }
  wgt*=(n-2.0)*(n-2.0)*(n-3.0);
  Kbranched[a]=Pa;
  Kbranched[N]=Pr;
  Kbranched[b]=Pb;
  pass=true;
  double R12min=1e20;
  int a0,r0;
  pass=false;
  for (int i=0;i<N;i++)
    for (int j=i+1;j<N+1;j++)
      { double s=R(Kbranched[i],Kbranched[j]);
	if (s<R12min) {R12min=s;a0=i;r0=j;} 
      }
  if ((a==a0) and (N==r0)) pass=true;
  if ((a==r0) and (N==a0)) pass=true;
  int b0=-1;
  if (pass)
    { double R123min=1e20;
      pass=false;
      for (int i=2;i<N;i++)
	if (not (i==a))
	  { double s=R(Kbranched[a],Kbranched[N],Kbranched[i]);
	    if (s<R123min) {R123min=s;b0=i;} 
	  }
      if (b0==b) pass=true; 
    }
  return Kbranched;
}

momentum<double> *Cluster(int N,momentum<double> *Kb)
{
  momentum<double> *Kc=new momentum<double>[N+1];
  double R12min=1e10;
  double R123min=1e10;
  int a,r,b;
  for (int i=0;i<N;i++)
    for (int j=i+1;j<N+1;j++)
      { double Rij=R(Kb[i],Kb[j]);
	if (Rij<R12min) {a=i;r=j;R12min=Rij;}
      }
  for (int i=2;i<N+1;i++)
    { if ((i!=a) and (i!=r))
	{ double Rijk=R(Kb[a],Kb[r],Kb[i]);
	  if (Rijk<R123min) {b=i;R123min=Rijk;}
	}
    }
  for (int i=0;i<N+1;i++) {Kc[i]=Kb[i];}
  if ((a>1) and (r>1)) 
    { Kc[a]=Kb[a]+Kb[r];
      Kc[b]=Kb[b];
      double gamma=(Kc[a]*Kc[a])/(2.0*Kc[a]*Kc[b]);
      Kc[a]=Kc[a]-gamma*Kc[b];
      Kc[b]=(1.0+gamma)*Kc[b];
      Kc[r].p[0]=0.0;
    }
  else
    {
      if (a<2)
	{ Kc[b]=Kb[b]+Kb[r];
	  Kc[a]=Kb[a];
	  double gamma=(Kb[a]*Kb[b]+Kb[b]*Kb[r]+Kb[r]*Kb[a])/(Kb[a]*Kb[b]+Kb[r]*Kb[a]);
	  Kc[b]=Kc[b]+(1.0-gamma)*Kc[a];
	  Kc[a]=gamma*Kc[a];
	  Kc[r].p[0]=0.0;
	}
      if (r<2)
	{ Kc[b]=Kb[b]+Kb[a];
	  Kc[r]=Kb[r];
	  double gamma=(Kb[r]*Kb[b]+Kb[b]*Kb[a]+Kb[a]*Kb[r])/(Kb[r]*Kb[b]+Kb[a]*Kb[r]);
	  Kc[b]=Kc[b]+(1.0-gamma)*Kc[r];
	  Kc[r]=gamma*Kc[r];
	  Kc[a].p[0]=0.0;
	}
    }
  int p=0;
  for (int i=0;i<N+1;i++)
    if (Kc[i].p[0]!=0.0) {Kc[p]=Kc[i];p++;}
  return Kc;
}

void DumpEvent(int N,momentum<double> *K,momentum<double> *Kc,momentum<double> *Kb)
{
  momentum<double> sum=momentum_(0.0,0.0,0.0,0.0);
  for (int n=0;n<N-1;n++) 
    { cout<<"particle "<<n<<": "<<endl;
      cout<<"   Momentum: "<<K[n].p[0]<<" "<<K[n].p[1]<<" "<<K[n].p[2]<<" "<<K[n].p[3]<<": "<<K[n]*K[n]<<endl;
      sum=sum+K[n];
    }
  cout<<"Momentum sum: "<<sum.p[0]<<" "<<sum.p[1]<<" "<<sum.p[2]<<" "<<sum.p[3]<<endl;
  sum=momentum_(0.0,0.0,0.0,0.0);
  for (int n=0;n<N;n++) 
    { cout<<"-> particle "<<n<<": "<<endl;
      cout<<"->    Momentum: "<<Kb[n].p[0]<<" "<<Kb[n].p[1]<<" "<<Kb[n].p[2]<<" "<<Kb[n].p[3]<<": "<<Kb[n]*Kb[n]<<endl;
      sum=sum+Kb[n];
    }
  cout<<"->   Momentum sum: "<<sum.p[0]<<" "<<sum.p[1]<<" "<<sum.p[2]<<" "<<sum.p[3]<<endl;
  cout<<endl;
  sum=momentum_(0.0,0.0,0.0,0.0);
  for (int n=0;n<N-1;n++) 
    { cout<<"--> particle "<<n<<": "<<endl;
      cout<<"-->    Momentum: "<<Kc[n].p[0]<<" "<<Kc[n].p[1]<<" "<<Kc[n].p[2]<<" "<<Kc[n].p[3]<<": "<<Kc[n]*Kc[n]<<endl;
      sum=sum+Kc[n];
    }
  cout<<"-->   Momentum sum: "<<sum.p[0]<<" "<<sum.p[1]<<" "<<sum.p[2]<<" "<<sum.p[3]<<endl;
  cout<<endl;
  cout<<"------------------------------------------------------------------------------"<<endl;
}

bool Compare(int N,momentum<double> *K1,momentum<double> *K2)
{
  bool pass=true;
  double tol=1e-6;
  for (int i=0;i<N;i++) 
    { double E1=K1[i].p[0];
      bool found=false;
      int t;
      for (int j=0;j<N;j++)
	{ double E2=K2[j].p[0];
	  if (abs(E1-E2)<tol) {found=true;t=j;}
	}
      if (not found) {cout<<"no matching energy for particle "<<i<<endl;pass=false;}
      else
	{ if (abs(K1[i].p[1]-K2[t].p[1])>tol) pass=false;
	  if (abs(K1[i].p[2]-K2[t].p[2])>tol) pass=false;
	  if (abs(K1[i].p[3]-K2[t].p[3])>tol) pass=false;
	}
    }
  return pass;
}
