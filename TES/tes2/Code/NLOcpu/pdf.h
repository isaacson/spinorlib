#include "/scratch/giele/lhapdf/include/LHAPDF/LHAPDF.h"

using namespace LHAPDF;

double Li2(const double x,const int kmax=250,const double xerr=1e-16)
{
  double pi2=M_PI*M_PI;
  if (x==0) return 0.0;
  if (x<0.0) return 0.5*Li2(x*x)-Li2(-x);
  //    if (x<0.0) {double l=log(1.0-x);return -Li2(x/(x-1.0))-0.5*l*l;}
  if (x<=0.5)
    {
      double sum  = x;
      double term = x;
      for(int k=2; k<kmax; k++) 
	{
	  double rk = (k-1.0)/k;
	  term *= x;
	  term *= rk*rk;
	  sum += term;
	  if (abs(term/sum) < xerr) return sum;
	}
      cout.precision(16);
      cout<<"Maximum number of iterations exceeded in Li2("<<x<<")="<<sum<<endl;
      return sum;
    }
  if (x<1.0) return pi2/6.0-Li2(1.0-x)-log(x)*log(1.0-x);
  if (x==1.0) return pi2/6.0;
  if (x<=1.01)
    {
      const double eps = x - 1.0;
      const double lne = log(eps);
      const double c0 = pi2/6.0;
      const double c1 =   1.0 - lne;
      const double c2 = -(1.0 - 2.0*lne)/4.0;
      const double c3 =  (1.0 - 3.0*lne)/9.0;
      const double c4 = -(1.0 - 4.0*lne)/16.0;
      const double c5 =  (1.0 - 5.0*lne)/25.0;
      const double c6 = -(1.0 - 6.0*lne)/36.0;
      const double c7 =  (1.0 - 7.0*lne)/49.0;
      const double c8 = -(1.0 - 8.0*lne)/64.0;
      return c0+eps*(c1+eps*(c2+eps*(c3+eps*(c4+eps*(c5+eps*(c6+eps*(c7+eps*c8)))))));
    }
  double log_x=log(x);
  if (x<=2.0) return pi2/6.0+Li2(1.0 - 1.0/x)-log_x*(log(1.0-1.0/x)+0.5*log_x);
  return pi2/3.0-Li2(1.0/x)-0.5*log_x*log_x;
}

double X0,PDF0,MU;

void polint(const int n,double *xa,double *ya,const double x,double &y, double &dy)
{ int ns=0;
  double den,dif,dift,ho,hp,w;
  double c[n],d[n];
  dif=abs(x-xa[0]);
  for (int i=0;i<n;i++)
    { if ((dift=abs(x-xa[i]))<dif) {ns=i;dif=dift;}
      c[i]=ya[i];d[i]=ya[i];
    }
  y=ya[ns--];
  for (int m=1;m<n;m++)
    {
      for (int i=0;i<n-m;i++)
	{ ho=xa[i]-x;hp=xa[i+m]-x;
	  w=c[i+1]-d[i];
	  if ((den=ho-hp)==0.0) cout<<"***** Error in routine polint  *****"<<endl;
	  den=w/den;
	  d[i]=hp*den;c[i]=ho*den;
	}
      y+=(dy=(2*(ns+1)<(n-m)?c[ns+1]:d[ns--]));
    }
}

double midpnt(double func(const double),const double a,const double b,const int n)
{ double x,tnm,sum,del,ddel;
  static double s;
  if (n==1) { return (s=(b-a)*func(0.5*(a+b))); }
  else
    { int it=1;
      for (int j=1;j<n-1;j++) it*=3;
      tnm=it;
      del=(b-a)/(3.0*tnm);
      ddel=del+del;
      x=a+0.5*del;
      sum=0.0;
      for (int j=0;j<it;j++)
	{ sum+=func(x);
	  x+=ddel;
	  sum+=func(x);
	  x+=del;
	}
      s=(s+(b-a)*sum/tnm)/3.0;
      return s;
    }
}

double qromb(double func(const double),double a, double b)
{ const int jmax=50,jmaxp=jmax+1,k=5;
  const double eps=1.0e-10;
  double ss=0.0,dss=0.0;
  double s[jmax],h[jmaxp];
  double *s_t=new double[k];double *h_t=new double[k];
  h[0]=1.0;
  for (int j=1;j<=jmax;j++)
    { s[j-1]=midpnt(func,a,b,j);
      if (j>=k)
	{ for (int i=0;i<k;i++) { h_t[i]=h[j-k+i];s_t[i]=s[j-k+i];} 
	  polint(k,h_t,s_t,0.0,ss,dss);
	  if (abs(dss)<=eps*abs(ss)) return ss;
	}
      h[j]=0.25*h[j-1];
    }
  cout<<"+++++ too many steps in routine qromb *****"<<endl;
  return 0.0;
}

double TestPDF(double x) { return (1.0-x)/x;}
double PDF(const double x,const double mu) 
{ double pdf;
  //pdf=TestPDF(x);
  pdf=xfx(x,mu,0)/x;
  return pdf;
}
double alphaS(const double Q) { return alphasPDF(Q);}

void initPDF(string pdf,SetType T,const int subset) 
{ setVerbosity(SILENT);
  initPDFSet(pdf,T,subset);
  cout<<"# LHAPDF: M.R. Whalley, D. Bourilkov and R.C. Group, hep-ph/0508110"<<endl;
  double Mz=91.1876;
  cout<<"# LHAPDF: PDF used "<<pdf<<"; Alpha_S(Mz)="<<alphaS(Mz)<<endl;
}

double TestCrossing(double x,double smin,double mu) 
{ double l=log(x),l1=log(1.0-x),pi2=M_PI*M_PI;
  double A=2.0*(1.0-1.0/x)+((1.0-x)*(1.8333333333333333+2.0*l1))/x
          -(2.0*(((x-1.0)*(-11.0+(x-2.0)*x))/6.0+(1.0+x)*l))/x;
  double B=(2.0*(x-1.0)*(-1.0+l1))/x+((1.0-x)*(-3.7222222222222222+pi2/3.0+pow(l1,2)))/x 
    +(-6.0*pi2*(1.0+x)+(x-1.0)*(-31.0+5.0*(x-2.0)*x)-6.0*(x-1)*(-11.0+(x-2.0)*x)*l1
      -36.0*x*l+36.0*(1.0+x)*Li2(x))/(18.0*x);;
  double C=A*log(smin/mu/mu)+B;
  return C;
}

double TestCrossingA(double x)
{ double l=log(x),l1=log(1.0-x);
  double A=2.0*(1.0-1.0/x)+((1.0-x)*(1.8333333333333333+2.0*l1))/x
          -(2.0*(((x-1.0)*(-11.0+(x-2.0)*x))/6.0+(1.0+x)*l))/x;
  return A;
}
double TestCrossingB(double x)
{ double l=log(x),l1=log(1.0-x),pi2=M_PI*M_PI;
  double B=(2.0*(x-1.0)*(-1.0+l1))/x+((1.0-x)*(-3.7222222222222222+pi2/3.0+pow(l1,2)))/x 
    +(-6.0*pi2*(1.0+x)+(x-1.0)*(-31.0+5.0*(x-2.0)*x)-6.0*(x-1)*(-11.0+(x-2.0)*x)*l1
      -36.0*x*l+36.0*(1.0+x)*Li2(x))/(18.0*x);;
  return B;
}

double Acrossing(const double z)
{ double pdf1=PDF(X0/z,MU),pdf2=PDF0;
  double T1=(pdf1-pdf2)/(1.0-z);
  double T2=pdf1*(1.0+z*z)*(1.0-z)/z/z;
  return 2.0*(T1+T2);
}
double Bcrossing(const double z)
{ double pdf1=PDF(X0/z,MU),pdf2=PDF0;
  double l1=log(1.0-z);
  double T1=(pdf1-pdf2)/(1.0-z)*l1;
  double T2=pdf1*(1.0+z*z)*(1.0-z)/z/z*l1;
  return 2.0*(T1+T2);
}

double CalcCrossingA(double x,double mu) 
{ X0=x;PDF0=PDF(x,mu);MU=mu;
  double l1=log(1.0-x);
  double A=(11.0/6.0+2.0*l1)*PDF(x,mu)+qromb(Acrossing,x,1.0);
  return A;
}
double CalcCrossingB(double x,double mu) 
{ X0=x;PDF0=PDF(x,mu);MU=mu;
  double l1=log(1.0-x);
  double B=(M_PI*M_PI/3.0-67.0/18.0+l1*l1)*PDF(x,mu)+qromb(Bcrossing,x,1.0);
  return B;
}

double CrossingA(double x,double mu) 
{ double crossa;
  //crossa=TestCrossingA(x);
  crossa=CalcCrossingA(x,mu);
  return crossa;
}
double CrossingB(double x,double mu) 
{ double crossb;
  //crossb=TestCrossingB(x);
  crossb=CalcCrossingB(x,mu);
  return crossb;
}


