#include <math.h>
#include <stdlib.h>
#include "momentum.cc"
#include "/home/giele/scratch/lhapdf/include/LHAPDF/LHAPDF.h"

#include <iostream>
#include <typeinfo>
#include <cassert>
#include <cmath>
#include <iomanip>

using namespace std;
using namespace LHAPDF;

extern double uniform(double,double);

momentum<double> orthogonal(const momentum<double> P) 
{
  momentum<double> J;
  double x, y, z, rsq;
  do {
    x=uniform(-1.0,1.0);
    y=uniform(-1.0,1.0);
    z=uniform(-1.0,1.0);
    rsq=x*x+y*y+z*z;
  } while(rsq>1.0 || rsq<1e-6);
  J.p[0]=0.0;
  J.p[1]=y*P.p[3]-z*P.p[2];
  J.p[2]=z*P.p[1]-x*P.p[3];
  J.p[3]=x*P.p[2]-y*P.p[1];
  double norm=sqrt(J.p[1]*J.p[1]+J.p[2]*J.p[2]+J.p[3]*J.p[3]);
  if (norm>0.0) {
    J.p[1]/=norm;
    J.p[2]/=norm;
    J.p[3]/=norm;
  }
  else J=momentum_<double>(0.0,0.0,0.0,0.0);
  return J;
}

momentum<double> Current(const int N,const int a,const momentum<double> *P,const momentum<double> *j)
{
  momentum<double> *Q=new momentum<double>[N+1];
  momentum<double> Jlk[N][N];
  for (int i=0;i<N;i++) Jlk[i][i]=j[i];
  Q[0]=momentum_<double>(0.0,0.0,0.0,0.0);Q[1]=P[0];for(int i=2; i<N+1; i++) Q[i]=Q[i-1]+P[i-1];
  for (int l=1;l<N-1;l++)
    { for (int m=1;m<N-l;m++) 
	{ int k=(m+l);
	  // Start Calculating current J(m,k)
	  //cout<<m<<" "<<k<<endl;
	  momentum<double> Jmk=momentum_<double>(0.0,0.0,0.0,0.0); 
	  for (int i=m;i<k;i++) 
	    {//cout<<"("<<m<<","<<i<<")("<<(i+1)<<","<<k<<")"<<endl;
	     // 2 J1.K2 J2 - 2 J2.K1 J1 + J1.J2 (K1-K2)
	      momentum<double> K1=Q[(i+1+a)%N]-Q[(m+a)%N],K2=Q[(k+1+a)%N]-Q[(i+1+a)%N];
	      momentum<double> J1=Jlk[(m+a)%N][(i+a)%N],J2=Jlk[(i+1+a)%N][(k+a)%N];
	      Jmk+=(2.0*(J1*K2))*J2-(2.0*(J2*K1))*J1+(J1*J2)*(K1-K2);
	    }
	  for (int i=m; i<k-1;i++) 
	    { for (int j=i+1;j<k;j++) 
		{ momentum<double> J1=Jlk[(m+a)%N][(i+a)%N],J2=Jlk[(i+1+a)%N][(j+a)%N],J3=Jlk[(j+1+a)%N][(k+a)%N];
		  Jmk+=(2.0*(J1*J3))*J2-(J1*J2)*J3-(J2*J3)*J1;
		}
	    }
	  if (l<N-2) 
	    { momentum<double> Pm=Q[(k+1+a)%N]-Q[(m+a)%N];
	      Jmk/=(Pm*Pm);
	      //cout<<(m+a)%N<<" "<<(k+a)%N<<" "<<Pm*Pm<<endl;
	    }
	  Jlk[(m+a)%N][(k+a)%N]=Jmk;
	  //cout<<"---->  "<<Jmk.p[0]<<" "<<Jmk.p[1]<<" "<<Jmk.p[2]<<" "<<Jmk.p[3]<<" "<<endl;
	}
    }
  delete Q;
  momentum<double> J=Jlk[(1+a)%N][(N-1+a)%N];
  return 2.0*J;
}

double *PolWeight(const int N,const momentum<double> *K,const momentum<double> *J) 
{
  double *polwgt=new double[N];
  for (int a=0;a<N;a++)
    { momentum<double> Jortho=momentum_(0.0,J[a].p[2]*K[a].p[3]-J[a].p[3]*K[a].p[2],J[a].p[3]*K[a].p[1]-J[a].p[1]*K[a].p[3],
			          	    J[a].p[1]*K[a].p[2]-J[a].p[2]*K[a].p[1]);
      Jortho=1.0/(sqrt(-Jortho*Jortho))*Jortho;
      momentum<double> C=Current(N,a,K,J);
      double T1=pow(J[a]*C,2);
      double T2=pow(Jortho*C,2);
      polwgt[a]=T1/(T1+T2);
    }
  return polwgt;
}


double TreeLevel(const int N,const int a,const momentum<double> *P,const momentum<double> *j,double smin)
{
  bool pass=true;
  /*
  for (int i=0;i<N-1;i++) 
    for (int j=i+1;j<N;j++)
      if (abs(2.0*(P[i]*P[j]))<smin) pass=false;
  */
  for (int i=0;i<N;i++) 
    if (abs(2.0*(P[i]*P[(i+1)%N]))<smin) pass=false;
  double Tree=0.0;
  if (pass)
    { momentum<double> J=Current(N,a,P,j);
      Tree=pow(j[a]*J,2);
    }
  return Tree;
}

double TreeLevel(const int N,const momentum<double> *P,const momentum<double> *j,double smin) { return TreeLevel(N,0,P,j,smin); }

double TreeLevelInsert(const int N,momentum<double> *P,momentum<double> *J,double smin) 
{ double sum=TreeLevel(N,0,P,J,smin);
  for (int r=N-1;r>1;r--)
    { momentum<double> swap;
      swap=P[r-1];
      P[r-1]=P[r];
      P[r]=swap;      
      swap=J[r-1];
      J[r-1]=J[r];
      J[r]=swap;
      sum+=TreeLevel(N,0,P,J,smin);
    }      
  return sum;
}


double TestTreeLevel(const int N,const momentum<double> *K,const double smin)
{
  double wgt=0.0;
  wgt=1.0;///((K[0]*K[1])*(K[1]*K[2])*(K[2]*K[3])*(K[3]*K[0]));
  return wgt;
}

double Li2(const double x,const int kmax=50,const double xerr=1e-8)
  {
    double pi2=M_PI;
    if (x==0) return 0.0;
    if (x<0.0) return 0.5*Li2(x*x)-Li2(-x);
    //    if (x<0.0) {double l=log(1.0-x);return -Li2(x/(x-1.0))-0.5*l*l;}
    if (x<=0.5)
      {
	double sum  = x;
	double term = x;
	for(int k=2; k<kmax; k++) 
	  {
	    double rk = (k-1.0)/k;
	    term *= x;
	    term *= rk*rk;
	    sum += term;
	    if (abs(term/sum) < xerr) return sum;
	  }
	cout.precision(16);
	cout<<"Maximum number of iterations exceeded in Li2("<<x<<")="<<sum<<endl;
	return sum;
      }
    if (x<1.0) return pi2/6.0-Li2(1.0-x)-log(x)*log(1.0-x);
    if (x==1.0) return pi2/6.0;
    if (x<=1.01)
      {
	const double eps = x - 1.0;
	const double lne = log(eps);
	const double c0 = pi2/6.0;
	const double c1 =   1.0 - lne;
	const double c2 = -(1.0 - 2.0*lne)/4.0;
	const double c3 =  (1.0 - 3.0*lne)/9.0;
	const double c4 = -(1.0 - 4.0*lne)/16.0;
	const double c5 =  (1.0 - 5.0*lne)/25.0;
	const double c6 = -(1.0 - 6.0*lne)/36.0;
	const double c7 =  (1.0 - 7.0*lne)/49.0;
	const double c8 = -(1.0 - 8.0*lne)/64.0;
	return c0+eps*(c1+eps*(c2+eps*(c3+eps*(c4+eps*(c5+eps*(c6+eps*(c7+eps*c8)))))));
      }
    double log_x=log(x);
    if (x<=2.0) return pi2/6.0+Li2(1.0 - 1.0/x)-log_x*(log(1.0-1.0/x)+0.5*log_x);
    return pi2/3.0-Li2(1.0/x)-0.5*log_x*log_x;
  }

double TestAnly(const int N,const momentum<double> *K,const double smin)
{
  double wgt=0.0;
  int i=2,j=3;
  double sij=2.0*(K[i]*K[j]);
  double s12=2.0*(K[0]*K[1]);
  double ymin=smin/sij;
  //Below is the Integral[1/2/sirj*(1-yir-yrj)*(2/yir/yrj+yir/yrj+yrj/yir+2),{y1,ymin,1-ymin},{y2,ymin,1-y1}];
  if (ymin<0.5) 
    { wgt=(67.0-6.0*M_PI*M_PI- 4.0*ymin*(39.0+ymin*(-21.0+20.0*ymin))
         +6.0*(-1.0+ymin)*(11.0+ymin*(-1.0+2.0* ymin))*log((1.0-ymin)/ymin) 
         +36.0*log(ymin)*log(ymin)+72.0*Li2(ymin))/36.0;
    }
  wgt*=0.5*(N-2.0)*(N-3.0)/s12;// This is the phase space factor due to going for N --> N+1 (rambo defined) phase space 
      
  return 0.5*wgt;// Extra 0.5 because we integrate over half the triangle: 
}


