template<typename T>
struct momentum {
  T p[4];
  
  inline
  const T &operator[](int mu) const {
    return p[mu];
  }

  inline
  T &operator[](int mu) {
    return p[mu];
  }

};

template<typename T>
 momentum<T> momentum_(T t, T x, T y, T z) {
  momentum<T> P;
  P.p[0]=t; P.p[1]=x; P.p[2]=y; P.p[3]=z;
  return P;
}

template<typename T1,typename T2>
 momentum<T2> operator+(const momentum<T1> P1, const momentum<T2> P2) {
  momentum<T2> P;
  P.p[0]=P1.p[0]+P2.p[0];
  P.p[1]=P1.p[1]+P2.p[1];
  P.p[2]=P1.p[2]+P2.p[2];
  P.p[3]=P1.p[3]+P2.p[3];
  return P;
}

template<typename T>
momentum<T> &operator+=(momentum<T> &a, const momentum<T> &b) {
  for (int i=0;i<4;i++) a.p[i]+=b.p[i];
  return a;
}


template<typename T1,typename T2>
 momentum<T2> operator-(const momentum<T1> P1, const momentum<T2> P2) {
  momentum<T2> P;
  P.p[0]=P1.p[0]-P2.p[0];
  P.p[1]=P1.p[1]-P2.p[1];
  P.p[2]=P1.p[2]-P2.p[2];
  P.p[3]=P1.p[3]-P2.p[3];
  return P;
}

template<typename T>
 momentum<T> operator-(const momentum<T> P1) {
  momentum<T> P;
  P.p[0]=-P1.p[0];
  P.p[1]=-P1.p[1];
  P.p[2]=-P1.p[2];
  P.p[3]=-P1.p[3];
  return P;
}

template<typename T1,typename T2>
 T2 operator*(momentum<T1> P1, momentum<T2> P2) {
   return P1.p[0]*P2.p[0]-P1.p[1]*P2.p[1]-P1.p[2]*P2.p[2]-P1.p[3]*P2.p[3];
}

template<typename T1,typename T2>
 momentum<T2> operator*(const T1 x, const momentum<T2> P1) {
   momentum<T2> P;
   P.p[0]=x*P1.p[0];
   P.p[1]=x*P1.p[1];
   P.p[2]=x*P1.p[2];
   P.p[3]=x*P1.p[3];
   return P;
}

template<typename T1,typename T2>
 momentum<T2> operator*(const momentum<T1> P1, const T2 x) {
   momentum<T2> P;
   P.p[0]=x*P1.p[0];
   P.p[1]=x*P1.p[1];
   P.p[2]=x*P1.p[2];
   P.p[3]=x*P1.p[3];
   return P;
}

template<typename T>
 momentum<T> operator/(const momentum<T> P1,const T x) {
   momentum<T> P;
   P.p[0]=P1.p[0]/x;
   P.p[1]=P1.p[1]/x;
   P.p[2]=P1.p[2]/x;
   P.p[3]=P1.p[3]/x;
   return P;
}

template<typename T>
 momentum<T> &operator/=(momentum<T> &P, const T x) {
  P.p[0]=P.p[0]/x;
  P.p[1]=P.p[1]/x;
  P.p[2]=P.p[2]/x;
  P.p[3]=P.p[3]/x;
  return P;
}

template<typename T>
std::ostream& operator<<(std::ostream& out,momentum<T> P) {
  out<<"("<<P.p[0]<<","<<P.p[1]<<","<<P.p[2]<<","<<P.p[3]<<")";
  return out;
}
