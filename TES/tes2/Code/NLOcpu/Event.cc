#include <iostream>
#include <math.h>
#include "gerben.h"
#include "Event.h"
#include "pdf.h"

using namespace std;

Event::Event(const int njets,const double ecm,const double ptmin,const double etamax,const double rmin,string PDF,const int op2)
{ Nparticle=njets+2;
  scale=njets/ecm;
  //scale=1.0/ecm;
  Elab=ecm*scale;
  initpos=op2-1;
  initPDF(PDF,LHPDF,0);
  int Gamma=1;
  for (int n=1;n<Nparticle-2;n++) Gamma*=n;
  CrossFactorBorn=gev2pb*pow(4.0*M_PI,3)*pow(Nc/8.0/M_PI,Nparticle-2.0)*(Nc*Nc-1.0);
  CrossFactorBorn*=(Nparticle-3.0)/4.0/Gamma/Gamma/ecm/ecm;
  CrossFactorBorn*=4.0*pow(2.0,Nparticle)/pow(2.0*(Nc*Nc-1.0),2);
  Gamma=1;
  for (int n=1;n<Nparticle-1;n++) Gamma*=n;
  CrossFactorReal=gev2pb*pow(4.0*M_PI,3)*pow(Nc/8.0/M_PI,Nparticle-1.0)*(Nc*Nc-1.0);
  CrossFactorReal*=(Nparticle-2.0)/4.0/Gamma/Gamma/ecm/ecm;
  CrossFactorReal*=4.0*pow(2.0,Nparticle+1)/pow(2.0*(Nc*Nc-1.0),2);
  PtMin=ptmin*scale;
  EtaMax=etamax;
  Rmin=rmin;
  samplingFF=true;//Important sampling in final-final state flag
  samplingIF=true;//Important sampling in initial-final state flag
  DOUBLE (*k)[4]=new DOUBLE[Nparticle][4];//Build the array structure K[Nparticle][4] for dynamical Nparticle
  K=k;                                   //as required by NGluon (the NGluon DOUBLE typename is used).
  H=new int[Nparticle]; //Helicity storage array (contains entries +1 or -1)
  Pjet=new momentum<double>[Nparticle]; //Jet momenta in internal momenta structure format
  Pord=new momentum<double>[Nparticle];
  Preal=new momentum<double>[Nparticle+1]; //Real radiation momenta in internal momenta structure format
  Pperm=new momentum<double>[Nparticle+1];
  Jjet=new momentum< complex<double> >[Nparticle];//Exernal jet sources in internal momenta structure format
  Jreal=new momentum< complex<double> >[Nparticle+1];//Exernal real radiation sources in internal momenta structure format
  JetEvent();//Generate an event with default event generator to initialize momentum vectors and PSweight
  Helicities();//Generate a helicity configuration to initialize helicity vectors and SourceWeight
  cout<<"# ONELOOP: using NGluon (S.Badger, B.Biedermann and P.Uwer:arXiv:1011.2900)"<<endl;
  nvirtual=new NGluon(Nparticle,K,H);//Define NGluon object (S.Badger, B.Biedermann and P.Uwer:arXiv:1011.2900)
  buffer_size=0;
  buffer=NULL;
  momenta=NULL;
  init_recursion(Nparticle+1);
}

Event::~Event()
{ 
  delete H;
  delete Pjet;
  delete Pord;
  delete Preal;
  delete Jjet;
  delete Jreal;
  delete K;
  free_recursion();
  cout<<"*** Deleting event class ***"<<endl;
}
double Event::Ht()
{ double ht=0.0;
  for (int i=2;i<Nparticle;i++)
    { double px=Pjet[i].p[2],py=Pjet[i].p[3];
      ht+=px*px+py*py;
    }
  return sqrt(ht)/(double) (Nparticle-2);

}
double Event::Azimuth()
{ int m1=0,m2=0;
  double Ptmax=0.0;
  for (int i=2;i<Nparticle;i++)
    { double px=Pjet[i].p[2],py=Pjet[i].p[3];
      double pt=sqrt(px*px+py*py);
      if (pt>Ptmax) { m1=i; Ptmax=pt;}
    }
  Ptmax=0.0;
  for (int i=2;i<Nparticle;i++)
    { double px=Pjet[i].p[2],py=Pjet[i].p[3];
      double pt=sqrt(px*px+py*py);
      if (pt>Ptmax and i!=m1) { m2=i; Ptmax=pt;}
    }
  double px=Pjet[m1].p[2],py=Pjet[m1].p[3];
  double qx=Pjet[m2].p[2],qy=Pjet[m2].p[3];
  double pt=sqrt(px*px+py*py);
  double qt=sqrt(qx*qx+qy*qy);
  double d=(px*qx+py*qy)/pt/qt;
  d=max(d,-1.0);d=min(d,1.0);
  double phi=acos(d);
  return phi;

}
double Event::JetEvent(){return JetEvent("Rambo");}
double Event::JetEvent(const string generator){ if (generator=="Rambo") PsWeight=rambo();return PsWeight;}
double Event::JetEvent(const momentum<double> *p,const double psweight)
{ PsWeight=psweight;
  for (int i=0;i<Nparticle;i++)
    for (int j=0;j<4;j++)
      Pjet[i].p[j]=p[i].p[j];
  return PsWeight;
}
double Event::JetEvent(double psweight){PsWeight=psweight;return PsWeight;}

void Event::Helicities(){for (int i=0;i<Nparticle;i++) H[i]=1-2*(int) (2.0*uniform());Sources();}
void Event::Helicities(const double sourceweight){SourceWeight=sourceweight;Sources();}
void Event::Helicities(const int *h){Helicities(h,1.0);}
void Event::Helicities(const int *h,const double sourceweight)
{ SourceWeight=sourceweight;
  for (int i=0;i<Nparticle;i++) H[i]=h[i];
  Sources();
}
void Event::Sources(){for (int i=0;i<Nparticle;i++) Jjet[i]=hsource(i);}

void Event::Scale() {Scale(91.2);}
void Event::Scale(const string type,const double factor) 
{ double q=100.0,Mz=91.1876;
  if (type=="Ht") q=Ht()/scale;
  if (type=="Mz") q=Mz;
  if (type=="Shat") q=sqrt(2.0*(Pjet[0]*Pjet[1]))/scale;
  Scale(q*factor);
  cout<<"Renormalization/Factorization scale ="<<q<<endl;
}
void Event::Scale(const double q) 
{ mu=q*scale;nvirtual->setMuR((DOUBLE) mu*mu);
  double x1=-2.0*Pjet[0].p[0]/Elab,x2=-2.0*Pjet[1].p[0]/Elab;
  PDF_x1=PDF(x1,mu/scale);CrossA_x1=CrossingA(x1,mu/scale);CrossB_x1=CrossingB(x1,mu/scale);
  PDF_x2=PDF(x2,mu/scale);CrossA_x2=CrossingA(x2,mu/scale);CrossB_x2=CrossingB(x2,mu/scale);
  AlphaS=alphaS(mu/scale);
}

void Event::PrintEvent()
{ cout<<"Jet Momenta: (scaled with njets/Ecm)"<<endl;
  momentum<double> sum=momentum_(0.0,0.0,0.0,0.0);
  double minS=1e10,maxS=0.0;
  for (int i=0;i<Nparticle;i++)
    { string h="+";
      if (H[i]==-1) h="-";
      cout<<"            P["<<i<<"]^"<<h<<"="<<Pjet[i]<<" (Pjet^2="<<Pjet[i]*Pjet[i]<<")"<<endl;
      sum=sum+Pjet[i];
    }
  cout<<"Momentum Conservation:"<<sum<<endl;
  for (int i=0;i<Nparticle-1;i++)
    for (int j=i+1;j<Nparticle;j++)
      { double sij=abs(2.0*(Pjet[i]*Pjet[j]));
	minS=min(minS,sij);
	maxS=max(maxS,sij);
      }
  cout<<"Minimum/maximum invariant mass: "<<minS<<" /  "<<maxS<<endl;
  for (int i=0;i<Nparticle-1;i++)
    for (int j=i+1;j<Nparticle;j++)
      { double sij=abs(2.0*(Pjet[i]*Pjet[j]));
	cout<<i<<" "<<j<<" "<<sij<<endl;
      }

}
void Event::PrintReal()
{ cout<<"Real Momenta: (scaled with njets/Ecm)"<<endl;
  momentum<double> sum=momentum_(0.0,0.0,0.0,0.0);
  for (int i=0;i<Nparticle+1;i++)
    { cout<<"            P["<<i<<"]="<<Preal[i]<<" (Pjet^2="<<Preal[i]*Preal[i]<<")"<<endl;
      sum=sum+Preal[i];
    }
  cout<<"Momentum Conservation:"<<sum<<endl;
}

double Event::LO(int *h)
{ Helicities(h); 
  return LO();
}

double Event::LO()
{ double sigmaLO=pow(2.0,2-Nparticle)*Abs2(Jjet[0]*BGcurrentBorn(0));
  HelSumLO=1.0/hel_summed_recursion(Nparticle,Pord);
  return sigmaLO;
}

double Event::Kfactor(const double smin,const int nevent)
{ //do not call Virtual, but calculate virtual here as NGluon requires the EvalAmp() call first.
  //combine with unresolved factor here... 1.0+((unresolved virtual) +(finite virtual)+ real)/LO
  double wgt=1.0+AlphaS*(Rfactor(smin)+Brem(nevent,smin));
  return wgt;
}

double Event::Rfactor(const double smin)
{ double wgt=0.0;
  for (int a=0;a<Nparticle;a++) 
    { int b=(a+1)%Nparticle;
      double sab=2.0*(Pord[a]*Pord[b]);
      if (sab<0.0) {sab=-sab;wgt-=M_PI*M_PI/2.0;}
      //else wgt+=M_PI*M_PI/2.0;
      double ymin=smin/sab;
      double l=log(ymin);
      // add antenna contribution: 67/18-pi^2/3+pi^2/2*(sab>0)+11/6*log(mu^2/(Yslise*abs(sab))-log^2(Yslice);
      wgt+=0.4323540885-l*l;
      if (ymin<1.0) wgt+=Finite(ymin);
      l=log(sab/mu/mu);
      wgt+=0.5*l*l;
      //cout<<a<<" "<<b<<" "<<sab<<endl;
    }
  //wgt-=Nparticle*M_PI*M_PI/2.0;
  wgt+=11.0/6.0*Nparticle*log(mu*mu/smin);
  double C_x1=CrossA_x1*log(smin/mu/mu)+CrossB_x1;
  double C_x2=CrossA_x2*log(smin/mu/mu)+CrossB_x2;
  double Cfactor=C_x1/PDF_x1+C_x2/PDF_x2;
  //cout<<smin<<" "<<Cfactor<<" "<<wgt<<endl;
  wgt+=Cfactor; 
  //double C1=Crossing(x1,smin,mu),C2=CalcCrossing(x1,smin,mu);
  //cout.precision(16);
  //cout<<x1<<": "<<C1<<" "<<C2<<" "<<(C2-C1)/C1<<endl;
  return wgt;
}

double Event::Finite(const double ymin)
{ //Add all finite ymin contributions from the antenna integration.
  double l=log(ymin),l1=log(1.0-ymin),li=Li2(ymin);
  double wgt=11.0/6.0*l1-2.0*li;
  wgt+=ymin*(13.0/3.0+2.0*(l-l1));
  wgt+=ymin*ymin*(-7.0/3.0-1.0/2.0*(l-l1));
  wgt+=ymin*ymin*ymin*(20.0/9.0+1.0/3.0*(l-l1));
  //cout<<ymin<<" "<<wgt<<endl;
  return wgt;
}

void Event::Virtual(double &B,double &A2, double &A1, double &A0)
{ complex<double> b,a2,a1,a0;
  // subtracted terms
  double s2=-Nparticle,s1=-11.0/3.0,s0=0.0;
  for (int a=0;a<Nparticle;a++) 
    { double sab=2.0*(Pord[a]*Pord[(a+1)%Nparticle]);
      if (sab<0.0) {sab=-sab;}
      double l=log(sab/mu/mu);
      //s0+=-0.5*l*l;
      s1+=l;
    }
  a0=(complex<double>) (nvirtual->evalAmp());
  a1=(complex<double>) (nvirtual->getAeps1());
  a2=(complex<double>) (nvirtual->getAeps2());
  b=(complex<double>) (nvirtual->getAtree());
  a0-=s0*b;a1-=s1*b;a2-=s2*b;
  A0=2.0*(double) real(a0*conj(b));
  A1=2.0*(double) real(a1*conj(b));
  A2=2.0*(double) real(a2*conj(b));
  B=(double) Abs2(b);
  return;
}

double Event::Brem(const int nevent,const double smin)
{ 
  double wgt=0.0;
  momentum< complex<double> > *J=new momentum< complex<double> >[Nparticle]; 
  for (int i=0;i<nevent;i++) 
    { int branched;
      double pswgt;
      if (Brancher(branched,pswgt,smin))
	{/*
	  bool PassSmin[Nparticle];
	  for (int i=0;i<Nparticle;i++) 
	    PassSmin[i]=(abs(2.0*(Preal[i]*Preal[((i+1)%Nparticle)]))>smin);
	  bool PassCycle[Nparticle];
	  for (int i=0;i<Nparticle;i++)
	    { PassCycle[i]=true;
	      for (int j=0;j<Nparticle;j++)
		if (i!=j) PassCycle[i]*=PassSmin[j];
	    }
	 */
	  //bool pass=true;
	  //for (int i=0;i<Nparticle;i++) 
	  //if (abs(2.0*(Preal[i]*Preal[((i+1)%Nparticle)]))<smin) pass=false;
	  //if (pass)
	    { Pperm[Nparticle]=Preal[Nparticle];
	      for (int i=0;i<Nparticle;i++)
		//if (PassCycle[i])
		{ for (int j=0;j<Nparticle;j++)
		    Pperm[j]=Preal[(i+j)%Nparticle];
		  //for (int j=0;j<Nparticle+1;j++) cout<<j<<" ---> "<<Pperm[j]<<endl;
		  bool pass=true;
		  for (int i=0;i<Nparticle+1;i++) 
		    if (abs(2.0*(Pperm[i]*Pperm[(i+1)%(Nparticle+1)]))<smin) pass=false;
		  if (pass) {
		    double mewgt=hel_summed_recursion(Nparticle+1,Pperm);
		    double x1=-2.0*Preal[0].p[0]/Elab;
		    double x2=-2.0*Preal[initpos].p[0]/Elab;
		    wgt+=mewgt*HelSumLO*pswgt*PDF(x1,mu/scale)*PDF(x2,mu/scale)*pow(x1*x2,Nparticle-4);}
		}
	      //cout<<"-------------------------------------------------------------"<<endl;
	    }
	}
    }
  delete J;
  double x1=-2.0*Pjet[0].p[0]/Elab;
  double x2=-2.0*Pjet[1].p[0]/Elab;
  wgt/=2.0;
  wgt*=Elab*Elab;
  wgt/=pow(x1*x2,Nparticle-4);
  wgt*=1.0/(2.0*(Nparticle-1)*(Nparticle-2)*(Nparticle-3));
  wgt/=PDF_x1*PDF_x2/x1/x2;
  wgt/=((double) (nevent));
  return wgt;
}

double Event::Abs2(const complex<double> Amp) {return (double) abs(Amp*conj(Amp));}

double Event::RealTest(const double smin)
{ double wgt;
  int branched;
  while(not Brancher(branched,wgt,smin)){};
  momentum< complex<double> > *J=new momentum< complex<double> >[Nparticle]; 
  BGcurrentReal(J);
  bool PassSmin[Nparticle];
  for (int i=0;i<Nparticle;i++) PassSmin[i]=(abs(2.0*(Preal[i]*Preal[(Nparticle-1+i)%Nparticle]))>smin);
  bool PassCycle[Nparticle];
  for (int i=0;i<Nparticle;i++)
    { PassCycle[i]=true;
      for (int j=0;j<Nparticle;j++)
	if (i!=j) PassCycle[i]*=PassSmin[j];
    }
  for (int i=0;i<Nparticle;i++) 
    { double Amp2=0.0,Ginv2=0.0;
      if (PassCycle[i]) 
	{ complex<double> Amp=Jreal[Nparticle]*J[i];
	  complex<double> Ginv=Preal[Nparticle]*J[i];
	  Amp2=(double) Abs2(Amp);
	  Ginv2=(double) Abs2(Ginv);
	}
      string o="";
      int c=48;
      for (int j=0;j<Nparticle+1;j++) 
	{ if (j==i) o+="r";
	  else o+=char(c++);
	}
      cout<<"|m("<<o<<")|^2="<<Amp2<<" ("<<Ginv2<<")"<<endl;
    }
  delete J;
  return wgt;
}


double Event::RealEvent(){ return rambo(Nparticle+1,Preal);}

// Calculates the real radiation phase space weight given the jet configuration Pjet[]. 
// (The real radiation amplitude is set to unity.) 
double Event::RealPS(const int n,const double smin)
{ int branched;
  double wgt,pswgt=0.0;
  for (int i=0;i<n;i++) if (Brancher(branched,wgt,smin)) pswgt+=wgt;
  return pswgt/(double) n;
}

bool Event::JetCuts() 
{ bool pass=true;
  for (int i=2;i<Nparticle;i++) 
    { double pe=Pjet[i].p[0],px=Pjet[i].p[2],py=Pjet[i].p[3],pz=Pjet[i].p[1];
      if (sqrt(px*px+py*py)<PtMin) pass=false;
      if (abs(0.5*log((pe+pz)/(pe-pz)))>EtaMax) pass=false;
    }
  if (pass) 
    { for (int i=2;i<Nparticle-1;i++) 
	{ double pe1=Pjet[i].p[0],px1=Pjet[i].p[2],py1=Pjet[i].p[3],pz1=Pjet[i].p[1];
	  for (int j=i+1;j<Nparticle;j++) 
	    { double pe2=Pjet[j].p[0],px2=Pjet[j].p[2],py2=Pjet[j].p[3],pz2=Pjet[j].p[1];
	      double Deta=0.5*log(abs((pe1+pz1)*(pe2-pz2)/(pe1-pz1)/(pe2+pz2)));
	      double d=(px1*px2+py1*py2)/(sqrt(px1*px1+py1*py1)*sqrt(px2*px2+py2*py2));
	      d=max(d,-1.0);d=min(d,1.0);
	      double Dphi=acos(d);
	      double R=sqrt(Deta*Deta+Dphi*Dphi);
	      if (R<Rmin) pass=false;
	    }
	}
    }
  return pass;
}


// Clusters the (Nparticle+1) Preal momenta to Nparticle Pjet momenta using the jet algorithm.
// This routine is the exact inverse of the Brancher function: Cluster(Brancher(Pjet))=Pjet
void Event::Cluster()
{
  double R12min=1e20;
  double R123min=1e20;
  int a=0,r=0,b=0;
  for (int i=0;i<Nparticle;i++)
    for (int j=i+1;j<Nparticle+1;j++)
      { double Rij=R(i,j);
	if (Rij<R12min) {a=i;r=j;R12min=Rij;}
      }
  for (int i=2;i<Nparticle+1;i++)
    { if (not (i==a or i==r))
	{ double Rijk=R(a,r,i);
	  if (Rijk<R123min) {b=i;R123min=Rijk;}
	}
    }
  momentum<double> *Kc=new momentum<double>[Nparticle+1];
  for (int i=0;i<Nparticle+1;i++) 
    for (int j=0;j<4;j++)
      Kc[i].p[j]=Preal[i].p[j];
  if ((a>1) and (r>1)) 
    { Kc[a]=Preal[a]+Preal[r];
      Kc[b]=Preal[b];
      double gamma=(Kc[a]*Kc[a])/(2.0*Kc[a]*Kc[b]);
      Kc[a]=Kc[a]-gamma*Kc[b];
      Kc[b]=(1.0+gamma)*Kc[b];
      Kc[r].p[0]=0.0;
    }
  else
    {
      if (a<2)
	{ Kc[b]=Preal[b]+Preal[r];
	  Kc[a]=Preal[a];
	  double gamma=(Preal[a]*Preal[b]+Preal[b]*Preal[r]+Preal[r]*Preal[a])/(Preal[a]*Preal[b]+Preal[r]*Preal[a]);
	  Kc[b]=Kc[b]+(1.0-gamma)*Kc[a];
	  Kc[a]=gamma*Kc[a];
	  Kc[r].p[0]=0.0;
	}
      if (r<2)
	{ Kc[b]=Preal[b]+Preal[a];
	  Kc[r]=Preal[r];
	  double gamma=(Preal[r]*Preal[b]+Preal[b]*Preal[a]+Preal[a]*Preal[r])/(Preal[r]*Preal[b]+Preal[a]*Preal[r]);
	  Kc[b]=Kc[b]+(1.0-gamma)*Kc[r];
	  Kc[r]=gamma*Kc[r];
	  Kc[a].p[0]=0.0;
	}
    }
  int pointer=0;
  for (int i=0;i<Nparticle+1;i++)
    if (Kc[i].p[0]!=0.0)
      { for (int j=0;j<4;j++)
	  Pjet[pointer].p[j]=Kc[i].p[j];
	pointer++;
      }
  delete Kc;
}

momentum< complex<double> > Event::hsource(const int k) {return hsource(H[k],Pord[k]);}
momentum< complex<double> > Event::hsource(const momentum<double> P) {int h=1-2*(int) (2.0*uniform());return hsource(h,P);}
momentum< complex<double> > Event::hsource(const int h,const momentum<double> P)
{ momentum<double> b=momentum_(0.0,uniform(-1.0,1.0),uniform(-1.0,1.0),uniform(-1.0,1.0));
  momentum<double> n1=orthogonal(P,b);
  momentum<double> n2=orthogonal(P,n1);
  if (abs(h)!=1) cout<<"Invalid helicity: "<<h<<endl;
  return (n1+n2*complex<double>(0.0,(1.0-2.0*(P.p[0]<0.0))*h))/((complex<double>) sqrt(2.0));
}
momentum<double> Event::orthogonal(const momentum<double> a,const momentum<double> b) 
{ momentum<double> c=momentum_(0.0,a.p[2]*b.p[3]-a.p[3]*b.p[2],a.p[3]*b.p[1]-a.p[1]*b.p[3],a.p[1]*b.p[2]-a.p[2]*b.p[1]);
  return c/sqrt(-c*c);
}

// This is the "standard" BG recursion relation. It calculates the current J(a+1,...,Nparticle-1,0,...a-1)
momentum< complex<double> > Event::BGcurrentBorn(const int a)
{ momentum<double> Q[Nparticle];
  momentum< complex<double> > J[Nparticle-1][Nparticle-1];
  for (int i=0;i<Nparticle-1;i++) J[i][i]=Jjet[(i+a+1)%Nparticle];
  Q[0]=momentum_(0.0,0.0,0.0,0.0);
  for (int i=1;i<Nparticle;i++) Q[i]=Pord[(i+a)%Nparticle]+Q[i-1];
  for (int l=1;l<Nparticle-1;l++)
    { for (int m=0;m<Nparticle-l-1;m++) 
	{ int k=(m+l);
	  momentum< complex<double> > Jmk=momentum_< complex<double> >(0.0,0.0,0.0,0.0); 
	  for (int i=m;i<k;i++) 
	    { momentum<double> K1=Q[i+1]-Q[m],K2=Q[k+1]-Q[i+1];
	      momentum< complex<double> >J1=J[m][i],J2=J[i+1][k];
	      Jmk=Jmk+(2.0*(K2*J1))*J2-(2.0*(K1*J2))*J1+(K1-K2)*(J1*J2);
	    }
	  for (int i=m; i<k-1;i++) 
	    { for (int j=i+1;j<k;j++) 
		{ momentum< complex<double> > J1=J[m][i],J2=J[i+1][j],J3=J[j+1][k];
		  Jmk=Jmk+(2.0*(J1*J3))*J2-(J1*J2)*J3-(J2*J3)*J1;
		}
	    }
	  if (l<Nparticle-2) 
	    { momentum<double> Pm=Q[k+1]-Q[m];
	      Jmk=(1.0/(Pm*Pm))*Jmk;
	    }
	  J[m][k]=Jmk;
	}
    }
  return J[0][Nparticle-2];  
}

momentum< complex<double> > Event::BGcurrentReal(const int a)
{ momentum<double> Q[Nparticle+1];
  momentum< complex<double> > J[Nparticle][Nparticle];
  for (int i=0;i<Nparticle;i++) J[i][i]=Jreal[(i+a+1)%(Nparticle+1)];
  Q[0]=momentum_(0.0,0.0,0.0,0.0);
  for (int i=1;i<Nparticle+1;i++) Q[i]=Preal[(i+a)%(Nparticle+1)]+Q[i-1];
  for (int l=1;l<Nparticle;l++)
    { for (int m=0;m<Nparticle-l;m++) 
	{ int k=(m+l);
	  momentum< complex<double> > Jmk=momentum_< complex<double> >(0.0,0.0,0.0,0.0); 
	  for (int i=m;i<k;i++) 
	    { momentum<double> K1=Q[i+1]-Q[m],K2=Q[k+1]-Q[i+1];
	      momentum< complex<double> >J1=J[m][i],J2=J[i+1][k];
	      Jmk=Jmk+(2.0*(K2*J1))*J2-(2.0*(K1*J2))*J1+(K1-K2)*(J1*J2);
	    }
	  for (int i=m; i<k-1;i++) 
	    { for (int j=i+1;j<k;j++) 
		{ momentum< complex<double> > J1=J[m][i],J2=J[i+1][j],J3=J[j+1][k];
		  Jmk=Jmk+(2.0*(J1*J3))*J2-(J1*J2)*J3-(J2*J3)*J1;
		}
	    }
	  if (l<Nparticle-1) 
	    { momentum<double> Pm=Q[k+1]-Q[m];
	      Jmk=(1.0/(Pm*Pm))*Jmk;
	    }
	  J[m][k]=Jmk;
	}
    }
  return J[0][Nparticle-1];  
}


// This BG recursion returns the list of ordered currents {J(0,1,...,N-1,N),J(1,2,...,N,0),...,J(N,1,....,N-2,N-1)}
// so that we can calculate all the amplitudes {m(0,1,...,N,r),m(0,1,...,r,N),...,m(0,1,r,...,N),m(0,r,1,...,N)}
// (N=Nparticle-1).
void Event::BGcurrentReal(momentum< complex<double> >* Jc)
{ momentum<double> P[Nparticle][Nparticle];
  momentum< complex<double> > J[Nparticle][Nparticle];
  for (int i=0;i<Nparticle;i++) {J[i][i]=Jreal[i];P[i][i]=Preal[i];}
  for (int l=1;l<Nparticle;l++)
    { for (int m=0;m<Nparticle;m++) 
	{ int k=(m+l);
	  momentum< complex<double> > Jmk=momentum_< complex<double> >(0.0,0.0,0.0,0.0);
	  P[m][k%Nparticle]=P[m][m]+P[(m+1)%Nparticle][k%Nparticle];
	  for (int i=m;i<k;i++) 
	    { momentum<double> K1=P[m][i%Nparticle],K2=P[(i+1)%Nparticle][k%Nparticle];
	      momentum< complex<double> >J1=J[m][i%Nparticle],J2=J[(i+1)%Nparticle][k%Nparticle];
	      Jmk=Jmk+(2.0*(K2*J1))*J2-(2.0*(K1*J2))*J1+(K1-K2)*(J1*J2);
	    }
	  for (int i=m; i<k-1;i++) 
	    { for (int j=i+1;j<k;j++) 
		{ momentum< complex<double> > J1=J[m][i%Nparticle],J2=J[(i+1)%Nparticle][j%Nparticle],J3=J[(j+1)%Nparticle][k%Nparticle];
		  Jmk=Jmk+(2.0*(J1*J3))*J2-(J1*J2)*J3-(J2*J3)*J1;
		}
	    }
	  if (l<Nparticle-1) 
	    { momentum<double> Pm=P[m][k%Nparticle];
	      Jmk=(1.0/(Pm*Pm))*Jmk;
	    }
	  J[m][k%Nparticle]=Jmk;
	}
    }
  for (int i=0;i<Nparticle;i++) Jc[i]=J[i][(i+Nparticle-1)%Nparticle];
};


// The helicity summed BG recursion relation and support functions:

#define INDEX(len, start, mask) (((N-(len)+3+(start))<<(len))-2*N-4+(mask))
#define MOM_INDEX(len, start) (((len)-1)*(N*2+2-(len))/2+(start))
struct Event::pamp_t {momentum<double> J;double W[6];};


double Event::hel_summed_recursion(int N, momentum<double> *p) {
  N--; // Not interested in last gluon
  
  init_recursion(N);	// make sure there is adequate memory allocated
  
  // init momentum table
  REP(i, N) momenta[MOM_INDEX(1, i)]=p[i];
  FOR(len, 2, N+1) REP(start, N+1-len) momenta[MOM_INDEX(len, start)]=p[start]+momenta[MOM_INDEX(len-1, start+1)];
    // nullify recursion table
  memset(buffer, 0, INDEX(N+1, 0, 0)*sizeof(pamp_t));
    // init recursion table
  momentum<double> b=momentum_<double>(0.0,uniform(-1.0,1.0),uniform(-1.0,1.0),uniform(-1.0,1.0));
  REP(i, N) {
    pamp_t pamp;
    REP(i, 6) pamp.W[i]=0.0;
    REP(i, N) {
      pamp.J=orthogonal(p[i],b);
      buffer[INDEX(1, i, 0)]=pamp;
      pamp.J=orthogonal(p[i],pamp.J);
      buffer[INDEX(1, i, 1)]=pamp;
    }
  }
  FOR(len, 2, N+1) REP(start, N+1-len) {
    FOR(k, 1, len) REP(mask, 1<<len) {
      bracket(buffer[INDEX(len, start, mask)], 
	      buffer[INDEX(k, start, mask&((1<<k)-1))], momenta[MOM_INDEX(k, start)],
	      buffer[INDEX(len-k, start+k, mask>>k)], momenta[MOM_INDEX(len-k, start+k)]);
    }

    if(len<N) {
      double P2=momenta[MOM_INDEX(len, start)]*momenta[MOM_INDEX(len, start)];
      REP(mask, 1<<len) buffer[INDEX(len, start, mask)].J/=P2;
    }
  }
  double res=0.0;
  REP(mask, 1<<N) res-=buffer[INDEX(N, 0, mask)].J*buffer[INDEX(N, 0, mask)].J;

  return res;
}

// index in the array is calculated
// the address of the array for a particular len is given by (N-len+3)*2^len-2*N
// the address in that array for a particular start is given by start*2^len

inline
void Event::bracket(pamp_t &res, const pamp_t &a, const momentum<double> &pa, const pamp_t &b, const momentum<double> &pb) {
  // J=J1.J2 (p1-p2) - 2p1.J2 J1 + 2p2.J1 J2
  res.J+=(a.J*b.J)*(pa-pb)+(2.0f*(pb*a.J))*b.J-(2.0f*(pa*b.J))*a.J;
  static const int mu_ind[]={0,0,0,1,1,2};
  static const int nu_ind[]={1,2,3,2,3,3};
  REP(i, 6) {
    int mu0=mu_ind[i], nu=nu_ind[i];
    // J+=W2.J1-W1.J2
    res.J[mu0]-=b.W[i]*a.J[nu]-a.W[i]*b.J[nu];  // nu>0 so extra minus from the Minkowski metric
    // extra minus because of anti-sym W
    if(mu0) res.J[nu]+=b.W[i]*a.J[mu0]-a.W[i]*b.J[mu0]; // mu0>0 so extra minus from the Minkowski metric
    else res.J[nu]-=b.W[i]*a.J[mu0]-a.W[i]*b.J[mu0];
    // W=J1 x J2 - J2 x J1
    res.W[i]+=a.J[mu0]*b.J[nu]-b.J[mu0]*a.J[nu];
  }
}

void Event::free_recursion() {
  if(buffer) free(buffer);
  if(momenta) free(momenta);
  buffer_size=0;
  buffer=NULL;
  momenta=NULL;
}

// allocate the necessary buffers
void Event::init_recursion(int N) {
  int size=INDEX(N+1, 0, 0);
  if(buffer) {	// something allocated?
    if(buffer_size>=size) return;	// enough memory
    free_recursion();
  }
  buffer_size=size;
  buffer=(pamp_t *)malloc(buffer_size*sizeof(pamp_t));
  momenta=(momentum<double> *)malloc(MOM_INDEX(N+1, 0)*sizeof(momentum<double>));
}

double Event::rambo() 
{
  double wgt=rambo(Nparticle,Pjet);
  for (int a=0;a<Nparticle;a++)
    { int a0=a;if (a==1) a0=initpos;if (a==initpos) a0=1;
      Pord[a]=Pjet[a0];
      //for (int j=0;j<4;j++)
      K[a][0]=(DOUBLE) Pjet[a0].p[0];
      K[a][1]=(DOUBLE) Pjet[a0].p[1];
      K[a][2]=(DOUBLE) Pjet[a0].p[2];
      K[a][3]=(DOUBLE) Pjet[a0].p[3];
    }
  return wgt;
}

double Event::rambo(const int N,momentum<double> *P)
{ double x1=uniform(xmin,1.0);
  double x2=uniform(xmin,1.0);
  P[0]=-Elab*x1*momentum_<double>(0.5, 0.5, 0.0, 0.0);
  P[1]=-Elab*x2*momentum_<double>(0.5,-0.5, 0.0, 0.0);
  double Ecm=Elab*sqrt(x1*x2);
  momentum<double> R=momentum_(0.0,0.0,0.0,0.0);
  for (int i=2;i<N;i++) 
    { double c=uniform(-1.0,1.0);
      double s=sqrt(abs(1.0-c*c));
      double f=2.0*M_PI*uniform(0.0,1.0);
      P[i]=-log(uniform(xmin,1.0)*uniform(xmin,1.0))*momentum_(1.0,c,s*cos(f),s*sin(f));
      R=R+P[i]; 
    }
  double Rmass=sqrt(abs(R*R));
  R=-R/Rmass;
  double a=1.0/(1.0-R.p[0]);
  double x=Ecm/Rmass;
  for (int i=2;i<N;i++) 
    { double bq=R.p[1]*P[i].p[1]+R.p[2]*P[i].p[2]+R.p[3]*P[i].p[3];
      double xq=P[i].p[0]+a*bq;
      P[i]=x*momentum_(-R.p[0]*P[i].p[0]+bq,P[i].p[1]+R.p[1]*xq,P[i].p[2]+R.p[2]*xq,P[i].p[3]+R.p[3]*xq);
    }
  ramboo(Ecm,N,P);
  return 1.0;
};

void Event::ramboo(const double Ec,const int N,momentum<double> *k)
{ for (int i=2;i<N;i++) 
    { double Eb=-(k[0].p[0]+k[1].p[0])/Ec;
      double pz=-(k[0].p[1]+k[1].p[1])/Ec;
      double f=k[i].p[0];
      k[i].p[0]=(Eb*f+pz*k[i].p[1]);
      f+=k[i].p[0];f*=pz/(1.0+Eb);
      k[i].p[1]+=f;
    }
}

bool Event::Brancher(int &branched,double &pswgt,const double Smin)
{ for (int i=0;i<Nparticle;i++) { Preal[i]=Pjet[i];Jreal[i]=Jjet[i];}
  int a,b;
  double nff=(double) (Nparticle-3)/(double) (Nparticle-1);
  if (uniform(0.0,1.0)<nff)
    { do 
	{
	  a=(int) (uniform(2.0,(double) Nparticle));
	  b=(int) (uniform(2.0,(double) Nparticle));
	} while (a==b); 
    }
  else
    { a=(int) (uniform(0.0,2.0));
      b=(int) (uniform(2.0,(double) Nparticle));
    }
  //if (uniform(0.0,1.0)<0.5) {a=2;b=3;}
  //else {a=3;b=2;}
  //a=1;b=2;
  if (a>1) 
    { pswgt=SequentialBrancherFF(a,b,Smin);
      pswgt*=0.5*(Nparticle-2.0)*(Nparticle-3.0)*(Pjet[a]*Pjet[b])/(Pjet[0]*Pjet[1]);
      //Jreal[a]=hsource(H[a],Preal[a]);// as Pjet[b] was rescaled, the polarization vector of b is unchanged.
      branched=a;
    }
  else if (a==0)
    { pswgt=SequentialBrancherIF(a,b,Smin);
      if (Nparticle>4)
	pswgt*=(Nparticle-2.0)*(Nparticle-3.0)*pow((Pjet[0]*Pjet[1])/abs(Preal[a]*Pjet[1]),Nparticle-4.0)/abs(Preal[a]*Pjet[1]);
      else
	pswgt*=2.0/abs((Preal[a]*Pjet[1]));
      //Jreal[b]=hsource(H[b],Preal[b]);// as the incoming momentum Pjet[a] was rescaled, the polarization vector of a is unchanged.
      branched=b;
    }
  else
    { pswgt=SequentialBrancherIF(a,b,Smin);
      if (Nparticle>4)
	pswgt*=(Nparticle-2.0)*(Nparticle-3.0)*pow((Pjet[0]*Pjet[1])/abs(Preal[a]*Pjet[0]),Nparticle-4.0)/abs(Preal[a]*Pjet[0]);
      else
	pswgt*=2.0/abs(Preal[a]*Pjet[0]);
      //Jreal[b]=hsource(H[b],Preal[b]); // as the incoming momentum Pjet[a] was rescaled, the polarization vector of a is unchanged.
      branched=b;
    }
  bool pass=true;
  if (pswgt>0.0) 
    { pswgt*=(Nparticle-1.0)*(Nparticle-1.0)*(Nparticle-2.0);
      double R12min=1e30;
      int a0=-1,r0=-1;
      pass=false;
      for (int i=0;i<Nparticle;i++)
	for (int j=i+1;j<Nparticle+1;j++)
	  { double s=R(i,j);
	    if (s<R12min) {R12min=s;a0=i;r0=j;} 
	  }
      if ((a==a0) and (Nparticle==r0)) pass=true;
      if ((a==r0) and (Nparticle==a0)) pass=true;
      int b0=-1;
      if (pass)
	{ double R123min=1e30;
	  pass=false;
	  for (int i=2;i<Nparticle;i++)
	    if (not (i==a))
	      { double s=R(a,Nparticle,i);
		if (s<R123min) {R123min=s;b0=i;} 
	      }
	  if (b0==b) {pass=true;}
	}
    }
  else
    pass=false;
  momentum<double> p=Preal[initpos];
  Preal[initpos]=Preal[1];
  Preal[1]=p;
  return pass;
}
double Event::SequentialBrancherIF(const int a,const int b,const double smin)
{ double x=-2.0*Pjet[a].p[0]/Elab;
  double Kmax=pow(Pjet[b].p[0]+0.5*(1.0-x)*Elab,2);
  //Kmax*=100;
  double wgt;
  if (samplingIF)
    { double r1=abs(uniform(0.0,1.0));
      double r2=abs(uniform(0.0,1.0));
      double r3=uniform(0.0,1.0);
      double ymin=abs(0.5*smin/Elab/sqrt(Kmax));
      double y=pow(ymin,r1);
      double ct=1.0-2.0*y;
      double st=sqrt(abs(1.0-ct*ct));
      wgt=2.0*M_PI*abs(y*log(ymin));
      double Kmin=pow(smin/Elab/y,2.0);
      Kmin/=100.0;
      double kr2=pow(Kmax,r2)*pow(Kmin,(1.0-r2));
      wgt*=abs(kr2*log(abs(Kmax/Kmin)));
      double phi=2.0*M_PI*r3;
      double cp=cos(phi);
      double sp=sin(phi);
      if (Pjet[a].p[1]<0.0)
	Preal[Nparticle]=sqrt(abs(kr2))*momentum_(1.0,ct,cp*st,sp*st);
      else
	Preal[Nparticle]=sqrt(abs(kr2))*momentum_(1.0,-ct,-cp*st,-sp*st);
    }
  else
    { double r1=abs(uniform(0.0,1.0));
      double r2=abs(uniform(0.0,1.0));
      double r3=uniform(0.0,1.0);
      double kr2=Kmax*r1;
      wgt=2.0*M_PI*Kmax;
      double ct=2.0*r2-1.0;
      double st=sqrt(abs(1.0-ct*ct));
      double phi=2.0*M_PI*r3;
      double cp=cos(phi);
      double sp=sin(phi);
      if (Pjet[a].p[1]<0.0)
	Preal[Nparticle]=sqrt(abs(kr2))*momentum_(1.0,ct,cp*st,sp*st);
      else
	Preal[Nparticle]=sqrt(abs(kr2))*momentum_(1.0,-ct,-cp*st,-sp*st);
    }
  double d=Pjet[a]*Preal[Nparticle]-Pjet[a]*Pjet[b];
  if (d<1e-18) wgt=-1.0;
  double z=1.0+(Preal[Nparticle]*Pjet[b])/d;
  if (z*x>1.0) wgt=-1.0;
  wgt*=-0.5*(Pjet[a]*Pjet[b])/d/M_PI;
  Preal[b]=Pjet[b]-Preal[Nparticle]+(1.0-z)*Pjet[a];
  if (Preal[b].p[0]<0.0) wgt=-1.0;
  Preal[a]=z*Pjet[a];
  double sar=2.0*abs(Preal[a]*Preal[Nparticle]);
  double srb=2.0*abs(Preal[Nparticle]*Preal[b]);
  if ((sar<smin) or (srb<sar)) wgt=-1.0; 
  if (R(Nparticle,b)<R(a,Nparticle)) wgt=-1.0;
  return wgt;
}

double Event::SequentialBrancherFF(const int a,const int b,const double smin)
{
  double s12=2.0*(Pjet[a]*Pjet[b]);
  double ymin=smin/s12;
  double sar,srb;
  double wgt=1.0;
  if (ymin>=1.0) {wgt=-1.0;ymin=0.5;}
  if (samplingFF)
    { double r1=abs(uniform(0.0,1.0));
      double r2=abs(uniform(0.0,1.0));
      double yrb=pow(0.5,r1)*pow(ymin,1.0-r1);
      double yar=pow(yrb,r2)*pow(ymin,1.0-r2);
      wgt*=2.0*yar*yrb*log(0.5/ymin)*log(yrb/ymin);
      //if (uniform(0.0,1.0)>0.5) yrb=1.0-yrb;
      sar=s12*yar;srb=s12*yrb;
      /*
      double yar=pow(ymin,r1);
      double a=pow(1.0/ymin-1.0,2.0*r2-1.0);
      double yrb=a/(1.0+a);
      if (yar<yrb) { sar=yar*s12;srb=yrb*s12; }
      else wgt=0.0;
      if (yar+yrb>1.0) wgt=0.0;
      wgt*=-2.0*yar*yrb*(1.0-yrb)*log(ymin)*log((1.0/ymin-1.0));
      */
    }
  else
    { double yar=abs(uniform(0.0,1.0));
      double yrb=abs(uniform(0.0,1.0));
      if (yar<yrb) { sar=yar*s12;srb=yrb*s12; }
      else { sar=yrb*s12;srb=yar*s12; }
      if (yar+yrb>1.0) {yar=s12-srb;yrb=s12-sar;sar=yar;srb=yrb; }
      wgt=0.25;
    }
  double gamma=sar/s12;
  Preal[b]=(1.0-gamma)*Pjet[b];
  // Goto rest frame of Q
  momentum<double> Q=Pjet[a]+gamma*Pjet[b];
  momentum<double> P;
  boost(1,Q,P,Preal[b]);
  // Calculate angles wrt positive z-axis, so we can rotate system such that Pb is along positive z-axis
  double dp=sqrt(abs(P.p[1]*P.p[1]+P.p[2]*P.p[2]));
  double sphi=P.p[2]/dp;
  double cphi=P.p[1]/dp;
  P.p[1]=dp;
  dp=sqrt(abs(P.p[1]*P.p[1]+P.p[3]*P.p[3]));
  double sthe=P.p[1]/dp;
  double cthe=P.p[3]/dp;
  // Calculate decay of Q in its rest system a K[0]*K[1])/(K[1]*Pb) and Pb is along the positive z-axis.
  // The decay is subject to the constraint 2*(Pr*Pb)=srb wich fixes the z-components. 
  double Ea=sqrt(abs(Q*Q));
  double c=1.0-2.0*srb/(s12-sar);
  double s=sqrt(abs(1.0-c*c));
  double f=2.0*M_PI*uniform(0.0,1.0);
  Preal[Nparticle]=0.5*Ea*momentum_(1.0,s*cos(f),s*sin(f),c);
  Preal[a]=0.5*Ea*momentum_(1.0,-s*cos(f),-s*sin(f),-c);
  // Rotate the system such that Pb is along its original direction and boost back to lab.
  double px=Preal[a].p[3]*cthe-Preal[a].p[1]*sthe;
  double py=Preal[a].p[3]*sthe+Preal[a].p[1]*cthe;
  Preal[a].p[3]=px;
  Preal[a].p[1]=py;
  px=Preal[a].p[1]*cphi-Preal[a].p[2]*sphi;
  py=Preal[a].p[1]*sphi+Preal[a].p[2]*cphi;
  Preal[a].p[1]=px;
  Preal[a].p[2]=py;
  boost(0,Q,Preal[a],Preal[a]);
  px=Preal[Nparticle].p[3]*cthe-Preal[Nparticle].p[1]*sthe;
  py=Preal[Nparticle].p[3]*sthe+Preal[Nparticle].p[1]*cthe;
  Preal[Nparticle].p[3]=px;
  Preal[Nparticle].p[1]=py;
  px=Preal[Nparticle].p[1]*cphi-Preal[Nparticle].p[2]*sphi;
  py=Preal[Nparticle].p[1]*sphi+Preal[Nparticle].p[2]*cphi;
  Preal[Nparticle].p[1]=px;
  Preal[Nparticle].p[2]=py;
  boost(0,Q,Preal[Nparticle],Preal[Nparticle]);
  sar=2.0*abs(Preal[a]*Preal[Nparticle]);
  srb=2.0*abs(Preal[Nparticle]*Preal[b]);
  if ((sar<smin) or (srb<sar)) wgt=-1.0; 
  if ((R(Nparticle,b)<R(a,Nparticle)) or (R(a,b)<R(Nparticle,b))) wgt=-1.0;
  return wgt;
}

void Event::boost(const int lflag,const momentum<double> q,momentum<double> &ph,momentum<double> &p)
{
  double rsq = sqrt(abs(q*q));
  if (lflag==0) {
    double dp = (q.p[0]*ph.p[0]+q.p[1]*ph.p[1]+q.p[2]*ph.p[2]+q.p[3]*ph.p[3])/rsq;
    double c1 = (ph.p[0]+dp)/(rsq+q.p[0]);
    p=ph+c1*q;
    p.p[0]=dp;
  }
  else {
    double dp = (q*p)/rsq;
    double c1 = (p.p[0]+dp)/(rsq+q.p[0]);
    ph =p-c1*q;
    ph.p[0]=dp;
  }
}

double Event::R(const int a,const int b){return 2.0*abs(Preal[a]*Preal[b]);}

/*
double Event::R(const int a,const int b)
{ double Pea=Preal[a].p[0],Pxa=Preal[a].p[2],Pya=Preal[a].p[3],Pza=Preal[a].p[1];
  double Peb=Preal[b].p[0],Pxb=Preal[b].p[2],Pyb=Preal[b].p[3],Pzb=Preal[b].p[1];
  double Pt2a=Pxa*Pxa+Pya*Pya;
  double rapa=0.5*log(abs((Pea+Pza)/(Pea-Pza)));
  double Pt2b=Pxb*Pxb+Pyb*Pyb;
  double rapb=0.5*log(abs((Peb+Pzb)/(Peb-Pzb)));
  double Q2=1e16;
  if ((a>1) and (b>1)) 
    { double Deta=rapa-rapb;
      double d=(Pxa*Pxb+Pya*Pyb)/(sqrt(Pt2a)*sqrt(Pt2b));
      d=max(d,-1.0);d=min(d,1.0);
      double Dphi=acos(d);
      double Rab=(Deta*Deta+Dphi*Dphi)/0.49;
      Q2=sqrt(Pt2a*Pt2b)*Rab;
      //Q2=min(Pt2a,Pt2b)*Rab;
    }

  //if ((a==0) and (b>1)) Q2=2.0*sqrt(Pt2b)*exp(-rapb);
  //if ((a==1) and (b>1)) Q2=2.0*sqrt(Pt2b)*exp( rapb);
  //if ((b==0) and (a>1)) Q2=2.0*sqrt(Pt2a)*exp(-rapa);
  //if ((b==1) and (a>1)) Q2=2.0*sqrt(Pt2a)*exp( rapa);

  if ((a==0) and (b>1)) Q2=2.0*Pea*sqrt(Pt2b)*exp(-rapb);
  if ((a==1) and (b>1)) Q2=2.0*Pea*sqrt(Pt2b)*exp( rapb);
  if ((b==0) and (a>1)) Q2=2.0*Peb*sqrt(Pt2a)*exp(-rapa);
  if ((b==1) and (a>1)) Q2=2.0*Peb*sqrt(Pt2a)*exp( rapa);

  return abs(Q2);
}
*/

/*
double Event::R(const int a,const int b)
{ double r=1.0;
  double Pea=Preal[a].p[0],Pxa=Preal[a].p[2],Pya=Preal[a].p[3],Pza=Preal[a].p[1];
  double Peb=Preal[b].p[0],Pxb=Preal[b].p[2],Pyb=Preal[b].p[3],Pzb=Preal[b].p[1];
  double Pt2a=Pxa*Pxa+Pya*Pya;
  double rapa=0.5*log(abs((Pea+Pza)/(Pea-Pza)));
  double Pt2b=Pxb*Pxb+Pyb*Pyb;
  double rapb=0.5*log(abs((Peb+Pzb)/(Peb-Pzb)));
  double Q2=1e16;
  if ((Pea>0.0) and (Peb>0.0)) 
    { 
      double Deta=rapa-rapb;
      double d=(Pxa*Pxb+Pya*Pyb)/(sqrt(Pt2a)*sqrt(Pt2b));
      d=max(d,-1.0);d=min(d,1.0);
      double Dphi=acos(d);
      double Rab=(Deta*Deta+Dphi*Dphi)/0.49;
      Q2=min(pow(Pt2a,r),pow(Pt2b,r))*Rab;
    }
  if ((Pea<0.0) and (Peb>0.0)) Q2=pow(Pt2b,r)*exp(-rapb);
  if ((Pea>0.0) and (Peb<0.0)) Q2=pow(Pt2a,r)*exp(rapa);
  if ((Pea<0.0) and (Peb<0.0)) Q2=2.0*(Preal[a]*Preal[b]); 
  return abs(Q2);
}
*/


 double Event::R(const int a,const int r,const int b){return min(R(a,b),R(r,b));}

double Event::uniform() 
{
  double random,s,t;
  int m,seed1=1429,seed2=9343;
  int i=(seed1/177)%177+2,j=(seed1%177)+2,k=(seed2/169)%178+1,l=seed2%169;
  static bool init=false;
  static int iranmr=96,jranmr=32;
  static double ranc=362436.0/16777216.0,rancd=7654321.0/16777216.0,
    rancm=16777213.0/16777216.0,ranu[97];
  if (init==false) 
    { init=true;
      for (int il=0;il<97;++il)
	{ s=0;t=0.5;
	  for (int jl=0;jl<24;++jl)
	    { m=(((i*j%179)*k)%179);i=j;j=k;k=m;l=(53*l+1)%169;
	      if (((l*m)%64)>=32) s+=t;
	      t*=0.5;
	    };
	  ranu[il]=s;
	};
    };
  random=ranu[iranmr]-ranu[jranmr];
  if (random<0.0) ++random;
  ranu[iranmr--]=random;
  jranmr--;
  if (iranmr<0) iranmr=96;
  if (jranmr<0) jranmr=96;
  ranc-=rancd;
  if (ranc<0.0) ranc+=rancm;
  random-=ranc;
  if (random<0.0) random++;
  return random;  
}

double Event::uniform(const double min,const double max) {return min+(max-min)*uniform();}



