#include <cstdio>
#include <iostream>
#include <string>
#include <math.h>
#include "RootLibs.h"
#include "Event.h"
#include "ExternalFunctions.h"

using namespace std;

int main(void)
{
  cout.precision(6);
  int jets=6;
  double Ecm=7000.0;
  double Ptmin=70.0,etamax=3.0,Rmin=0.4;
  string PDF="cteq6";
  Event Njets(jets,Ecm,Ptmin,etamax,Rmin,PDF);//create event, set all physics parameters
  //int *h=new int[5];
  //h[0]=-1;h[1]=-1;h[2]=1;h[3]=-1;h[4]=1;
  for (int j=0;j<1;j++){
  Njets.JetEvent();//Generate an event (using the defaul event generator Rambo) which passes Jet Cuts.
  Njets.Helicities();//Generate a random Helicity configuration and calculate helicity vectors (with randomized gauge choice)
  Njets.PrintEvent();//Print Event information
  double mu=1000.0;
  Njets.Scale(mu);
  double LO=Njets.Born();
  double B,V2,V1,V0;
  Njets.Virtual(B,V2,V1,V0);
  Njets.Sources();//Recalculates helicity vectors (with randomized gauge choice).
  double LOc=Njets.LO();
  double Bc,V2c,V1c,V0c;
  Njets.Virtual(Bc,V2c,V1c,V0c);
  cout<<"Gauge invariance check:"<<endl;
  cout<<"         Born      :    "<<LO<<" "<<LOc<<" ("<<LO/LOc<<")"<<endl;
  cout<<"         Tree-Level:    "<<B<<" "<<Bc<<" ("<<B/Bc<<")"<<endl;
  cout<<"         One-loop:e^-2:"<<V2<<" "<<V2c<<" ("<<V2/V2c<<")"<<endl;
  cout<<"         One-loop:e^-1:"<<V1<<" "<<V1c<<" ("<<V1/V1c<<")"<<endl;
  cout<<"         One-loop:e^0 :"<<V0<<" "<<V0c<<" ("<<V0/V0c<<")"<<endl;
  cout<<"----------------------------------------------------------------------------"<<endl;}
  cout<<"Testing real"<<endl;
  double smin=1e-5;
  for (int i=0;i<1;i++)
    { cout<<"+++++++++ Real event "<<i<<" +++++++++++++"<<endl;
      double real=Njets.RealTest(smin);
      Njets.Real(1,smin);
      Njets.PrintReal();
      Njets.Cluster();
      cout<<"Clustering Preal --> Pjet"<<endl;
      Njets.PrintEvent();
    }
  cout<<"Done"<<endl;
}  

