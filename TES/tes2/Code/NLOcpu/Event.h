#include "momentum.h"
#include <complex>
#include "NGluon.h"

using namespace std;

class Event
{
 public:
  Event(const int,const double,const double,const double,const double,string,const int);
  double JetEvent();
  double JetEvent(const string);
  double JetEvent(const double);
  double JetEvent(const momentum<double>*,const double);
  void Helicities();
  void Helicities(const double);
  void Helicities(const int*);
  void Helicities(const int*,const double);
  void Sources();
  void Scale();
  void Scale(const string,const double);
  void Scale(const double);
  void PrintEvent();
  void PrintReal();
  double LO();
  double LO(int*);
  double Kfactor(const double,const int);  
  double Rfactor(const double);  
  void Virtual(double&,double&,double&,double&);  
  double Brem(const int,const double);
  double RealEvent();
  double RealPS(const int,const double);
  double RealTest(const double);
  void Cluster();
  bool JetCuts();
  double Ht();
  double Azimuth();
  double AlphaS;

  ~Event();
 private:
  struct pamp_t;
  double Finite(const double);
  void bracket(pamp_t&,const pamp_t&,const momentum<double>&p,const pamp_t&,const momentum<double>&);
  double hel_summed_recursion(int,momentum<double>*);
  void free_recursion();
  void init_recursion(int);
  momentum< complex<double> > hsource(const int);
  momentum< complex<double> > hsource(const momentum<double>);
  momentum< complex<double> > hsource(const int,const momentum<double>);
  momentum<double> orthogonal(const momentum<double>,const momentum<double>);
  momentum< complex<double> > BGcurrentBorn(const int);
  momentum< complex<double> > BGcurrentReal(const int);
  void BGcurrentReal(momentum< complex<double> >*);
  double Abs2(const complex<double>);
  double rambo();
  double rambo(const int,momentum<double>*);
  void ramboo(const double,int,momentum<double>*);
  bool Brancher(int&,double&,const double);
  double SequentialBrancherIF(const int,const int,const double);
  double SequentialBrancherFF(const int,const int,const double);
  void boost(const int,const momentum<double>,momentum<double>&,momentum<double>&);
  double R(const int,const int);
  double R(const int,const int,const int);
  double uniform();
  double uniform(const double,const double);
  bool samplingFF,samplingIF;
  int Nparticle,initpos;
  static const int Nc=3;
  static const double xmin=1e-8;
  static const double gev2pb=0.389379304;
  double Elab,PtMin,EtaMax,Rmin,scale;
  double PsWeight,SourceWeight,mu,CrossFactorBorn,CrossFactorReal,HelSumLO;
  string megenerator;
  double PDF_x1,CrossA_x1,CrossB_x1;
  double PDF_x2,CrossA_x2,CrossB_x2;
  //double AlphaS;
  int *H;
  DOUBLE (*K)[4];//NGluon interface requires this structure for momenta storage
  momentum<double> *Pjet,*Pord,*Preal,*Pperm;
  momentum< complex<double> > *Jjet,*Jreal;
  NGluon *nvirtual;
  momentum<double> *momenta;
  pamp_t *buffer;
  int buffer_size;
};
