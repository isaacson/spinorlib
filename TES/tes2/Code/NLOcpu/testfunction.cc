#include <cstdio>
#include <iostream>
#include <math.h>
#include "momentum.h"
#include "TH1.h"
#include "TF1.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"
#include "TGaxis.h"
#include "TLegend.h"
#include "TLatex.h"

using namespace std;

extern momentum<double> *Rambo(double,int,double&);
extern double Ht(int,momentum<double>*);
extern double Aveta(int,momentum<double>*);
extern double Azimuth(int,momentum<double>*);
extern bool cuts(int,momentum<double>*,double,double,double);
extern bool *Brancher(const int,momentum<double>*,momentum<double>*,momentum<double>*,momentum<double>*
		      ,int&,double&,const double,const double);
extern bool JetCuts(int,momentum<double>*,double,double,double);
extern momentum<double> *Cluster(int,momentum<double>*);
extern double TestTreeLevel(const int,const momentum<double>*,const double);
extern double TreeLevel(const int,const int,const momentum<double>*,const momentum<double>*,double);
extern double TreeLevel(const int,const momentum<double>*,const momentum<double>*,double);
extern double TreeLevelInsert(const int,momentum<double>*,momentum<double>*,double);
extern double *PolWeight(const int,const momentum<double>*,const momentum<double>*);
extern double TestAnly(const int,const momentum<double>*,const double);
extern momentum<double> orthogonal(const momentum<double>); 
extern int fac(int);
extern void initPDF();

int main(void)
{
  int Jets=5;
  int sweeps=1000;
  int Nrad=1000;
  float Ecm=7000.0/7000.0*(double) Jets;
  float Ptmin=70.0/7000.0*(double) Jets;
  float etamax=3.0;
  float Rmin=0.4;
  double Smax=Ecm*Ecm;
  //
  // set-up
  //
  initPDF();
  int N=Jets+2;
  // Rambo(N) vs Rambo(N-1)*Brancher --> plot N particle observables for comparison
  // root set-up mumbo jumbo
  gROOT->Reset();
  gROOT->SetStyle("Plain");
  TGaxis::SetMaxDigits(3);
  //gROOT->ForceStyle();
  TApplication myapp("myapp",0,0);
  // set-up root screens
  TCanvas *screen = new TCanvas("screen","Antenna test",600,600);
  gStyle->SetOptStat(0);
  gStyle->SetTitleOffset(1.0,"y");
  gStyle->SetTitleOffset(1.0,"x");
  gStyle->SetTitleSize(0.10,"x");
  gStyle->SetPadBottomMargin(0.25);
  TPad* s1=new TPad("s1","s1",0,0,1.0,1.0);
  s1->Draw();
  //s1->SetLogx();
  //s1->SetLogy();
  s1->cd();
  screen->cd(0);
  //TLegend *legend1=new TLegend(0.58,0.70,0.75,0.88);
  //TLatex *header=new TLatex(0.2,0.91,"Smin cancelation Antenna Function: 2 jet inclusive");
  //header->SetNDC();
  //header->SetTextSize(0.06);
  //legend1->SetFillColor(0);
  //legend1->SetBorderSize(0);
  //legend1->SetTextSize(0.045);
  //legend1->AddEntry(h1,"Rambo + Clustering","l");
  TH1D *h1=new TH1D("","",100,-9.95,0.05);
  TF1 *fit=new TF1("fit","pol2",-10,-6);
  h1->SetLineColor(kBlue);
  fit->SetLineColor(kRed);
  fit->SetLineWidth(1);
  //fit->FixParameter(0,0);
  fit->FixParameter(2,N);
  cout<<endl<<"Number of jets = "<<Jets<<endl;
  double *PolWgt;
  momentum<double> *K1;
  momentum<double> *J1=new momentum<double>[N];
  momentum<double> *Kb=new momentum<double>[N+1];
  momentum<double> *Jb=new momentum<double>[N+1];
  double Rwgt=0.0;
  do K1=Rambo(Ecm,N,Rwgt); while (not JetCuts(N,K1,Ptmin,etamax,Rmin));
  cout.precision(10);
  double fac=1.0/sqrt(2.0);
  for (int i=0;i<N;i++) J1[i]=fac*(orthogonal(K1[i]));
  double T=Rwgt*TreeLevel(N,K1,J1,1e-10);
  cout<<"Tree: "<<T<<endl;
  PolWgt=PolWeight(N,K1,J1);
  for (int i=0;i<N;i++)
    cout<<PolWgt[i]<<endl;
  double smin[80],x[80];
  int f=0;
  for (int s=0;s<sweeps;s++) 
    { for (int i=20;i<100;i++)
	{ double s=-i/10.0;
	  double Smin=pow(10.0,s);
	  double EventWgt=0.0,PsWgt=0.0;
	  int a=0;
	  for (int l=0;l<Nrad;l++) 
	    { f++;
	      if (Brancher(N,K1,J1,Kb,Jb,a,PsWgt,Smax,Smin))
		{ double Wgt=PsWgt*PolWgt[a]*TreeLevelInsert(N+1,Kb,Jb,Smin)/(double) (N-1);
		  h1->Fill(-i/10.0,Wgt/T);
		}
	    }
	}
      cout<<"Sweep: "<<s+1<<endl;
      screen->cd();
      s1->cd();
      TH1D h=(*h1);
      fac=1.0/(double) f;
      h.SetLineColor(kBlue);
      h.Scale(fac);
      h.Fit("fit","RL");
      h.Draw();
      screen->cd();
      screen->Update();
    }
 
  delete Jb,Kb,J1,PolWgt,K1;

  screen->cd();
  s1->cd();
  TH1D h=(*h1);
  fac=1.0/(double) f;
  h.SetLineColor(kBlue);
  h.Scale(fac);
  h.Fit("fit","RL");
  h.Draw("");
  screen->cd();
  screen->Update();
  screen->SaveAs("SminTest.ps");
  cout << endl << "Enter q to terminate..." << endl;
  char tmp;cin>>tmp;
}  

