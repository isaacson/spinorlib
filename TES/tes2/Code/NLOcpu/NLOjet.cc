#include <cstdio>
#include <iostream>
#include <string>
#include <math.h>
#include "RootLibs.h"
#include "Event.h"
#include "ExternalFunctions.h"

using namespace std;

int main(void)
{ int jets=4; 
  double Ecm=7000.0;
  double Ptmin=250.0,etamax=2.0,Rmin=0.5;
  string PDF="cteq6";
  int op2=2;  // position of incoming particle: m(1,op2,3,...,(op2-1),2,(op2+1),...,n)
  //setup event with physics parameters
  Event Njets(jets,Ecm,Ptmin,etamax,Rmin,PDF,op2);
  //set-up run parameter(s)
  int sweeps=10000,Nrad=1000;
  //set-up root & graphs
  gROOT->Reset();
  gROOT->SetStyle("Plain");
  TGaxis::SetMaxDigits(3);
  TApplication *vcApp = new TApplication("vcApp",0,0);
  vcApp->InitializeGraphics();
  TCanvas *screen = new TCanvas("screen","Smin cancelation",600,600);
  gStyle->SetOptStat(0);
  gStyle->SetTitleOffset(1.0,"y");
  gStyle->SetTitleOffset(1.0,"x");
  gStyle->SetTitleSize(0.10,"x");
  gStyle->SetPadBottomMargin(0.25);
  TPad* s1=new TPad("s1","s1",0,0.33,1.0,1.0);
  s1->SetBottomMargin(0.01);
  s1->SetGridy();
  s1->Draw();
  s1->cd();
  screen->cd(0);
  TPad* s2=new TPad("s2","s2",0.0,0.0,1.0,0.33);
  s2->SetTopMargin(0.05);
  s2->SetGridy();
  s2->SetTicks(1,0);
  s2->SetGridy();
  s2->Draw();
  s2->cd();
  TLegend *legend3=new TLegend(0.65,0.55,0.90,0.65);
  TLegend *legend1=new TLegend(0.65,0.65,0.90,0.75);
  TLegend *legend2=new TLegend(0.65,0.75,0.90,0.85);

  //string n="s_{min} cancelation: "+I2S(jets)+" jets inclusive";
  string n="Smin dependence: m(1,"+I2S(op2);
  for (int i=3;i<=jets+2;i++) 
    if (i==op2) n+=",2";
    else n+=","+I2S(i);
  n+=")";
  char *hdr = (char*) n.c_str();
  TLatex *header1=new TLatex(0.2,0.93,hdr);
  header1->SetNDC();
  header1->SetTextSize(0.06);
  TLatex *header2=new TLatex(0.75,0.80,"K-factor");
  header2->SetNDC();
  header2->SetTextSize(0.09);
  legend1->SetFillColor(0);
  legend1->SetBorderSize(0);
  legend1->SetTextSize(0.045);
  legend2->SetFillColor(0);
  legend2->SetBorderSize(0);
  legend2->SetTextSize(0.045);
  legend3->SetFillColor(0);
  legend3->SetBorderSize(0);
  legend3->SetTextSize(0.045);
  TH1D *hvirtual=new TH1D("","",80,-9.95,-1.95);
  hvirtual->SetLineColor(kBlack);
  hvirtual->GetYaxis()->SetTitle("#sigma/#sigma_{LO}");
  legend1->AddEntry(hvirtual,"Unresolved","l");
  TH1D *hreal=new TH1D("","",80,-9.95,-1.95);
  hreal->SetLineColor(kRed);
  legend2->AddEntry(hreal,"Resolved","l");
  TH1D *hsummed=new TH1D("","",80,-9.95,-1.95);
  hsummed->SetLineColor(kBlue);
  legend3->AddEntry(hsummed,"Sum","l");
  TH1D *hkfactor=new TH1D("","",80,-9.95,-1.95);
  hsummed->SetLineColor(kBlue);
  TF1 *fit=new TF1("fit","pol0",-10,-2);
  fit->SetLineColor(kBlue);
  fit->SetLineWidth(1);
  //fit->FixParameter(0,0);
  //Generate a rambo event which passes Jet Cuts.
  double Rwgt,LO=0.0;
  do {Rwgt=Njets.JetEvent();} while (not Njets.JetCuts());
  //do {Rwgt=Njets.JetEvent();} while (not Njets.JetCuts());
  //do {Rwgt=Njets.JetEvent();} while (not Njets.JetCuts());
  //do {Rwgt=Njets.JetEvent();} while (not Njets.JetCuts());
  //do {Rwgt=Njets.JetEvent();} while (not Njets.JetCuts());
  //do {Rwgt=Njets.JetEvent();} while (not Njets.JetCuts());
  Njets.Scale("Shat",0.5);
  cout<<"alpha_S = "<<Njets.AlphaS<<endl;
  int *h=new int[jets+2];
  int s=1;for (int i=0;i<jets+2;i++) {h[i]=s;}//s=-s;}
  h[0]=-1;h[1]=-1;
  LO=Rwgt*Njets.LO(h);
  double Born,V0,V1,V2;
  Njets.Virtual(Born,V2,V1,V0);
  V0=0.0;
  cout<<"|m^(0)(";
  for (int j=0;j<jets+2;j++) 
    if (h[j]==1) cout<<"+"; else cout<<"-";
  cout<<")|^2="<<LO<<"  ("<<Born<<")"<<endl;
  //cout<<"|m^(1)(";
  //for (int j=0;j<jets+2;j++) 
  //  if (h[j]==1) cout<<"+"; else cout<<"-";
  //cout<<")|^2="<<V2<<"/e^2 + "<<V1<<"/e + "<<V0<<"  (subtracted)"<<endl;
  cout<<"----------------------------------------------------------------------------"<<endl;
  cout<<"Number of jets: "<<jets<<endl;
  cout<<"Center of mass energy: "<<Ecm<<" GeV"<<endl;
  cout<<"Jet Cuts: Pt(j)>"<<Ptmin<<", eta(j)<"<<etamax<<", R(j,j)>"<<Rmin<<endl;
  Njets.PrintEvent();
  //calculate LO weight with ren/fac scale mu^2;
  cout<<endl;
  cout<<"Leading order weight: "<<LO<<endl;
  TH1D hr,hv,hk,hk1;
  for (int s=0;s<sweeps;s++) 
    { cout<<"Sweep "<<s+1<<endl;
      for (int i=20;i<100;i++)
	{ double p=-(double) i/10.0;
	  double smin=pow(10.0,p);
	  double NLO=Njets.Rfactor(smin);
	  hvirtual->Fill(p,NLO);
	  double Real=Njets.Brem(Nrad,smin);
	  hreal->Fill(p,Real);
	  double Kfactor=1.0+(3.0/2.0/M_PI)*Njets.AlphaS*(NLO+Real+V0/2.0/LO);
	  //double Kfactor=(3.0/2.0/M_PI)*(NLO+Real+V0/2.0/LO);
	  hkfactor->Fill(p,Kfactor);
	  hsummed->Fill(p,NLO+Real);
	}
      screen->cd();
      s1->cd();
      double fac=1.0/(double) (s+1);
      hv=(*hvirtual);
      hv.SetLineColor(kBlack);
      hv.Scale(fac);
      double Hmax=1.1*abs(hv.GetBinContent(1));
      hv.SetMaximum( Hmax);
      hv.SetMinimum(-Hmax);
      hv.Draw();
      hr=(*hreal);
      hr.SetLineColor(kRed);
      hr.Scale(fac);
      hr.Draw("same");
      hk=(*hsummed);
      hk.SetLineColor(kBlue);
      hk.Scale(fac);
      //hk.Fit("fit","RL");
      hk.Draw("same");
      legend1->Draw();
      legend2->Draw();
      legend3->Draw();
      header1->Draw();
      screen->cd();
      s2->cd();
      hk1=(*hkfactor);
      hk1.SetLineColor(kBlue);
      hk1.Scale(fac);
      //hk1.Fit("fit","RL");
      hk1.SetTitle("");
      hk1.GetXaxis()->SetTitle("log_{10}(s_{min}/S_{collider})"); 
      hk1.GetYaxis()->SetTitle("K");
      //hk1.GetYaxis()->SetTitleSize(0.06);
     //hk1.SetMaximum((int)  2.0*Kmax*fac);
      //hk1.SetMinimum((int) -2.0*Kmax*fac);
      hk1.GetXaxis()->SetLabelFont(63);
      hk1.GetXaxis()->SetLabelSize(16);
      hk1.GetYaxis()->SetLabelFont(63);
      hk1.GetYaxis()->SetLabelSize(16);
      hk1.GetXaxis()->SetNdivisions(510,true);
      //hk1.GetYaxis()->SetNdivisions(204,false);
      hk1.Draw(); 
      header2->Draw();
      screen->cd();
      screen->Update();
    }
  n="Smin"+I2S(jets)+"_n"+I2S(sweeps)+"_b"+I2S(Nrad)+"_p"+I2S(op2)+".ps";
  hdr = (char*) n.c_str();
  screen->SaveAs(hdr);
  cout << endl << "Enter q to terminate..." << endl;
  char tmp;cin>>tmp;
} 
