#include <cstdio>
#include <iostream>
#include "architecture.h"
#include "momentum.h"

using namespace std;

extern void *g_momentum,*g_source,*g_weight;
extern momentum<double> *Rambo(double,int,double&,double&);
extern bool cuts(int,momentum<double>*,double,double,double);
extern momentum<double> Source(momentum<double>);
extern void init(int,int);
extern void memcopy(momentum<double>*,int,void*);
extern void RealRad(int,int,float,float,int);
extern double Virtual(int,double,double,int,double,double,double,momentum<double>*,momentum<double>*,double&);
extern double uniform();

int main(void)
{
  //
  // run set-up
  //
  int Njets=2;
  int Events=10;
  float Ecm=7000.0/7000.0;
  float Ptmin=30.0/7000.0;
  float etamax=2.0;
  float Rmin=0.4;
  int Nc=3;
  double Smin=1.0/7000.0;
  double scale=1.0;
  double mu=1000.0/1000.0;
  //
  // set-up
  //
  int N=Njets+2;
  //
  //
  // Initialize GPU runs
  //
  int events_per_MP=(1024-64-2*N)/(N*(N+1));
  events_per_MP=40;
  events_per_MP=10;
  int Nevent=NUMBER_MP*events_per_MP; 
  cudaEvent_t start,stop,event;
  float event_time=0.0;
  float time;
  cudaEventCreate(&start);cudaEventCreate(&stop);cudaEventCreate(&event);
  init(N,Nevent);
  int Naccepted=0;
  //
  // start calculating the events
  //
  cout<<"--------------------------------------------------------------------"<<endl;
  for (int i=0;i<Events;i++)
    {
      // Generate momenta
      double x1,x2;
      momentum<double> *k=Rambo(Ecm,N,x1,x2);
      double PSweight=1.0;
      if (cuts(N,k,Ptmin,etamax,Rmin)) {
	cout<<"Momenta:"<<endl;
	for (int i=0;i<N;i++)
	  cout<<k[i].p[0]<<", "<<k[i].p[1]<<", "<<k[i].p[2]<<", "<<k[i].p[3]<<"   ("<<k[i]*k[i]<<")"<<endl;
	momentum<double> *j=new momentum<double>[N];
	for (int n=0;n<N;n++) j[n]=Source(k[n]);
	momentum<double> *K=new momentum<double>[N],*J=new momentum<double>[N];
	for (int n=0;n<N;n++) {K[n]=k[n];J[n]=j[n];}
	cudaEventRecord(start, 0);
	memcopy(K,N,g_momentum);
	memcopy(J,N,g_source);
	//Randomize the position of one of the incoming particles
	int b1=(int) (uniform()*N);
	cout<<"b1 = "<<b1<<endl;
	K[2]=k[b1];K[b1]=k[2];
	J[2]=j[b1];J[b1]=j[2];
	// call GPU event launcher: 
	RealRad(N,events_per_MP,Ecm,Rmin,b1);
	double BornWgt;
	double VirtualWgt=Virtual(N,mu,Smin,Nc,x1,x2,scale,K,J,BornWgt);
	// signal GPU virtual calculation is done
	// copy RealWgt from GPU to CPU memory
	float *weight=new float[Nevent]; 
	cudaMemcpy(weight,g_weight,sizeof(float)*Nevent,cudaMemcpyDeviceToHost);
	cudaEventRecord(stop, 0);cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);
	event_time+=time;
	Naccepted++;
	double RealWgt=0.0;
	for (int i=0;i<Nevent;i++) {RealWgt+=weight[i];cout<<i<<" "<<weight[i]<<endl;}
	VirtualWgt*=PSweight;RealWgt*=PSweight;
	double EventWgt=VirtualWgt+RealWgt;
	cout<<"LO Event weight  = "<<BornWgt<<endl;
	cout<<"NLO Event weight = "<<EventWgt<<endl;
	cout<<"       (Virtual  = "<<VirtualWgt<<")"<<endl;
	cout<<"       (Real     = "<<RealWgt<<")"<<endl;
	cout<<"K-factor         = "<<EventWgt/BornWgt<<endl;
	cout<<"--------------------------------------------------------------------"<<endl;
      }
    }
  int GPU_events=1000;
  cout<<endl<<endl;
  cout<<"Number of jets: "<<Njets<<endl;
  cout<<"Events per MP: "<<events_per_MP<<endl;
  cout<<"Used GPU Time/Virtual event: "<<time/1000.0/Events<<endl;
  double Tevent=time/1000.0/GPU_events/Nevent/Naccepted;
  cout<<"Elapsed time per real event: "<<Tevent<<" seconds"<<endl;
  cout<<"Number of evaluated real events: "<<1.0/((double) Tevent)<<endl;
  // output distributions
  cout << endl << "Enter q to terminate..." << endl;
  char tmp;cin>>tmp;
}
