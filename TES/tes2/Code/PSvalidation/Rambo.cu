#include "momentum.cu"
#include <iostream>
#include <stdlib.h>

using namespace std;

extern double uniform();
extern double uniform(double,double);

int fac(int n) {
  int r=1;
  for (int i=2;i<n+1;i++) {r*=i;}
  return r;
}
  

void ramboo(double Ec,int N,momentum<double> *K)
{
  for (int i=2;i<N;i++) {
    double Eb=-(K[0].p[0]+K[1].p[0])/Ec;
    double pz=-(K[0].p[1]+K[1].p[1])/Ec;
    double f=K[i].p[0];
    K[i].p[0]=(Eb*f+pz*K[i].p[1]);
    f+=K[i].p[0];f*=pz/(1.0+Eb);
    K[i].p[1]+=f;
  }
}

double R(momentum<double> P1,momentum<double> P2) 
{
  return 2.0*abs(P1*P2);
}

double R(momentum<double> P1,momentum<double> P2,momentum<double> P3) 
{
  return min(R(P1,P3),R(P3,P2));
}

momentum<double> *Rambo(double Elab,int Nparticle,double &x1,double &x2)
{
  momentum<double> *P;
  P=new momentum<double>[Nparticle];
  x1=uniform();
  x2=uniform();
  //x2=x1; x1=0.2; x2=0.5;
  P[0]=-Elab*x1*momentum_(0.5, 0.5, 0.0, 0.0);
  P[1]=-Elab*x2*momentum_(0.5,-0.5, 0.0, 0.0);
  double Ecm=Elab*sqrt(x1*x2);
  momentum<double> R=momentum_(0.0,0.0,0.0,0.0);
  for (int i=2;i<Nparticle;i++) {
    double c=uniform(-1.0,1.0);
    double s=sqrt(1.0-c*c);
    double f=2.0*M_PI*uniform(0.0,1.0);
    P[i]=-log(uniform(0.0,1.0)*uniform(0.0,1.0))*momentum_(1.0,c,s*cos(f),s*sin(f));
    R=R+P[i]; 
  }
  double Rmass=sqrt(R*R);
  R=-R/Rmass;
  double a=1.0/(1.0-R.p[0]);
  double x=Ecm/Rmass;
  for (int i=2;i<Nparticle;i++) {
    double bq=R.p[1]*P[i].p[1]+R.p[2]*P[i].p[2]+R.p[3]*P[i].p[3];
    double xq=P[i].p[0]+a*bq;
    P[i]=x*momentum_(-R.p[0]*P[i].p[0]+bq,P[i].p[1]+R.p[1]*xq,P[i].p[2]+R.p[2]*xq,P[i].p[3]+R.p[3]*xq);
  }
  ramboo(Ecm,Nparticle,P);
  return P;
};

double Ht(int n,momentum<double> *k)
{
  double ht=0.0;
  for (int i=2;i<n;i++)
    {
      double px=k[i].p[2],py=k[i].p[3];
      ht+=px*px+py*py;
    }
  return sqrt(ht);
}

momentum<double> Source(const momentum<double> P) {
  momentum<double> J1;
  double x, y, z, rsq;
  do {
    x=uniform(-1.0,1.0);
    y=uniform(-1.0,1.0);
    z=uniform(-1.0,1.0);
    rsq=x*x+y*y+z*z;
  } while(rsq>1.0 || rsq<1e-6);
  J1.p[0]=0.0;
  J1.p[1]=y*P.p[3]-z*P.p[2];
  J1.p[2]=z*P.p[1]-x*P.p[3];
  J1.p[3]=x*P.p[2]-y*P.p[1];
  double norm=sqrt(J1.p[1]*J1.p[1]+J1.p[2]*J1.p[2]+J1.p[3]*J1.p[3]);
  if (norm>0.0) {
    J1.p[1]/=norm;
    J1.p[2]/=norm;
    J1.p[3]/=norm;
  }
  else J1=momentum_(0.0,0.0,0.0,0.0);
  return J1;
}

bool cuts(int Np,momentum<double> *k,double smin) 
{
  bool pass=true;
  for (int i=0;i<Np-1;i++)
    for (int j=i+1;j<Np;j++)
      if (2.0*abs(k[i]*k[j])<smin) pass=false;
  return pass;
}
bool cuts(int Np,momentum<double> *k,double Ptmin,double etamax,double Rmin) {
  bool pass=true;
  for (int i=2;i<Np;i++) {
    double pe=k[i].p[0],px=k[i].p[1],py=k[i].p[2],pz=k[i].p[3];
    if (sqrt(px*px+py*py)<Ptmin) pass=false;
    if (abs(0.5*log((pe+pz)/(pe-pz)))>etamax) pass=false;
  }
  if (pass) {
    for (int i=2;i<Np-1;i++) {
      double pe1=k[i].p[0],px1=k[i].p[1],py1=k[i].p[2],pz1=k[i].p[3];
      for (int j=i+1;i<Np;i++) {
	double pe2=k[j].p[0],px2=k[j].p[1],py2=k[j].p[2],pz2=k[j].p[3];
	double Deta=0.5*log(abs((pe1+pz1)*(pe2-pz2)/(pe1-pz1)/(pe2+pz2)));
	double d=(px1*px2+py1*py2)/(sqrt(px1*px1+py1*py1)*sqrt(px2*px2+py2*py2));
	d=max(d,-1.0);d=min(d,1.0);
	double Dphi=acos(d);
	double R=sqrt(Deta*Deta+Dphi*Dphi);
	if (R<Rmin) pass=false;
      }}
  }
  return pass;
}
momentum<double> *Cluster(int N,momentum<double> *Kb)
{
  momentum<double> *Kc=new momentum<double>[N+1];
  double R12min=1e10;
  double R123min=1e10;
  int a,r,b;
  for (int i=0;i<N;i++)
    for (int j=i+1;j<N+1;j++)
      { 
	double Rij=R(Kb[i],Kb[j]);
	if (Rij<R12min) {a=i;r=j;R12min=Rij;}
      }
  for (int i=2;i<N+1;i++)
    { if ((i!=a) and (i!=r))
	{ double Rijk=R(Kb[a],Kb[r],Kb[i]);
	  if (Rijk<R123min) {b=i;R123min=Rijk;}
	}
    }
  //cout<<a<<" "<<r<<" "<<b<<endl;
  for (int i=0;i<N+1;i++) {Kc[i]=Kb[i];}
  if ((a>1) and (r>1)) 
    {
      Kc[a]=Kb[a]+Kb[r];
      Kc[b]=Kb[b];
      double gamma=(Kc[a]*Kc[a])/(2.0*Kc[a]*Kc[b]);
      Kc[a]=Kc[a]-gamma*Kc[b];
      Kc[b]=(1.0+gamma)*Kc[b];
      Kc[r].p[0]=0.0;
    }
  else
    {
      if (a<2)
	{
	  Kc[b]=Kb[b]+Kb[r];
	  Kc[a]=Kb[a];
	  double gamma=(Kb[a]*Kb[b]+Kb[b]*Kb[r]+Kb[r]*Kb[a])/(Kb[a]*Kb[b]+Kb[r]*Kb[a]);
	  Kc[b]=Kc[b]+(1.0-gamma)*Kc[a];
	  Kc[a]=gamma*Kc[a];
	  Kc[r].p[0]=0.0;
	}
      if (r<2)
	{
	  Kc[b]=Kb[b]+Kb[a];
	  Kc[r]=Kb[r];
	  double gamma=(Kb[r]*Kb[b]+Kb[b]*Kb[a]+Kb[a]*Kb[r])/(Kb[r]*Kb[b]+Kb[a]*Kb[r]);
	  Kc[b]=Kc[b]+(1.0-gamma)*Kc[r];
	  Kc[r]=gamma*Kc[r];
	  Kc[a].p[0]=0.0;
	}
    }
  int p=0;
  for (int i=0;i<N+1;i++)
    if (Kc[i].p[0]!=0.0) {Kc[p]=Kc[i];p++;}
  return Kc;
}
