#include <cstdio>
#include <iostream>
#include <math.h>
#include "momentum.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"

using namespace std;

extern momentum<double> *Rambo(double,int);
extern double Ht(int,momentum<double>*);
extern bool cuts(int,momentum<double>*,double,double,double);
extern momentum<double> *Brancher(int,momentum<double>*,bool&,double&,double);
extern momentum<double> *BrancherTest1(int,momentum<double>*,bool&,double&,double);
//extern momentum<double> *Cluster(int,momentum<double>*);
extern momentum<double> *Cluster(int,momentum<double>*);
extern bool Compare(int,momentum<double>*,momentum<double>*);
extern void DumpEvent(int,momentum<double>*,momentum<double>*,momentum<double>*);

int main(void)
{
  int Jets=4;
  int Iterations=100;
  int Events=1000000;
  float Ecm=14000.0/14000.0;
  float Ptmin=60.0/14000.0;
  float etamax=2.0;
  float Rmin=0.4;
  //output control
  int test=2;
  bool plots=true;
  double Smax=Ecm*Ecm;
  //
  // set-up
  //
  int N=Jets+2;
  if (test==1) {
    // Tests branchers: Rambo(N) vs Rambo(N-1)*Brancher
    // root set-up mumbo jumbo
    gROOT->Reset();
    gROOT->SetStyle("Plain");
    //gROOT->ForceStyle();
    TApplication myapp("myapp",0,0);
    // set-up root screens
    TCanvas *screen = new TCanvas("screen","Phase space comparisons",0,0,1600,800);
    gStyle->SetOptStat(0);
    gStyle->SetTitleOffset(1.25,"y"); 
    screen->Divide(2,1);
    screen->cd(1);
    gPad->SetLogy();
    TH1F *h1=new TH1F("Run Stats","6 jet comparison",100,0.0,0.75);
    h1->SetLineColor(kBlue);
    TString xaxis="H_{T}";
    h1->GetXaxis()->SetTitle(xaxis);
    h1->GetYaxis()->SetTitle("# of events");
    TH1F *h2=new TH1F("Run Stats","6 jet comparison",100,0.0,0.75);
    h2->SetLineColor(kRed);
    TH1F hr;
    cout<<endl<<"Number of jets = "<<Jets<<endl;
    int Njets=0,Nbranch=0;
    //double *o1,*o2;
    for (int i=0;i<Iterations;i++)
      {
	for (int j=0;j<Events;j++)
	  {
	    double o;
	    // Generate N momenta passing jet cuts using rambo
	    bool pass=false,cutpass=true;
	    while (not pass) 
	      {
		Njets++;
		momentum<double> *K=Rambo(Ecm,N);
		o=Ht(N,K);
		//cutpass=cuts(N,K,Ptmin,etamax,Rmin);
		pass=true;
		delete K;
	      }
	    if (cutpass) h1->Fill(o/sqrt(Smax));
	    // Generate (N-1) momenta using rambo + brem-event to get N momenta
	    pass=false;
	    double wgt;
	    while (not pass)
	      {
		Nbranch++;
		momentum<double> *K1=Rambo(Ecm,N-1);
		momentum<double> *Kb=BrancherTest1(N-1,K1,pass,wgt,Smax);
		if (pass) o=Ht(N,Kb);
		//if (pass) cutpass=cuts(N,Kb,Ptmin,etamax,Rmin);
		delete K1;
		delete Kb;
	      }
	    if (cutpass) h2->Fill(o/sqrt(Smax),wgt);
	  }
	if (plots)
	  {
	    screen->cd(1);
	    h1->Draw("");
	    h2->Draw("same");
	    screen->cd(2);
	    hr=((*h1)-(*h2))/(*h1);
	    hr.SetLineColor(kBlue);
	    hr.GetYaxis()->SetTitle("Relative ratio");
	    hr.Draw("");
	    hr.SetMaximum(1.0);
	    hr.SetMinimum(-1.0);
	    screen->Modified();
	    screen->Update();
	  }
      }
    screen->SaveAs("PScomparison.ps");
    delete h1,h2,screen;
    double Nevents=(double) (Iterations*Events);
    cout<<"Generated events:    "<<Nevents<<endl;
    cout<<"Rambo efficiency:    "<<Nevents/Njets<<endl;
    cout<<"Brancher efficiency: "<<Nevents/Nbranch<<endl;
    cout << endl << "Enter q to terminate..." << endl;
    char tmp;cin>>tmp;
  }  
  if (test==2) 
    {
      //Test the invertability of the brancher
      for (int i=0;i<Iterations;i++)
	{
	  cout<<"Rambo Event "<<i+1<<" out of "<<Iterations<<endl;
	  bool pass=false;
	  momentum<double> *K;
	  while (not pass) 
	    {
	      K=Rambo(Ecm,N-1);
	      pass=cuts(N-1,K,Ptmin,etamax,Rmin);
	      if (not pass) delete K;
	    }
	  for (int j=0;j<Events;j++)
	    {
	      pass=false;
	      momentum<double> *Kb,*K1;
	      double wgt;
	      while (not pass) 
		{
		  Kb=Brancher(N-1,K,pass,wgt,Smax);
		  if (not pass) delete Kb;
		}
	      K1=Cluster(N-1,Kb);
	      if (not Compare(N-1,K,K1)) 
		{
		  cout<<"Event FAILED clustering:"<<endl;
		  cout<<"Jet Event "<<i<<";Branched Event "<<j<<endl;
		  DumpEvent(N,K,K1,Kb);
		}
	      delete K1; delete Kb;
	    }
	  delete K;
	}
    }
}

