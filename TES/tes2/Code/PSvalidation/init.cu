#include <iostream>
#include "momentum.h"
#include "/home/giele/scratch/lhapdf/include/LHAPDF/LHAPDF.h"

using namespace std;
using namespace LHAPDF;

//
// initializing device memory
//

void *g_momentum,*g_source,*g_entropy,*g_weight;

extern void RealRad_init(int,int);

void init(int N,int Nevent) 
{
  //
  // set-up alpha_S and PDF's
  //
  //printf("Initialization PDF's...\n");
  //setVerbosity(SILENT);
  //const int SUBSET=0;
  //initPDFSet("NNPDF20_100", LHGRID,SUBSET);
  //printf("done\n");
  //
  // mapping device memory
  //
  cudaMalloc(&g_momentum,N*sizeof(momentum<float>));
  cudaMalloc(&g_source,N*sizeof(momentum<float>));
  cudaMalloc(&g_entropy,Nevent*sizeof(uint2));
  cudaMalloc(&g_weight,Nevent*sizeof(float));
  //printf("Cuda Malloc: %s\n", cudaGetErrorString(cudaThreadSynchronize()));
  //
  // Kernel initializations
  //
  RealRad_init(N,Nevent);
}  

void memcopy(momentum<double> *A,int N,void *g_pointer) {
        momentum<float> *a=new momentum<float>[N];
	for (int n=0;n<N;n++) 
	  for (int i=0;i<4;i++)
	  a[n].p[i]=(float) A[n].p[i];
	cudaMemcpy(g_pointer,a,N*sizeof(momentum<float>),cudaMemcpyHostToDevice);
	//printf("Cuda MemCopy: %s\n", cudaGetErrorString(cudaThreadSynchronize()));
	delete []a;
}

