#include <iostream>

#include "architecture.h"
#include "momentum.cu"
#include "random.h"

using namespace std;
extern void *g_momentum,*g_source,*g_entropy,*g_weight;
__constant__ momentum<float> *Kmom,*Jmom;
__constant__ float *weight;
__constant__ uint2 *event_entropy;


void RealRad_init(int N,int Nevent)
{
  //
  // set-up device memory pointers
  //
  cudaMemcpyToSymbol("Kmom", &g_momentum, sizeof(void *), 0, cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol("Jmom", &g_source, sizeof(void *), 0, cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol("event_entropy", &g_entropy, sizeof(void *), 0, cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol("weight", &g_weight, sizeof(void *), 0, cudaMemcpyHostToDevice);

  uint2 buffer[Nevent];
  for (int i=0;i<Nevent;i++) 
    {
      buffer[i].x=rand(); 
      buffer[i].y=rand(); 
    }
  cudaMemcpy(g_entropy,buffer,sizeof(uint2)*Nevent,cudaMemcpyHostToDevice);
}

__device__ float R(momentum<float> P1,momentum<float> P2) 
{
  return 2.0*abs(P1*P2);
}

__device__ float R(momentum<float> P1,momentum<float> P2,momentum<float> P3) 
{
  return min(R(P1,P3),R(P3,P2));
}


__device__ float Born(const int N,momentum<float>* K,momentum<float>& Kr,momentum<float>* J,const unsigned int k,const unsigned int mu)
{
  return 1.0;
}

__device__ void orthogonal(uint2 &w, momentum<float> &J, const momentum<float> P) 
{
  momentum<float> J1;
  float x, y, z, rsq;
  do {
    x=uniform(w, -1.0f, 1.0f);
    y=uniform(w, -1.0f, 1.0f);
    z=uniform(w, -1.0f, 1.0f);
    rsq=x*x+y*y+z*z;
  } while(rsq>1.0 || rsq<1e-6f);
  __syncthreads();
  J1.p[0]=0.0f;
  J1.p[1]=y*P.p[3]-z*P.p[2];
  J1.p[2]=z*P.p[1]-x*P.p[3];
  J1.p[3]=x*P.p[2]-y*P.p[1];
  float norm=sqrt(J1.p[1]*J1.p[1]+J1.p[2]*J1.p[2]+J1.p[3]*J1.p[3]);
  if (norm>0.0f) {
    J1.p[1]/=norm;
    J1.p[2]/=norm;
    J1.p[3]/=norm;
  }
  else J1=momentum_<float>(0.0,0.0,0.0,0.0);
  __syncthreads();
  J=J1;
}
__device__ void orthogonal(momentum<float> &J1, const momentum<float> P, const momentum<float> K)
{
  float x=P.p[2]*J1.p[3]-P.p[3]*J1.p[1];
  float y=P.p[3]*J1.p[1]-P.p[1]*J1.p[3];
  float z=P.p[1]*J1.p[2]-P.p[2]*J1.p[1];
  J1.p[0]=0.0f;
  J1.p[1]=y*K.p[3]-z*K.p[2];
  J1.p[2]=z*K.p[1]-x*K.p[3];
  J1.p[3]=x*K.p[2]-y*K.p[1];
  float norm=sqrt(J1.p[1]*J1.p[1]+J1.p[2]*J1.p[2]+J1.p[3]*J1.p[3]);
  if (norm>0.0f) {
    J1.p[1]/=norm;
    J1.p[2]/=norm;
    J1.p[3]/=norm;
  }
  else J1=momentum_<float>(0.0,0.0,0.0,0.0);
  __syncthreads();
}

__device__ void boost(unsigned int lflag,momentum<float> q,momentum<float> &ph,momentum<float> &p)
{
  /*
                                        _                       
   Boost of a 4-vector ( relative speed q/q(0) ):               
                                                                
   ph is the 4-vector in the rest frame of q                    
   p is the corresponding 4-vector in the lab frame             
                                                                  
              INPUT                               OUTPUT         
                                                            
  lflag= 0:   q, ph                               p             
                                                                
  lflag= 1:   q, p                                ph            

  */

  float rsq = sqrt(abs(q*q));
  if (lflag==0) {
    float dp = (q.p[0]*ph.p[0]+q.p[1]*ph.p[1]+q.p[2]*ph.p[2]+q.p[3]*ph.p[3])/rsq;
    float c1 = (ph.p[0]+dp)/(rsq+q.p[0]);
    p=ph+c1*q;
    p.p[0]=dp;
  }
  else {
    float dp = (q*p)/rsq;
    float c1 = (p.p[0]+dp)/(rsq+q.p[0]);
    ph =p-c1*q;
    ph.p[0]=dp;
  }
}

__device__ float FFbrancher(momentum<float> K1,momentum<float> K2,momentum<float> &Pa,momentum<float> &Pr,momentum<float> &Pb,uint2 &w)
{
  float smin=1e-6;
  float s12=abs(2.0*(K1*K2));
  float wgt=pow(log(s12/smin),2.0f);
  float sar,srb;
  do 
    {
      float r1=abs(uniform(w,0.0f,1.0f));
      float r2=abs(uniform(w,0.0f,1.0f));
      //sar=r1*s12;
      //srb=r2*s12;
      sar=pow(s12,r1)*pow(smin,(1.0f-r1));
      srb=pow(s12,r2)*pow(smin,(1.0f-r2));
    } while ((sar+srb>s12) or (sar>srb));
    __syncthreads();
    wgt*=sar*srb;
    float gamma=sar/s12;
    Pb=(1.0f-gamma)*K2;
    // Goto rest frame of Q
    momentum<float> Q=K1+gamma*K2;
    momentum<float> P;
    boost(1,Q,P,Pb);
    // Calculate angles wrt positive z-axis, so we can rotate system such that Pb is along positive z-axis
    float dp=sqrt(abs(P.p[1]*P.p[1]+P.p[2]*P.p[2]));
    float sphi=P.p[2]/dp;
    float cphi=P.p[1]/dp;
    P.p[1]=dp;
    dp=sqrt(abs(P.p[1]*P.p[1]+P.p[3]*P.p[3]));
    float sthe=P.p[1]/dp;
    float cthe=P.p[3]/dp;
    // Calculate decay of Q in its rest system and Pb is along the pozitive z-axis.
    // The decay is subject to the constraint 2*(Pr*Pb)=srb wich fixes the z-components. 
    float Ea=sqrt(abs(Q*Q));
    float c=1.0-2.0*srb/(s12-sar);
    float s=sqrt(abs(1.0-c*c));
    float f=M_PI*abs(uniform(w,0.0,1.0));
    Pr=0.5f*Ea*momentum_(1.0f,s*cos(f),s*sin(f),c);
    Pa=0.5f*Ea*momentum_(1.0f,-s*cos(f),-s*sin(f),-c);
    // Rotate the system such that Pb is along its original direction and boost back to lab.
    float px=Pa.p[3]*cthe-Pa.p[1]*sthe;
    float py=Pa.p[3]*sthe+Pa.p[1]*cthe;
    Pa.p[3]=px;
    Pa.p[1]=py;
    px=Pa.p[1]*cphi-Pa.p[2]*sphi;
    py=Pa.p[1]*sphi+Pa.p[2]*cphi;
    Pa.p[1]=px;
    Pa.p[2]=py;
    boost(0,Q,Pa,Pa);
    px=Pr.p[3]*cthe-Pr.p[1]*sthe;
    py=Pr.p[3]*sthe+Pr.p[1]*cthe;
    Pr.p[3]=px;
    Pr.p[1]=py;
    px=Pr.p[1]*cphi-Pr.p[2]*sphi;
    py=Pr.p[1]*sphi+Pr.p[2]*cphi;
    Pr.p[1]=px;
    Pr.p[2]=py;
    boost(0,Q,Pr,Pr);
    return wgt;
}

__device__ float IFbrancher(momentum<float> P1,momentum<float> Ka,momentum<float> &p1,momentum<float> &kr,momentum<float> &ka,float Ecm,uint2 &w)
{
  float smin=1e-6;
  float x=2.0f*P1.p[0]/Ecm;
  float wgt=1.0f;
  float d;
  do
    {
      float Kmax=pow(abs(Ka.p[0]+0.5f*(1.0f-x)*Ecm),2.0f);
      float r1=abs(uniform(w,0.0f,1.0f));
      float ymin=abs(0.5f*smin/Ecm/sqrt(Kmax));
      float y=pow(ymin,r1);
      wgt*=-y*log(ymin);
      float ct=2.0f*y-1.0f;
      float r2=abs(uniform(w,0.0f,1.0f));
      float Kmin=pow(0.5f*smin/Ecm/y,2.0f);
      float kr2=pow(Kmax,r2)*pow(Kmin,(1.0f-r2));
      wgt*=kr2*log(abs(Kmax/Kmin));
      float r3=abs(uniform(w,0.0f,1.0f));
      float cp=cos(2.0f*M_PI*r3);
      float st=sqrt(abs(1.0f-ct*ct));
      float sp=sqrt(abs(1.0f-cp*cp));
      if (P1.p[1]>0.0f)
	kr=sqrt(abs(kr2))*momentum_(1.0f,ct,cp*st,sp*st);
      else
	kr=sqrt(abs(kr2))*momentum_(1.0f,-ct,cp*st,sp*st);
      d=abs(P1*Ka-P1*kr);
    } while (d<1e-6);
  float z=1.0f+(kr*Ka)/d;
  if (z*x>1.0f) wgt=0.0f;
  wgt*=0.5f*(P1*Ka)/d/M_PI;
  ka=Ka-kr+(z-1.0f)*P1;
  if (ka.p[0]<0.0f) wgt=0.0f;
  p1=z*P1;
  if ((2.0*(p1*kr)<smin) or (2.0*(kr*ka)<smin)) wgt=0.0f;
  return wgt;
}

template<int B> __global__  void CUDA_RealRad(unsigned int N,float Ecm,float Rmin,int number_events_per_MP,int p2,int nev) 
{
  //
  //set up thread, event and antenna identifiers.
  //
  unsigned int t=threadIdx.x;
  unsigned int event_thread=t%(4*N);
  unsigned int mu=event_thread%4;
  unsigned int k=event_thread/4;// the thread usage J[IDX(k,.)].p[mu]=...
  unsigned int eventIdx=t/(4*N);
  unsigned int event=blockIdx.x*number_events_per_MP/2+eventIdx;
  unsigned int sizePerEvent=N*(N+1);
  //set up memory pointers
  extern __shared__ momentum<float> sharedMem[];
  momentum<float> *Kborn=sharedMem;
  momentum<float> *Jborn=sharedMem+N;
  momentum<float> *K=sharedMem+2*N+sizePerEvent*eventIdx;
  momentum<float> *J=sharedMem+3*N+sizePerEvent*eventIdx;
  // load momenta, sources in shared memeory.
  if (t<4*N) 
    {
      Kborn[k].p[mu]=Kmom[k].p[mu];
      Jborn[k].p[mu]=Jmom[k].p[mu];
    }
  __syncthreads();
  // Load random number seed for each event
  uint2 w;
  float Realweight;
  if (k==0 and mu==0) 
    {      
      w=event_entropy[event];
      Realweight=0.0;
    }
  __syncthreads();
  for (unsigned int n=0;n<nev;n++) 
    {
      // For each event copy the Born configuration to the event memory
      K[k].p[mu]=Kborn[k].p[mu];
      J[k].p[mu]=Jborn[k].p[mu];
      __syncthreads();
      float PSweight;
      // perform a branching (random gluons) using 1 thread/event
      if (k==0 and mu==0) 
	{
	  unsigned int b1,b2;
	  momentum<float> Kr;
	  if (B==0) 
	    {
	      do 
		{
		  b1=(unsigned int) uniform(w,2.0f,(float) N);
		  b2=(unsigned int) uniform(w,2.0f,(float) N);
		} while (b1==b2);
	      PSweight=FFbrancher(Kborn[b1],Kborn[b2],K[b1],Kr,K[b2],w);
	      PSweight*=0.5*(N-2.0)*(N-3.0)*(Kborn[b1]*Kborn[b2])/(Kborn[0]*Kborn[1]);
	    }
	  if (B==1) 
	    {
	      b1=(unsigned int) uniform(w,0.0f,2.0f);
	      b2=(unsigned int) uniform(w,2.0f,(float) N);
	      if (b1==0)
		{
		  PSweight=IFbrancher(-Kborn[b1],Kborn[b2],K[b1],Kr,K[b2],Ecm,w);
		  K[b1]=-K[b1];
		  PSweight*=(N-2.0)*(N-3.0)*pow(abs((Kborn[0]*Kborn[1])/(K[b1]*Kborn[1])),(float) (N-4.0))/(K[b1]*Kborn[1]);
		}
	      else
		{
		  PSweight=IFbrancher(-Kborn[b1],Kborn[b2],K[b1],Kr,K[b2],Ecm,w);
		  K[b1]=-K[b1];
		  PSweight*=(N-2.0)*(N-3.0)*pow(abs((Kborn[0]*Kborn[1])/(K[b1]*Kborn[0])),(float) (N-4.0))/(K[b1]*Kborn[0]);
		}
	    }
	  float R12min=1e8;
	  float R123min=1e8;
	  bool pass=true;
	  for (unsigned int i=0;i<N-1;i++)
	    for (unsigned int j=i+1;j<N;j++)
	      R12min=min(R(K[i],K[j]),R12min);
	  if (R12min<R(K[b1],Kr)) pass=false;
	  for (unsigned int i=2;i<N;i++)
	    if (not ((i==b1) or (i==b2) ))
	      R123min=min(R(K[b1],Kr,K[i]),R123min);
	  if (R123min<R(K[b1],Kr,K[b2])) pass=false;
	  if (not pass) PSweight=0.0f;
	  // Start ME evaluation preparations:
	  // recalculate J[b1] and J[b2]
	  orthogonal(J[b1],Kborn[b1],K[b1]);
	  orthogonal(J[b2],Kborn[b2],K[b2]);
	  // Calculate new polarization state
	  orthogonal(w,J[N],Kr);
	  //Debugging code:
	  J[N+2]=Kr;
	  //end debugging code
	}
      __syncthreads();
      // Permute incoming momentum 2
      if (k==0)
	{
	  J[N+1].p[mu]=K[1].p[mu];K[1].p[mu]=K[p2].p[mu];K[p2].p[mu]=J[N+1].p[mu];
	  J[N+1].p[mu]=J[1].p[mu];J[1].p[mu]=J[p2].p[mu];J[p2].p[mu]=J[N+1].p[mu];
	}
      __syncthreads();
      /*
      // Calculate the currents
      Current(N,K,J,k,mu);
      // Calculate the sum of ordered amplitudes |(123...nr)|^2+|(234..n1r)|^2+|(345...n12r)|^2+...+|(n12...(n-1)r)|^2
      if (k==0)
      {
      // calculate Jr
	orthogonal(w,Jr,Kr);
        float MEweight=0.0;
        for (int j=0;j<N;j++) 
	  MEweight+=pow(Jr*J[IDX(N,j)],2);
	Realweight+=PSweight*MEweight;
	}
	__syncthreads();
      */
      if (k==0 and mu==0)
	{
	  float MEweight=1.0;
	  //PSweight=1.0;
	  Realweight+=PSweight*MEweight;
	}
      __syncthreads();
  }
  if (k==0 and mu==0)
    {
      // Store random number seed for next calculation
      event_entropy[event]=w;
      // Copy result to global GPU memory
      if (B==0) weight[event]+=Realweight;//*((float) N-2.0f);
      if (B==1) weight[event]+=Realweight;//*2.0f;
    }
  __syncthreads();
  return;
}

void RealRad(int N,int events_per_MP,float Ecm,float Rmin,int p2,int nev)
{
  int threads_per_MP=events_per_MP*4*N;
  //cout<<"threads used "<<threads_per_MP<<endl;
  int sharedMem_size=SHARED_MEMORY-64;
  unsigned int Np=N;
  //CUDA_RealRad<<<NUMBER_MP,threads_per_MP,sharedMem_size>>>(Np,Ecm,Rmin,events_per_MP);
  dim3 grid(30,1);
  threads_per_MP=events_per_MP*2*N;
  //cout<<threads_per_MP<<" "<<sharedMem_size/2<<endl;
  //cout<<Np<<" "<<Ecm<<" "<<Rmin<<" "<<events_per_MP<<" "<<p2<<endl;
  int nevents=(int) (((float) N-2.0f)*(float) nev/((float) N));
  CUDA_RealRad<0><<<60,threads_per_MP,sharedMem_size/2>>>(Np,Ecm,Rmin,events_per_MP,p2,nevents);
  nevents=(int) (2.0f*(float) nev/((float) N));
  CUDA_RealRad<1><<<60,threads_per_MP,sharedMem_size/2>>>(Np,Ecm,Rmin,events_per_MP,p2,nevents);
  //printf("Cuda Launch: %s\n", cudaGetErrorString(cudaThreadSynchronize()));
}
