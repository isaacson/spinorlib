#include <cstdio>
#include <iostream>
#include "architecture.h"
#include "momentum.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"

using namespace std;

extern void *g_momentum,*g_source,*g_weight;
extern momentum<double> *Rambo(double,int,double&,double&);
extern bool cuts(int,momentum<double>*,double,double,double);
extern double Ht(int,momentum<double>*);
extern bool cuts(int,momentum<double>*,double);
extern momentum<double> Source(momentum<double>);
extern void init(int,int);
extern void memcopy(momentum<double>*,int,void*);
extern void RealRad(int,int,float,float,int,int);
extern double Virtual(int,double,double,int,double,double,double,momentum<double>*,momentum<double>*,double&);
extern double uniform();
extern momentum<double> *Cluster(int,momentum<double>*);
extern int fac(int);

int main(void)
{
  //
  // run set-up
  //
  int Njets=3;
  int Iterations=10;
  int Events=1000;
  int nev=10;
  float Ecm=7000.0/7000.0;
  float Ptmin=30.0/7000.0;
  float etamax=2.0;
  float Rmin=0.4;
  int Nc=3;
  double Smin=1.0/7000.0;
  double scale=1.0;
  double mu=1000.0/1000.0;
  //
  // set-up
  //
  int N=Njets+2;
  //
  // Initialize root
  //
  gROOT->Reset();
  gROOT->SetStyle("Plain");
  TApplication myapp("myapp",0,0);
  // set-up root screens
  TCanvas *screen = new TCanvas("screen","Phase space comparisons",0,0,1600,800);
  gStyle->SetOptStat(0);
  gStyle->SetTitleOffset(1.25,"y"); 
  screen->Divide(2,1);
  screen->cd(1);
  gPad->SetLogy();
  TH1F *h1=new TH1F("Run Stats","PS comparison",100,0.0,0.75);
  h1->SetLineColor(kBlue);
  TString xaxis="H_{T}";
  h1->GetXaxis()->SetTitle(xaxis);
  h1->GetYaxis()->SetTitle("# of events");
  TH1F *h2=new TH1F("Run Stats","PS comparison",100,0.0,0.75);
  h2->SetLineColor(kRed);
  TH1F *h3=new TH1F("Run Stats","PS comparison",100,0.0,0.75);
  h2->SetLineColor(kRed);
  TH1F hr;
  //
  // Initialize GPU runs
  //
  int events_per_MP=(1024-64-2*N)/(N*(N+1));
  events_per_MP=30;
  //events_per_MP=10;
  int Nevent=NUMBER_MP*events_per_MP; 
  init(N,Nevent);
  //
  // start calculating the events
  //
  cout<<"--------------------------------------------------------------------"<<endl;
  float *weight=new float[Nevent];
  momentum<double> *j=new momentum<double>[N];
  momentum<double> *K=new momentum<double>[N];
  momentum<double> *J=new momentum<double>[N];
  for (int i1=0;i1<Iterations;i1++)
    {
      for (int i2=0;i2<Events;i2++)
	{
	  // Generate momenta
	  double x1,x2;
	  momentum<double> *k=Rambo(Ecm,N,x1,x2);
	  double PSweight=pow(0.5*M_PI,Njets-1)*pow(2.0*x1*x2*Ecm*Ecm,Njets-2)/fac(Njets-1)/fac(Njets-2);
	  //if (jetcuts(N+1,k,Ptmin,etamax,Rmin))
	  cout<<i1<<" "<<i2<<endl;
	  if (cuts(N,k,Smin)) {
	      for (int m=0;m<Nevent;m++) weight[m]=0.0f;
	      cudaMemcpy(g_weight,weight,sizeof(float)*Nevent,cudaMemcpyHostToDevice);
	      for (int n=0;n<N;n++) j[n]=Source(k[n]);
	      for (int n=0;n<N;n++) {K[n]=k[n];J[n]=j[n];}
	      memcopy(K,N,g_momentum);
	      memcopy(J,N,g_source);
	      //Randomize the position of one of the incoming particles
	      int b1=(int) (uniform()*N);
	      //cout<<"b1 = "<<b1<<endl;
	      K[2]=k[b1];K[b1]=k[2];
	      J[2]=j[b1];J[b1]=j[2];
	      // call GPU event launcher: 
	      RealRad(N,events_per_MP,Ecm,Rmin,b1,nev);
	      double BornWgt;
	      //double VirtualWgt=Virtual(N,mu,Smin,Nc,x1,x2,scale,K,J,BornWgt);
	      // signal GPU virtual calculation is done
	      // copy RealWgt from GPU to CPU memory
	      cudaMemcpy(weight,g_weight,sizeof(float)*Nevent,cudaMemcpyDeviceToHost);
	      double RealWgt=0.0;
	      for (int i=0;i<Nevent;i++) {RealWgt+=weight[i]/nev/Nevent;}
	      //VirtualWgt*=PSweight;
	      RealWgt*=PSweight;
	      double o=Ht(N,k);
	      //cout<<o<<" "<<RealWgt<<endl;
	      h2->Fill(o/Ecm,RealWgt*700.0);
	      h3->Fill(o/Ecm,PSweight);
	    }
	  delete k;
	  momentum<double> *k1=Rambo(Ecm,N+1,x1,x2);
	  PSweight=pow(0.5*M_PI,Njets)*pow(2.0*x1*x2*Ecm*Ecm,Njets-1)/fac(Njets)/fac(Njets-1);
	  //PSweight=1.0;
	  momentum<double> *k2=Cluster(N,k1);
	  //for (int i=0;i<N;i++)
	  //cout<<k2[i].p[0]<<", "<<k2[i].p[1]<<", "<<k2[i].p[2]<<", "<<k2[i].p[3]<<"   ("<<k2[i]*k2[i]<<")"<<endl;

	  if (cuts(N,k2,Smin)) {double o=Ht(N,k2);h1->Fill(o/Ecm,PSweight);}
	  //if (jetcuts(N,kjets,Ptmin,etamax,Rmin))
	  //  {
	  //
	  //  }
	  delete k1;
	  delete k2;
	}
      screen->cd(1);
      h1->Draw("");
      h2->Draw("same");
      h3->Draw("same");
      screen->cd(2);
      hr=((*h1)-(*h2))/(*h1);
      hr.SetLineColor(kBlue);
      hr.GetYaxis()->SetTitle("Relative ratio");
      hr.Draw("");
      hr.SetMaximum(1.0);
      hr.SetMinimum(-1.0);
      screen->Modified();
      screen->Update();
    }
  screen->cd(1);
  h1->Draw("");
  h2->Draw("same");
  h3->Draw("same");
  screen->cd(2);
  hr=((*h1)-(*h2))/(*h1);
  hr.SetLineColor(kBlue);
  hr.GetYaxis()->SetTitle("Relative ratio");
  hr.Draw("");
  hr.SetMaximum(1.0);
  hr.SetMinimum(-1.0);
  screen->Modified();
  screen->Update();
  //screen->SaveAs("PScomparison.ps");
  delete h1,h2,screen;
  // output distributions
  cout << endl << "Enter q to terminate..." << endl;
  char tmp;cin>>tmp;
}
