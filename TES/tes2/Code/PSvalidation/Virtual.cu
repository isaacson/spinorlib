#include <math.h>
#include <stdlib.h>
#include "momentum.cu"
#include "/home/giele/scratch/lhapdf/include/LHAPDF/LHAPDF.h"

#include <iostream>
#include <typeinfo>
#include <cassert>
#include <cmath>
#include <iomanip>

using namespace std;
using namespace LHAPDF;

double Li2(const double x,const int kmax=50,const double xerr=1e-8)
  {
    double pi2=M_PI;
    if (x==0) return 0.0;
    if (x<0.0) return 0.5*Li2(x*x)-Li2(-x);
    //    if (x<0.0) {double l=log(1.0-x);return -Li2(x/(x-1.0))-0.5*l*l;}
    if (x<=0.5)
      {
	double sum  = x;
	double term = x;
	for(int k=2; k<kmax; k++) 
	  {
	    double rk = (k-1.0)/k;
	    term *= x;
	    term *= rk*rk;
	    sum += term;
	    if (abs(term/sum) < xerr) return sum;
	  }
	cout.precision(16);
	cout<<"Maximum number of iterations exceeded in Li2("<<x<<")="<<sum<<endl;
	return sum;
      }
    if (x<1.0) return pi2/6.0-Li2(1.0-x)-log(x)*log(1.0-x);
    if (x==1.0) return pi2/6.0;
    if (x<=1.01)
      {
	const double eps = x - 1.0;
	const double lne = log(eps);
	const double c0 = pi2/6.0;
	const double c1 =   1.0 - lne;
	const double c2 = -(1.0 - 2.0*lne)/4.0;
	const double c3 =  (1.0 - 3.0*lne)/9.0;
	const double c4 = -(1.0 - 4.0*lne)/16.0;
	const double c5 =  (1.0 - 5.0*lne)/25.0;
	const double c6 = -(1.0 - 6.0*lne)/36.0;
	const double c7 =  (1.0 - 7.0*lne)/49.0;
	const double c8 = -(1.0 - 8.0*lne)/64.0;
	return c0+eps*(c1+eps*(c2+eps*(c3+eps*(c4+eps*(c5+eps*(c6+eps*(c7+eps*c8)))))));
      }
    double log_x=log(x);
    if (x<=2.0) return pi2/6.0+Li2(1.0 - 1.0/x)-log_x*(log(1.0-1.0/x)+0.5*log_x);
    return pi2/3.0-Li2(1.0/x)-0.5*log_x*log_x;
  }

double AsFactor(double Q,int Nc){return 0.118/2.0/M_PI;}// {return alphasPDF(Q)*Nc/2.0/M_PI;}

double Unresolved(int N,momentum<double> *k,double mu,double Smin)
{
  // We have always 2 invariants smaller than zero, so the -log^2 term generates 2*pi^2...
  double pi2=M_PI*M_PI;
  double factor=0.0;
  for (int a=0;a<N;a++) {
    int b=(a+1)%N;
    //printf("%d %d\n",a,b);
    double sab=2.0*(k[a]*k[b]);
    if (sab<0) {factor+=pi2;sab=-sab;}
    double l=log(Smin/sab);
    // add antenna contribution: 67/18-pi^2/3+11/6*log(mu^2/(Yslise*abs(sab))-log^2(Yslice);
    factor+=0.4323540885+1.833333333*log(mu*mu/Smin)-l*l;
  }
  return factor;
}

double TreeLevel(const int Nparticle,const momentum<double> *P,const momentum<double> *j)
{
  momentum<double> *K;
  K=new momentum<double>[Nparticle];
  int N=Nparticle-1;
  momentum<double> Jlk[N+1][N+1];
  for (int k=1;k<=N;k++) Jlk[k][k]=j[k];
  K[0]=P[0];
  for(int i=1; i<=N; i++) K[i]=K[i-1]+P[i];
  for(int l=1; l<N; l++) {
    for (int k=1;k<=N-l;k++){
      momentum<double> J=momentum_<double>(0.0,0.0,0.0,0.0);
      for (int n=k; n<k+l; n++) {
        // 2 J1.K2 J2 - 2 J2.K1 J1 + J1.J2 (K1-K2)
        momentum<double> K1=K[n]-K[k-1];
        momentum<double> K2=K[k+l]-K[n];
        momentum<double> J1=Jlk[k][n], J2=Jlk[n+1][k+l];
        J+=(2.0f*(J1*K2))*J2-(2.0f*(J2*K1))*J1+(J1*J2)*(K1-K2);
      }
      for (int n=k; n<k+l-1; n++) {
	for (int m=n+1;m<k+l;m++) {
	  momentum<double> J1=Jlk[k][n],J2=Jlk[n+1][m],J3=Jlk[m+1][k+l];
	  J+=(2.0f*(J1*J3))*J2-(J1*J2)*J3-(J2*J3)*J1;
	}
      }
      if (l<N-1) {
	momentum<double> Pm=K[k+l]-K[k-1];
	J/=Pm*Pm;
      }
      Jlk[k][k+l]=J;
    }
  }
  delete K;
  return pow(j[0]*Jlk[1][N],2)/pow(2.0,Nparticle-2);
}

double TestPDF(double x) {return (1-x)/2.0/x;}

double Crossing(double x,double Smin,double mu) {
  double l=log(x),l1=log(1.0-x),pi2=M_PI*M_PI;
  double A=2.0*(1.0-1.0/x)+((1.0-x)*(1.8333333333333333+2.0*l1))/x
          -(2.0*(((x-1.0)*(-11.0+(x-2.0)*x))/6.0+(1.0+x)*l))/x;
  double B=(2.0*(x-1.0)*(-1.0+l1))/x+((1.0-x)*(-3.7222222222222222+pi2/3.0+pow(l1,2)))/x 
          +(-6.0*pi2*(1.0+x)+(x-1.0)*(-31.0+5.0*(x-2.0)*x)-6.0*(x-1)*(-11.0+(x-2.0)*x)*l1
	    -36.0*x*l+36.0*(1.0+x)*Li2(x))/(18.0*x);
  double C=(A*log(Smin/mu/mu)+B);
  return C/2.0;
}

void oneloop(int N,momentum<double> *k,momentum<double> *j,double mu,double &Born,double &FiniteVirtual)
{
  Born=TreeLevel(N,k,j);
  FiniteVirtual=0.0;
  return;
}

double Virtual(int N,double mu,double Smin,int Nc,double x1,double x2,double scale,
	       momentum<double> *k,momentum<double> *j,double& Born)
{
  double FiniteVirtual;
  oneloop(N,k,j,mu,Born,FiniteVirtual);
  double as=AsFactor(mu,Nc);
  double s=scale*scale;
  double Kfactor=1.0+as*Unresolved(N,k,mu/s,Smin/s);
  double Cfactor=as*(TestPDF(x1)*Crossing(x2,Smin,mu)+Crossing(x1,Smin,mu)*TestPDF(x2));
  double virtualwgt=pow(as,N-2)*((Kfactor+Cfactor)*Born+as*FiniteVirtual);
  return virtualwgt;
}


