
// random integer between 0 and 2^32-1 inclusive
__device__ unsigned int random(uint2 &w) {
  w.x = 36969 * (w.x & 65535) + (w.x >> 16);
  w.y = 18000 * (w.y & 65535) + (w.y >> 16);
  return (w.x << 16) + w.y;
}

// random float uniform in interval (0,1]
__device__ float uniform(uint2 &w) {
  //  return __int_as_float(__float_as_int(__uint2float_rn((w.x << 16) + w.y))-(32<<23));  /* 32-bit result */
  unsigned int tmp=random(w);
  return tmp ? __uint2float_rn(tmp)/4294967296.0f : 1.0f;  /* 32-bit result */
}

// random float uniform in interval (min, max]
__device__ float uniform(uint2 &w, float min, float max) {
  return min+(max-min)*uniform(w);
}

double uniform() 
{
  double random,s,t;
  int m,seed1=1429,seed2=9343;
  int i=(seed1/177)%177+2,j=(seed1%177)+2,k=(seed2/169)%178+1,l=seed2%169;
  static bool init=false;
  static int iranmr=96,jranmr=32;
  static double ranc=362436.0/16777216.0,rancd=7654321.0/16777216.0,
    rancm=16777213.0/16777216.0,ranu[97];
  if (init==false) 
    {
      init=true;
      for (int il=0;il<97;++il)
        {
          s=0;t=0.5;
          for (int jl=0;jl<24;++jl)
            {
              m=(((i*j%179)*k)%179);i=j;j=k;k=m;l=(53*l+1)%169;
              if (((l*m)%64)>=32) s+=t;
              t*=0.5;
            };
          ranu[il]=s;
        };
    };
  random=ranu[iranmr]-ranu[jranmr];
  if (random<0.0) ++random;
  ranu[iranmr--]=random;
  jranmr--;
  if (iranmr<0) iranmr=96;
  if (jranmr<0) jranmr=96;
  ranc-=rancd;
  if (ranc<0.0) ranc+=rancm;
  random-=ranc;
  if (random<0.0) random++;
  return random;  
}

double uniform(double min,double max) {return min+(max-min)*uniform();}
