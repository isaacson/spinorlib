% Forward Brancher 1-->2
% =======================================

\documentclass[11pt,onecolumn,twoside,notitlepage,fleqn,letterpaper,final]
	      {article}[]

\input{texsetup.tex}

%\preprint{FERMILAB-PUB-11-xxx-T}
%\author{\hphantom{MisterX}\\[0mm]\sffamily\bfseries\normalsize%
%  Walter~T.~Giele\thanks{giele@fnal.gov}
%  \,\!,\
%  Gerben~C.~Stavenga\thanks{stavenga@fnal.gov}
%  \\[4mm]
%  {\small\sl Theoretical Physics Department}\\[-2mm]
%  {\small\sl Fermi National Accelerator Laboratory, Batavia, IL 60510, USA}
%  \\[10mm]\sffamily\bfseries\normalsize%
%  Jan Winter\thanks{jwinter@cern.ch}
%  \\[4mm]
%  {\small\sl PH-TH, CERN, CH-1211 Geneva 23, Switzerland}
%  \\[14mm]
%}
%\title{\sffamily\bfseries\Large%
%  Title here}
%\institute{Theoretical Physics Department, Fermi National Accelerator
%  Laboratory, Batavia, IL 60510, USA}



% ======= more definitions ====================================================

\newcommand{\order}[1]{$\mathcal O(#1)$}
\newcommand{\tsc}[1]{\textsc{#1}}

\newcommand{\pt}{p_{\rm T}}
\newcommand{\dwt}[1]{{\widetilde {d#1}}}

% ======= start ===============================================================



\begin{document}
\renewcommand{\baselinestretch}{1.4}
%\maketitle
%\thispagestyle{empty}
%\begin{flushleft}\vspace*{-130mm}\date{\today}\\[127mm]\end{flushleft}
%\begin{flushright}
%  \vspace*{-127mm}
%  {\small FERMILAB-PUB-11-xxx-T}\\[130mm]
%\end{flushright}

%\renewcommand{\baselinestretch}{1.1}
%\begin{abstract}
%\noindent

%\end{abstract}

%\clearpage%\newpage
%\renewcommand{\baselinestretch}{0.5}
%\tableofcontents
%\thispagestyle{empty}
%\clearpage
%\renewcommand{\baselinestretch}{1.04}
%\vspace*{3mm}
%\noindent\hrulefill
%\vspace*{4mm}



% ======= main ================================================================


\section{Introduction}

We will discuss a formalism for semi-numerical evaluations of $k_T$-jet functions.
When adding these functions to the virtual corrections, the result is a fully differential
jet cross section.
The construction of the phase space generator uses two techniques. The first one is a 
forward branching phase space generator using a $1\rightarrow 2$ brancher. It is used
to integrate over all bremsstrahlung contributions to a fixed jet phase space point. 
Because we use a $1\rightarrow 2$ bracher we {\it must} integrate over the jet mass.
For lepton colliders we can {\it exactly} implement any sequential clustering jet algorithm.
For hadron colliders, we must ensure no unclustered momenta are left. To this end we must
introduce a version of the beam jet. This beam jet collects all momenta clustered with the
initial state. The current jet algorithms used in the experiments do not use beam jets
and leave unclustered energy in the event. The differences with the theory calculation will
be small. If needed, the finite difference function can always be calculated numerically 
and added to the theoretical beam-jet result to give the result obtained with the experimental
jet definition.  The second technique we will use is an OPP decomposition of the tree-level
matrix element. This allows us to semi-analytically calculate the jet core contribution to
the jet functions. The outer regions of the jet function need to be calculated numerically
as they depend on the specific jet algorithm and the configuration of the jets.

\section{Lepton Colliders}

In this section we will consider the process $l^+l^-\rightarrow n$-jets. Specifically,
we will define the NLO corrections to the fully differential jet cross section
\beq
\frac{d^{(1)}\sigma}{d\,\vec{J}_1d\,\vec{J}_2\cdots d\,\vec{J}_n}=
K\left(\vec{J}_1,\vec{J}_2,\ldots,\vec{J}_n\right)\frac{d^{(0)}\sigma}{d\,\vec{J}_1d\,\vec{J}_2\cdots d\,\vec{J}_n}\ ,
\eeq
where the LO fully differential cross section is given by
\beq
\frac{d^{(0)}\sigma}{d\,\vec{J}_1d\,\vec{J}_2\cdots d\,\vec{J}_n}=(2\pi)^{4-3n}
\left|\overline{\cal M}\left(l^+l^-;\vec{J}_1,\vec{J}_2,\ldots,\vec{J}_n\right)\right|^2\ .
\eeq

We assume the jet algorithm has a 2-parton resolution function, $R_{ab}=R(\vec{p}_a,\vec{p}_b)$,
a jet resolution value, $R_{\mbox{\tiny min}}$
and a $2\rightarrow 1$ cluster map of the form $\vec{p}_{ab}=\vec{p}_a+\vec{p}_b$. 
The energy component/mass of the clusters is ignored and its treatment during the
jet clustering is irrelevant.
The pair of clusters with the smallest resolution $R_{ab}$ are combined using the
cluster map. This is repeated until the minimal resolution of two clusters exceeds 
$R_{\mbox{\tiny min}}$. Note that the class of $k_T$-algorithms fullfills these requirements.
It has a resolution function 
\beq
R_{ab}^{\alpha}=2\min\left(\left|\vec{p}_{a}\right|^{2\alpha},\left|\vec{p}_{b}\right|^{2\alpha}\right)
\left(1-\cos\left(\theta_{ab}\right)\right)\,,\
\cos(\theta_{ab})=\frac{\vec{p}_a\cdot\vec{p}_b}{\left|\vec{p}_{T,a}\right|\left|\vec{p}_{T,b}\right|}
\eeq
and the cluster map $p_{ab}=p_a+p_b$.

At NLO we have one pair of partons clustering back to one of the jets. That is
we can write the bremsstrahlung phase space as
\beq
d\,\Phi_{n+1}(l^+l^-\rightarrow p_1\cdots p_np_r)=
\sum_{i=1}^nd\,\Phi^{[i]}_{n+1}(l^+l^-\rightarrow J_1(p_1)\cdots J_i(p_i+p_r)\cdots J_n(p_n))\ ,
\eeq
where
\beq
d\,\Phi^{[i]}_{n+1}(l^+l^-\rightarrow J_1\cdots J_n)=
d\,\Phi_n(l^+l^-\rightarrow J_1\cdots J_n)
\times d\,J_i^2\times d\,\Phi_2(J_i\rightarrow p_ip_r)\,\Theta^{[i]}_{\mbox{\tiny JET}}\ ,
\eeq
and the jet constraint is given by
\beq
\Theta^{[i]}_{\mbox{\tiny JET}}=\theta(R_{ir}=\min_{kl}(R_{kl}))\,\theta(R_{ir}<R_{\mbox{\tiny MIN}})
\eeq

We can write the phase space of the jet function as
\beq
d\,\Phi(\vec{J}_i)=\frac{d\,\Phi^{[i]}_{n+1}(l^+l^-\rightarrow J_1\cdots J_n)}{d\,\Phi_n(l^+l^-\rightarrow J_1\cdots J_n)}
=d\,J_i^2\times d\,\Phi_2(J_i\rightarrow p_ip_r)\,\Theta^{[i]}_{\mbox{\tiny JET}}\ ,
\eeq
and the jet function is given by
\beq
B_{\mbox{\tiny JET}}(\vec{J}_i)=d\,\Phi(\vec{J}_i) 
\left|\overline{\cal M}\left(l^+,l^-,p_1,\ldots,p_n,p_r\right)\right|^2
=d\,\Phi(\vec{J}_i) A(p_i,p_r)\ .
\eeq 
The integration is effectively an one-particle integral as $p_i=J_i-p_r$.
The matrix element ${\cal M}$ can be an ordered amplitude summed over the appropriate insertions
of parton $r$ in the ordering. Once we calculated all $n^2$ off-shell currents, the computational
effort for each new phase space point in any of the $n$ jet-functions is just of complexity $n$.
making the phase space integration fast. The singular part of the matrix element can be subtracted
per jet-function sector and added back in after analytic integration. To accomodate the analytic
integral we define the jet core as that part of the jet function phase space where 
$s_{ir}<s_{\mbox{\tiny min}}$. The $s_{\mbox{\tiny min}}$-parameter is sufficiently small such that the core 
is fully contained withing the jet\footnote{We could use here a ``+''-prescription in the subtraction funtion.
In this way the integrations are all numerical and in principle we can use a jet fragmentation function.}. 
That is: for every $p_i$ and $p_r$ for which $s_{ir}<s_{\mbox{\tiny min}}$,
$\Theta^{[i]}_{\mbox{\tiny JET}}=1$. The jet function is now given by
\beqa
B_{\mbox{\tiny JET}}(\vec{J}_i)
&=&d\,\Phi^{[4]}(\vec{J}_i) \Big(A(p_i,p_r)-S(p_i,p_r)A(\vec{J}_i)\theta(s_{ir}<s_{\mbox{\tiny min}})\Big)\nn
&+&A(\vec{J}_i)d\,\Phi^{[D=4-2\epsilon]}(\vec{J}_i)S(p_i,p_r)\theta(s_{ir}<s_{\mbox{\tiny min}})
\eeqa 
where the $D$-dimensional phase space integral over the divergent subtraction function $S$ 
has to be performed analytically.

The above conventional method uses the matrix elements and subtraction terms. However, as an
alternative we will explore the tree-level OPP method. Here we calculate the master
integrals analytically and determine the coefficients of the master integrals numerically. 
Specifically
\beq
{\cal M}(p_1,\ldots,p_n,p_r)=\sum_{ij} \frac{c_{ij}(p_r)}{D_iD_j}+\sum_i\frac{b_i(p_r)}{D_i}+c(p_r)
\eeq
We can determine the parametric form of the numerator in a similar fashion as we do for 
loop integrals. Note that the function has singularities and need a regularization procedure.
Next we can interate the function analytically over the core part of the jet and analytically
over the remaining outer part of the jet 
\beqa
B_{\mbox{\tiny JET}}(\vec{J}_i)=d\,J_i^2d\,\Phi^{[D]}(J_i\rightarrow p_ip_r)A(p_r)\theta(s_{ir}<s_{\mbox{\tiny min}})
+d\,\Phi^{[4]}(\vec{J}_i) A(p_r)\theta(s_{ir}\geq s_{\mbox{\tiny min}})\Big)
\eeqa
For the first integral we need to know the master integrals, integrated over the jet core. The second
integral has to be performed numerically over the fast to evaluate parametric form of the amplitude
squared.
Note that this method is subtraction free. The phase space integrations are performed semi-analytical,
just like the one-loop integrations in generalized unitarity.
The generalization to NNLO triple parton contributions to the jet function are in this language 
quite easy. We just need the LO matrix element parametrized as a function of two external momenta.
This will give us the master integrals and their coefficients can be determined by solving the
OPP equations.

We define the event bremsstrahlung contribution
\beq
B_{\mbox{\tiny EVENT}}(\vec{J}_1,\ldots,\vec{J}_n)=\sum_{i=1}^n B_{\mbox{\tiny JET}}(\vec{J}_i)\ , 
\eeq
so that the LO and NLO $n$-jet cross section are given by
\beqa
\frac{d^{(0)}\sigma}{d\,\vec{J}_1d\,\vec{J}_2\cdots d\,\vec{J}_n}&\sim&
\left|{\cal M}^{(0)}(\vec{J}_1,\ldots,\vec{J}_n)\right|^2\nn
\frac{d^{(1)}\sigma}{d\,\vec{J}_1d\,\vec{J}_2\cdots d\,\vec{J}_n}&\sim&
\left|{\cal M}^{(0)}(\vec{J}_1,\ldots,\vec{J}_n)\right|^2
+2\Re\left({\cal M}^{(1)}{{\cal M}^{(0)}}^\dagger\right)(\vec{J}_1,\ldots,\vec{J}_n)
+B_{\mbox{\tiny EVENT}}(\vec{J}_1,\ldots,\vec{J}_n)
\eeqa
\section{Hadron Colliders}

In a hadron collider enviroment we have the additional problem of the incoming
color charged partons. This requires a modification to the jet algorithm. From
a theoretical point of view the modification is easily implemented in a consistent
manner using beam jets. However, in the experimental enviroment there is the complication
of the beam remnants overlayed on the hard scattering event. As a result, the
jet algorithm used by the experimentalists have some issues. 

To start we give a short description of the jet algorithm used be the experimenters.
Instead of one resolution parameter, as is the case for the lepton colliders, we 
have two resolution parameters depending on whether the two clusters under consideration
are final--final (ff) or initial--final (if). The resolution functions are given by
\beqa
R_{ij}^{\alpha}&=&\min\left(\left|\vec{p}_{T,i}\right|^{2\alpha},\left|\vec{p}_{T,j}\right|^{2\alpha}\right)
\frac{\Delta^2_{ij}}{R^2}\nn
R_{iB}^{\alpha}&=&\left|\vec{p}_{T,i}\right|^{2\alpha}
\eeqa
where $\Delta^2_{ij}=(y_i-y_j)^2+(\phi_i-\phi_j)^2$ is given in terms of the pseudorapidity
$y_i=-\ln(\tan(\theta_i/2))=\frac{1}{2}\ln\left(\frac{|\vec{p}_i|+p_z}{|\vec{p}_i|-p_z}\right)$
and the azimuthal angle $\phi_i$.
The jet algorithm follows the steps
\begin{enumerate}
\item
If the smallest invariant mass is larger than $R_{\mbox{\tiny MIN}}$ the clustering is stopped
and the remaining clusters are jets.
\item
If the smallest invariant mass is a final--final state pair, the two 4-vectors are added
to form the new cluster.
\item
If the smallest invariant mass is a final--initial state pair, the final state cluster 
is removed (i.e. will remain unclustered).
\end{enumerate}
The problem for the NLO calculation lays in the final point. The violation of 
momentum conservation for the jet final state during the clustering separates
virtual and real in the presence of initial state radiation.

From the purely theoretical point of view this is easily corrected. The 3th step
in the jet algorithm is replaced. We now simply add the 4-vectors of the incoming
beam parton and the final state collinear particle. Now the incoming parton is
no longer along the $z$-direction. Instead the beam-axis is along the $z$-direction.
We can now define the observables in the frame where the beam jet axis is along the 
$z$-direction. This has much better IR-properties than the experimental algorithm
as we now have jet momentum conservation and we integrate out the incoming partons
inside the beam jet.

The purely theoretical jet algorithm does not take in consideration the beam remnants.
They all would be clustered as part of the jet algorithm. A possible ``in-between'' jet
algorithm would borrow some techniques developed for boosted jets. In this case we would
before starting the jet algorithm ``filter'' the event. This would mean we would sum all
4-vectors of particles with $p_t<p_{\mbox{\tiny FILTER}}$, resulting in a single 4-vector.
The 3-vector part of this momentum should be small and replaces all filtered particles
in the list of momenta to be clustered by the jet algorithm. We then apply the theoretical
jet algorithm..

\section{Branching Phase Spaces}

We need two forward branching phase spaces in $D$-dimensions. 
The first one is the simple 2 body decay for a final--final state branchings.
The other one is the backward evolving initial state parton.

We start with the lepton collider case. The $D$-dimensional $n$-particle
phase space is given by
\beqa
d\,\Phi^{[D]}_n(Q;p_1,\ldots,p_n)&=&
\left[\prod_{i=1}^n\frac{d^{[D-1]}\vec{p}_i}{(2\pi)^{D-1}\,2E_i}\right]
\times (2\pi)^D\delta^{[D]}(Q-p_1-\cdots-p_n)\nn
&=&(2\pi)^{n-D(n-1)}d\,R_n^{[D]}(Q;p_1,\ldots,p_n)
\eeqa
We will use the following factorization of phase space
\beq
d\,R_{n+2}^{[D]}(Q;p_1,\ldots,p_n,p_a,p_b)=R_{n+1}^{[D]}(Q;p_1,\ldots,p_n,p_{ab})\times d\,p_{ab}^2
\times d\,R_2^{[D]}(p_{ab};p_a,p_b)\ .
\eeq
The two body phase space is given by
\beq
d\,R_2^{[D]}(p_{ab};p_a,p_b)=2^{1-D} s_{ab}^{\frac{D-4}{2}} d\,\Omega^{[D-1]} d\,s_{ab}
\delta(s_{ab}-p^2_{ab})
\eeq
This is a really simple generator for the forward brancher. The explicit
momenta $p_a$ and $p_b$ are also easily obtained.

For the initial state we have a more complicated phase space. We will
use section III.A of the JETRAD paper (without assuming the collinear 
limit) to derive the forward branching phase space brancher.
We start with the expressions for the $2\rightarrow 1$ and $2\rightarrow 2$
scattering phase spaces
\beqa
\frac{1}{2s_{ab}}d\,\Phi^{[D]}_1(p_a+p_b\rightarrow Q) 
&=&\frac{2\pi}{2s_{ab}}\delta(s_{ab}-Q^2) \nn
\frac{1}{2s_{ab}}d\,\Phi_2^{[D]}(p_a+p_b\rightarrow p_r+Q)&=&
\left(\frac{s_{a\hat b}}{s_{ab}}\right)\left(\frac{d\,t_{ar}d\,t_{rb}}{s_{ab}}\right)
\left(\frac{t_{ar}t_{rb}}{s_{ab}}\right)^{(D-4)/2}
\frac{d\Omega^{[D-3]}}{4} \nn
&&\times\left[\frac{1}{2s_{a\hat b}}d\,\Phi^{[D]}_1(p_a+\hat{p}_b\rightarrow Q)\right]\ .
\eeqa
We first follow the normal NLO derivation, to check the validity of this expression.
This means we assume $p_b$ is given and we generate $p_r$ and $\hat{p}_b$ (integrate
over $\hat{x}_b$). In this case we introduce $\hat z$ by $\hat{x}_b=\hat z\,x_b$ 
($\hat z<1$) and use 
\beqa
&&t_{ar}=2(p_b-\hat{p}_b)\cdot p_a=x_aS(x_b-\hat{x}_b)=x_ax_bS(1-z)\nn
&&\frac{s_{a\hat b}}{s_{ab}}=\hat z;\ \frac{t_{ar}}{s_{ab}}=1-\hat z;\ \frac{d\,t_{ar}}{s_{ab}}=d\,\hat z\ ,
\eeqa
to find
\beq
\frac{1}{2s_{ab}}d\,\Phi_2^{[D]}(p_a+p_b\rightarrow p_r+Q)=
\Phi^{[D]}_{\mbox{\tiny INIT}}(p_b\rightarrow\hat p_b+p_r)
\times\left[\frac{1}{2s_{a\hat b}}d\,\Phi^{[D]}_1(p_a+\hat{p}_b\rightarrow Q)\right]
\eeq
where
\beq
\Phi^{[D]}_{\mbox{\tiny INIT}}(p_b\rightarrow\hat p_b+p_r)=\frac{1}{4}\hat z\,d\,\hat z d\,t_{rb}
[(1-\hat z)|t_{rb}|]^{-\epsilon}d\,\Omega^{[D-3]}
\eeq
The construction of the two momenta $\hat{p}_b$ and $p_r$ out of the integration
variables is no longer trivial and needs a derivation. This is the familiar
initial state phase space, which in the collinear limit gives the usual integration
over the AP splitting function.

However, we want a forward branching phase space. In this case we have $\hat{p}_b$
given and need to generate $p_b$. This also means that instead of integrating over
$\hat{x}_b$ we integrate over $x_b$. By defining $z=\hat{z}^{-1}$ ($z>1$), we have
$x_b=z\times\hat{x}_b$. This gives us then for the FBPS (which for the initial state is
equivalent to backward evolution)
\beq
\frac{1}{2s_{ab}}d\,\Phi_2^{[D]}(p_a+p_b\rightarrow p_r+Q)=
\Phi^{[D]}_{\mbox{\tiny FBPS}}(\hat p_b\rightarrow p_b+p_r)
\times\left[\frac{1}{2s_{a\hat b}}d\,\Phi^{[D]}_1(p_a+\hat{p}_b\rightarrow Q)\right]
\eeq
where
\beq
\Phi^{[D]}_{\mbox{\tiny FBPS}}(\hat p_b\rightarrow p_b+p_r)=\frac{1}{4}\frac{d\,z}{z^3} d\,t_{rb}
\left[\frac{z-1}{z}|t_{rb}|\right]^{-\epsilon}d\,\Omega^{[D-3]}\ .
\eeq
The momenta $p_b$ and $p_r$ need to be constructed from the integration variables.
Note that $t_{ar}$ is the beam-jet mass and $p_b$ is not along the beam direction.
However, the jet-axis (given by $\hat{p}_b$) is along the beam direction, meaning
we are not in the lab frame but the theoretically preferred frame.
 


% ======= bibliography =========================================================



\bibliographystyle{JHEP}%{amsunsrt_mod}
{\raggedright\bibliography{FBPS}}

 

% ======= the end ==============================================================



%\end{fmffile}
\end{document}



% ======= EoF ==================================================================

