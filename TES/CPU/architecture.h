// These 3 GPU properties should be read-out from device.
//
#define NUMBER_MP 1*30
#define SHARED_MEMORY 16384 // =2^14
#define REGISTER_MEMORY 16384 //=2^14
//
//
// Set the maximum number of registers ever used by program
//
#define MAX_REGISTER 35



