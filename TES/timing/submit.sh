#!/bin/sh
#SBATCH --job-name=tes
#SBATCH --partition=gpu_gce
#SBATCH --nodes=1
#SBATCH --gres=gpu:p100:1

nvidia-smi -L
./tess
exit

