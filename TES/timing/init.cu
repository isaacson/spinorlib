//
// initializing device memory
//
uint2 *g_entropy;
float *g_weight;
bool *g_cut;

extern void event_init(int,int);

void init(int N,int total_events) 
{
  //
  // mapping device memory
  //
  cudaMalloc((void **)&g_cut,total_events*sizeof(bool));
  cudaMalloc((void **)&g_entropy,total_events*(N-1)*sizeof(uint2));
  cudaMalloc((void **)&g_weight,total_events*sizeof(float));
  //
  // Kernel initializations
  //
  event_init(N,total_events);
}  
