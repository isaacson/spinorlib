\documentstyle{article}
\newcommand{\beq}{\begin{equation}}
\newcommand{\eeq}{\end{equation}}
\newcommand{\beqa}{\begin{eqnarray}}
\newcommand{\eeqa}{\end{eqnarray}}
\newcommand{\nn}{\nonumber}
\newcommand{\e}{\varepsilon}

\begin{document}

\section{Reproducing previous results}

First we reproduce the results from ref.~\cite{Giele:2010ks} and 
compare the speed-up from the old GPU architecture 
of the Fermi C1060 to the newer Kepler K20m. 
The relevant architecture difference are given in
table.~\ref{GPUcomp}. Because the shared memory increased by a factor of 3
and hence 3 times more events can run per Multi-Processor (MP) we
expect a speed-up per MP of about a factor of 3. However the FERMI GPU
has 30 MP compared to the 13 MP of the Kepler negating much of the
speed-up. So the overall expectation is only a marginal improvement in
computing speed. The time per event, event per MP and the total cross
section ($\sqrt{S}=14$ TeV, $P_T^{\mbox{\tiny jet}}>20$ GeV,
$|\eta^{\mbox{\tiny jet}}|<2.5$, $\Delta R^{\mbox{\tiny (jet,
    jet)}}>0.4$, $\mu_R=\mu_F=91.188$ GeV, CTEQ6L1) is given in
table~\ref{ResultComp}.

\begin{table}[t]\label{GPUcomp}
\begin{center}
\begin{tabular}{|c||c|c|}
\hline 
 &Fermi C1060 & Kepler K20m \\
\hline 
clock & 1296 & 706 \\
MP & 30 & 13 \\
threads/MP & 1024 & 192 \\
shared memory & 16,384 & 49,152 \\
register memory & 16,384 & 65,536 \\
\hline 
\end{tabular}
\caption{The GPU properties relevant for TES}
\end{center}
\end{table}

\begin{table}[t]\label{ResultComp}
\begin{tabular}{|c||r|r|r|}
\cline{2-4}
\multicolumn{1}{c}{}&\multicolumn{1}{|c||}{Fermi C1060} & \multicolumn{2}{c|}{Kepler K20m} \\
\cline{2-4}\hline
$n$ & \multicolumn{1}{|c||}{sec} & \multicolumn{1}{|c|}{sec} & \multicolumn{1}{|c||}{sec (optimized)} \\
\hline
$4$ & $3.0\times 10^{-8}$ & $1.9\times 10^{-8}$ & $5.3\times 10^{-9}$ \\
$5$ & $4.4\times 10^{-8}$ & $3.8\times 10^{-8}$ & $\times 10^{-8}$ \\
$6$ & $8.6\times 10^{-8}$ & $6.6\times 10^{-8}$ & $\times 10^{-8}$ \\
$7$ & $2.3\times 10^{-7}$ & $1.4\times 10^{-7}$ & $\times 10^{-8}$ \\
$8$ & $3.5\times 10^{-7}$ & $2.3\times 10^{-7}$ & $\times 10^{-8}$ \\
$9$ & $4.3\times 10^{-7}$ & $3.0\times 10^{-7}$ & $\times 10^{-8}$ \\
$10$ & $6.8\times 10^{-7}$ & $5.1\times 10^{-7}$ & $\times 10^{-8}$ \\
$11$ & $9.8\times 10^{-7}$ & $7.3\times 10^{-7}$ & $\times 10^{-8}$ \\
$12$ & $1.4\times 10^{-6}$ & $1.0\times 10^{-6}$ & $\times 10^{-8}$ \\
\hline
\end{tabular}
\caption{hiiojhn}
\end{table}

\begin{table}[t]\label{ResultComp}
\begin{tabular}{|c||r||r|r|}
\cline{2-4}
  \multicolumn{1}{c}{}&\multicolumn{1}{|c||}{Fermi C1060} & \multicolumn{2}{c|}{Kepler K20m} \\
\cline{2-4}\hline
$n$ & \multicolumn{1}{c||}{x-section (pb)} &
\multicolumn{1}{c|}{x-section $10^7$ iter (pb)}
&\multicolumn{1}{c|}{x-section $10^8$ iter (pb)}  \\
\hline
4 & $(2.3242\pm 0.0005)\times10^8$ 
& $(2.3163\pm 0.0001)\times10^8$
& $(2.31638\pm 0.00004)\times10^8$\\
5 & $(1.435\pm 0.001)\times10^7$
& $(1.4277\pm 0.0003)\times10^7$
&\\
6 & $(2.848\pm 0.001)\times10^6$
&  $(2.826\pm 0.003)\times10^6$
&\\
7 & $(6.36\pm 0.01)\times10^5$
& $(6.29\pm 0.03)\times10^5$
&\\
8 & $(1.61\pm 0.01)\times10^5$
& $(1.55\pm 0.01)\times10^5$
&\\
9 & $(4.4\pm 0.1)\times10^4$
& $(4.5\pm 0.3)\times10^4$
&\\
10 & $(1.19\pm 0.02)\times10^4$
& $(1.21\pm 0.06)\times10^4$
&\\
11 & $(3.55\pm 0.02)\times10^4$ 
& $(5\pm 1)\times10^3$
&\\
12 & $(9.6\pm 0.7)\times10^3$ 
& $(9\pm 1)\times10^2$
&\\
\hline 
\end{tabular}
\caption{Comparison with results from ref.~\cite{Giele:2010ks}. The run
parameters are given in the text. The number of iterations in both
cases (?) is $10^7$, giving ${\cal O}(10^9)$ events.}
\end{table}

\section{Generating LO, gluons only}

We need to generate the full color dependence. This in principle means
we have to calculate all possible permutations of the ordered
amplitudes. Naively this will lead to an algorithm of factorial
complexity.
Moreover, we also have to sum over polarizations/helicities, giving
naively $2^n$ amplitudes.

The polarization power complexity can be overcome by using randomly
oriented polarization vectors. The added benefit is that the currents
are not complex numbers. The polarization vector is simply given by
$e_\mu(p)$ with $e^2=1$ and $e\cdot p=0$. This leaves one degree of
freedom, azimuthal angle wrt to the 3-momentum vector. The spin
averaging is performed by randomly choosing the azimuthal angles per
event.

To combat the factorial complexity we can use explicit colors. I.e. we
choose the explicit colors of the gluon randomly event-by-event in the
color decomposition formalism~\cite{Draggiotis:1998gr,Caravaglios:1998yr,Maltoni:2002mq} . This
is implemented in COMIX at LO~\cite{Duhr:2006iq} and at NLO in
ref.~\cite{Giele:2009ui}.

Using these techniques keep the algorithm at polynomial complexit
(i.e. evaluation time is proportional to $n^a$ and not $a^n$ or $n!$).

\section{Other papers of note}

\begin{itemize}
\item Kleiss
\end{itemize}
 
\begin{thebibliography}{99}
%\cite{Giele:2010ks}
\bibitem{Giele:2010ks} 
  W.~Giele, G.~Stavenga and J.~C.~Winter,
  %``Thread-Scalable Evaluation of Multi-Jet Observables,''
  Eur.\ Phys.\ J.\ C {\bf 71}, 1703 (2011)
  doi:10.1140/epjc/s10052-011-1703-5
  [arXiv:1002.3446 [hep-ph]].
  %%CITATION = doi:10.1140/epjc/s10052-011-1703-5;%%
  %6 citations counted in INSPIRE as of 18 Oct 2016
%\cite{Draggiotis:1998gr}
\bibitem{Draggiotis:1998gr} 
  P.~Draggiotis, R.~H.~P.~Kleiss and C.~G.~Papadopoulos,
  %``On the computation of multigluon amplitudes,''
  Phys.\ Lett.\ B {\bf 439}, 157 (1998)
  doi:10.1016/S0370-2693(98)01015-6
  [hep-ph/9807207].
  %%CITATION = doi:10.1016/S0370-2693(98)01015-6;%%
  %88 citations counted in INSPIRE as of 20 Oct 2016
%\cite{Caravaglios:1998yr}
\bibitem{Caravaglios:1998yr} 
  F.~Caravaglios, M.~L.~Mangano, M.~Moretti and R.~Pittau,
  %``A New approach to multijet calculations in hadron collisions,''
  Nucl.\ Phys.\ B {\bf 539}, 215 (1999)
  doi:10.1016/S0550-3213(98)00739-1
  [hep-ph/9807570].
  %%CITATION = doi:10.1016/S0550-3213(98)00739-1;%%
  %200 citations counted in INSPIRE as of 20 Oct 2016
%\cite{Maltoni:2002mq}
\bibitem{Maltoni:2002mq} 
  F.~Maltoni, K.~Paul, T.~Stelzer and S.~Willenbrock,
  %``Color flow decomposition of QCD amplitudes,''
  Phys.\ Rev.\ D {\bf 67}, 014026 (2003)
  doi:10.1103/PhysRevD.67.014026
  [hep-ph/0209271].
  %%CITATION = doi:10.1103/PhysRevD.67.014026;%%
  %89 citations counted in INSPIRE as of 20 Oct 2016
%\cite{Duhr:2006iq}
\bibitem{Duhr:2006iq} 
  C.~Duhr, S.~Hoeche and F.~Maltoni,
  %``Color-dressed recursive relations for multi-parton amplitudes,''
  JHEP {\bf 0608}, 062 (2006)
  doi:10.1088/1126-6708/2006/08/062
  [hep-ph/0607057].
  %%CITATION = doi:10.1088/1126-6708/2006/08/062;%%
  %63 citations counted in INSPIRE as of 20 Oct 2016
%\cite{Giele:2009ui}
\bibitem{Giele:2009ui} 
  W.~Giele, Z.~Kunszt and J.~Winter,
  %``Efficient Color-Dressed Calculation of Virtual Corrections,''
  Nucl.\ Phys.\ B {\bf 840}, 214 (2010)
  doi:10.1016/j.nuclphysb.2010.07.007
  [arXiv:0911.1962 [hep-ph]].
  %%CITATION = doi:10.1016/j.nuclphysb.2010.07.007;%%
  %31 citations counted in INSPIRE as of 20 Oct 2016
\end{thebibliography}

\end{document}