#include <cstdio>
#include <iostream>

//
// initializing device memory
//

void *g_nan,*g_entropy,*g_weight,*g_cut;
void *g_observable0,*g_observable1,*g_observable2,*g_observable3,*g_observable4,*g_observable5,*g_observable6;

extern void event_init(int N,int M);

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}


void init(int N,int total_events) 
{
  //
  // mapping device memory
  //
  gpuErrchk( cudaMalloc(&g_nan,total_events*sizeof(bool)) );
  cudaMalloc(&g_cut,total_events*sizeof(bool));
  cudaMalloc(&g_entropy,total_events*(N-1)*sizeof(uint2));
  gpuErrchk( cudaMalloc(&g_weight,total_events*sizeof(float)) );
  cudaMalloc(&g_observable0,total_events*sizeof(float));
  cudaMalloc(&g_observable1,total_events*sizeof(float));
  cudaMalloc(&g_observable2,total_events*sizeof(float));
  cudaMalloc(&g_observable3,total_events*sizeof(float));
  cudaMalloc(&g_observable4,total_events*sizeof(float));
  cudaMalloc(&g_observable5,total_events*sizeof(float));
  cudaMalloc(&g_observable6,total_events*sizeof(float));
  //
  // Kernel initializations
  //
  event_init(N,total_events);
}  

void cleanup()
{
  //
  // mapping device memory
  //
  gpuErrchk( cudaFree(g_nan) );
  cudaFree( g_cut );
  cudaFree( g_entropy );
  gpuErrchk( cudaFree( g_weight ) );
  cudaFree( g_observable0 );
  cudaFree( g_observable1 );
  cudaFree( g_observable2 );
  cudaFree( g_observable3 );
  cudaFree( g_observable4 );
  cudaFree( g_observable5 );
  cudaFree( g_observable6 );

  cudaDeviceReset();
}
