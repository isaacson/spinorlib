// These 3 GPU properties should be read-out from device.
//
//#define NUMBER_MP 30
#define NUMBER_MP 13
//#define SHARED_MEMORY 16384 // =2^14
#define SHARED_MEMORY 3*16384 // =2^14
//#define REGISTER_MEMORY 16384 //=2^14
#define REGISTER_MEMORY 4*16384 //=2^14
//
// NUMBER_MP =   30 for Tesla C1060
// NUMBER_MP = 4*30 for Tesla S1070
//
// Set the maximum number of registers ever used by program
//
//#define MAX_REGISTER 36
#define MAX_REGISTER 62



