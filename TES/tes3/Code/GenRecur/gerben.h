#ifndef __GERBEN_H__
#define __GERBEN_H__

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cctype>
#include <iostream>

#include <algorithm>
#include <vector>
#include <complex>
#include <utility>

#define PB push_back
#define ALL(x) (x).begin(),(x).end()
#define SZ(x) ((int)(x).size())
#define REP(i, n) for(int i=0; i<n; ++i)
#define REPD(i, n) for(int i=(n)-1; i>=0; --i)
#define FOR(i, b, e) for(typeof(e) i=b; i!=e; ++i)

// #define DIM 4

using namespace std;

typedef complex<float> Complex;
typedef vector<int> VI;
typedef vector<VI> VVI;
typedef vector<double> VD;
typedef vector<VD> VVD;
typedef vector<Complex> VC;
typedef vector<VC> VVC;


#endif
