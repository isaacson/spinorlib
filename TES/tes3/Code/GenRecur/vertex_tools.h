#ifndef __VERTEX_TOOLS_H__
#define __VERTEX_TOOLS_H__

#include "momentum.h"

///////////////////////////// gluon vertex functions ///////////////////////////

template <const int MATD>
struct gauge_amp {
  momentum<matrix<MATD, double> > J;
  vect<6, matrix<MATD, double> > W;
};

template <const int MATD>
const gauge_amp<MATD> &operator+=(gauge_amp<MATD> &a, const gauge_amp<MATD> &b) {
  a.J+=b.J;
  a.W+=b.W;
  return a;
}

template <const int MATD>
const gauge_amp<MATD> &operator-=(gauge_amp<MATD> &a, const gauge_amp<MATD> &b) {
  a.J-=b.J;
  a.W-=b.W;
  return a;
}

template <const int MATD>
inline
gauge_amp<MATD> gauge_gauge_gauge(const gauge_amp<MATD> &a, const momentum<double> &pa, const gauge_amp<MATD> &b, const momentum<double> &pb) {
  gauge_amp<MATD> res;
  // static momentum<matrix<MATD, double> > J;
  // static vect<6, matrix<MATD, double> > W;
  null(res.J); null(res.W);
  
  // J+=[J1;J2] (p2-p1) + [J1, 2p1.J2] + [J2, 2p2.J1]
  static matrix<MATD, double> C[4][4], inner;
  REP(mu, 4) REP(nu, 4) C[mu][nu]=com(a.J[mu], b.J[nu]);
  inner=C[0][0];
  FOR(mu, 1, 4) inner-=C[mu][mu];
  
  static momentum<double> pa2, pb2;
  pa2=-2.0*pa; pb2=-2.0*pb;
  pa2[0]=-pa2[0]; pb2[0]=-pb2[0];               // raise index
  REP(mu, 4) {
    res.J[mu]+=(pb[mu]-pa[mu])*inner;
    REP(nu, 4) res.J[mu]+=C[mu][nu]*pa2[nu]-pb2[nu]*C[nu][mu];
  }
  
  // J_mu += [J1^nu, W2_mu nu]+[J2^nu, W1_mu nu]
  // W_mu nu= [J1_mu, J2_nu] + [J2_mu, J1_nu]
  static const int mu_ind[]={0,0,0,1,1,2};
  static const int nu_ind[]={1,2,3,2,3,3};
  REP(i, 6) {
    int mu=mu_ind[i], nu=nu_ind[i];
    res.J[mu]-=com(a.J[nu], b.W[i])+com(b.J[nu], a.W[i]);               // nu>0 so extra minus from the Minkowski metric
    // extra minus because of anti-sym W
    if(mu) res.J[nu]+=com(a.J[mu], b.W[i])+com(b.J[mu], a.W[i]);        // mu>0 so extra minus from the Minkowski metric
    else res.J[nu]-=com(a.J[mu], b.W[i])+com(b.J[mu], a.W[i]);
    res.W[i]+=C[mu][nu]-C[nu][mu];
  }

  return res;
}

typedef vect<4, double> spinor ;

template<const int DIM>
struct fermion_rep {
  vect<DIM,  spinor> psi;
};

template<const int DIM>
const fermion_rep<DIM> &operator+=(fermion_rep<DIM> &a, const fermion_rep<DIM> &b) {
  a.psi+=b.psi;
  return a;
}

template<const int DIM>
const fermion_rep<DIM> &operator-=(fermion_rep<DIM> &a, const fermion_rep<DIM> &b) {
  a.psi-=b.psi;
  return a;
}

// lambda matrices (dirac matrices gamma=i lambda) in the Majoranna rep
// they are real and 
// for each row give the column (not if minus 1)
const int lambda_row[5][4]={{~3,2,~1,0},{0,~1,2,~3},{3,~2,~1,0},{~1,~0,~3,~2},{~1,0,3,~2}};
// for each column give the row
const int lambda_col[5][4]={{3,~2,1,~0},{0,~1,2,~3},{3,~2,~1,0},{~1,~0,~3,~2},{~1,0,3,~2}};

// return lambda^mu * psi
inline
spinor upper_mu_spinor(int mu, spinor psi) {
  spinor res;
  REP(i, 4) {
    int j=lambda_row[mu][i];
    if(j<0) res[i]=-psi[~j];
    else res[i]=psi[j];
  }
  return res;
}

// return lambda_mu * psi
inline
spinor lower_mu_spinor(int mu, spinor psi) {
  spinor res;
  REP(i, 4) {
    int j=lambda_row[mu][i];
    if(j<0) res[~j]=psi[i];
    else res[j]=-psi[i];
  }
  return res;
}

// return psibar * lambda^mu
inline
spinor upper_mu_spinorbar(int mu, spinor psibar) {
  spinor res;
  REP(i, 4) {
    int j=lambda_row[mu][i];
    if(j<0) res[~j]=-psibar[i];
    else res[j]=psibar[i];
  }
  return res;
}

// return psibar * lambda_mu
inline
spinor lower_mu_spinorbar(int mu, spinor psibar) {
  spinor res;
  REP(i, 4) {
    int j=lambda_row[mu][i];
    if(j<0) res[i]=psibar[~j];
    else res[i]=-psibar[j];
  }
  return res;
}


template<const int DIM>
inline
gauge_amp<DIM> gauge_rep_repb(const fermion_rep<DIM> &a, const fermion_rep<DIM> &b) {
  gauge_amp<DIM> res;
  null(res.J); null(res.W);
  REP(mu, 4) {
    REP(m, DIM) REP(n, DIM) res.J[mu][n][m]+=b.psi[m]*lower_mu_spinor(mu, a.psi[n]);
    
    res.J[mu]*=0.5;                             // T(F)=1/2
    // SU(DIM) instead
    double t=trace(res.J[mu])/DIM;
    REP(m, DIM) res.J[mu][m][m]-=t;               // make sure J is traceless
  }
  return res;
}

template<const int DIM>
inline
fermion_rep<DIM> rep_rep_gauge(const fermion_rep<DIM> &a, const gauge_amp<DIM> &b) {
  static momentum<vect<DIM, vect<4, double> > > tmp;
  fermion_rep<DIM> res;
  null(tmp); null(res.psi);
  REP(mu, 4) REP(n, DIM) tmp[mu][n]=upper_mu_spinor(mu, a.psi[n]);
  REP(mu, 4) REP(o, DIM) REP(n, DIM) res.psi[o]+=b.J[mu][o][n]*tmp[mu][n];
  return res;
}

template<const int DIM>
inline
fermion_rep<DIM> repb_repb_gauge(const fermion_rep<DIM> &a, const gauge_amp<DIM> &b) {
  static momentum<vect<DIM, vect<4, double> > > tmp;
  fermion_rep<DIM> res;
  null(tmp); null(res.psi);
  REP(mu, 4) REP(m, DIM) tmp[mu][m]=upper_mu_spinorbar(mu, a.psi[m]);
  REP(mu, 4) REP(o, DIM) REP(m, DIM) res.psi[o]+=tmp[mu][m]*b.J[mu][m][o];
  return res;
}

template<const int DIM>
inline
void rep_propagator(fermion_rep<DIM> &res, const momentum<double> &p) {
  // res.psi=(lambda^mu p_mu)res.psi /p^2
  static vect<4, double> psi;
  double p2=p*p;
  REP(m, DIM) {
    null(psi);
    REP(mu, 4) psi+=p[mu]*upper_mu_spinor(mu, res.psi[m]);
    res.psi[m]=psi/p2;
  }
}

template<const int DIM>
inline
void repb_propagator(fermion_rep<DIM> &res, const momentum<double> &p) {
  // res.psi=res.psi (-lambda^mu p_mu)/p^2
  static vect<4, double> psi;
  double p2=p*p;
  REP(m, DIM) {
    null(psi);
    
    REP(mu, 4) {
      psi-=p[mu]*upper_mu_spinorbar(mu, res.psi[m]);
    }
    res.psi[m]=psi/p2;
  }
}

/////////////////////////////////// our gluon amplitude //////////////////

struct boson_amp {
  gauge_amp<3> gluon;
};

const boson_amp &operator+=(boson_amp &a, const boson_amp &b) {
  a.gluon+=b.gluon;
  return a;
}

const boson_amp &operator-=(boson_amp &a, const boson_amp &b) {
  a.gluon-=b.gluon;
  return a;
}

inline
boson_amp boson_bb(const boson_amp &a, const momentum<double> &pa, const boson_amp &b, const momentum<double> &pb, bool &valid) {
  valid=true;
  boson_amp res;
  res.gluon=gauge_gauge_gauge(a.gluon , pa, b.gluon, pb);
  return res;
}


struct fermion_amp {
  int type;             // 0 -> invalid, >0 -> psi, <0 -> psibar antiquark negative of the quark type
  fermion_rep<3> quark;
};

const fermion_amp &operator+=(fermion_amp &a, const fermion_amp &b) {
/*  if(a.type!=b.type) {
    printf("Adding different amplitudes\n");
    exit(1);
  }*/
  a.type=b.type;
  a.quark+=b.quark;
  return a;
}

const fermion_amp &operator-=(fermion_amp &a, const fermion_amp &b) {
/*  if(a.type!=b.type) {
    printf("Adding different amplitudes\n");
    exit(1);
  }*/
  a.type=b.type;
  a.quark-=b.quark;
  return a;
}

inline
fermion_amp fermion_fb(const fermion_amp &a, const boson_amp &b, bool &valid) {
  fermion_amp res;
  valid=true;
  res.type=a.type;
  if(res.type<0) res.quark=repb_repb_gauge(a.quark, b.gluon);
  else res.quark=rep_rep_gauge(a.quark, b.gluon);
  return res;
}

inline
boson_amp boson_ff(const fermion_amp &a, const fermion_amp &b, bool &valid) {
  boson_amp res;
  if(a.type!=-b.type) {
    valid=false;
    return res; // particles are incompatible
  }
  valid=true;
  if(a.type<0) {
    gauge_amp<3> tmp=gauge_rep_repb(b.quark, a.quark);           // a is psibar current
    res.gluon.J=-1.0*tmp.J;
    res.gluon.W=tmp.W;
  }
  else res.gluon=gauge_rep_repb(a.quark, b.quark);
  return res;
}

inline
void fermion_propagator(fermion_amp &res, const momentum<double> &p) {
  if(!res.type) return;
  if(res.type<0) repb_propagator(res.quark, p);      // psibar propagator
  else rep_propagator(res.quark, p);
}

inline
void boson_propagator(boson_amp &res, const momentum<double> &p) {
  double p2=p*p;
  res.gluon.J/=p2;  
}

#endif
