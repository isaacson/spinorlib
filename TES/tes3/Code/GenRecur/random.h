#ifndef __RANDOM_H__
#define __RANDOM_H__

double uniform();
inline
double uniform(double min,double max) {return min+(max-min)*uniform();}

inline
int random_integer(int m) {
  return rand()%m;
}

#endif
