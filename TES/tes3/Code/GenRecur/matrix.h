#ifndef __MATRIX_H__
#define __MATRIX_H__

#include "gerben.h"

inline
void null(double &a) {a=0.0;}
inline
void null(float &a) {a=0.0;}
inline
void null(int &a) {a=0;}

inline
void display(double &a) {printf("%lf", a);}
inline
void display(float &a) {printf("%f", a);}
inline
void display(int &a) {printf("%d", a);}

///////////////////////////////// vector /////////////////////////////////

template<const int VECD, typename T>
struct vect {
  T elem[VECD];
  
  inline
  const T &operator[](int i) const {
    return elem[i];
  }

  inline
  T &operator[](int i) {
    return elem[i];
  }

/*  matrix() {
    REP(i, 3) REP(j, 3) elem[i][j]=0.0;
  }*/
  
};

template<const int VECD, typename T>
inline
void null(vect<VECD, T> &a) {
  REP(i, VECD) null(a.elem[i]);
}

template<const int VECD, typename T>
inline
void display(vect<VECD, T> &a) {
  REP(i, VECD) {display(a.elem[i]); printf("\n");}
}


template<const int VECD, typename T>
inline
vect<VECD, T> operator+(const vect<VECD, T> &a, const vect<VECD, T> &b) {
  vect<VECD, T> res;
  REP(i, VECD) res[i]=a[i]+b[i];
  return res;
}

template<const int VECD, typename T>
inline
vect<VECD, T> operator-(const vect<VECD, T> &a, const vect<VECD, T> &b) {
  vect<VECD, T> res;
  REP(i, VECD) res[i]=a[i]-b[i];
  return res;
}

template<const int VECD, typename T>
inline
vect<VECD, T> &operator+=(vect<VECD, T> &a, const vect<VECD, T> &b) {
  REP(i, VECD) a[i]+=b[i];
  return a;
}

template<const int VECD, typename T>
inline
vect<VECD, T> &operator-=(vect<VECD, T> &a, const vect<VECD, T> &b) {
  REP(i, VECD) a[i]-=b[i];
  return a;
}

template<const int VECD, typename T>
inline
vect<VECD, T> &operator*=(vect<VECD, T> &a, const T &b) {
  REP(i, VECD) a[i]*=b;
  return a;
}

template<const int VECD, typename T>
inline
vect<VECD, T> &operator/=(vect<VECD, T> &a, const T &b) {
  REP(i, VECD) a[i]/=b;
  return a;
}

template<const int VECD, typename T>
inline
vect<VECD, T> operator*(const vect<VECD, T> &a, const T &b) {
  vect<VECD, T> res;
  REP(i, VECD) res[i]=a[i]*b;
  return res;
}

template<const int VECD, typename T>
inline
vect<VECD, T> operator*(const T &b, const vect<VECD, T> &a) {
  vect<VECD, T> res;
  REP(i, VECD) res[i]=b*a[i];
  return res;
}

template<const int VECD, typename T>
inline
vect<VECD, T> operator/(const vect<VECD, T> &a, const T &b) {
  vect<VECD, T> res;
  REP(i, VECD) res[i]=a[i]/b;
  return res;
}

template<const int VECD, typename T>
inline
T operator*(const vect<VECD, T> &a, const vect<VECD, T> &b) {
  T res;
  null(res);
  REP(i, VECD) res+=a[i]*b[i];
  return res;
}

//////////////////////////////////// matrix operations ////////////////////////////

template<const int MATD, typename T>
struct matrix {
  T elem[MATD][MATD];
  
  inline
  const T *operator[](int i) const {
    return elem[i];
  }

  inline
  T *operator[](int i) {
    return elem[i];
  }
};

template<const int MATD, typename T>
inline
void null(matrix<MATD, T> &a) {
  REP(i, MATD) REP(j, MATD) null(a.elem[i][j]);
}

template<const int MATD, typename T>
inline
void display(matrix<MATD, T> &a) {
  REP(i, MATD) {
    REP(j, MATD) {display(a.elem[i][j]); printf(" ");}
    printf("\n");
  }
}


template<const int MATD, typename T>
inline
matrix<MATD, T> operator+(const matrix<MATD, T> &a, const matrix<MATD, T> &b) {
  matrix<MATD, T> res;
  REP(i, MATD) REP(j, MATD) res[i][j]=a[i][j]+b[i][j];
  return res;
}

template<const int MATD, typename T>
inline
matrix<MATD, T> operator-(const matrix<MATD, T> &a, const matrix<MATD, T> &b) {
  matrix<MATD, T> res;
  REP(i, MATD) REP(j, MATD) res[i][j]=a[i][j]-b[i][j];
  return res;
}

template<const int MATD, typename T>
inline
matrix<MATD, T> &operator+=(matrix<MATD, T> &a, const matrix<MATD, T> &b) {
  REP(i, MATD) REP(j, MATD) a[i][j]+=b[i][j];
  return a;
}

template<const int MATD, typename T>
inline
matrix<MATD, T> &operator-=(matrix<MATD, T> &a, const matrix<MATD, T> &b) {
  REP(i, MATD) REP(j, MATD) a[i][j]-=b[i][j];
  return a;
}

template<const int MATD, typename T>
inline
matrix<MATD, T> &operator*=(matrix<MATD, T> &a, const T &b) {
  REP(i, MATD) REP(j, MATD) a[i][j]*=b;
  return a;
}

template<const int MATD, typename T>
inline
matrix<MATD, T> &operator/=(matrix<MATD, T> &a, const T &b) {
  REP(i, MATD) REP(j, MATD) a[i][j]/=b;
  return a;
}


template<const int MATD, typename T>
inline
matrix<MATD, T> operator*(const matrix<MATD, T> &a, const matrix<MATD, T> &b) {
  matrix<MATD, T> res;
  null(res);
  REP(i, MATD) REP(j, MATD) {
    REP(k, MATD) res[i][j]+=a[i][k]*b[k][j];
  }
  return res;
}

template<const int MATD, typename T>
inline
matrix<MATD, T> operator*(const matrix<MATD, T> &a, const T &b) {
  matrix<MATD, T> res;
  REP(i, MATD) REP(j, MATD) res[i][j]=a[i][j]*b;
  return res;
}

template<const int MATD, typename T>
inline
matrix<MATD, T> operator*(const T &b, const matrix<MATD, T> &a) {
  matrix<MATD, T> res;
  REP(i, MATD) REP(j, MATD) res[i][j]=b*a[i][j];
  return res;
}

template<const int MATD, typename T>
inline
matrix<MATD, T> operator/(const matrix<MATD, T> &a, const T &b) {
  matrix<MATD, T> res;
  REP(i, MATD) REP(j, MATD) res[i][j]=a[i][j]/b;
  return res;
}

template<const int MATD, typename T>
inline
T trace(const matrix<MATD, T> &a) {
  T res=0.0;
  REP(i, MATD) res+=a[i][i];
  return res;
}

template<const int MATD, typename T>
inline
vect<MATD, T> operator*(const matrix<MATD, T> &a, const vect<MATD, T> &b) {
  vect<MATD, T> res;
  null(res);
  REP(i, MATD) REP(j, MATD) res[i]+=a[i][j]*b[j];
  return res;
}

template<const int MATD, typename T>
inline
vect<MATD, T> operator*(const vect<MATD, T> &b, const matrix<MATD, T> &a) {
  vect<MATD, T> res;
  null(res);
  REP(i, MATD) REP(j, MATD) res[i]+=b[j]*a[j][i];
  return res;
}

template<const int MATD, typename T>
inline
matrix<MATD, T> com(const matrix<MATD, T> &a, const matrix<MATD, T> &b) {return a*b-b*a;}


#endif
