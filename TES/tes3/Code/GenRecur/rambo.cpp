#include "gerben.h"
#include "random.h"
#include "momentum.h"

const double Elab=1.0, xmin=0.1;

void ramboo(double Ec,int N, momentum<double> *K)
{
  for (int i=2;i<N;i++)
    { double Eb=-(K[0][0]+K[1][0])/Ec;
      double pz=-(K[0][1]+K[1][1])/Ec;
      double f=K[i][0];
      K[i][0]=(Eb*f+pz*K[i][1]);
      f+=K[i][0];f*=pz/(1.0+Eb);
      K[i][1]+=f;
    }
}

double rambo(const int N, momentum<double> *P) {
  double x1=uniform(xmin,1.0);
  double x2=uniform(xmin,1.0);
  //x1=0.5;x2=x1;
  P[0]=(float)(-Elab*x1)*momentum_<double>(0.5, 0.5, 0.0, 0.0);
  P[1]=(float)(-Elab*x2)*momentum_<double>(0.5,-0.5, 0.0, 0.0);
  double Ecm=Elab*sqrt(x1*x2);
  momentum<double> R=momentum_<double>(0.0, 0.0, 0.0, 0.0);
  for (int i=2;i<N;i++) {
    double c=uniform(-1.0,1.0);
    double s=sqrt(1.0-c*c);
    double f=2.0*M_PI*uniform(0.0,1.0);

    P[i]=(-log(uniform(0.0,1.0)*uniform(0.0,1.0)))*momentum_<double>(1.0,c,s*cos(f),s*sin(f));
    R=R+P[i];
  }
  double Rmass=sqrt(R*R);
  R=-R/(double)Rmass;
  double a=1.0/(1.0-R[0]);
  double x=Ecm/Rmass;
  for (int i=2;i<N;i++) {
    double bq=R[1]*P[i][1]+R[2]*P[i][2]+R[3]*P[i][3];
    double xq=P[i][0]+a*bq;
    P[i]=(double)x*momentum_<double>(-R[0]*P[i][0]+bq,P[i][1]+R[1]*xq,P[i][2]+R[2]*xq,P[i][3]+R[3]*xq);
  }
  double wgt=1.0;               // No pdf's at the moment

  ramboo(Ecm,N,P);
  return wgt;
};
