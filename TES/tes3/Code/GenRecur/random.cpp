#include "gerben.h"

#include "random.h"

double uniform() {
  double random;
  int seed1=1429,seed2=9343;
  static bool init=false;
  static int iranmr=96, jranmr=32;
  static double ranc=362436.0/16777216.0, rancd=7654321.0/16777216.0, rancm=16777213.0/16777216.0, ranu[97];
  if(init==false) {
    int i=(seed1/177)%177+2, j=(seed1%177)+2,k= (seed2/169)%178+1, l=seed2%169;
    init=true;
    for(int il=0;il<97;++il) {
      double s=0, t=0.5;
      for(int jl=0;jl<24;++jl) {
	int m=(((i*j%179)*k)%179);
        i=j; j=k; k=m; l=(53*l+1)%169;
	if (((l*m)%64)>=32) s+=t;
	t*=0.5;
      }
      ranu[il]=s;
    }
  }
  random=ranu[iranmr]-ranu[jranmr];
  if (random<0.0) ++random;
  ranu[iranmr--]=random;
  jranmr--;
  if (iranmr<0) iranmr=96;
  if (jranmr<0) jranmr=96;
  ranc-=rancd;
  if (ranc<0.0) ranc+=rancm;
  random-=ranc;
  if (random<0.0) random++;
  return random;  
}
