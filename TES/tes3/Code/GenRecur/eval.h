#include "gerben.h"

#include "momentum.h"

template<typename Fermion, typename Boson>
union Amplitude {
  Fermion fermion;
  Boson boson;
};

#define STD_TEMPLATE typename T, typename Fermion, typename Boson
#define AMPLITUDE Amplitude<Fermion, Boson>
#define MOM momentum<T>

template<STD_TEMPLATE, Fermion vertex(const Fermion &a, const Boson &b, bool &)>
inline
bool f_fb_vertex_wrapper(AMPLITUDE &res, const AMPLITUDE &a, const MOM &pa, const AMPLITUDE &b, const MOM &pb, bool sign) {
  bool valid;
  Fermion tmp=vertex(a.fermion, b.boson, valid);

  if(valid) {
    if(sign) res.fermion-=tmp;
    else res.fermion+=tmp;
    return true;
  }
  return false;
}

template<STD_TEMPLATE, Fermion vertex(const Fermion &a, const Boson &b, bool &)>
inline
bool f_bf_vertex_wrapper(AMPLITUDE &res, const AMPLITUDE &a, const MOM &pa, const AMPLITUDE &b, const MOM &pb, bool sign) {
  bool valid;
  Fermion tmp=vertex(b.fermion, a.boson, valid);
  
  if(valid) {
    if(sign) res.fermion-=tmp;
    else res.fermion+=tmp;
    return true;
  }
  return false;
}

template<STD_TEMPLATE, Boson vertex(const Fermion &a, const Fermion &b, bool &)>
inline
bool b_ff_vertex_wrapper(AMPLITUDE &res, const AMPLITUDE &a, const MOM &pa, const AMPLITUDE &b, const MOM &pb, bool sign) {
  bool valid;
  Boson tmp=vertex(a.fermion, b.fermion, valid);
  
  if(valid) {
    if(sign) res.boson-=tmp;
    else res.boson+=tmp;
    return true;
  }
  return false;
}

template<STD_TEMPLATE, Boson vertex(const Boson &a, const MOM &, const Boson &b, const MOM &, bool &)>
inline
bool b_bb_vertex_wrapper(AMPLITUDE &res, const AMPLITUDE &a, const MOM &pa, const AMPLITUDE &b, const MOM &pb, bool sign) {
  bool valid;
  Boson tmp=vertex(a.boson, pa, b.boson, pb, valid);
  
  if(valid) {
    if(sign) res.boson-=tmp;
    else res.boson+=tmp;
    return true;
  }
  return false;
}


template<STD_TEMPLATE, Fermion f_fb(const Fermion &a, const Boson &b, bool &valid),
Boson b_ff(const Fermion &a, const Fermion &b, bool &),Boson b_bb(const Boson &a, const MOM &pa, const Boson &b, const MOM &pb, bool &valid),
void f_prop(Fermion &, const MOM &), void b_prop(Boson &, const MOM &)>
class Eval {
  int MAX_N;
  AMPLITUDE *amplitudes;
  bool  *valid;
  MOM *momenta;
  int fermion_mask;

  inline
  bool is_fermion(int s) {return __builtin_parity(s&fermion_mask);}

  inline
  int count(int s) {return __builtin_popcount(s);}

  inline
  int first_elem(int s) {return __builtin_ctz(s);}

  inline
  bool calc_sign(int left, int right) {
    left&=fermion_mask; right&=fermion_mask;
    int mask=left, res=0;
    while(mask) {
      res+=count((mask-1)&right);  // (mask-1)&right contains the bits of right which are to the left of
      // the first 1 in left
      mask=mask&(mask-1);          // set the first 1 to 0 in mask
    }
    return res&1;
  }

  
  void free() {
    MAX_N=0;
    if(amplitudes) {delete amplitudes; amplitudes=NULL;}
    if(valid) {delete valid; valid=NULL;}
    if(momenta) {delete momenta; momenta=NULL;}
  }
  
  void init(int N) {
    if(N>MAX_N) {
      free();
      MAX_N=N;
      amplitudes=new AMPLITUDE[1<<N];
      valid=new bool[1<<N];
      momenta=new MOM[1<<N];
    }
  }

  void init_momenta(const vector<MOM> &mom) {
    null(momenta[0]);
    int N=SZ(mom)-1;
    FOR(mask, 1, 1<<N) {
      int rest=mask&(mask-1);
      int first=first_elem(mask);
      momenta[mask]=mom[first]+momenta[rest];
    }
  }
  
  template<bool vertex1(AMPLITUDE &, const AMPLITUDE &, const MOM &, const AMPLITUDE &, const MOM &, bool), bool vertex2(AMPLITUDE &, const AMPLITUDE &, const MOM &, const AMPLITUDE &, const MOM &, bool)>
  inline
  void calc_amplitude(int s) {
    // we partition the set into 2 subsets but we don't double count
    // so the first particle always goes into the left subset
    int mask=s&(s-1);
    // with a bit of bit magic we iterate over all the subsets of s that doesn't contain the first elem
    for(int right=((~mask)+1)&mask; right!=0; right=((right|(~mask))+1)&mask) {
      int left=s^right;
      
      // calc the sign of the split
      bool sign=calc_sign(left, right);
      
//      bool tmp_val=false;
      
      if(valid[left] && valid[right]) {
        if(is_fermion(left)) {
          valid[s]|=vertex1(amplitudes[s], amplitudes[left], momenta[left], amplitudes[right], momenta[right], sign);
        }
        else {
          valid[s]|=vertex2(amplitudes[s], amplitudes[left], momenta[left], amplitudes[right], momenta[right], sign);
        }
      }
//      printf("s=%X left=%X right=%X sign=%d valid=%d res_valid=%d\n", s, left, right, sign, pamp[left].valid && pamp[right].valid, tmp_val);
//      if(is_fermion(s)) printf("fermion s=%X type%d\n", s, pamp[s].amp.fermion.type);
    }
    if(!valid[s]) printf("s=%X is not valid\n", s);
  }
  
public:
  Eval() {MAX_N=0; amplitudes=NULL; valid=NULL; momenta=NULL;}

  ~Eval() {free();}

  T recurse(const vector<Fermion> &fermions, const vector<Boson> &bosons, const vector<MOM > &mom) {
    int NF=SZ(fermions), NB=SZ(bosons);
    fermion_mask=(1<<NF)-1;
    int N=NF+NB-1;
    if(N!=SZ(mom)-1) {
      printf("Wtf! N=%d SZ(mom)=%d\n", N, SZ(mom));
      return 0;
    }
    init(N);
    memset(amplitudes, 0, sizeof(AMPLITUDE)<<N);
    memset(valid, 0, sizeof(bool)<<N);
    init_momenta(mom);
    FOR(s, 1, 1<<N) { // s runs over all subsets of external particles as a bitmask
      if(count(s)==1) {
        // this a partial amplitude of 1 external state so we just need to point it to the states
        int particle=first_elem(s);
        if(is_fermion(s)) amplitudes[s].fermion=fermions[particle];
        else amplitudes[s].boson=bosons[particle-NF];
        valid[s]=true;
        continue;
      }
      
      if(is_fermion(s)) {
        // this partial amplitude is fermionic
        // f_fb_vertex_wrapper<T, Fermion, Boson, f_fb>(amplitudes[s], amplitudes[s], momenta[s], amplitudes[s], momenta[s], false);
        calc_amplitude<f_fb_vertex_wrapper<T, Fermion, Boson, f_fb>, f_bf_vertex_wrapper<T, Fermion, Boson, f_fb> >(s);  // fermion_pamp(s);
        if(s!=(1<<N)-1) f_prop(amplitudes[s].fermion, momenta[s]);
        #if 0
        int t=0;
        REP(i, 32) if((s&fermion_mask)&(1<<i)) {if(i&1) t--; else t++;}
        if(!(t&1)) printf("%X is bosonic! %d\n", s, t);
        if((t!=-1 && t!=1)  && pamp[s].valid) printf("fermion %X is valid while invalid %d\n!", s, t);
        if(!pamp[s].valid) {continue;}
        printf("fermion mask=0%0Xh\n", s);
        display(pamp[s].fermion.quark.psi);
        printf("end fermion\n");
        #endif
      }
      else {
        // this partial amplitude is bosonic
        calc_amplitude<b_ff_vertex_wrapper<T, Fermion, Boson, b_ff>, b_bb_vertex_wrapper<T, Fermion, Boson, b_bb> >(s);  // boson_pamp(s);
        if(s!=(1<<N)-1) b_prop(amplitudes[s].boson, momenta[s]);
        #if 0
        int t=0;
        REP(i, 32) if((s&fermion_mask)&(1<<i)) {if(i&1) t--; else t++;}
        if(t&1) printf("%X is fermionic!\n", s);
        if(t && pamp[s].valid) printf("boson %X is valid while invalid\n!", s);
        if(!pamp[s].valid) {continue;}
        #elif 0
        printf("boson mask=0%0Xh\n", s);
        display(pamp[s].boson.gluon.J);
        printf("end boson\n");
        #endif
        // check gauge invariance
        matrix<3, double> x;
        x=momenta[s][0]*amplitudes[s].boson.gluon.J[0];
        FOR(mu,1,4) x-=momenta[s][mu]*amplitudes[s].boson.gluon.J[mu];
        double mx=1.0;
        REP(mu, 4) REP(i, 3) REP(j, 3) mx=max(abs(amplitudes[s].boson.gluon.J[mu][i][j]),mx);
        REP(i, 3) REP(j, 3) if(abs(x[i][j])/mx>1e-6) {
          printf("0%0Xh\n", s);
          REP(i, 3) {REP(j, 3) cout << x[i][j] << " "; cout << endl;}
          cout << endl;
          printf("WTF!!!!!!!\n");
          display(amplitudes[s].boson.gluon.J);
          goto endlabel;
        }
        endlabel:;
        // #endif
      }
    }
    return 0;
  }
};
