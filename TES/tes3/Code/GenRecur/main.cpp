#include "gerben.h"

#include "matrix.h"
//#include "eval_tree.h"
#include "eval.h"
#include "vertex_tools.h"
#include "random.h"
#include "rambo.h"

//////////////////// TEST ///////////////////////


momentum<double> orthogonal(const momentum<double> &a,const momentum<double> &b) {
  momentum<double> c=momentum_<double>(0.0,a[2]*b[3]-a[3]*b[2],a[3]*b[1]-a[1]*b[3],a[1]*b[2]-a[2]*b[1]);
  return c/sqrt(-(c*c));
}

vector<matrix<3, double> > su3gen;

void setup_su3() {
  su3gen.clear();
    int rows[3]={0,0,1}, cols[3]={1,2,2};
  matrix<3, double> tmp; 
  REP(i, 3) {
    null(tmp);
    tmp[rows[i]][cols[i]]=tmp[cols[i]][rows[i]]=0.5;
    su3gen.PB(tmp);
  }
  REP(i, 3) {
    null(tmp);
    tmp[cols[i]][rows[i]]=0.5;
    tmp[rows[i]][cols[i]]=-0.5;
    su3gen.PB(tmp);
  }
  null(tmp);
  tmp[0][0]=0.5;
  tmp[1][1]=-0.5;
  su3gen.PB(tmp);
  
  tmp[0][0]=0.5/sqrt(3);
  tmp[1][1]=0.5/sqrt(3);
  tmp[2][2]=-1.0/sqrt(3);
  su3gen.PB(tmp);
  
  // unfortunately this is not the standard order of the Gell-Mann matrices so we have to swap
  swap(su3gen[1], su3gen[3]);
  swap(su3gen[2], su3gen[6]);
  swap(su3gen[5], su3gen[6]);
}


int main(int argc, char **argv) {
  
  setup_su3();
  
  Eval<double, fermion_amp, boson_amp,
       fermion_fb, boson_ff, boson_bb, fermion_propagator, boson_propagator> eval;
       
  int N=6;
  int fermion_mask=0xF;         // 4 fermions
  cout<<"***"<<fermion_mask<<endl;
  vector<momentum<double> > p(N);
  rambo(N, &p[0]);
  momentum<double> sum;
  sum=0.0*sum;
  REP(i,N) 
    { cout<<i<<" "<<p[i]<<endl;
      sum+=p[i];
    }
  cout<<"Sum: "<<sum<<endl;

  vector<fermion_amp> extfermions;
  vector<boson_amp> extbosons;
  REP(i, N) {
    if(fermion_mask&(1<<i)) {
      fermion_amp fermion;
      if(i&1) fermion.type=-1;
      else fermion.type=1;
      null(fermion.quark.psi);
      int color=random_integer(3);
      vect<4,double> polar;
      if(fermion.type<0) {
        // anti-particle
        if(random_integer(2)) {
          polar[0]=-p[i][1];
          polar[1]=p[i][3];
          polar[2]=0;
          polar[3]=p[i][0]-p[i][2];
        }
        else {
          polar[0]=-p[i][3];
          polar[1]=-p[i][1];
          polar[2]=p[i][0]-p[i][2];
          polar[3]=0;
        }
      }
      else {
        // particle
        if(random_integer(2)) {
          polar[0]=p[i][1];
          polar[1]=-p[i][3];
          polar[2]=0;
          polar[3]=p[i][0]+p[i][2];
        }
        else {
          polar[0]=p[i][3];
          polar[1]=p[i][1];
          polar[2]=p[i][0]+p[i][2];
          polar[3]=0;
        }
      }
      matrix<4, double> ps; 
      ps[0][0]=p[i][1];        ps[1][0]=-p[i][3];        ps[2][0]=0.0;            ps[3][0]=-p[i][0]+p[i][2];
      ps[0][1]=-p[i][3];       ps[1][1]=-p[i][1];        ps[2][1]=p[i][0]-p[i][2];ps[3][1]=0.0;
      ps[0][2]=0.0;            ps[1][2]=-p[i][0]-p[i][2];ps[2][2]=p[i][1];        ps[3][2]=-p[i][3];
      ps[0][3]=p[i][0]+p[i][2];ps[1][3]=0.0;             ps[2][3]=-p[i][3];       ps[3][3]=-p[i][1];
      if(fermion.type<0) cout<<"anti-fermion "<<i<<": ";
      else cout<<"fermion "<<i<<": ";
      vect<4,double> x=polar*ps;
      cout<<x[0]<<" "<<x[1]<<" "<<x[2]<<" "<<x[3];
      cout<<"   ("<<polar[0]<<" "<<polar[1]<<" "<<polar[2]<<" "<<polar[3]<<") "<<endl;
      fermion.quark.psi[color]=polar;
      extfermions.PB(fermion);
    }
    else {      // boson ergo gluon
      momentum<double> b = momentum_<double>(0.0,uniform(-1.0,1.0),uniform(-1.0,1.0),uniform(-1.0,1.0));
      boson_amp boson;
      int color=(int) (uniform()*8.0);
      momentum<double> pol=orthogonal(p[i],b);
      cout<<"boson "<<i<<"  "<<pol*p[i]<<" "<<pol<<endl;
      REP(i, 6) null(boson.gluon.W[i]);
      REP(mu, 4) boson.gluon.J[mu]=pol[mu]*su3gen[color];
      extbosons.PB(boson);
    }
  }
  
  double res=eval.recurse(extfermions, extbosons, p);
  
  return 0;
}

