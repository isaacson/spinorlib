template<typename T>
struct momentum {
  T p[4];
  momentum<T> &operator+=(const momentum<T> &);
  momentum<T> &operator=(const volatile momentum<T> &);
};

template<typename T>__device__ __host__ momentum<T> momentum_(T t, T x, T y, T z);
template<typename T>__device__ __host__ momentum<T> operator+(const momentum<T> P1, const momentum<T> P2);
template<typename T>__device__ __host__ momentum<T> operator-(const momentum<T> P1, const momentum<T> P2);
template<typename T>__device__ __host__ momentum<T> operator-(const momentum<T> P1);
template<typename T>__device__ __host__ T operator*(momentum<T> P1, momentum<T> P2);
template<typename T>__device__ __host__ momentum<T> operator*(const T x, const momentum<T> P1);
template<typename T>__device__ __host__ momentum<T> operator*(const momentum<T> P1, const T x);
template<typename T>__device__ __host__ momentum<T> operator/(const momentum<T> P1,const T x);
