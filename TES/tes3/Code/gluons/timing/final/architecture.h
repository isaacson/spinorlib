// These 3 GPU properties should be read-out from device.
//

#define NUMBER_MP 14
#define BLOCKS_PER_MP 2
#define SHARED_MEMORY 24576
#define REGISTER_MEMORY 32768
//
//
// Set the maximum number of registers ever used by program
//
#define MAX_REGISTER 53

