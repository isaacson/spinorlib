#ifndef __GERBEN_CUDA_MOMENTUM__
#define __GERBEN_CUDA_MOMENTUM__

template<typename T>
struct momentum { T p[4];};

template<typename T>
__device__ __host__ momentum<T> momentum_(T t, T x, T y, T z) {
  momentum<T> P;
  P.p[0]=t; P.p[1]=x; P.p[2]=y; P.p[3]=z;
  return P;
}

template<typename T>
__device__ __host__ momentum<T> operator+(const momentum<T> P1, const momentum<T> P2) {
  momentum<T> P;
  P.p[0]=P1.p[0]+P2.p[0];
  P.p[1]=P1.p[1]+P2.p[1];
  P.p[2]=P1.p[2]+P2.p[2];
  P.p[3]=P1.p[3]+P2.p[3];
  return P;
}

template<typename T>
__device__ __host__ momentum<T> &operator+=(momentum<T> &P, const momentum<T> P2) {
  P.p[0]+=P2.p[0];
  P.p[1]+=P2.p[1];
  P.p[2]+=P2.p[2];
  P.p[3]+=P2.p[3];
  return P;
}

template<typename T>
__device__ __host__ momentum<T> operator-(const momentum<T> P1, const momentum<T> P2) {
  momentum<T> P;
  P.p[0]=P1.p[0]-P2.p[0];
  P.p[1]=P1.p[1]-P2.p[1];
  P.p[2]=P1.p[2]-P2.p[2];
  P.p[3]=P1.p[3]-P2.p[3];
  return P;
}

template<typename T>
__device__ __host__ momentum<T> operator-(const momentum<T> P1) {
  momentum<T> P;
  P.p[0]=-P1.p[0];
  P.p[1]=-P1.p[1];
  P.p[2]=-P1.p[2];
  P.p[3]=-P1.p[3];
  return P;
}

template<typename T>
__device__ __host__ T operator*(momentum<T> P1, momentum<T> P2) {
   return P1.p[0]*P2.p[0]-P1.p[1]*P2.p[1]-P1.p[2]*P2.p[2]-P1.p[3]*P2.p[3];
}

template<typename T>
__device__ __host__ momentum<T> operator*(const T x, const momentum<T> P1) {
   momentum<T> P;
   P.p[0]=x*P1.p[0];
   P.p[1]=x*P1.p[1];
   P.p[2]=x*P1.p[2];
   P.p[3]=x*P1.p[3];
   return P;
}

template<typename T>
__device__ __host__ momentum<T> operator*(const momentum<T> P1, const T x) {
   momentum<T> P;
   P.p[0]=x*P1.p[0];
   P.p[1]=x*P1.p[1];
   P.p[2]=x*P1.p[2];
   P.p[3]=x*P1.p[3];
   return P;
}

template<typename T>
__device__ momentum<T> operator/(const momentum<T> P1,const T x) {
   momentum<T> P;
   P.p[0]=__fdividef(P1.p[0],x);
   P.p[1]=__fdividef(P1.p[1],x);
   P.p[2]=__fdividef(P1.p[2],x);
   P.p[3]=__fdividef(P1.p[3],x);
   return P;
}

template<typename T>
__device__ momentum<T> &operator/=(momentum<T> &P, const T x) {
  P.p[0]=__fdividef(P.p[0],x);
  P.p[1]=__fdividef(P.p[1],x);
  P.p[2]=__fdividef(P.p[2],x);
  P.p[3]=__fdividef(P.p[3],x);
  return P;
}

#endif
