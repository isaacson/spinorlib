#include <cstdio>
#include <iostream>
#include "architecture.h"
#include "momentum.h"
#include "KahanAdder.h"

using namespace std;

extern void *g_weight,*g_cut;
extern void init(int N,int N);
extern void Event(int N,float Ecm,float Ptmin,float etamax,float Rmin,int N);

int main(int a,const char*av[])
{
  //
  // run set-up
  //
  //int N=atoi(av[1]);
  //int events_per_MP=atoi(av[2]);
  long int Iterations=1000000;
  Iterations=1000;
  float Ecm=14000.0;
  float Ptmin=20.0;
  float etamax=2.5;
  float Rmin=0.4;
  //
  // set-up
  //
  for (int N=12;N<=12;N++){
  int events_limit_register=REGISTER_MEMORY/MAX_REGISTER;
  int max_events_per_BLOCK=(SHARED_MEMORY-16)/(sizeof(momentum<float>)*N*(N+1)/2);
  int events_per_BLOCK=min(events_limit_register,max_events_per_BLOCK);
  int total_events_per_iteration=events_per_BLOCK*BLOCKS_PER_MP*NUMBER_MP;

  cout<<endl<<endl<<endl;
  cout<<"Running configuration: gg ---> "<<N-2<<" gluons"<<endl;
  cout<<"                       number of MP's used                 = "
      <<NUMBER_MP<<endl;
  cout<<"                       number of BLOCKS per MP used        = "
      <<BLOCKS_PER_MP<<endl;
  cout<<"                       events/BLOCK limit from used registers = "
      <<events_limit_register<<endl;
  cout<<"                       events/BLOCK limit from shared memory  = "
      <<max_events_per_BLOCK<<endl;
  cout<<"                       events/BLOCK                           = "
      <<events_per_BLOCK<<endl;
  cout<<"                       threads used/BLOCK                     = "
      <<events_per_BLOCK*(N-1)<<endl;
  cout<<"                       events per iteration                = "
      <<total_events_per_iteration<<endl;
  cout<<"                       number of iterations                = "
      <<Iterations<<endl;
  cout<<"                       total generated event in this run   = "
      <<(double) Iterations*(double) total_events_per_iteration<<endl;
  cout<<endl<<endl<<endl;
  //
  cudaEvent_t start,stop,event;
  float init_time=0.0,event_time=0.0,histogram_time=0.0;
  float time;
  cudaEventCreate(&start);cudaEventCreate(&stop);cudaEventCreate(&event);
  //
  // Initialize run
  //
  cudaEventRecord(start, 0);
  init(N,total_events_per_iteration);
  cudaEventRecord(stop, 0);cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time, start, stop);
  init_time+=time;
  //
  // start calculating the events
  //
  int update=-10000;
  KahanAdder<double> xsection,x2section;
  KahanAdder<long int> Nevents,Naccepted;

  //float maxwgt=0.0;
  for (int eventsblocks=0;eventsblocks<Iterations;++eventsblocks)
    {
      if (eventsblocks!=0)
	{
	  float wgt[total_events_per_iteration];
	  bool cut[total_events_per_iteration];
	  cudaMemcpy(wgt,g_weight,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost);
	  cudaMemcpy(cut,g_cut,sizeof(bool)*total_events_per_iteration,cudaMemcpyDeviceToHost);
	  for (int i=0;i<total_events_per_iteration;i++) {
	      Nevents+=1;
	      if (cut[i]==true) {
		Naccepted+=1;
		xsection+=wgt[i];
		x2section+=wgt[i]*wgt[i];
	      }}
	  if (eventsblocks==update) 
	    {
	      cout<<"Total events:"<<(double) Nevents();
	      cout<<"   Accepted events:"<<(double) Naccepted();
	      double avg=xsection()/(double) Nevents();
	      double avg2=x2section()/(double) Nevents();
	      double sigma_mean=sqrt((avg2-avg*avg)/(double) Nevents());
	      cout<<"   cross section: ("<<avg<<" +/- "<<sigma_mean<<" pb)"<<endl;
	      //cout<<eventsblocks<<" "<<avg<<" "<<sigma_mean<<endl;
	      update+=10000;
	    }
	  cudaEventRecord(stop, 0);
	  cudaEventSynchronize(stop);
	  cudaEventElapsedTime(&time,start,event);
	  event_time+=time;
	  cudaEventElapsedTime(&time,event,stop);
	  histogram_time+=time;
	}
      cudaEventRecord(start, 0);
      Event(N,Ecm,Ptmin,etamax,Rmin,events_per_BLOCK);
      cudaEventRecord(event, 0);
    }
  float wgt[total_events_per_iteration];
  bool cut[total_events_per_iteration];
  cudaMemcpy(wgt,g_weight,sizeof(float)*total_events_per_iteration,cudaMemcpyDeviceToHost);
  cudaMemcpy(cut,g_cut,sizeof(bool)*total_events_per_iteration,cudaMemcpyDeviceToHost);
  for (int i=0;i<total_events_per_iteration;i++) {
    Nevents+=1;
    if (cut[i]==true) {
      Naccepted+=1;
      xsection+=wgt[i];
      x2section+=wgt[i]*wgt[i];
    }}
  cudaEventRecord(stop, 0);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,event);
  event_time+=time;
  cudaEventElapsedTime(&time,event,stop);
  histogram_time+=time;
  float total_time=init_time+event_time+histogram_time;
  cout<<"Total events:"<<(double) Nevents();
  cout<<"   Accepted events:"<<(double) Naccepted();
  double avg=xsection()/(double) Nevents();
  double avg2=x2section()/(double) Nevents();
  double sigma_mean=sqrt((avg2-avg*avg)/(double) Nevents());
  //cout<<E<<" "<<init_time/1000.0<<" "<<event_time/1000.0<<" "<<histogram_time/1000.0<<" "
  //    <<event_time/1000.0/((double) Iterations*(double) E*(double) NUMBER_MP)<<" "
  //    <<total_time/1000.0/((double) Iterations*(double) E*(double) NUMBER_MP)<<endl;
  //cout<<Iterations<<" "<<avg<<" "<<sigma_mean<<endl;
  cout<<endl<<"   Cross section: ("<<avg<<" +/- "<<sigma_mean<<" pb)"<<endl;
  cout<<endl<<endl<<endl;
  cout<<"Total events:"<<(double) Nevents()<<endl;
  cout<<"Accepted events:"<<(double) Naccepted()<<endl;;
  cout<<endl;
  /*
  cout<<endl<<"Timing:"<<endl;
  cout<<"       Initialization:"<<init_time/1000.0<<" seconds ("
      <<100.0*init_time/total_time<<"%)"<<endl;
  cout<<"       Event         :"<<event_time/1000.0<<" seconds ("
      <<100.0*event_time/total_time<<"%)"<<endl;
  cout<<"       Histogramming :"<<histogram_time/1000.0<<" seconds ("
      <<100.0*histogram_time/total_time<<"%)"<<endl;
  cout<<endl;
  cout<<"      Total event time/events   :"
      <<event_time/1000.0/((double) Iterations*(double) E*(double) NUMBER_MP)<<" seconds"<<endl;
  cout<<"       Total/event   :"
      <<total_time/1000.0/((double) Iterations*(double) E*(double) NUMBER_MP)<<" seconds"<<endl;
  cout<<"       Total         :"
      <<total_time/1000.0<<" seconds"<<endl;
  */
  cout<<" effective time/event: "<<event_time/1000.0/((double) Iterations*(double) total_events_per_iteration)<<endl<<endl;
  cout<<" initialization time :  "<<init_time/1000.0<<endl;
  cout<<" histogram time      :  "<<histogram_time/1000.0<<endl;
  cout<<" event time          :  "<<event_time/1000.0<<endl;
  cout<<" total time          :  "<<(init_time+event_time+histogram_time)/1000.0<<endl;
  }
}
