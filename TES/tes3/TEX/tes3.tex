\documentclass[11pt,onecolumn,twoside,notitlepage,fleqn,letterpaper,final]
	      {article}[]

\input{texsetup.tex}
\newcommand{\order}[1]{$\mathcal O(#1)$}
\def\query#1{{\red{\bf[QUERY]}\quad\textbf{\textit{ #1}}}}
\let\ifpdf\relax
\usepackage{ifpdf}

\preprint{FERMILAB-PUB-11-XXX-T}
\author{\hphantom{MisterX}\\[0mm]\sffamily\bfseries\normalsize%
  Walter~T.~Giele\thanks{giele@fnal.gov}
  \,\!,\ \
  Gerben~C.~Stavenga\thanks{stavenga@fnal.gov}
  \\[4mm]
  {\small\sl Theoretical Physics Department}\\[-2mm]
  {\small\sl Fermi National Accelerator Laboratory, Batavia, IL 60510, USA}
  \\[10mm]\sffamily\bfseries\normalsize%
  Jan Winter\thanks{jwinter@cern.ch}
  \\[4mm]
  {\small\sl PH-TH Department, CERN, CH-1211 Geneva 23, Switzerland}
  \\[9mm]
}
\title{\sffamily\bfseries\Large%
  A GPU-based Leading Order Monte Carlo for\\
  Multi-Jet Production at the LHC}
\institute{Theoretical Physics Department, Fermi National Accelerator
  Laboratory, Batavia, IL 60510, USA}

\begin{document}
\renewcommand{\baselinestretch}{1.4}
\maketitle
\thispagestyle{empty}
%\begin{flushleft}\vspace*{-130mm}\date{\today}\\[127mm]\end{flushleft}
\begin{flushright}
  \vspace*{-155mm}
  {\small FERMILAB-PUB-11-xxx-T\\CERN-PH-TH/2011-xxx}\\[131mm]
\end{flushright}
\renewcommand{\baselinestretch}{0.9}
\begin{abstract}
\noindent
We construct a parton level Leading Order Monte Carlo for the calculation
of multi-jet final states at hadron colliders. The Monte Carlo is designed
for use on a single CPU/GPU desktop system and has similar event
generation speed as a CPU farm with several 100 nodes. 

The Monte Carlo uses the GPU to generate unit weight Leading Order 
events in the Leading Color approximation. 
In parallel, the CPU threads
calculate the color correction to the GPU generated events.
In this manner the GPU is used as a sophisticated phase space generator
which generates events distributed according to the leading order,leading color
scattering probability. The resulting color corrections calculated by
the CPU threads should give only a small corrections.

While the focus of this paper is the performance optimization 
of the Monte Carlo, we will use the tool to study the effects
of the leading color approximation on high multiplicity jet
observables. Furthermore we will compare predictions obtained
from the Monte Carlo with multi-jet data from the LHC experiments. 
\end{abstract}

\clearpage%\newpage
\renewcommand{\baselinestretch}{0.7}
\tableofcontents
\thispagestyle{empty}
%\clearpage
\renewcommand{\baselinestretch}{1.04}
\vspace*{4mm}
\noindent\hrulefill
\vspace*{7mm}



% ======= main ================================================================



\section{Introduction}\label{sec:Intro}

The workhorse for phenomenology at colliders is the Leading
Order (LO) parton level Monte Carlo (MC)~\cite{Stelzer:1994ta,
Krauss:2001iv,Draggiotis:2002hm,Mangano:2002ea,Boos:2004kh,Gleisberg:2008fv}. 
When matched to a parton shower~\cite{Mangano:2006rw,Hoche:2006ph,Catani:2001cc,
Lonnblad:2001iq,Lavesson:2005xu,Schumann:2007mg,Frixione:2002ik,Dinsdale:2007mf,
Hamilton:2010wh,Frixione:2007vw,Nagy:2006kb,Bengtsson:1986et,Bengtsson:1986hr}, 
it becomes a invaluable tool to 
understand the hard scattering measurements.

By using recursive techniques~\cite{Berends:1987me,Kosower:1989xy,Caravaglios:1995cd,
Kanaki:2000ey,Moretti:2001zz,Cachazo:2004kj,Risager:2005vk,Britto:2004ap,
Britto:2005fq,Draggiotis:2005wq,Vaman:2005dt,Schwinn:2005pi}
or direct diagrammatic evaluation~\cite{Murayama:1992gi,Stelzer:1994ta,
Krauss:2001iv,Boos:2004kh}
several reliable LO MC's have been developed to
make Standard Model (SM) predictions at hadron colliders.
Of these, the COMIX MC~\cite{Gleisberg:2008fv} pushes the jet multiplicity to
8 final state jets.

In this paper we will make the first steps towards a full-fledged
GPU-based MC, named TESS. We already explored in ref.~\cite{Giele:2010ks} the
GPU implementation of the pure gluonic contribution to the $n$-jet
cross section. Here we will extend the Monte Carlo
to generate the full $n$-jet cross section at hadron
colliders in the LO approximation.  By utilizing the GPU we
obtain an increase in the evaluation time by at least 2 orders
of magnitude on a single CPU/GPU-based desktop system. This
not only allows us to calculate jet cross section well in
excess of 8 final jets, but also allows for
fast and accurate evaluations of lower multiplicity jet
final states. This negates the need for large scale PC farms/grids,
making this a very attractive alternative to the traditional
CPU-farm based Monte Carlo's.

In this paper all results and timings were obtained using 
a 4-core AMD Phenom(tm) II X4 940 CPU Processor and the 
NVIDIA (tm) C2050 GPU\footnote{See ref.~\cite{Giele:2010ks}
for details on the use of the GPU in the context LO event
generators.}.
The basic run-time setup is to use the GPU
to generate unit weight phase space events distributed according
to the Leading Color scattering amplitudes. For these unweighted 
events we calculate in parallel on the CPU the color dressed 
and/or helicity summed amplitudes, resulting in a re-weighting 
of the events. In this manner, the GPU is used as a sophisticated
phase space generator.   

The outline of the paper is as follows. In section 2, we discuss
the recursive generators for both the CPU and GPU used in the 
TESS MC. The actual run-time setup, timing and accuracies of
the TESS MC are detailed in section 3. The validation of the
MC is done in section 4 by comparing the numerical results
to existing MC's. The comparison to data and predictions
for the LHC are shown in section 5 and the conclusions are
given in section 6.

\section{Generating the Leading Order Amplitudes}

In this section we describe the algorithms used in
the Monte Carlo for the
generating the multi-parton amplitudes.
We will show that as long no massive quarks are
involved in the scattering amplitude, complex numbers
can be avoided. This enhances evaluation time as well
as minimizing the required memory usage. The latter
is especially important for GPU usage. Next we will
detail the generic recursive algorithms we use in the 
amplitude generation. Finally, we outline an algorithm
for use on the CPU which calculates all helicity combinations
of an ordered $n$-parton amplitude at an complexity $n\times 2^n$.
This is much faster that the naive calculation of all the helicity
configurations which have complexity $n^4\times 2^n$. For example,
a factor of 20 is gained in evaluation time of the helicity
summed 8 jet ordered amplitude squared.

\subsection{Real versus Complex Valued Amplitudes}\label{Majorana}

From a computational point of view it is highly desirable to avoid the use
of complex numbers for both speed and memory requirements. 
As we already demonstrated in ref.~\cite{Giele:2010ks}
this is trivially accomplished in the all-gluonic case
by using real valued polarization vectors for the external gluon states.

At first sight, the inclusion of quarks requires the use of complex numbers
due to the presence of Dirac-matrices. However, by using the Majorana
representation each Dirac-matrices becomes purely imaginary. This 
leads for massless quarks to a real valued amplitude. Only the presence of
massive quarks necessitates the use of complex valued amplitudes.


When fermions (quarks) are present in the scattering amplitude
we use the Majorana basis for the Dirac matrices:\ $\gamma_{\mu}^{(i)}=i\lambda_{\mu}^{(i)}$.
The $\lambda$-matrices are real valued 4x4 matrices.
\beqa
\lambda_{\mu}^{(0)}&=&\left(
\begin{array}{rrrr}
0 & 0 & 0 & -1 \\
0 & 0 & \phantom{-}1 & 0 \\
0 & -1 & 0 & 0 \\
\phantom{-}1 & 0 & 0 & 0\end{array}\right),\ 
\lambda_{\mu}^{(1)}=\left(
\begin{array}{rrrr}
\phantom{-}1 & 0 & 0 & 0 \\
0 & -1 & 0 & 0 \\
0 & 0 & \phantom{-}1 & 0 \\
0 & 0 & 0 & -1 \end{array}\right),\nn
\lambda_{\mu}^{(2)}&=&\left(
\begin{array}{rrrr}
0 & 0 & 0 & \phantom{-}1 \\
0 & 0 & -1 & 0 \\
0 & -1 & 0 & 0 \\
\phantom{-}1 & 0 & 0 & 0 \end{array}\right),\
\lambda_{\mu}^{(3)}=\left(
\begin{array}{rrrr}
0 & -1 & 0 & 0 \\
-1 & 0 & 0 & 0 \\
0 & 0 & 0 & -1 \\
0 & 0 & -1 & 0 \end{array}\right).
\eeqa
Note that the Majorana matrices can be viewed as simple 
permutation matrices up to a possible sign. As a consequence, 
matrix multiplication with one of the Majorana matrices 
becomes a simple reshuffling algorithm, leading
to fast algorithmic implementations for multiplying many
gamma matrices together.

The (anti) quark propagator and quark--antiquark--gluon vertex
in term of the Majorana matrices becomes
\beqa
V_{q\bar q}&=&-i\delta^{kl}\left(\frac{p_{\mu}\gamma^{\mu}\pm m}{p^2-m^2}\right)
=\delta^{kl}\left(\frac{p_{\mu}\lambda^{\mu}\mp i m}{p^2-m^2}\right)\nn\nn
V_{qg\bar q}&=&-igT^a_{kl}\gamma_{\mu}=gT^a_{kl}\lambda_{\mu}\ .
\eeqa
As we see, for massless quarks the Feynman rules are now real valued and
lead to real valued amplitudes.

\subsection{Generalized Recursion Relations}

The generalized Berends-Giele recursion relations~\cite{Berends:1987me} are well
suited for GPU implementation as was shown in Ref.~\cite{Giele:2010ks}. The main
constraint comes from the available memory. This means that for current GPU's
we only implement ordered recursion relations on the GPU. For the CPU we develop
recursion relations calculating the full color--dressed amplitudes.

\subsubsection{Recursion Relations for Generic Processes}

It is convenient to write the $(n+1)$-particle tree-level Greens function
as a product of connected operators $\langle O_1(p_1)\cdots O_{n+1}(p_{n+1})\rangle$. 
For $n=1$ the Greens function is a propagator connecting the two fields.
For larger values of $n$, the Greens function is calculated using the
recursion relation:
\begin{comment}
\beqa\label{genrec}
\lefteqn{J_b(1,2\ldots,n)=\langle O_1\cdots O_n F_b\rangle} \nn
&=&\sum_{k=3}^{V_{\mbox{\tiny max}}}\sum_{S_1,\ldots,S_{k-1}}%^{(k-1)!{\cal S}_2(n,k-1)}
\frac{1}{(k-1)!}\ \pi(S_1,\ldots,S_{k-1})\, V_k^{b_1\cdots b_k}
\langle S_1 F_{b_1}\rangle\cdots\langle S_{k-1} F_{b_{k-1}}\rangle
\langle F_b F_{b_k}\rangle\ .
\eeqa
\end{comment}
\beqa\label{genrec}
\lefteqn{J_b(1,2\ldots,n)=}\nn
&=&\sum_{k=3}^{V_{\mbox{\tiny max}}}\sum_{S_1,\ldots,S_{k-1}}%^{(k-1)!{\cal S}_2(n,k-1)}
\frac{1}{(k-1)!}\ \pi(S_1,\ldots,S_{k-1})\, V_k^{b_1\cdots b_k}
J_{b_1}(S_1)\cdots J_{b_{k-1}}(S_{k-1}) P_{b_kb}
\eeqa
where the current $J_b$ of field $F_b$ is calculated by summing over all 
$(k-1)!{\cal S}_2(n,k-1)$\footnote{The ${\cal S}_2(n,k-1)$ is the Stirling number of the second type.} 
ordered partitions $\{S_1,\ldots,S_{k-1}\}$ 
(i.e. $\{S_1,S_2,\ldots\}\neq\{S_2,S_1,\ldots\}$) of the set $\{O_1,\ldots,O_n \}$. The vertices,
$V_k^{b_1\cdots b_k}$, range from $k=3$ particles to $k=V_{\mbox{\tiny max}}$ particles, 
while the factorial factor
takes into account the over-counting due to the vertex symmetrization. The factor
$\pi(S_1,\ldots,S_k)=\pm 1$, depending on the fermi-statistics of the fermionic field
permutations. This recursion relation has exponential complexity $V_{\mbox{\tiny max}^n}$.
The exponential complexity can be changed to $3^n$, albeit at the cost of more memory usage.
This recursion is derived from the generic recursion of eq.~\ref{genrec} and gives
\begin{comment}
\beqa
J_b(S)&=&\langle SF_b\rangle=\frac{1}{2}\sum_{L\cup R=S} 
\pi(L,R)\ \langle F_bF_{b_1}\rangle\langle LF^{b_1}F^a\rangle\langle RF_a\rangle\nn
W^{b_1\cdots b_m}(S)&=&
\langle SF^{b_1}\cdots F^{b_m}\rangle = \frac{1}{m}\left(V_{m+1}^{b_1\cdots b_ma}\langle SF_a\rangle +
\sum_{L\cup R=S}\pi(L,R)\  \langle LF^{b_1}\cdots F^{b_m}F^a\rangle\langle RF_a\rangle\right)\nn
W^{b_1\cdots b_{V_{\mbox{\tiny max}}-1}}(S)&=&
\langle SF^{b_1}\cdots F^{b_{V_{\mbox{\tiny max}}-1}}\rangle =\frac{1}{V_{\mbox{\tiny max}}-1}
V_{V_{\mbox{\tiny max}}}^{b_1\cdots b_{V_{\mbox{\tiny max}}-1}a}\ \langle SF_a\rangle
\eeqa
\end{comment}
\beqa\label{3n-rec}
J_b(S)&=&\sum_{L\cup R=S} 
\pi(L,R)\ P_{bb_1}W^{b_1a}(L)J_a(R)\nn
W^{b_1\cdots b_m}(S)&=&
\frac{1}{m}\left(V_{m+1}^{b_1\cdots b_ma}J_a(S)+
\sum_{L\cup R=S}\pi(L,R)\  W^{b_1\cdots b_ma}(L)J_a(R)\right)\nn
W^{b_1\cdots b_{V_{\mbox{\tiny max}}-1}}(S)&=&\frac{1}{V_{\mbox{\tiny max}}-1}
V_{V_{\mbox{\tiny max}}}^{b_1\cdots b_{V_{\mbox{\tiny max}}-1}a}\ J_a(S)
\eeqa
To obtain a complexity $3^n$, in addition to the currents $J$ all tensors $W$ have to be stored in memory.

Note that in numerical implementations one wants to choose field $f^{B_1}$ to be in the  $L$ partition.
This way, we avoid the double counting and the fractions for each term disappear. In case of non-identical
particles in the vertices proper care now has to be taken to ensure a correct result.


\subsubsection{Recursion Relations for Multi-Jet Production}

In this subsection we will specify the generic recursion relations for the case of
$SU(N_c)$ QCD. The external fields are given by the gluons, $A_{\mu}$,
the quark, $\psi_i$ and the anti-quark fields, $\overline\psi_j$. The three non-zero
interaction vertices 
are the 3-gluon vertex, $V_3^{\mu_1\mu_2\mu_3}$, the 4-gluon vertex 
$V_4^{\mu_1\mu_2\mu_3\mu_4}$ and the fermionic vertex $V_{ff}^{i\mu j}$.
All other vertices occurring in the recursion relation are defined to be zero.
Given fermion number and flavor conservation, the current $J_b(S)$ is either
gluonic or fermionic of a specific flavor. This is unambiguously determined 
by the fields $S$ in the argument of $J_b$.

We start with the gluonic current $J_{\mu}^a$, where $a$ represents the color
label. The recursion relation is constructed  by 
summing over all possible ways to partitioning the fields in 2 or 3 sets
and fusing the smaller currents through the interaction vertices.
The generic current of eq.~\ref{genrec} with exponential complexity $4^n$
specifies to
\begin{equation}
\begin{split}
J_\mu^a(1,2,\ldots,n)=J_\mu^a(S)
&=\frac{1}{2!}\sum_{S_1\cup S_2=S} \pi(S_1,S_2)\, V_{3\,a_1a_2a_3}^{\mu_1\mu_2\mu_3}\, 
J_{\mu_1}^{a_1}(S_1)J_{\mu_2}^{a_2}(S_2)
P_{\mu_3\mu}^{a_3a}\\
&+\frac{1}{3!}\sum_{S_1\cup S_2\cup S_3=S} \pi(S_1,S_2,S_3)\, V_{4\,a_1a_2a_3a_4}^{\mu_1\mu_2\mu_3\mu_4}\, 
J_{\mu_1}^{a_1}(S_1)J_{\mu_2}^{a_2}(S_2)J_{\mu_3}^{a_3}(S_3)
P_{\mu_4\mu}^{a_4a}\\
&+\sum_{S_1\cup S_2=S} \pi(S_1,S_2)\, V_{ff\,k_1a_1k_2}^{i\mu_1 j}
J_i^{k_1}(S_1)\bar J_j^{k_2}(S_2)P_{\mu1\mu}^{a_1a}
\end{split}
\end{equation}
\begin{comment}
\begin{equation}
\begin{split}
J_\mu^a(S)=\langle O_1\cdots O_n A_\mu^a\rangle
&=\sum_{L,R} \pi(L,R)\, V_3^{b_1b_2b_3}\, 
\langle L A_{b_1} \rangle \langle R  A_{b_2}\rangle 
\langle A_{b_3} A_\mu^a\rangle\\
&+\sum_{L,R} \pi(L,R)\, V_4^{b_1b_2b_3b_4}\, 
\langle L A_{b_1} A_{b_2} \rangle 
\langle R  A_{b_3}\rangle \langle A_{b_4} A_\mu^a\rangle\\
&+\sum_{L,R} \pi(L,R)\, V_4^{b_1b_2b_3b_4}\, 
\langle L  A_{b_3}^{a_2}\rangle \langle R A_{b_1} A_{b_2} 
\rangle \langle A_{b_4} A_\mu^a\rangle\\
&+\sum_{L,R} \pi(L,R)\, V_{ff}^{f_1b_1f_2}\, \langle L  \psi_{f_1}\rangle 
\langle R \bar{\psi}_{f_2} \rangle \langle A_{b_1} A_\mu^a\rangle\\
&-\sum_{L,R} \pi(L,R) V_{ff}^{f_1b_1f_2} \langle L  \bar{\psi}_{f_2}\rangle 
\langle R \psi_{f_1}\rangle \langle A_{b_1} A_\mu^a\rangle\ ,
\end{split}
\end{equation}
where the index $b_a$ represents both the Lorentz label $\mu_a$ and color index $c_a$ of the gluonic fields
and the index $f_i$ the spinor label $s_i$ and color index $j_i$ of the fermionic fields. 
\end{comment}
The actual nested recursion algorithm implemented on the CPU has exponential complexity $3^n$ and directly results
from eq.~\ref{3n-rec}
\beqa
J_{\mu}^a(S)&=&\sum_{L\cup R=S} 
\pi(L,R)\ P_{\mu\mu_1}^{aa_1}\left(W^{\mu_1\mu_2}_{a_1a_2}(L)J_{\mu_2}^{a_2}(R)
+V_{ff\,k_1a_1k_2}^{i\mu_1 j} J_i^{k_1}(L)\bar J_j^{k_2}(R)\right)\nn
W^{\mu_1\mu_2}_{a_1a_2}(S)&=&
\frac{1}{2}\left(V_{3\,a_1a_2a_3}^{\mu_1\mu_2\mu_3}J_{\mu_3}^{a_3}(S)+
\sum_{L\cup R=S}\pi(L,R)\  W^{\mu_1\mu_2\mu_3}_{a_1a_2a_3}(L)J_{\mu_3}^{a_3}(R)\right)\nn
W^{\mu_1\mu_2\mu_3}_{a_1a_2a_3}(S)&=&\frac{1}{3}
V_{4\,a_1a_2a_3a_4}^{\mu_1\mu_2\mu_3\mu_4}\ J_{\mu_4}^{a_4}(S)
\eeqa
where we have to store all the sub-currents $J_{\mu}^a$ and all the sub-tensors $W_{\mu_1\mu_2}^{a_1a_2}$.
Furthermore, to avoid the double counting we require in the actual numerical implementation that
field $1$ is always in the left current. This lead to trivial modifications in the above recursion
relation.

\begin{comment}
The 3-gluon contribution to the gluonic current is given by 
\begin{equation}
\begin{split}
J_\mu^a&=\, V_3^{b_1b_2b_3}\,\langle L A_{b_1} \rangle \langle R  A_{b_2}\rangle\langle A_{b_3} A_\mu^a\rangle\\
&=-i\frac{g_{\mu\mu_3}}{P_{LR}^2} f^{a_1a_2a}\Big\{(p_L-p_R)^{\mu_3} g^{\mu_1\mu_2}
+(p_R+P_{LR})^{\mu_1} g^{\mu_2\mu_3}-(P_{LR}+p_L)^{\mu_2} g^{\mu_3\mu_1}\Big\}J_{L\mu_1}^{a_1}J_{R\mu_2}^{a_2}\\
&=\frac{1}{P_{LR}^2}\Big\{(p_R-p_L)_\mu [J_{L\nu},J_R^\nu]^a
+[J_{R\mu},(p_R+P_{LR})\cdot J_L]^a+[J_{L\mu},(P_{LR}+p_L)^a\cdot J_{R\mu}]\Big\}
\end{split}
\end{equation}
where we used $[J_{R\mu},J_{L\nu}]^a=i\,f^{abc}J^b_{R\mu}J^c_{L\nu}$.

The 4-gluon vertex is
rewritable to a combination of 3-gluon vertices~\cite{Draggiotis:1998gr}. 
This has as an advantage that the complexity of the algorithm implementation
of the recursion relation is reduced from $n^4\rightarrow n^3$.
The 4-gluon contribution to the gluonic current is
\begin{equation}
\begin{split}
J_\mu^a&=-i\frac{g_{\mu\mu_1}}{P^2} 
\left\{-i f^{eaa_2} \langle L A_{\nu_1}^{b_1} A_{\nu_2}^{b_2} f^{eb_1 b_2} (g^{\mu_1 \nu_1}g^{\mu_2 \nu_2}-g^{\mu_1 \nu_2}g^{\mu_2 \nu_1})\rangle J_{R\mu_2}^{a_2}\right\}+L\leftrightarrow R \\
J_\mu&=\frac{g_{\mu\mu_1}}{P^2} 
\left\{[T^{a_2}, T^e] \langle L A_{\nu_1}^{b_1} A_{\nu_2}^{b_2} i f^{eb_1 b_2} (g^{\mu_1 \nu_1}g^{\mu_2 \nu_2}-g^{\mu_1 \nu_2}g^{\mu_2 \nu_1})\rangle J_{R\mu_2}^{a_2}\right\}+L\leftrightarrow R \\
J_\mu&=\frac{g_{\mu\mu_1}}{P^2}\left[J_{R\mu_2}, W_L^{\mu_1\mu_2}\right]+\left[J_{L\mu_2}, W_R^{\mu_1\mu_2}\right]\\
&=\frac{1}{P^2}\left[J_R^\nu, W_{L\mu\nu}\right]+\left[J_L^\nu, W_{R\mu\nu}\right]\\
W_L^{\mu_1\mu_2}&=\langle L A_{\nu_1}^{b_1} A_{\nu_2}^{b_2}\rangle [T^{b_1}, T^{b_2}] (g^{\mu_1 \nu_1}g^{\mu_2 \nu_2}-g^{\mu_1 \nu_2}g^{\mu_2 \nu_1})
\end{split}
\end{equation}
Here we introduced the auxiliary field $W$, which was mentioned earlier. For $W$ there exists naturally a completely analogous recursion
\begin{equation}
\begin{split}
W^{\mu_1\mu_2}&=\sum_{L,R} \pi(L,R) \langle L A_{\nu_1}^{b_1} \rangle \langle R  A_{\nu_2}^{b_2}\rangle [T^{b_1}, T^{b_2}] (g^{\mu_1 \nu_1}g^{\mu_2 \nu_2}-g^{\mu_1 \nu_2}g^{\mu_2 \nu_1})\\
W^{\mu_1\mu_2}&=\sum_{L,R} \pi(L,R) \left[J_{L\nu_1}, J_{R\nu_1}\right](g^{\mu_1 \nu_1}g^{\mu_2 \nu_2}-g^{\mu_1 \nu_2}g^{\mu_2 \nu_1})\\
W_{\mu\nu}&=\left[J_{L\mu}, J_{R\nu}\right]+\left[J_{R\mu}, J_{L\nu}\right]
\end{split}
\end{equation}
The fermion contribution to the gluon current is
\begin{equation}
\begin{split}
J_\mu^a=-i\frac{g_{\mu\nu}}{P^2} \left(-\lambda^\nu\right)^i_j (T^a)^{mn} \left\{\langle L \psi^{nj}\rangle \langle R \bar{\psi}_i^m\rangle-\langle L \bar\psi_i^m\rangle \langle R \psi^{nj}\rangle\right\}\\
J_\mu^a=i\frac{g_{\mu\nu}}{P^2} \left(\lambda^\nu\right)^i_j (T^a)^{mn} \left\{\psi_L^{nj}\bar{\psi}_{Ri}^m-\bar\psi_{Li}^m \psi_R^{nj}\rangle\right\}
\end{split}
\end{equation}
This is anti-symmetric interchanging $L$ and $R$, but this compensated by the anti-symmetry of $\pi(L,R)$. Also note that one of the two terms above is always zero due to current conservation. So we assume without loss of generality that the second term is zero (otherwise we exchange $L,R$). If we contract the current with the color generators, we have 
\begin{equation}
T^a (T^a)^{mn} \psi_L^{nj}\bar{\psi}_{Ri}^m=T^a Tr_C [T^a \psi_L^j\times\bar{\psi}_{Ri}].
\end{equation}
Because $1, \{T^a\}$ is a basis of all matrices and $Tr[T^a T^b]=T \delta^{ab}$, this becomes
\begin{equation}
T^a(T^a)^{mn} \psi_L^{nj}\bar{\psi}_{Ri}^m=T[\psi_L^j\times\bar{\psi}_{Ri}]_\text{traceless}.
\end{equation}
So it symplifies to
\begin{equation}
\label{eq_gluon_ff}
J_\mu=i\frac{1}{P^2} \left(\lambda_\mu\right)^i_j T[\psi_L^j\times \bar{\psi}_{Ri}]_\text{traceless}
\end{equation}
\end{comment}

The quark current, $J_i^k$, and anti-quark current, $\bar J_j^k$
with color index $k$, are given by 
\beqa
J_i^k(1,2,\ldots,n)=J_i^k(S)&=&\sum_{F\cup B=S}\pi(F,B)\ V_{ff\,k_1ak_2}^{i_1\mu j_1}J_{i_1}^{k_1}(F)
J_{\mu}^a(B)  P^{kk_2}_{ij_1}\nn
\bar J_j^k(1,2,\ldots,n)=\bar J_j^k(S)&=&\sum_{F\cup B=S}\pi(F,B)\ V_{ff\,k_1ak_2}^{i_1\mu j_1}\bar J_{j_1}^{k_2}(F)
J_{\mu}^a(B)  P^{kk_1}_{i_1j}
\eeqa
By using the Majorana basis of subsection~\ref{Majorana} we get for massless quarks a real valued current.

\begin{comment}
\begin{equation}
\bar\psi=\langle O_1\ldots O_n \bar\psi(-P)\rangle=\sum_{F, B} \pi(F, B) V_3\langle F\bar\psi(-p_F)\rangle \langle B A(-p_B)\rangle \langle \psi(P)\bar\psi(-P) \rangle.
\end{equation}
Here instead of left and right subsets we define the left set to be fermionic and the right to be bosonic.
The precise version, suppressing the sum and the sign
\begin{equation}
\begin{split}
\bar\psi_k^o&=\frac{(-P_\nu \lambda^\nu)^j_k}{P^2}\delta^{on} (-\lambda^\mu)^i_j (T^a)^{mn} \bar\psi^m_{Fi} J_{B\mu}^a \\
\bar\psi^o&=\bar\psi^m_{F}\lambda^\mu J_{B\mu}^{mo}\frac{(P_\nu \lambda^\nu)}{P^2}
\end{split}
\end{equation}
And for the quark
\begin{equation}
\begin{split}
\psi=\langle O_1\ldots O_n \psi(-P)\rangle&=\sum_{F, B} \pi(F, B) V_3\langle F\psi(-p_F)\rangle \langle B A(-p_B)\rangle \langle \psi(-P)\bar\psi(P) \rangle\\
\psi^{ko}&=\frac{(P_\nu \lambda^\nu)^k_i}{P^2}\delta^{mo} (-\lambda^\mu)^i_j (T^a)^{mn} \psi^{nj}_F J_{B\mu}^a \\
\psi^o&=\frac{(-P_\nu \lambda^\nu)}{P^2} J_{B\mu}^{on}\lambda^\mu\psi^n_{F} 
\end{split}
\end{equation}
Note the relative minus sign due to the fact that momentum flows in opposite direction with respect to the charge.


In the above formulas in the Majorana basis there is only one $i$ present \eqref{eq_gluon_ff}. However if one inspects the recursion for $\psi$ we see that one can freely multiply $\psi$ with a constant without changing that relation. Therefore if we redefine $\psi\rightarrow -i\psi$ \eqref{eq_gluon_ff} becomes
\begin{equation}
\label{eq_gluon_ff_real}
J_\mu=\frac{1}{P^2} \left(\lambda_\mu\right)^i_j T[\psi_L^j\times \bar{\psi}_{Ri}]_\text{traceless}
\end{equation}
Now all the recursion steps are real. We can take the gluon polarization real, the color states are either real or completely imaginary (which is an inconsequential overall factor of $i$) and we can take the spinor polarization real (Majorana basis). The complete recursion becomes real which is a sizeable optimalization in both memory and speed.

If we inspect the gluon recursion relations than we see that all recursions relation consist of commutators. If we don't compute the commutators but work them out in a giant polynomial consisting of the initial gluon color states, we get all kinds of different orderings. The gluon quark-antiquark recursion is a little bit different because the quark is always left and the antiquark right.
The quark gluon-quark orders the gluon always left and the antiquark antiquark-gluon orders the gluon always right. In the end we
get a huge polynomial with different orderings of the external color factors. The ordering consist of gluon color factors interspersed with $\psi\times\bar\psi$ pairs.
Suppose we want to calculate the coefficient of a particular ordering we can order the external legs according to such an ordering. The recursion relations then simplify to
\begin{equation}
\begin{split}
J_\mu&=\frac1{P^2}\{(p_R-p_L)_\mu J_L\cdot J_R+2J_L p_L\cdot J_R-2J_R p_R\cdot J_L+W_R\cdot J_L-W_L\cdot J_R\\
&+T\bar\psi_R\lambda_\mu\psi_L \}\\
W&=J_L\times J_R-J_R\times J_L\\
\bar\psi&=\bar\psi_F\lambda^\nu J_{B\mu} \frac{(P_\nu \lambda^\mu)}{P^2}\\
\psi&=\frac{(-P_\nu \lambda^\nu)}{P^2}J_{B\mu} \lambda^\mu \psi_F
\end{split}
\end{equation}
\end{comment}

\subsection{Ordered Recursion}

The ordered recursion relations are for execution on the GPU. In
ref.~\cite{Giele:2010ks} the pure gluon ordered recursion relation was 
implemented on the GPU. This gave a event generation speed on $10^6$-$10^8$
events per second for up to 12 gluon LO scattering amplitudes. The generation
speed was limited by the available fast accessible memory.
Adding massless quarks to the recursion relation does not alter the memory
requirements on the GPU. Therefore we can expect the same performance gain achieved
by using the GPU in the pure gluonic case. When using massive
quarks, more memory is required due to the complex valued
massive quark propagator and the performance gain will be
reduced.

The only change in the recursion relations is that the summations are no longer
over all partitions of the particles, but over ordered partitions (i.e. all summations
become linear sums).

\subsection{An algorithm for helicity summed color-ordered amplitudes}
\label{appendix:hel-sum}

To facilitate the evaluation of the helicity summed squared
ordered $n$-gluon amplitude, we modify the Berends-Giele recursion to simultaneously calculate 
all the helicity configurations for an ordered amplitude. 

The evaluation time of a full $n$-gluon helicity amplitude scales as \order{3^n},
with the memory usage scaling as \order{2^n}. Naively, to calculate the helicity summed
squared matrix element all the $2^n$ individual helicity 
amplitudes need to be evaluated. This results in a time scaling of \order{6^n}. 
By using more memory, \order{3^n} scaling, we can reduce the scaling of the evaluation
time to \order{5^n},
These are quit formidable running times together with the fact that the constant factor hidden 
in the big O notation is also quit sizable for realistic theories, 
constraining the number of legs for realistic calculations.

A notable exception is the BG-recursion for colored ordered amplitudes with a running time of \order{n^3} 
and a small constant, we can easily go to hundreds of external particles. 
Parton showers shower in the order of 20 external particles, 
so it would be advantageous to have a matrix element generator up to around 20 particles.

The naive way of combining BG-recursion with helicity summation would result in a \order{n^3 2^n} algorithm []. 
However storing results for helicity configuration gives a \order{n 2^n} algorithm at then expense of \order{2^n} memory usage.

\begin{figure}[t!]
  \centerline{\includegraphics[clip,width=0.77\columnwidth,angle=-90]
    {RecursionComparison.epsi}}
  \caption{\label{RecComp}
    ...}
\end{figure}

\begin{table}[t!]
\begin{center}\vskip4mm
\begin{tabular}{|c||c|c||c|c||c|}\hline
jets & \multicolumn{2}{c||}{naive} & \multicolumn{2}{c||}{helicity summed} &
gain\\& CPU (Sec) & Theory (sec) & CPU (Sec) & Theory (sec) &\\\hline 
2  & $7.0\times 10^{-6}$ & $1.1\times 10^{-5}$ & $4.0\times 10^{-6}$ & $9.6\times 10^{-6}$ & 1.8   \\
3  & $4.0\times 10^{-5}$ & $5.1\times 10^{-5}$ & $1.3\times 10^{-5}$ & $2.5\times 10^{-5}$ & 3.1   \\
4  & $1.8\times 10^{-4}$ & $2.1\times 10^{-4}$ & $3.4\times 10^{-5}$ & $6.4\times 10^{-5}$ & 5.3   \\
5  & $7.1\times 10^{-4}$ & $7.9\times 10^{-4}$ & $9.0\times 10^{-5}$ & $1.5\times 10^{-4}$ & 7.9   \\
6  & $2.5\times 10^{-3}$ & $2.7\times 10^{-3}$ & $2.2\times 10^{-4}$ & $3.5\times 10^{-4}$ & 11.4  \\
7  & $8.3\times 10^{-3}$ & $8.6\times 10^{-3}$ & $5.4\times 10^{-4}$ & $8.2\times 10^{-4}$ & 15.4  \\
8  & $2.5\times 10^{-2}$ & $2.6\times 10^{-2}$ & $1.3\times 10^{-3}$ & $1.8\times 10^{-3}$ & 19.6  \\
9  & $7.5\times 10^{-2}$ & $7.7\times 10^{-2}$ & $3.0\times 10^{-3}$ & $4.1\times 10^{-3}$ & 25.0  \\
10 & $2.1\times 10^{-1}$ & $2.2\times 10^{-1}$ & $6.8\times 10^{-3}$ & $9.0\times 10^{-3}$ & 31.2  \\
11 & $5.9\times 10^{-1}$ & $6.0\times 10^{-1}$ & $1.5\times 10^{-2}$ & $2.0\times 10^{-2}$ & 38.1  \\
12 & $1.5\times 10^{ 0}$ & $1.6\times 10^{ 0}$ & $3.4\times 10^{-2}$ & $4.3\times 10^{-2}$ & 46.2  \\
13 & $4.2\times 10^{ 0}$ & $4.3\times 10^{ 0}$ & $7.6\times 10^{-2}$ & $9.2\times 10^{-2}$ & 55.0  \\
14 & $1.0\times 10^{ 1}$ & $1.1\times 10^{ 1}$ & $1.7\times 10^{-1}$ & $2.0\times 10^{-1}$ & 64.9  \\
15 & $2.8\times 10^{ 1}$ & $2.8\times 10^{ 1}$ & $3.8\times 10^{-1}$ & $4.2\times 10^{-1}$ & 73.1  \\
16 & $7.2\times 10^{ 1}$ & $7.1\times 10^{ 1}$ & $8.1\times 10^{-1}$ & $8.9\times 10^{-1}$ & 88.5  \\
17 & $1.8\times 10^{ 2}$ & $1.8\times 10^{ 2}$ & $1.9\times 10^{ 0}$ & $1.9\times 10^{ 0}$ & 95.4  \\
18 & $4.4\times 10^{ 2}$ & $4.3\times 10^{ 2}$ & $3.9\times 10^{ 0}$ & $4.0\times 10^{ 0}$ & 113.6 \\
19 & $1.0\times 10^{ 3}$ & $1.0\times 10^{ 3}$ & $8.1\times 10^{ 0}$ & $8.3\times 10^{ 0}$ & 130.2 \\\hline
\end{tabular}
\caption{\label{tab2}
  Theory time for naive $(1.2\times 10^{-7})\times (n+2)^4\,
  2^{n+1}/24$, Theory time for helicity summed $(1.0\times
  10^{-7})\times 4\,(n+1)\, 2^{n+1}$.}
\end{center}
\end{table}

The BG-recursion can be written
\begin{equation}
A(l, s) = \sum_{k=1}^{l-1} \{A(k, s), A(l-k, s+k)\},
\end{equation}
here $A(l, s)$ is the partial amplitude of particles $s,\ldots, s+l-1$. 
The partial current consists of a current $J_\mu$ and an auxiliary anti-symmetric tensor $W_{\mu\nu}$. 
The bracket is given by
\begin{equation}
\begin{split}
A &= \{A_1, A_2\}\\
J_\mu &= (p_2-p_1)_\mu J_1\cdot J_2 - (2 p_2+p_1)\cdot J_1 J_{2\mu} + (2 p_1+p_2)\cdot J_2 J_{1\mu} + W_{2\mu}\cdot J_1 - W_{1\mu}\cdot J_2\\
W_{\mu\nu} &= J_{1\mu} J_{2\nu} - J_{1\nu} J_{2\mu}.
\end{split}
\end{equation}
In this form the number of brackets to be evaluated in the recursion for $n+1$ external particles 
(so $n$ particles in the recursion). Is given by
\begin{equation}
\sum_{l=2}^n \sum_{s=1}^{n+1-l} \sum_{k=1}^{l-1} 1=\sum_{l=2}^n (n+1-l)(l-1)=\frac{n^3-n}6,
\end{equation}
hence a \order{n^3} algorithm. The storage requirement is given by
\begin{equation}
\sum_{l=1}^n \sum_{s=1}^{n+1-l} 1=\frac{n^2+n}2.
\end{equation}
In this algorithm we calculate the partial amplitudes for all helicity configurations. The recursion
becomes 
\begin{equation}
A(l, s, \{h_{s\ldots s+l-1}\}) = \sum_{k=1}^{l-1} \{A(k, s, \{h_{s\ldots s+k-1}\}), A(l-k, s+k, 
\{h_{s+k\ldots s+l-1}\})\},
\end{equation}
We store the amplitudes into one big array, first the amplitudes for $l=1$ then $l=2$ etc. 
The number of helicity configuration for a particular $l$ is of course $2^l$. 
This means that we can index
a particular amplitude in the big array by the following formula
\begin{equation}
\text{index}(l, s, h) = (n-l+s+3)2^l-2n+h-4.
\end{equation}
We store the helicity configuration as a bit mask $h$ which ranges from $0\ldots 2^l-1$. The total memory usage is thus
\begin{equation}
\text{index}(n+1, 0, 0) = 4\, 2^n-2 n - 4
\end{equation},
which \order{2^n}. The running time is given by
\begin{equation}
\sum_{l=2}^n \sum_{s=1}^{n+1-l} \sum_{k=1}^{l-1} 2^l=\sum_{l=2}^n (n+1-l)(l-1) 2^l=4 n 2^n-12\, 2^n+4 n-12,
\end{equation}
which is \order{n 2^n}. With $n=20$ this amounts to $\approx 2\, 10^7$ bracket evaluations, 
something very doable on modern machines. This ways wins a factor $n^2$ against the naive implementation.
My first implementation requires $\approx 6 \text{sec}$ on a 20 leg amplitude ($n=19$).


\section{The TESS Monte Carlo}

Algorithmic layout, assembly issues, raw speed of event evaluation.


\subsection{TESLA vs FERMI GPU}

\begin{table}[t!]
\begin{center}\vskip4mm
\begin{tabular}{|c||c|c||c|c||}\hline
n & \multicolumn{2}{c||}{C1060} & \multicolumn{2}{c||}{C2050} \\\hline
  & events/MP & events/GPU & events/MP & events/GPU \\\hline 
4  & 102 & 3072 & 306 & 4284 \\\hline
5  &  68 & 2048 & 204 & 2856 \\\hline
6  &  48 & 1462 & 146 & 2044 \\\hline
7  &  36 & 1097 & 108 & 1512 \\\hline
8  &  28 &  853 &  84 & 1176 \\\hline
9  &  22 &  682 &  68 &  952 \\\hline
10 &  18 &  558 &  54 &  756 \\\hline
11 &  15 &  465 &  46 &  644 \\\hline
12 &  13 &  393 &  38 &  532 \\\hline
\end{tabular}
\caption{\label{threads_per_MP}
The number of events evaluated on a single MP and the GPU device in parallel.
}
\end{center}
\end{table}
\begin{table}[t!]
\begin{center}\vskip4mm
\begin{tabular}{|c||c|c||c|c|c||}\hline
n  & \multicolumn{2}{c||}{C1060} & \multicolumn{3}{c||}{C2050} \\\hline
   & time/event (sec) & gain CPU & time/event (sec) & time/event (sec) & gain CPU \\
   & & & & improved & \\\hline 
4  & $2.97\times 10^{-8}$ & 294 & $1.66\times 10^{-8}$ & $1.05\times 10^{-8}$ & 833 \\\hline
5  & $4.44\times 10^{-8}$ & 281 & $3.00\times 10^{-8}$ & $1.91\times 10^{-8}$ & 654 \\\hline
6  & $8.55\times 10^{-8}$ & 230 & $5.36\times 10^{-8}$ & $3.38\times 10^{-8}$ & 582 \\\hline
7  & $2.30\times 10^{-7}$ & 132 & $9.90\times 10^{-8}$ & $5.51\times 10^{-8}$ & 554 \\\hline
8  & $3.55\times 10^{-7}$ & 133 & $1.62\times 10^{-7}$ & $8.93\times 10^{-8}$ & 531 \\\hline
9  & $4.27\times 10^{-7}$ & 170 & $1.96\times 10^{-7}$ & $1.26\times 10^{-7}$ & 576 \\\hline
10 & $6.82\times 10^{-7}$ & 153 & $2.88\times 10^{-7}$ & $1.99\times 10^{-7}$ & 522 \\\hline
11 & $9.75\times 10^{-7}$ & 157 & $4.14\times 10^{-7}$ & $2.90\times 10^{-7}$ & 528 \\\hline
12 & $1.36\times 10^{-6}$ & 158 & $5.85\times 10^{-7}$ & $4.15\times 10^{-7}$ & 513 \\\hline
\end{tabular}
\caption{\label{gpu_timing}
The gain over the CPU is using a single core of the xxx CPU. The 4 to 8 gluon case was compiled
with a restriction of 32 registers to be used such that we can run with 2 blocks per MP. 
}
\end{center}
\end{table}
In the previous paper~\cite{Giele:2010ks} we used the C1060 GPU to calculate
the gluonic LO contribution to multi-jet production. In this subsection we
will adapt the algorithm to run on the C2050 GPU. From the viewpoint of the
MC generation the only relevant information is the shared memory size. This
memory controls the maximum number of events we can run on a Multi-Processor.
The C1060 GPU has 30 Multi-Processors, each with 16,384 bytes of shared memory and 1024 threads.
The C2050 GPU has 14 Multi-Processors, each with 49,152 bytes of shared memory and 1536 threads.
Because of this different hardware layout, the number of events which can be evaluated in
parallel changes. The threads are organized in blocks. Each block can hold up to 512 threads.
Per $n$-gluon event we use $n-1$ threads. The number of events we can run on one Multi-Processor
is determined by the shared memory. Given a shared memory size in Kb of $M_{\mbox{\tiny shared}}$
we can process $M_{\mbox{\tiny shared}}/(8\times n(n+1))$ events per Multi-Processor. This gives us
$M_{\mbox{\tiny shared}}\times (n-1)/(8\times n(n+1))$ threads used per Multi-Processor. These results
are tabulated in Table~\ref{threads_per_MP}. Because the number of threads per MP exceeds 512 for the
C2050 GPU, we divided the events in two equally sized blocks. The gain in performance expected solely from
the increase in evaluated events is around a factor of 1.4. 
Other hardware improvements will make this a lower bound on the evaluation time improvement per event.

In Table~\ref{gpu_timing} we show the effective evaluation time/event in seconds and the gains obtained
over the CPU (and the older GPU). We see that a cloud of 364 top of the line cores is needed to compete
with the GPU evaluation speed.

\section{Validation of the TESS Monte Carlo}

Use~\cite{Draggiotis:2002hm} for 4 and 5 jets.

Use~\cite{Gleisberg:2008fv} for 2, 3, 4, 5, 6, 7 and 8 jets.

\section{Predictions and Data Comparison using the TESS Monte Carlo}

Atlas note with data on multijets:~\cite{ATLAS-CONF-2010-084}.

\section{Conclusions}

\section*{Acknowledgments}

Fermilab is operated by Fermi Research Alliance, LLC, under contract
DE-AC02-07CH11359 with the United States Department of Energy.



% ======= appendices ===========================================================

%\appendix

% ======= bibliography =========================================================



\bibliographystyle{JHEP}%{amsunsrt_mod}
{\raggedright\bibliography{tes3}}
\vspace*{0mm}\noindent\hrulefill



% ======= the end ==============================================================



%\end{fmffile}
\end{document}
