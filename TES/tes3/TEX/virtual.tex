\documentclass[11pt,onecolumn,twoside,notitlepage,fleqn,letterpaper,final]
	      {article}[]

\input{texsetup.tex}
\newcommand{\order}[1]{$\mathcal O(#1)$}
\def\query#1{{\red{\bf[QUERY]}\quad\textbf{\textit{ #1}}}}

\preprint{FERMILAB-PUB-11-XXX-T}
\author{\hphantom{MisterX}\\[0mm]\sffamily\bfseries\normalsize%
  Walter~T.~Giele\thanks{giele@fnal.gov}
  \,\! and \
  Gerben~C.~Stavenga\thanks{stavenga@fnal.gov}
  \\[4mm]
  {\small\sl Theoretical Physics Department}\\[-2mm]
  {\small\sl Fermi National Accelerator Laboratory, Batavia, IL 60510, USA}
  \\[9mm]
}
\title{\sffamily\bfseries\Large%
  A GPU-based Evaluation of One-Loop Amplitudes\\
\institute{Theoretical Physics Department, Fermi National Accelerator
  Laboratory, Batavia, IL 60510, USA}}

\begin{document}
\renewcommand{\baselinestretch}{1.4}
\maketitle
\thispagestyle{empty}
%\begin{flushleft}\vspace*{-130mm}\date{\today}\\[127mm]\end{flushleft}
\begin{flushright}
  \vspace*{-128mm}
  {\small FERMILAB-PUB-11-xxx-T}\\[131mm]
\end{flushright}

\renewcommand{\baselinestretch}{1.1}
\begin{abstract}
\noindent
A quick and accurate evaluation of one-loop amplitudes is essential
for the development of reliable Next-to-Leading order event generators.
By abandoning the last vestige of analytic unitarity methods,
we use the GPU linear
algebra libraries to determine all coefficients in the parametric form 
of the integrand at once. The advantages are numerous, no complex loop momenta
are required and numerical stability is enhanced to the level not even double
precision accuracy is required.

We will demonstrate the capabilities of the GPU based method by evaluating
multi-parton one-loop amplitudes up to 15 external partons.

\end{abstract}

\clearpage%\newpage
\renewcommand{\baselinestretch}{0.7}
\tableofcontents
\thispagestyle{empty}
%\clearpage
\renewcommand{\baselinestretch}{1.04}
\vspace*{4mm}
\noindent\hrulefill
\vspace*{7mm}



% ======= main ================================================================



\section{Introduction}\label{sec:Intro}

A NLO parton level Monte Carlo has to be a black-box application
for a public version to provide useful phenomenology. 
This requires from the algorithms simplicity, accuracy, stability  and sufficient speed.   
See e.g. LO MC's and MCFM.

This goal becomes difficult at NLO for processes with 4 or more final state particles.
Often farms of order 100's of nodes are requires for sufficient accuracy.
The evaluation of the virtual corrections using generalized unitarity based on the
OPP parametric integration techniques does not give a particularly stable algorithmic
procedure. This is reflected in the accuracy, where it is difficult to detect accuracy
problems and if found one is required to recalculate the loop amplitude using quadrupole
(or even higher) precision.

This is in contrast to a program like MCFM, where mostly analytic answers are used for
the one-loop amplitudes. This leads to stability and the code can be made public.

In this paper we outline a method which uses a GPU in conjunction with a CPU to
evaluate one-loop amplitudes using the OPP method. The resulting algorithm is
simple, accurate and stable, suitable for usage in public NLO Monte Carlo's. 
It is sufficiently fast for use on a desktop computers for NLO predictions
involving scattering which include high multiplicities of jets.

This method will be one of the cornerstones upon which we build the NLO parton level
Monte Carlo TESS. We already outlined the other cornerstones.
The LO part of the TESS Monte Carlo and the 
bremsstrahlung phase space integration techniques, in previous publications.

\section{The Parametric Form of the One-Loop Integrand}

The number of parameters,,needed to describe the 4-dimensional parametric form of
an $n$-particle ordered one-loop amplitude is given by
\beqa
{\cal N}_{0}^{(4)}(n)&=&
 2\times\left(\begin{array}{c} n \\ 4\end{array}\right) 
+7\times\left(\begin{array}{c} n \\ 3\end{array}\right) 
+9\times\left(\begin{array}{c} n \\ 2\end{array}\right) 
+5\times\left(\begin{array}{c} n \\ 1\end{array}\right) 
+1\times\left(\begin{array}{c} n \\ 0\end{array}\right) \nn
&=& \frac{1}{12}(n+1)(n+2)^2(n+3)
\eeqa
By applying a one-cut or a two-cut we can reduce the number
of parameters to be determined per cut significantly. 
For the one-cut we have $n$ cuts, each with 
${\cal N}_1^{(4)}=\frac{1}{6}(n+1)(n+2)(2n+3)$ parameters.
For the two-cut we have $\frac{1}{2}n(n-1)$ cuts, each
with ${\cal N}_2^{(4)}=(n+1)^2$ parameters. 
However, different cuts can have the same higher point integral
coefficients. This reduces the number of parameters to be solved
from the number of parameters in the parametric form.

\section{Timing}
See table~\ref{timing}
\begin{table}[ht!]
\begin{center}\vskip4mm
\begin{tabular}{|c||c|c|c||c|c|c||c|c|c|}\hline
 &\multicolumn{3}{c||}{no-cut} &\multicolumn{3}{c||}{single-cut} &\multicolumn{3}{c|}{double-cut}\\ \cline{2-10}
$n$ & ${\cal N}_{0}$ & GPU & MKL & ${\cal N}_{1}$ &GPU & MKL & ${\cal N}_{2}$ & GPU & MKL \\ \hline
3 & 50 & & & & & & &&\\
4 & 105 & & & & & & &&\\
5 & 196 & 0.01 & & & & & &&\\
6 & 336 & 0.01 & & & & & &&\\
7 & 540 & 0.01 & & & & & &&\\
8 & 825 & 0.02 & 0.01 & & & & &&\\
9 & 1,210 & 0.03 & 0.03 & & & & &&\\
10 & 1,716 & 0.05 & 0.07  & & & & &&\\
11 & 2,366 & 0.08 & 0.17 & & & & &&\\
12 & 3,185 & 0.14 & 0.39 & & & & &&\\
13 & 4,200 & 0.24 & 0.81 & & & & &&\\
14 & 5,440 & 0.43 & 1.67 & & & & &&\\
15 & 6,936 & 0.70 & 3.41 & & & & &&\\
16 & 8,721 & 1.29 & 6.59 & & & & &&\\
17 & 10,830 & 2.21 & 12.49 & & & & &&\\
18 & 13,300 & 3.72 & 22.81 & & & & &&\\
19 & 16,170 & 6.30 & 40.25 & & & & &&\\ 
20 & 19,481 & 10.60 & 69.19 & & & & &&\\ 
21 & 23,276 & 17.05 & 117.89 & & & & &&\\ 
\hline
\end{tabular}
\caption{\label{timing}
  Listed are the matrix inversion time for a $n$-particle ordered one-loop
  amplitude, together with 
  the evaluation time for a $n$-gluon, a $q\overline{q}+(n-2)$ gluon 
  and a $q\overline{q}$ $+W$-boson $+(n-3)$ gluon ordered polarized one-loop amplitude. 
  The partonic processes were generated on the CPU using a BG-recursion relation, 
  while the matrix inversion was performed the C2050 NVIDIA GPU.
}
\end{center}
\end{table}

\section{Accuracy and Stability}

Add figure with timing of table 1 and ``improved'' fixed accuracy timing 
(i.e. relative error is less than $\epsilon$ for several values) for e.g.
10,000 one-loop evaluations using Rambo for phase space generation.

\section{Conclusions}


\section*{Acknowledgments}

Fermilab is operated by Fermi Research Alliance, LLC, under contract
DE-AC02-07CH11359 with the United States Department of Energy.



% ======= appendices ===========================================================

%\appendix

% ======= bibliography =========================================================



\bibliographystyle{JHEP}%{amsunsrt_mod}
{\raggedright\bibliography{tes3}}
\vspace*{0mm}\noindent\hrulefill



% ======= the end ==============================================================



%\end{fmffile}
\end{document}
