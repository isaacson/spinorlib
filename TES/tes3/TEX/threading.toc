\contentsline {section}{\numberline {1}The Generic Algorithm}{1}
\contentsline {section}{\numberline {2}Phase Space}{4}
\contentsline {subsection}{\numberline {2.1}Invertible Branchers}{5}
\contentsline {subsubsection}{\numberline {2.1.1}The final-final state particle brancher}{6}
\contentsline {subsubsection}{\numberline {2.1.2}The initial-final state particle brancher}{8}
\contentsline {section}{\numberline {3}Using the GPU in the Calculation of Virtual Corrections}{9}
