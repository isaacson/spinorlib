\documentclass[nohyper]{JHEP3}
\let\ifpdf\relax
\usepackage{ifpdf}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{slashed}
\usepackage{graphicx}
\usepackage{verbatim}
% \usepackage{subfigure}
\usepackage{cite}

\newcommand{\order}[1]{$\mathcal O(#1)$}


\title{Vertex rules of the recursion}

\author{Gerben Stavenga\\
Fermi National Accelerator Laboratory, Batavia, IL 60510, USA\\
 E-mail: \email{stavenga@fnal.gov}}
\author{Walter Giele\\
Fermi National Accelerator Laboratory, Batavia, IL 60510, USA\\
 E-mail: \email{giele@fnal.gov}}


\abstract{}

\keywords{}

\preprint{}

\begin{document}

\section{Lagrangian}

We have a $\text{SU}(N)$ gauge theory coupled to a fundamental fermion. We use the following conventions
\begin{equation}
\begin{split}
g^{\mu\nu}&=\text{diag}(+,-,-,-)\\
A_\mu&=A_\mu^a T^a ,\, G_{\mu\nu}=G_{\mu\nu}^a T^a\\
[T^a, T^b]&=i f^{abc} T^c ,\, \text{Tr}[T^a T^b]= T(F) \delta^{ab}=\frac12 \delta^{ab}\\
D_\mu&=\partial_\mu-i A_\mu\\
G_{\mu\nu}&=i[D_\mu, D_nu]=\partial_\mu A_\nu-\partial_\nu A_\mu-i[A_\mu,A_\nu]\\
G_{\mu\nu}^a&=\partial_\mu A_\nu^a-\partial_\nu A_\mu^a+f^{abc} A_\mu^b A_\nu^c\\
\end{split}
\end{equation}
The Lagrangian is given by
\begin{equation}
{\mathcal L}=-\frac14 G_{\mu\nu}^a G^{\mu\nu a} + \bar{\psi} i\slashed{D}\psi
\end{equation}
The quadratic part of the gauge field Lagrangian is
\begin{equation}
\frac12 A_\mu^a(-p^2 g^{\mu\nu}+p^\mu p^\nu) A_\nu^a
\end{equation}
Therefore the propagator is (Feynman gauge)
\begin{equation}
\langle A_\mu(p)^a A_\nu(-p)^b\rangle=-i\frac{g_{\mu\nu}}{p^2}\delta^{ab}
\end{equation}
The 3-vertex piece of the gauge field Lagrangian is
\begin{equation}
i \frac12 (p_1^{\mu_2} g^{\mu_3 \mu_1}-p_1^{\mu_3} g^{\mu_1 \mu_2}) f^{a_1 a_2 a_3} A_{\mu_1}^{a_1} A_{\mu_2}^{a_2}A_{\mu_3}^{a_3}
\end{equation}
This then becomes the folowing gluon 3-vertex
\begin{multline}
- \sum_\text{cycl. perm.} (p_1^{\mu_2} g^{\mu_3 \mu_1}-p_1^{\mu_3} g^{\mu_1 \mu_2}) f^{a_1 a_2 a_3}=\\
\big((p_1-p_2)^{\mu_3} g^{\mu_1 \mu_2}+(p_2-p_3)^{\mu_1} g^{\mu_2 \mu_3}+(p_3-p_1)^{\mu_2} g^{\mu_3 \mu_1}\big) f^{a_1 a_2 a_3}
\end{multline}
The 4-vertex piece of the gluon Lagrangian is
\begin{multline}
-\frac14 f^{eab} A_\mu^a A_\nu^b f^{ecd} A^{\mu c} A^{\nu d}=\\
-\frac18 f^{ea_1a_2} A_{\mu_1}^{a_1} A_{\mu_2}^{a_2}(g^{\mu_1 \nu_1}g^{\mu_2 \nu_2}-g^{\mu_1 \nu_2}g^{\mu_2 \nu_1}) 
f^{eb_1b_2} A_{\nu_1}^{b_1} A_{\nu_2}^{b_2}
\end{multline}
The vertex is thus
\begin{equation}
-i f^{ea_1a_2} (g^{\mu_1 \nu_1}g^{\mu_2 \nu_2}-g^{\mu_1 \nu_2}g^{\mu_2 \nu_1}) f^{eb_1b_2}
\end{equation}

This is a 4-vertex with 8 as symmetry factor (swap first pair, swap second pair and swap the pairs). This 4-vertex has
a natural structure as two 3-vertices tied together with an auxiliary field which is an anti-symmetric two tensor with an adjoint color index.

For the Fermion (quark) we use the Majorana basis $\gamma^\mu=i\lambda^\mu$, where the $\lambda$'s are completely real.
\begin{equation}
\lambda^0=\left(
\begin{array}{cccc}
0 & 0 & 0 & -1 \\
0 & 0 & 1 & 0 \\
0 & -1 & 0 & 0 \\
1 & 0 & 0 & 0\end{array}\right)\,
\lambda^1=\left(
\begin{array}{cccc}
1 & 0 & 0 & 0 \\
0 & -1 & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & -1 \end{array}\right)\,
\lambda^2=\left(
\begin{array}{cccc}
0 & 0 & 0 & 1 \\
0 & 0 & -1 & 0 \\
0 & -1 & 0 & 0 \\
1 & 0 & 0 & 0 \end{array}\right)\,
\lambda^3=\left(
\begin{array}{cccc}
0 & -1 & 0 & 0 \\
-1 & 0 & 0 & 0 \\
0 & 0 & 0 & -1 \\
0 & 0 & -1 & 0 \end{array}\right)
\end{equation}

The quark part splits up as quadratic part
\begin{equation}
\bar{\psi}(-p) \slashed{p}\psi(p)
\end{equation}
This means that the Fermion propagator is given by
\begin{equation}
\langle \psi(p)\times\bar{\psi}(-p)\rangle=i\frac{\slashed p}{p^2}=\frac{-\lambda^\mu p_\mu}{p^2}
\end{equation}
The interaction part of the Lagrangian is given by
\begin{equation}
\bar{\psi}\slashed{A}\psi=(\gamma^\mu)^i_j (T^a)^{mn} \bar{\psi}_i^m \psi^{jn}  A_\mu^a
\end{equation}
The vertex is thus
\begin{equation}
-(\lambda^\mu)^i_j (T^a)^{mn}
\end{equation}

\section{recursion}

We want to evaluate $\langle O_1(p_1)\ldots O_{n+1}(p_{n+1})\rangle$ at tree level and connected. If $n=1$ it is just a propagator
and we are done. Otherwise operator $O_{n+1}$ is connected to a vertex, which is always a 3-vertex. Therefore is $O_{n+1}=A_\mu^a$ we have
\begin{equation}
\begin{split}
J_\mu^a=\langle O_1\ldots O_n A_\mu^a\rangle
&=\sum_{L,R} \pi(L,R) V_3 \langle L A_{\mu_1}^{a_1} \rangle \langle R  A_{\mu_2}^{a_2}\rangle \langle A_{\mu_3}^{a_3} A_\mu^a\rangle\\
&+\sum_{L,R} \pi(L,R) V_4 \langle L A_{\nu_1}^{b_1} A_{\nu_2}^{b_2} V_4\rangle \langle R  A_{\mu_2}^{a_2}\rangle \langle A_{\mu_1}^{a_1} A_\mu^a\rangle\\
&+\sum_{L,R} \pi(L,R) V_4 \langle L  A_{\mu_2}^{a_2}\rangle \langle R A_{\nu_1}^{b_1} A_{\nu_2}^{b_2} V_4\rangle \langle A_{\mu_1}^{a_1} A_\mu^a\rangle\\
&+\sum_{L,R} \pi(L,R) V_{ff} \langle L  \psi\rangle \langle R \bar{\psi}\rangle \langle A_{\mu_1}^{a_1} A_\mu^a\rangle\\
&-\sum_{L,R} \pi(L,R) V_{ff} \langle L  \bar{\psi}\rangle \langle R \psi\rangle \langle A_{\mu_1}^{a_1} A_\mu^a\rangle
\end{split}
\end{equation}
Here we sum over all partitions of the set of operators in a left and right subset. The left subset always contains the first
operator $O_1$, therefore we are not over counting. $\pi(L, R)$ gives the sign to permute the ordered list of operators into $L$ and $R$. The above notation is symbolic the indices of the different correlators have to be contracted according to the vertex prescription denoted by $V$.

The recursion formula are now given by:
The gluon 3-vertex contribution ($P=p_L+p_R$)
\begin{equation}
\begin{split}
J_\mu^a&=-i\frac{g_{\mu\mu_3}}{P^2} f^{a_1a_2a}\left\{(p_L-p_R)^{\mu_3} g^{\mu_1\mu_2}+(p_R+P)^{\mu_1} g^{\mu_2\mu_3}-(P+p_L)^{\mu_2} g^{\mu_3\mu_1}\right\}J_{L\mu_1}^{a_1}J_{R\mu_2}^{a_2}\\
J_\mu&=-\frac{1}{P^2}\left\{(p_L-p_R)_\mu [J_{L\nu}, J_R^\nu]+[(p_R+P)\cdot J_L, J_{R_\mu}]-[J_{L\mu}, (P+p_L)\cdot J_{R\mu}]\right\}\\
&=\frac{1}{P^2}\left\{(p_R-p_L)_\mu [J_{L\nu}, J_R^\nu]+[J_{R_\mu},(p_R+P)\cdot J_L]+[J_{L\mu},(P+p_L)\cdot J_{R\mu}]\right\}
\end{split}
\end{equation}
The gluon 4-vertex contribution,
\begin{equation}
\begin{split}
J_\mu^a&=-i\frac{g_{\mu\mu_1}}{P^2} 
\left\{-i f^{eaa_2} \langle L A_{\nu_1}^{b_1} A_{\nu_2}^{b_2} f^{eb_1 b_2} (g^{\mu_1 \nu_1}g^{\mu_2 \nu_2}-g^{\mu_1 \nu_2}g^{\mu_2 \nu_1})\rangle J_{R\mu_2}^{a_2}\right\}+L\leftrightarrow R \\
J_\mu&=\frac{g_{\mu\mu_1}}{P^2} 
\left\{[T^{a_2}, T^e] \langle L A_{\nu_1}^{b_1} A_{\nu_2}^{b_2} i f^{eb_1 b_2} (g^{\mu_1 \nu_1}g^{\mu_2 \nu_2}-g^{\mu_1 \nu_2}g^{\mu_2 \nu_1})\rangle J_{R\mu_2}^{a_2}\right\}+L\leftrightarrow R \\
J_\mu&=\frac{g_{\mu\mu_1}}{P^2}\left[J_{R\mu_2}, W_L^{\mu_1\mu_2}\right]+\left[J_{L\mu_2}, W_R^{\mu_1\mu_2}\right]\\
&=\frac{1}{P^2}\left[J_R^\nu, W_{L\mu\nu}\right]+\left[J_L^\nu, W_{R\mu\nu}\right]\\
W_L^{\mu_1\mu_2}&=\langle L A_{\nu_1}^{b_1} A_{\nu_2}^{b_2}\rangle [T^{b_1}, T^{b_2}] (g^{\mu_1 \nu_1}g^{\mu_2 \nu_2}-g^{\mu_1 \nu_2}g^{\mu_2 \nu_1})
\end{split}
\end{equation}
Here we introduced the auxiliary field $W$, which was mentioned earlier. For $W$ there exists naturally a completely analogous recursion
\begin{equation}
\begin{split}
W^{\mu_1\mu_2}&=\sum_{L,R} \pi(L,R) \langle L A_{\nu_1}^{b_1} \rangle \langle R  A_{\nu_2}^{b_2}\rangle [T^{b_1}, T^{b_2}] (g^{\mu_1 \nu_1}g^{\mu_2 \nu_2}-g^{\mu_1 \nu_2}g^{\mu_2 \nu_1})\\
W^{\mu_1\mu_2}&=\sum_{L,R} \pi(L,R) \left[J_{L\nu_1}, J_{R\nu_1}\right](g^{\mu_1 \nu_1}g^{\mu_2 \nu_2}-g^{\mu_1 \nu_2}g^{\mu_2 \nu_1})\\
W_{\mu\nu}&=\left[J_{L\mu}, J_{R\nu}\right]+\left[J_{R\mu}, J_{L\nu}\right]
\end{split}
\end{equation}
The fermion contribution to the gluon current is
\begin{equation}
\begin{split}
J_\mu^a=-i\frac{g_{\mu\nu}}{P^2} \left(-\lambda^\nu\right)^i_j (T^a)^{mn} \left\{\langle L \psi^{nj}\rangle \langle R \bar{\psi}_i^m\rangle-\langle L \bar\psi_i^m\rangle \langle R \psi^{nj}\rangle\right\}\\
J_\mu^a=i\frac{g_{\mu\nu}}{P^2} \left(\lambda^\nu\right)^i_j (T^a)^{mn} \left\{\psi_L^{nj}\bar{\psi}_{Ri}^m-\bar\psi_{Li}^m \psi_R^{nj}\rangle\right\}
\end{split}
\end{equation}
This is anti-symmetric interchanging $L$ and $R$, but this compensated by the anti-symmetry of $\pi(L,R)$. Also note that one of the two terms above is always zero due to current conservation. So we assume without loss of generality that the second term is zero (otherwise we exchange $L,R$). If we contract the current with the color generators, we have 
\begin{equation}
T^a (T^a)^{mn} \psi_L^{nj}\bar{\psi}_{Ri}^m=T^a Tr_C [T^a \psi_L^j\times\bar{\psi}_{Ri}].
\end{equation}
Because $1, \{T^a\}$ is a basis of all matrices and $Tr[T^a T^b]=T \delta^{ab}$, this becomes
\begin{equation}
T^a(T^a)^{mn} \psi_L^{nj}\bar{\psi}_{Ri}^m=T[\psi_L^j\times\bar{\psi}_{Ri}]_\text{traceless}.
\end{equation}
So it symplifies to
\begin{equation}
\label{eq_gluon_ff}
J_\mu=i\frac{1}{P^2} \left(\lambda_\mu\right)^i_j T[\psi_L^j\times \bar{\psi}_{Ri}]_\text{traceless}
\end{equation}
Now we are left with the quark amplitudes, schematically for the anti-quark
\begin{equation}
\bar\psi=\langle O_1\ldots O_n \bar\psi(-P)\rangle=\sum_{F, B} \pi(F, B) V_3\langle F\bar\psi(-p_F)\rangle \langle B A(-p_B)\rangle \langle \psi(P)\bar\psi(-P) \rangle.
\end{equation}
Here instead of left and right subsets we define the left set to be fermionic and the right to be bosonic.
The precise version, suppressing the sum and the sign
\begin{equation}
\begin{split}
\bar\psi_k^o&=\frac{(-P_\nu \lambda^\nu)^j_k}{P^2}\delta^{on} (-\lambda^\mu)^i_j (T^a)^{mn} \bar\psi^m_{Fi} J_{B\mu}^a \\
\bar\psi^o&=\bar\psi^m_{F}\lambda^\mu J_{B\mu}^{mo}\frac{(P_\nu \lambda^\nu)}{P^2}
\end{split}
\end{equation}
And for the quark
\begin{equation}
\begin{split}
\psi=\langle O_1\ldots O_n \psi(-P)\rangle&=\sum_{F, B} \pi(F, B) V_3\langle F\psi(-p_F)\rangle \langle B A(-p_B)\rangle \langle \psi(-P)\bar\psi(P) \rangle\\
\psi^{ko}&=\frac{(P_\nu \lambda^\nu)^k_i}{P^2}\delta^{mo} (-\lambda^\mu)^i_j (T^a)^{mn} \psi^{nj}_F J_{B\mu}^a \\
\psi^o&=\frac{(-P_\nu \lambda^\nu)}{P^2} J_{B\mu}^{on}\lambda^\mu\psi^n_{F} 
\end{split}
\end{equation}
Note the relative minus sign due to the fact that momentum flows in opposite direction with respect to the charge.

\section{Complex numbers}

In the above formulas in the Majorana basis there is only one $i$ present \eqref{eq_gluon_ff}. However if one inspects the recursion for $\psi$ we see that one can freely multiply $\psi$ with a constant without changing that relation. Therefore if we redefine $\psi\rightarrow -i\psi$ \eqref{eq_gluon_ff} becomes
\begin{equation}
\label{eq_gluon_ff_real}
J_\mu=\frac{1}{P^2} \left(\lambda_\mu\right)^i_j T[\psi_L^j\times \bar{\psi}_{Ri}]_\text{traceless}
\end{equation}
Now all the recursion steps are real. We can take the gluon polarization real, the color states are either real or completely imaginary (which is an inconsequential overall factor of $i$) and we can take the spinor polarization real (Majorana basis). The complete recursion becomes real which is a sizeable optimalization in both memory and speed.

\section{Color ordered}

If we inspect the gluon recursion relations than we see that all recursions relation consist of commutators. If we don't compute the commutators but work them out in a giant polynomial consisting of the initial gluon color states, we get all kinds of different orderings. The gluon quark-antiquark recursion is a little bit different because the quark is always left and the antiquark right.
The quark gluon-quark orders the gluon always left and the antiquark antiquark-gluon orders the gluon always right. In the end we
get a huge polynomial with different orderings of the external color factors. The ordering consist of gluon color factors interspersed with $\psi\times\bar\psi$ pairs.
Suppose we want to calculate the coefficient of a particular ordering we can order the external legs according to such an ordering. The recursion relations then simplify to
\begin{equation}
\begin{split}
J_\mu&=\frac1{P^2}\{(p_R-p_L)_\mu J_L\cdot J_R+2J_L p_L\cdot J_R-2J_R p_R\cdot J_L+W_R\cdot J_L-W_L\cdot J_R\\
&+T\bar\psi_R\lambda_\mu\psi_L \}\\
W&=J_L\times J_R-J_R\times J_L\\
\bar\psi&=\bar\psi_F\lambda^\nu J_{B\mu} \frac{(P_\nu \lambda^\mu)}{P^2}\\
\psi&=\frac{(-P_\nu \lambda^\nu)}{P^2}J_{B\mu} \lambda^\mu \psi_F
\end{split}
\end{equation}


\section{Implementation notes Majorana matrices}

The Majorna matrices are just permutation matrices except for a possible sign. Therefore multiplying them is just reshuffling making it much faster.


\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
