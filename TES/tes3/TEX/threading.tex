%%%%%%%%%%%%%%%%%%%%%%%%%%%% FOR JHEPcls 3.1.0 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt]{JHEP3}

%\JHEP{00(2002)000}
%\JHEPspecialurl{http://jhep.sissa.it/JOURNAL/JHEP3.tar.gz}
%

\usepackage{amsmath,amssymb,amsfonts}
\usepackage{epsfig,multicol}
\usepackage[square, comma, sort&compress]{natbib}


\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage{epsfig}

%   ...                                                                    %
\newcommand{\beq}{\begin{equation}}
\newcommand{\eeq}{\end{equation}}
\newcommand{\beqa}{\begin{eqnarray}}
\newcommand{\eeqa}{\end{eqnarray}}
\newcommand{\nn}{\nonumber \\}
\newcommand{\comment}[1]{\textbf{[#1]}\\}
%\setlength{\topmargin}{7mm}
%   ...                                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\jhepname}{\raisebox{-4pt}{\epsfig{file=JHEPlogo.eps, width=2.5em}}}
\title{Notes on invertable phase space and virtual corrections using GPU's}
\author{W.~Giele}
\abstract{Expect typo's and oversights...}
\keywords{}
\preprint{}
\begin{document} 

\section{The Generic Algorithm}

This is a first go through. There are still some ``rough spots'' in the arguments. 
Also the notations between the different sections are not uniform at the moment. 

In fixed order calculations the jet axis is calculated order by order.
The jet itself is opaque, i.e. we should not ask questions about its internal structure.
By doing this we make sure perturbative QCD works well. Of course, the jets should be 
sufficiently isolated by enforcing appropriate jet cuts. 
The opaqueness integrates out all physics below
the jet energy scale ($Q_{\mbox{jet}}\sim E_t^{\mbox{jet}}\times\Delta R_{jj}^{\mbox{min}}$ 
or simply $\sqrt{s_{\mbox{cut}}}$). This guarantees all
phase space regions with enhanced logarithmic contributions have been integrated out
and are inaccessible .
 
To make the jet transparent again we can shower the jet configuration, starting
from color charged partonic jet axis (requiring that the jet reconstruction gives
back the original jet configuration). The transparent jet is a series of 
possible hadronic configurations. These hadronic realizations are important 
for interfacing with realistic detector models and can be used to derive the detector 
response function for the given jet configuration. However, for detector corrected
date only the average jet is relevant. Hence the perturbatively calculated opaque multi-jet 
observables can be compared to the detector corrected data directly.

A typical $2\rightarrow 1$ clustering jet algorithm generates a jet mass. However,
this quantity is an artifact and should be integrated/averaged over. That is, we can calculate
the three-vectors of the jets and their correlations. In a $3\rightarrow 2$ clustering
algorithm the jet remains massless and no artificial mass is generated. However,
it calculates again the correlations between the jet three-vectors.

As we will evaluate the real radiation contribution to the NLO corrections on
a GPU, we favor a simple, brute force phase space integration method. The
generally used subtraction methods are the exact opposite of what we want to use.
On the other hand, the slicing method is perfect in all respects for a brute force
GPU implementation. 
Therefore we will use slicing method to regulate the phase space divergences as outlined in 
Ref.~\cite{Giele:1993dj}. In this picture we imagine measuring resolved partons.
I.e. we can observe partons with a resolution of scale $s_{min}$. For example,
if two partons have an invariant mass smaller than $s_{min}$ 
they are observed as one parton. Or one could use a dipole picture, if 
$R(p_a,p_1,p_b)<s_{min}$ (with e.g. $R(p_a,p_1,p_b)=s_{a1}s_{1b}/s^2_{a1b}$)
parton $p_1$ is unresolved and we observe state
$p_a,p_b$ instead of $p_a,p_1,p_b$. We take $s_{min}$ small enough such we
can use soft/collinear matrix element factorization and approximate
the phase space by neglecting kinematic effects of the unobserved particle.
Within this approximation we have an universal soft/collinear factor which
can be calculated by integrating out the unresolved partons analytically
in dimensional regularization. The final observed resolved partons are 
4-dimensional. The analytically calculated soft/collinear factor is added to
the virtual corrections and renders them IR finite. The real radiation 
corrections involve now an integration over resolved parton phase space.
When combining real and virtual any dependence on the resolution parameter
will cancel in the limit $s_{min}\rightarrow 0$. 

By integrating over the jet phase space we can calculate jet observables
defined by  $X=X(J_1,\ldots,J_n)$
\beq
\frac{d\,\sigma}{d\,X}=
\int d\,\mbox{PS}(J_1,\ldots,J_n)\ \delta\left(X-X(J_1,\ldots,J_n)\right)
\frac{d^{(n)}\sigma}{d\,J_1\cdots d\,J_n}\ .
\eeq
Given the slicing method we can calculate the NLO corrections
to a multi-jet phase space point. A multi-jet phase space point is simply a 
set of jet 4-vector axis $\{J_1,\ldots,J_n\}$ generated according to flat phase space.
The leading order weight to this event is simply given by the LO matrix element
squared where each parton momentum is given by a jet-axis.
\beqa
\frac{d^{(n)}\sigma_{\mbox{\tiny LO}}}{d\,J_1\cdots d\,J_n}&=&
\int d\,\mbox{PS}(p_1,\ldots,p_n)
\ \delta(J_1-p_1)\cdots\delta(J_n-p_n)
\left|{\cal M}^{(0)}(p_1,\ldots,p_n)\right|^2\nn
&=&\left|{\cal M}^{(0)}(J_1,\ldots,J_n)\right|^2\ .
\eeqa
We can endow each massless 
jet with a polarization state, which is trivially transferred to the parton at LO.

At NLO the situation is more involved. The fully differential multi-jet cross
section is given by a virtual and real contribution
\beqa
\frac{d^{(n)}\sigma_{\mbox{\tiny NLO}}}{d\,J_1\cdots d\,J_n}&=&
\int d\,\mbox{PS}(p_1,\ldots,p_n)
\delta(J_1-p_1)\cdots\delta(J_n-p_n)
2\Re\left(\left[{\cal M}^{(0)}\!\!\times\!\!
{{\cal M}^{(1)}}^\dagger\right](p_1,\ldots,p_n)\right)
\nn &+&
\int d\,\mbox{PS}(\widehat p_1,\ldots,\widehat p_{n+1})
\Theta_J(\widehat p_1,\ldots,\widehat p_{n+1}|J_1,\ldots,J_n)
\left|{\cal M}^{(0)}(\widehat p_1,\ldots,\widehat p_{n+1})\right|^2
\eeqa
where the jet function $\Theta_J $ is non-zero if and only if the momenta
$\{\widehat p_1,\ldots,\widehat p_{n+1}\}$ are mapped to the jet-axis
$\{J_1,\ldots,J_n\}$ using the jet algorithm.
Using the dipole decomposition we can expand the jet function in terms
of dipoles
\beqa
\Theta_J(\widehat p_1,\ldots,\widehat p_{n+1}|J_1,\ldots,J_n)
&=&\sum_{a,r,b}
\Theta_J(\widehat p_a,\widehat p_r,\widehat p_b|J_a,J_b)\,
\theta(\widehat R_{arb}<R_{cut})
\theta(\widehat R_{abr}=\min_{ijk}\widehat R_{ijk})\nn
&&\times
\prod_{m\neq\{a,r,b\}} \delta(J_m-\widehat p_{i_m}) \nn
\eeqa
where $\Theta_J(p_a,p_b,p_c|J_1,J_2)$ is non-zero if and only if the jet algorithm
clusters momentum $p_a$, $p_b$ and $p_c$ to give jet-axis $J_1$ and $J_2$.
The function $\theta(X)$ equals 1 if $X$ is true and 0 otherwise.
The jet resolution function is given by $R_{ijk}=R(p_i,p_j,p_k)$, i.e.
if $\min_{ijk} R_{ijk}>R_{cut}$ there are no more clusterings.

The real radiation contribution from an antenna to the differential jet cross 
section can be rewritten as
\beqa\lefteqn{
d\,\mbox{PS}(\widehat p_1,\ldots,\widehat p_{n+1})\,
\Theta_J(\widehat p_1,\widehat p_2,\widehat p_3|J_1,J_2)
\delta(J_3-\widehat p_4)\cdots\delta(J_n-\widehat p_{n+1})=} \nn &&
d\,\mbox{PS}(p_1,\ldots,p_n)\times
d\,\mbox{PS}(\widehat p_1,\widehat p_r,\widehat p_2|p_1,p_2)
\delta(J_1-p_1)\delta(J_2-p_2)\delta(J_3-p_3)
\cdots\delta(J_n-p_n)\nn
\eeqa
by relabeling the integration variables/momenta.

This means we can write the NLO jet cross section as
\beqa\lefteqn{
\frac{d^{(n)}\sigma_{\mbox{\tiny NLO}}}{d\,J_1\cdots d\,J_n}=}\nn 
&&
\int d\,\mbox{PS}(p_1,\ldots,p_n)
\delta(J_1-p_1)\cdots\delta(J_n-p_n)
\left|{\cal M}^{(0)}(p_1,\ldots,p_n)\right|^2
\Big(1+{\cal K}(p_1,\ldots,p_n)\Big)\nn
&=&
\left|{\cal M}^{(0)}(J_1,\ldots,J_n)\right|^2
\Big(1+{\cal K}(J_1,\ldots,J_n)\Big)
\eeqa
Each jet phase space point gets a ``{\cal K}-factor'' correcting the LO
prediction.
The ``${\cal K}$-factor'' is given by
\beqa\lefteqn{
\left|{\cal M}^{(0)}(p_1,\ldots,p_n)\right|^2\Big(1+{\cal K}(p_1,\ldots,p_n)\Big)=}\nn &&
\left|{\cal M}^{(0)}(p_1,\ldots,p_n)\right|^2+
2\Re\left(\left[{\cal M}^{(0)}\times
{{\cal M}^{(1)}}^\dagger\right](p_1,\ldots,p_n)\right) \nn
&+&\sum_{a,b}\int
d\,\mbox{PS}(\widehat p_a,\widehat p_r,\widehat p_b|p_a,p_b)
\,\theta(\widehat R_{arb}<R_{cut})\theta(\widehat R_{abr}=\min_{ijk}\widehat R_{ijk})
\,\left|{\cal M}^{(0)}(\widehat p_a,\widehat p_r,\widehat p_b,p_1,\ldots,p_n)\right|^2\nn
\eeqa
As we see this will be IR finite and after mass/coupling constant
renormalization give the NLO physical correction to the 
fully differential multi-jet cross section. Note that we neglected
a constraint which is generated by demanding that the antenna where
we generate the extra particle is the antenna which actually clusters.
  
The slicing is trivially implemented
as the $2\rightarrow 3$ antenna phase space is given by
\beq
d\,\mbox{PS}(\widehat p_a,\widehat p_r,\widehat p_b|p_a,p_b)
=\frac{1}{16\pi^2}\frac{1}{s_{ab}}\ 
d\,\widehat s_{ar}\ d\,\widehat s_{rb}\
\theta(\widehat s_{ar}>s_{min})\theta(\widehat s_{rb}>s_{min})
\theta(\widehat s_{ar}+\widehat s_{rb}<s_{ab})
\eeq
We can choose to add the further partition
$\Big(\theta(\widehat s_{ar}<\widehat s_{rb})+\theta(\widehat s_{rb}\leq\widehat s_{ar})\Big)$ to 
the phase space.

The calculational algorithm will be simple:
\begin{enumerate}
\item Generate an $n$-jet phase space point (using Rambo, Sarge, Haag,...) with
zero mass jets. 
\item Start in parallel a CPU and GPU calculation:
  \begin{enumerate}
    \item The CPU calculates the one-loop correction to the $2\rightarrow\ n$
      gluon scattering using the code of Ref~\cite{Winter:2009kd}. 
      The analytically calculated unresolved part of the radiative
      corrections, as given in Ref.~\cite{Giele:1993dj}, are added.
      The CPU can use the GPU to evaluate tree-level blobs arising
      in the on-shell method. However, for a useful speed-up one
      needs to give many tree-level blobs at once to the GPU so they
      can be evaluated in parallel.
    \item The GPU calculates the resolved part of the radiative corrections constrained
      by the jet algorithm. I.e. after we apply the jet algorithm we find the initial
      jet phase space point.
  \end {enumerate}
\item Once the CPU is done calculating the loop correction, the GPU stops generating
  the real emissions and returns the weight to be combined with the unresolved 
  loop correction.
\end {enumerate}


\section{Phase Space}

Before constructing the invertible branchers, we write down the generic 
leading order differential cross sections. This will define the needed
notations and normalizations. The LO cross section (in picobarn) is given by
\beq
d\,\sigma_n={\cal C}_{\mbox{\tiny GeV$\mapsto$pb}}\frac{(2\pi)^{4-3n}}{2\,S}\sum_{ab}
\int_0^1\frac{d\,x_1}{x_1}\int_0^1\frac{d\,x_2}{x_2}
f_a(x_1)f_b(x_2)\ d\,\widehat\sigma_n(x_1H_1,x_2H_2)
\eeq
where $x_1x_2S=2\,x_1x_2\,H_1\cdot H_2=2\,p_1\cdot p_2=s_{12}$ and
${\cal C}_{\mbox{\tiny GeV$\mapsto$ pb}}=(\hbar\,c)^2=389385.73\times 10^3\ \mbox{GeV}^2\ \mbox{pb}$.
The partonic differential cross section is given by
\beq
d\,\widehat\sigma_n(p_1,p_2)=\frac{1}{{\cal S}_n}\ d\,\Phi_n(p_1p_2\mapsto k_1\cdots k_n)
\left|{\cal M}_{n+2}(p_1p_2k_1\cdots k_n)\right|^2
\eeq
where the phase space symmetry factor for identical final state particles is given by
${\cal S}_n$. The phase space factor is given by
\beqa
d\,\Phi_n(p_1p_2\mapsto k_1\cdots k_n)&=&
\delta^{(4)}\left(p_1+p_2-\sum_{i=1}^n k_i\right)\prod_{i=1}^n \frac{d^{(3)}\,\vec{k}_i}{2\,E_i} \nn
&=&\delta^{(4)}\left(p_1+p_2-\sum_{i=1}^n k_i\right)\prod_{i=1}^n d^{(4)}\,k_i\ \delta\left(k_i^2\right)\theta(E_i)\nn
&=&\left(\frac{\pi}{2}\right)^{(n-1)}\frac{s_{12}^{(n-2)}}{(n-1)!(n-2)!}\ d\,{\cal R}_n(p_1p_2\mapsto k_1\cdots k_n)
\eeqa
where $d\,{\cal R}_n$ is the phase space generated by the RAMBO algorithm~\cite{Kleiss:1985gy}. We will use
this generator as the jet phase space generator.



\subsection{Invertible Branchers}

To construct the $2\mapsto 3$ branching algorithm we 
have to make sure it is invertable. That is, the jet algorithm
should be able to find the branched momenta uniquely and apply
the inverse $3\mapsto 2$ map. This ensures the radiative events
always reconstruct back to the {\it same} jet phase space point.
We want to model this as close as possible to the ``standard''
$2\rightarrow 1$ clustering algorithm. By amending the standard jet algorithm
with an extra step to render the final state clusters massless we accomplish
this as. We do this by selecting an additional parton as the recoiler.

Starting from the jet phase space point
\[d\,\Phi_n\left(P_1P_2\mapsto K_1K_2\cdots K_n\right)\]
we generate a radiative event using an antenna $2\mapsto 3$ mapping.
This will result in a radiative event
 \[d\,\Phi_{n+1}\left(p_1p_2\mapsto k_1k_2\cdots k_{n+1}\right)\]
To make sure the jet algorithm finds the correct branched triple $\{k_a,k_r,k_b\}$
which originated from the parent-recoiler pair $\{K_a,K_b\}$ we do 2 partitions of unity to
divide phase space in $n\times (n-1)\times (n-2)/6$ sectors. Each sector will
contain its unique branching $K_aK_b\mapsto k_ak_rk_b$.
This is done in the following manner
\[
\theta( R_{arb}=\min_{ijk} R_{ijk})\rightarrow\theta^{(arb)}=
\theta\left( R_{ar}=\min_{ij} R_{ij}\right)\times \theta\left( R_{arb}= R_{ar}\times\min_k( R_{rk}, R_{ak})\right)
\]
where $R_{ij}=R(k_i,k_j)$ is the standard jet algorithm resolution parameter.
The radiative phase space now becomes
\beq
d\,\Phi_{n+1}\left(p_1p_2\mapsto k_1k_2\cdots k_{n+1}\right)
=\sum_{a,r,b}d\,\Phi_{n+1}^{(arb)}\left(p_1p_2\mapsto k_1k_2\cdots k_{n+1}\right)
\eeq
with
\beqa\lefteqn{
d\,\Phi_{n+1}^{(arb)}\left(p_1p_2\mapsto k_1k_2\cdots k_{n+1}\right)}\nn &=&
d\,\Phi_{n+1}\left(p_1p_2\mapsto k_1k_2\cdots k_{n+1}\right)\times
\theta\left( R_{ar}=\min_{ij} R_{ij}\right)\times 
\theta\left( R_{arb}= R_{ar}\times\min_k( R_{rk}, R_{ak})\right)\nn
&=&\theta^{(arb)}d\,\Phi_{n+1}\left(p_1p_2\mapsto k_1k_2\cdots k_{n+1}\right)
\eeqa
This partitioning of phase space is in essence the $p_T$-ordered partitioning using antenna resolution 
parameter $R_{arb}=R_{ar}R_{rb}$. 
Note that the summations include also the initial state partons, i.e. clustering with the beam jets.

With the partitioned phase space we can guarantee that the jet algorithm can find the branched triplet
by performing the following steps
\begin{enumerate}
\item Find the pair of clusters $\{k_a,k_r\}$ with the smallest resolution $R_{ar}$.
\item Subsequently find the recoiler particle $k_b$ by finding the smallest double resolution
$R_{ar}\times\min\left( R_{rb}, R_{ab}\right)\sim\min\left( R_{rb}, R_{ab}\right)$.
\end{enumerate}
We now have selected a unique phase space $d\,\Phi_n^{(arb)}$ out of the partitioned sets. The next step is to branch
the phase space $d\,\Phi_n(p_1p_2\mapsto K_1\cdots K_aK_b\cdots K_n)$ to the radiative event 
$d\,\Phi_{n+1}^{(arb)}$. Depending on whether the
parent parton $a$ is a final or initial state particles
we get 2 distinct branchers and the associated jet clusterings.
The 2 cases are the final-final state and the initial-final state branchers.
The basic form of the needed phase space factorizations are given Ref.~\cite{Daleo:2006xa}. We will use
these factorizations as our starting points.

\subsubsection{The final-final state particle brancher}

For the final-final state brancher we have the factorization
\beqa\lefteqn{
d\,\Phi_{n+1}^{(arb)}(P_1P_2\mapsto K_1\cdots k_ak_rk_b\cdots K_n)} \nn
&=&d\,\Phi_n(P_1,P_2\mapsto K_1\cdots K_aK_b\cdots K_n)\
d\,\Phi_{\mbox{\tiny antenna}}(K_aK_b\mapsto k_ak_rk_b)\times\theta^{(arb)}\nn
&=&\left(\frac{\pi}{4}\right)\,s_{AB}\ d\,\Phi_n(P_1P_2\mapsto K_1\cdots K_n)\,
d\,y_{ar}\ d\,y_{rb}\ \left(\frac{d\,\phi}{2\pi}\right)\ \theta(y_{ar}+y_{rb}\leq 1)
\times\theta^{(arb)}\nn
\eeqa
where $s_{\bullet}=s_{AB}\,y_{\bullet}=2\,K_A\cdot K_B\,y_{\bullet}$.
Note that the partitioning constraint $\theta^{(arb)}$ enforces the constraint $R_{ar}<R_{rb}$
onto the brancher phase space.

We can now explicitly formulate the branching algorithm and its 
inverse the jet algorithm for final state clusters.
The branching algorithm goes as follows
\begin{enumerate}
\item Select a final-final state pair to branch $\{K_a,K_b\}$.
\item Generate the integration variables $y_{ar}$, $y_{rb}$ and $\phi/2\pi$
on the interval $(0,1]$ with the constraints $y_{ar}+y_{rb}<1$.
\item Rescale the momenta $\{K_a,K_b\}$:
\beq
\left\{\begin{array}{l}
\widehat k_a=K_a+\gamma\,K_b \\
         k_b=(1-\gamma)\,K_b
\end{array}\right. 
\eeq
such that $\widehat k_a^2=s_{ar}$, i.e. $\gamma=s_{ar}/s_{AB}$. (Note that $K_a+K_b=\widehat k_a+k_b$.)
\item Calculate $\{k_a,k_r\}$ by performing a $1\rightarrow 2$ decay:
$\widehat k_a\rightarrow k_a+k_r$ with the constraints $k_a^2=k_r^2=0$ and $(k_r+k_b)^2=s_{AB}\,y_{rb}=s_{rb}$.
  This is readily done using the following algorithm:
  \begin{enumerate}
    \item Boost and rotate system from lab frame to the rest frame of $\widehat k_a$ with $k_b$ along
      the positive $z$-axis.
    \item Choose the parametrization
      \begin{eqnarray}
      k_r&=&\frac{1}{2}\sqrt{s_{ar}}
      \left(1,\sin(\theta)\cos(\phi),\sin(\theta)\sin(\phi),\cos(\theta)\right)\nn
      k_a&=&\frac{1}{2}\sqrt{s_{ar}}
      \left(1,-\sin(\theta)\cos(\phi),-\sin(\theta)\sin(\phi),-\cos(\theta)\right)\nonumber
      \end{eqnarray}
      The angle $\theta$ is given by the constraint $(k_r+k_b)^2=s_{AB}\,y_{rb}$:
      \[\cos\theta=1-\frac{2\,y_{rb}}{1-y_{ar}}=1-\frac{2\,s_{rb}}{s_{AB}-s_{ar}}
      =\frac{y_{ab}-y_{rb}}{y_{ab}+y_{rb}}\]
      \item Rotate and boost the system back to the lab frame.
  \end{enumerate}
  We now have generated the branching phase space
  \[ d\,\widehat\Phi_{arb}^{(ff)}=d\,y_{ar}\ d\,y_{rb}\ \left(\frac{d\,\phi}{2\pi}\right)
  \theta(1-y_{ar}-y_{rb})\]
\item Pass event with weight $\pi/4\times s_{AB}$ if and only if it obeys the partitioning constraint $\theta^{(arb)}$.
  That is, only pass the event if $R_{ar}$ is the smallest 2-particle resolution and 
  $\min\left(R_{ab},R_{rb}\right)$ is the smallest 3-particle
  resolution in the event. In this final step we added the resolution partitioning and the event weight:
  \[ d\,\Phi_{arb}^{(ff)}=\left(\frac{\pi}{4}\right)\, s_{AB}\,\theta^{(arb)}\,d\,\widehat\Phi_{arb}^{(ff)} \]
\end{enumerate}

We perform two numerical tests to validate the correctness of the algorithm.
The first test is to compare the $(n+1)$ particle phase space generated by RAMBO with the
$n$ particle generated Rambo phase space followed by the algorithm described above. 
Explicitly, we test the numerical implementation of the
brancher by comparing
\beq
d\,R_{n+1}(p_1p_2\mapsto k_1\cdots k_{n+1})=n(n-1)\ \left(\frac{s_{AB}}{s_{12}}\right)\
d\,R_n(p_1p_2\mapsto K_1\cdots K_n)\ d\,\widehat\Phi_{arb}^{(ff)}
\eeq
using RAMBO and the above described branching algorithm for a randomly selected pair of momenta $\{K_a,K_b\}$.
Note that we ignore here the partitioning veto (i.e. step 5 of the algorithm. 
\newline\comment{Include graphs and discussion}
For the second test we use the brancher to see that the sliced phase space is independent of $s_{\mbox{\tiny min}}$
by testing
\beqa
\frac{\partial}{\partial y_{\mbox{\tiny min}}}&&\left(K_{ab}(y_{\mbox{\tiny min}})
\left|{\cal M}_{n+2}(p_1p_2\mapsto K_1\cdots K_n)\right|^2\right.\nn&&\left.+
2\times d\,\Phi_{arb}^{(ff)}\theta(y_{ar}<y_{rb})\theta(y_{ar}>y_{\mbox{\tiny min}})\theta(y_{rb}>y_{\mbox{\tiny min}})
\left|{\cal M}_{n+3}(p_1p_2\mapsto k_1\cdots k_{n+1})\right|^2
\right)=0\nn
\eeqa

The jet clustering algorithm which inverts the brancher is given by
\begin{enumerate}
\item Select pair $\{k_a,k_r\}$ with smallest $R_{ar}$.
\item Select cluster $\{k_b\}$ with smallest $\min\left(R_{ab},R_{rb}\right)$.
\item If $a$, $r$ and $b$ are final state partons:
  \begin{enumerate}
    \item Cluster momenta $k_a$ and $k_r$: $\widehat k_a=k_a+k_r$
    \item Use the recoiler momentum $k_b$ to rescale momentum $\widehat k_a$.
      \beq
      \left\{\begin{array}{l}
      K_a=\widehat k_a+(1-\gamma)\,k_b \\

      K_b=\gamma\,k_b
      \end{array}\right. 
      \eeq
      with $\gamma=s_{arb}/(s_{arb}-s_{ar})$
  \end{enumerate}
\end{enumerate}

\subsubsection{The initial-final state particle brancher}

The phase space factorization for the initial-final state brancher was derived in
Ref.~\cite{Daleo:2006xa}. It is given by
\beqa\lefteqn{
d\,\Phi_{n+1}^{(arb)}(p_1P_2\mapsto K_1\cdots k_ak_r\cdots K_n)=
d\,\Phi_n(xp_1,P_2\mapsto K_1\cdots K_a\cdots K_n)\theta^{(arb)}}\nn&&\times
\left(\frac{2x\,p_1\cdot K_a}{2\pi}\right)\left(\frac{d\,x}{x}\right)
d\,k_r\ d\,k_a\delta(k_a^2)\delta(k_r^2)\delta(K_a-(k_a+k_r-(1-x)p_1))\nn
\eeqa
where $a$or $b$ is the initial state parton.
Integrating  first over $k_a$ and then over $x$ gives
\beqa
d\,\Phi_{n+1}^{(arb)}(p_1P_2\mapsto K_1\cdots k_ak_r\cdots K_n)&=&
d\,\Phi_n(xp_1,P_2\mapsto K_1\cdots K_a\cdots K_n)\nn&&\times
\left(\frac{1}{2\pi}\right)\left(\frac{d\,\vec{k}_r}{2E_r}\right)
\left(\frac{p_1\cdot K_a}{p_1\cdot K_a-p_1\cdot k_r}\right)
\eeqa
with $x=1-K_a\cdot k_r/(p_1\cdot K_a-p_1\cdot k_r)$. We now can define $P_1=xp_1$, we find then
the final formula
\beqa
d\,\Phi_{n+1}^{(arb)}(zP_1,P_2\mapsto K_1\cdots k_ak_r\cdots K_n)&=&
d\,\Phi_n(P_1,P_2\mapsto K_1\cdots K_a\cdots K_n)\nn&&\times
\left(\frac{1}{2\pi}\right)\left(\frac{d\,\vec{k}_r}{2E_r}\right)
\left(\frac{P_1\cdot K_a}{P_1\cdot K_a-P_1\cdot k_r}\right)
\eeqa
with $z=1/x=1+K_a\cdot k_r/(P_1\cdot K_a-P_1\cdot k_r)$.

This makes the branching algorithm very simple
\begin{enumerate}
\item
  Select a final-initial state pair $\{P_1,K_a\}$ to branch
\item
  Generate the momentum $\vec k_r$. The phase space constraints on the
  momentum generator are induced by $zP_1=zx_1H_1=x_1z\frac{1}{2}\sqrt{S}(1,0,0,\pm 1)$.
  This means we have the constraint $1\leq z\leq 1/x_1$ where 
  $z=1+K_a\cdot k_r/(P_1\cdot K_a-P_1\cdot k_r)$.
  To be able to optimize the momentum generation we can make a change of variables.
  \[ \frac{d\,\vec k_r}{2E_r}=d\left|\vec k_r\right|\,d\cos(\theta)\,d\phi\times \frac{1}{2}\left|\vec k_r\right| \]
  where 
  \[ \vec k_r=\left|\vec k_r\right|(\cos\phi\sin\theta,\sin\phi\sin\theta,\cos\theta) \]
  The initial state singularity is of the form $\left(\left|\vec k_r\right|^2(1-\cos\theta)\right)^{-1}$
  which can be easily sampled using the integration variables. The upper integration boundary 
  on the momentum magnitude is given by the requirement that the incoming partonic energy cannot
  exceed the beam energy. This condition is simply given by 
  \[ E_1+\left|\vec k_r\right|<\frac{1}{2}\sqrt{S}
  \Rightarrow \left|\vec k_r\right|<\frac{1}{2}(1-x_1)\sqrt{S} \]
\item
  Calculate the other 2 momenta
  \beq
  \left\{\begin{array}{l}
  k_a=K_a-k_r+(z-1)\,P_1 \\
  p_1=zP_1
  \end{array}\right. 
  \eeq
\item
  Pass event with weight $\frac{1}{2\pi}\frac{P_1\cdot K_a}{(P_1\cdot K_a-P_1\cdot k_r)}$ 
  if and only if it obeys the partitioning constraint $\theta^{(arb)}$.
\end{enumerate}

The jet clustering is does not change a lot from the final-final state jet algorithm:
\begin{enumerate}
\item Select pair $\{k_a,k_r\}$ with smallest $R_{ar}$.
\item Select cluster $\{k_b\}$ with smallest $\min\left(R_{ab},R_{rb}\right)$.
\item If $a$ is an initial state particle and $r$ a final state particle:
  \begin{enumerate}
    \item Cluster momenta $k_b$ and $k_r$: $\widehat k_b=k_b+k_r$
    \item Use the initial state recoiler momentum $k_a$ to rescale momentum $\widehat k_b$.
      \beq
      \left\{\begin{array}{l}
      K_b=\widehat k_b+(1-\gamma)\,k_a \\
      P_a=\gamma\,k_a
      \end{array}\right. 
      \eeq
      with $\gamma=(k_a\cdot k_b+k_b\cdot k_r+k_a\cdot k_r)/(k_a\cdot k_b+k_a\cdot k_r)$
  \end{enumerate}
\end{enumerate}

Note that the used kinematic map has all the correct soft/collinear limits while leaving the incoming 
beam particle longitudinal. If
\beq
\left\{\begin{array}{l}
K_a=k_a+k_r-(1-\gamma)\,p_1 \\
P_1=\gamma\,p_1
\end{array}\right.\
\mbox{with}\ \gamma=1-\frac{s_{ar}}{s_{1a}+s_{1r}} 
\eeq
then we have for the soft limit $k_r\rightarrow 0$ that $\gamma=1$, $K_a=k_a$ and $P_1=p_1$.
In the final state collinear limit $k_r=z\,k_a$ we find that $\gamma=1$, $K_a=k_a+k_r=(1+z)\, k_a$ and $P_1=p_1$.
Finally, for the initial state collinear limit $k_r=z\,p_1$ we have $\gamma=1-z$, $K_a=k_a$ and $P_1=(1-z)\,p_1$.
So we see that we do not need to do a beam jet clustering, which would require boosts etc.

\section{Using the GPU in the Calculation of Virtual Corrections}

Using the on-shell method to calculate the virtual corrections,
the algorithm spends around 90\% of the time evaluating tree-level
blobs. These tree-level blobs can be evaluated on the GPU. In order
to do this one needs to prepare the input to the GPU so that it
can evaluate many tree-level blobs in parallel. Because the cut lines
are complex, the resulting currents involving the cut line are complex
4-vectors. However, on the bright side all currents involving only external
lines have to be calculated only once and can be stored. Furthermore we only have
to sow on 1 extra internal particle to get the current.

We assume now the calculation of an ordered $n$-gluon one-loop amplitude: $m(12\cdots n)$. 
Then we organize the calculation of all the tree-level blobs as follows:
\begin{enumerate}
\item Copy the momenta and external sources to the GPU main memory.
\item For each cut determine the loop momentum and the tree-level ordered amplitudes.
This generates the list $\{m_1^{(i)},m_2^{(i)},l^{(i)}\}_{i=1}^{N_{\mbox{cuts}}}\}$
which will tell the GPU core to calculate the current $J_{\mu}(m_1,\ldots,m_2,l)$ and
the resulting matrix element $m(-(l+m_1+\cdots+m_2),m_1,\ldots,m_2,l)$ for the 4 polarization
combinations of the 2 cut lines.
\item Split the list in $n$ equal sized lists $L_k$ ($k=1,\ldots,n$)
of size $N_{\mbox{cuts}}/n$ in the following manner
$L_k=\{m_k^{(i)},m_2^{(i)},l^{(i)}\}_{i=1}^{N_{\mbox{cuts}}/n}\}$
\item Transfer the lists to the GPU main memory
\item Start the GPU kernel:
  \begin{enumerate}
    \item Core 1 calculates the external currents $J_1,J_{12},\ldots,J_{12\cdots n},\ldots,J_n$
      and stores them together with the external 4-vectors in the shared memory. The other
      cores do a cyclic version of the core 1 calculation (i.e. core 2 starts with $J_2$ etc.).
      The shared memory usage so far is $n\times(n+1)$ 4-vectors.
    \item Core $k$ loads an entry from list $L_k$. The currents are all lined up correctly for
      adding the cut particle with complex momenta to the currents. The storage requirement here
      is one complex momentum and $m_2^{(i)}-m_1^{(i)}+1$ complex 4-currents. The storage requirement
      is $2\times(m_2^{(i)}-m_1^{(i)}+2)$ 4-vectors (the factor of 2 takes into account the complex values).
    \item Load as many entries as fits in the shared memory
    \item Start the recursion and calculate the 4 polarization states
    \item Copy the resulting 4 matrix elements to the GPU main memory.
    \item Repeat if all entries in the list are not yet processed.
  \end{enumerate}
  \item Copy the results to the CPU.
\end{enumerate}

%-----------------------------------------------------------------
%\newpage
\begin{thebibliography}{99}

%\cite{Winter:2009kd}
\bibitem{Winter:2009kd}
  J.~-C.~Winter, W.~T.~Giele,
  %``Calculating gluon one-loop amplitudes numerically,''
  [arXiv:0902.0094 [hep-ph]].

%\cite{Giele:1991vf}
\bibitem{Giele:1991vf}
  W.~T.~Giele, E.~W.~N.~Glover,
  %``Higher order corrections to jet cross-sections in e+ e- annihilation,''
  Phys.\ Rev.\  {\bf D46 } (1992)  1980-2010.
  
%\cite{Giele:1993dj}
\bibitem{Giele:1993dj}
  W.~T.~Giele, E.~W.~N.~Glover, D.~A.~Kosower,
  %``Higher order corrections to jet cross-sections in hadron colliders,''
  Nucl.\ Phys.\  {\bf B403 } (1993)  633-670.
  [hep-ph/9302225].

%\cite{Giele:2010ks}
\bibitem{Giele:2010ks}
  W.~Giele, G.~Stavenga, J.~-C.~Winter,
  %``Thread-Scalable Evaluation of Multi-Jet Observables,''
  [arXiv:1002.3446 [hep-ph]].

\bibitem{NVIDIAtesla}
See the website ``http://www.nvidia.com/object/product\_tesla\_c1060\_us.html'' for
further information.

\bibitem{NVIDIAcuda}
See the website ``http://www.nvidia.com/object/cuda\_home.html'' for further information.

%\cite{GehrmannDeRidder:2005cm}
\bibitem{GehrmannDeRidder:2005cm}
  A.~Gehrmann-De Ridder, T.~Gehrmann, E.~W.~N.~Glover,
  %``Antenna subtraction at NNLO,''
  JHEP {\bf 0509 } (2005)  056.
  [hep-ph/0505111].

%\cite{Giele:2007di}
\bibitem{Giele:2007di}
  W.~T.~Giele, D.~A.~Kosower, P.~Z.~Skands,
  %``A Simple shower and matching algorithm,''
  Phys.\ Rev.\  {\bf D78 } (2008)  014026.
  [arXiv:0707.3652 [hep-ph]].

%\cite{Berends:1987me}
\bibitem{Berends:1987me}
  F.~A.~Berends, W.~T.~Giele,
  %``Recursive Calculations for Processes with n Gluons,''
  Nucl.\ Phys.\  {\bf B306 } (1988)  759.
  
%\cite{Eynck:2001en}
\bibitem{Eynck:2001en}
  T.~O.~Eynck, E.~Laenen, L.~Phaf {\it et al.},
  %``Comparison of phase space slicing and dipole subtraction methods for gamma* ---> anti-Q,''
  Eur.\ Phys.\ J.\  {\bf C23}, 259-266 (2002).
  [hep-ph/0109246].

%\cite{Kleiss:1985gy}
\bibitem{Kleiss:1985gy}
  R.~Kleiss, W.~J.~Stirling, S.~D.~Ellis,
  %``A New Monte Carlo Treatment Of Multiparticle Phase Space At High-energies,''
  Comput.\ Phys.\ Commun.\  {\bf 40}, 359 (1986).

%\cite{Daleo:2006xa}
\bibitem{Daleo:2006xa}
  A.~Daleo, T.~Gehrmann, D.~Maitre,
  %``Antenna subtraction with hadronic initial states,''
  JHEP {\bf 0704 } (2007)  016.
  [hep-ph/0612257].


\end{thebibliography}



\newpage


\end{document}



% LocalWords:  helicities
