\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Generating the Leading Order Amplitudes}{3}
\contentsline {subsection}{\numberline {2.1}Real versus Complex Valued Amplitudes}{3}
\contentsline {subsection}{\numberline {2.2}Generalized Recursion Relations}{4}
\contentsline {subsubsection}{\numberline {2.2.1}Recursion Relations for Generic Processes}{4}
\contentsline {subsubsection}{\numberline {2.2.2}Recursion Relations for Multi-Jet Production}{5}
\contentsline {subsection}{\numberline {2.3}Ordered Recursion}{6}
\contentsline {subsection}{\numberline {2.4}An algorithm for helicity summed color-ordered amplitudes}{6}
\contentsline {section}{\numberline {3}The TESS Monte Carlo}{9}
\contentsline {subsection}{\numberline {3.1}TESLA vs FERMI GPU}{9}
\contentsline {section}{\numberline {4}Validation of the TESS Monte Carlo}{11}
\contentsline {section}{\numberline {5}Predictions and Data Comparison using the TESS Monte Carlo}{11}
\contentsline {section}{\numberline {6}Conclusions}{11}
