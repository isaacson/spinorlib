%%%%%%%%%%%%%%%%%%%%%%%%%%%% FOR JHEPcls 3.1.0 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[]{JHEP3} % 10pt is ignored!

\let\ifpdf\relax
\usepackage{ifpdf}
%\JHEP{00(2009)000}

%\JHEPspecialurl{http://jhep.sissa.it/JOURNAL/JHEP3.tar.gz}

\usepackage{epsfig,multicol,bbm}
\usepackage[sort&compress,numbers]{natbib}
\usepackage{graphicx}

\newcommand{\eps}{\epsilon}
\newcommand{\veps}{\varepsilon}
\newcommand{\vs}{\varsigma}
\newcommand{\td}{\tilde}
\newcommand{\beq}{\begin{equation}}
\newcommand{\eeq}{\end{equation}}
\newcommand{\beqa}{\begin{eqnarray}}
\newcommand{\eeqa}{\end{eqnarray}}
\newcommand{\Tr}{ {\rm Tr} }
\def\trm{\textrm}
\def\nn{\\ \nonumber}
\def\nnb{\nonumber}
\def\cal{\mathcal}

\title{Notes on calculating squared QCD LO matrix elements on GPU's
with full color dependence} 


\author{Walter T. Giele}
%\received{\today} 		%%
%\revised{}
%\accepted{\today}		%% These are for published papers.


\abstract{}
%\notoc

\begin{document} 

\section{Introduction}

Outlined are two strategies to tackle the calculation. 
The first one is based on ordered amplitudes and is compute 
intensive but memory lean. 
The second strategy is based on color dressed recursion similar
to the methods used in COMIX. This method is more memory intensive
and less compute intensive. We can explore the all gluonic 
cross sections first to see what the right compute latency 
vs memory latency balance is. Depending on the GPU architecture one 
should reduce the memory latency on the GPU so the threads idle time
is minimized. That is, recalculating currents can be faster that retrieving
them from memory.
 
Another issue to explore are the polarization and color states of the 
external partons.
Again there are several approaches to explore. The most efficient 
method might not be the same for GPU vs CPU.
The traditional method is using the COMIX approach 
of MC over the discrete degrees of freedom. I.e. randomly assign
a helicity and color per event of each external parton.
The other method is to use continues polarization and color
as outlined in
hep-ph/9807207 (``On the computation of multigluon amplitudes'' by
Draggiotis, Kleiss and Papadopoulos).
Now the discreet states are replaced by continues integration
variables. Which of these two methods will be better needs to
be explored. The continues polarization method served well on the GPU
in
arXiv:1002.3446 (``Thread-Scalable Evaluation of Multi-Jet Observables''
by Walter Giele, Gerben Stavenga, Jan-Christopher Winter).

\section{Constructing amplitude scalars}

The squared tree-level amplitude is given by 
\beq
|M_n|^2=\sum_{\mu_1\cdots\mu_n}\sum_{a_1\cdots a_n} m_{a_1\cdots a_n}^{\mu_1\cdots\mu_n}\times 
(m^{a_1\cdots a_n}_{\mu_1\cdots\mu_n})^\dagger
\eeq
where $a_k$ is a color index.
In this form calculating the amplitude would be numerically very expensive
as it is a high rank tensor. 
To circumvent this issue we use a dyadic seperation of the tensors turning
them in scalars. Specifically
\beq
-g_{\mu_k\nu_k}+\frac{b_{\mu_k} p_{\nu_k}+p_{\mu_k} b_{\nu_k}}{b\cdot p}
\sim\sum_{\lambda_k}\epsilon_{\mu_k}^{\lambda_k}\epsilon_{\nu_k}^{-\lambda_k}
\sim\int\ d\phi_k e_{\mu_k}(\phi_k)e_{\nu_k}(\phi_k)
\eeq
where we use the light-cone gauge.

Similarly for color we can construct color polarizations such that
\beq
\delta_{a_kb_k}=\int d[z_k] \eta_{a_k}([z]_k)(\eta_{b_k}([z]_k))^\dagger
\eeq 
One can also construct a discreet variable decomposition. Formally
COMIX uses the ``trivial'' decomposition
\beq
\delta_{a_kb_k}=\sum_i \eta^i_{a_k}\eta^i_{b_k}
\eeq 
with $\eta^i_a=\delta_{i\,a}$. One could explore more efficient discreet
compositions.

Using the continues variable dyadic decompositions we obtain
\beqa
|M_n|^2&=&\prod_i\int d\phi_i\,d [z]_i |m(\phi_1,[z]_1,\ldots,\phi_n,[z]_n)|^2
\nonumber\\
m(\phi_1,[z]_1,\cdots,\phi_n,[z]_n)&=&m_{a_1\cdots a_n}^{\mu_1\cdots\mu_n}\times
e_{\mu_1}(\phi_1)\cdots e_{\mu_n}(\phi_n)\times
\eta_{a_1}([z]_1)\cdots\eta_{a_n}([z]_n)
\eeqa

The helicity decomposition is particularly useful in analytic calculations
as it allows for the judicious choice of gauge vectors greatly simplifying
the calculations. Similarly the trivial color decomposition is also useful
in analytic calculations as the color part and kinematic part of the
amplitude factorize in a few pieces resulting in the color-ordered
decomposition methods.

For numerical evaluations on a CPU/GPU such as recursion methods the
continues variable method might be more efficient. It removes the 
administrative part of dealing with many helicities and color-ordered
amplitudes. Furthermore the continues poarization variables can easily
be included in a MC integration over phase space.   


\section{Construction continues variable color polarizations}

We need to construct two dyadic decompositions. One is for the quark
color delta function 
\beq
\delta_{ij}=\int d[y]\, y_iy_j
\eeq
and the other is for the gluonic delta function
\beqa
\delta^{ab}&=&Tr(T^aT^b)=T^a_{ij}T^b_{ji}\nonumber\\
&=&\int d[y]_1 d[y]_2\,y_{1i}T^a_{ij}y_{2j}\, y_{2k}T^b_{kl}y_{1l}\nonumber\\
&=&\int d[y]_1 d[y]_2\,\eta^a(y_1,y_2)\eta^b(y_2,y_1)\nonumber\\
&=&\int d[y]_1 d[y]_2\,\eta^a(y_1,y_2)(\eta^b(y_1,y_2))^\dagger
\eeqa
with the gluon color polarization vector
\beq
\eta^a(y_1,y_2)=y_{1i}T^a_{ij}y_{2j}
\eeq
Note that this is a different construction from the hep-ph/9807207
paper with less integration variables

The only remaining task is to construct the quark color polarization
vector. This is quite straightforward by choosing vectors on a unit sphere
\beq
y_i=\left(
\begin{array}{c}
\sqrt{3}\cos\phi \\
\sqrt{2}\sin\phi\cos\theta \\
\sqrt{2}\sin\phi\sin\theta \\
\end{array}
\right)
\eeq
and
\beq
\int d[y]=\frac{1}{4\pi}\int_0^{2\pi}d\theta\int_0^\pi d\phi\,\sin\phi
=\frac{1}{4\pi}\int_0^{2\pi}d\theta\int_{-1}^1 d\cos\phi
\eeq


The explicit form of the gluon color polarization vector is given y
\beq
\eta^a(y,z)=x_iT^a_{ij}z_j
=\left(
\begin{array}{c}
y_2z_1+y_1z_2 \\
i (y_2z_1-y_1z_2) \\
y_1z_1-y_2z_2 \\
y_3z_1+y_1z_3 \\
i (y_3z_1-y_1z_3) \\
y_3z_2+y_2z_3 \\
i (y_3z_2-y_2z_3) \\
y_1z_1+y_2z_2-2y_3z_3
\end{array}
\right)
\eeq

Alternatively we can choose the gluon color polarization vector to be a unit
vector on an 8-dimensional sphere
\beq
\delta_{ab}=\int d\,[z]\, z_az_b
\eeq
with 
\beq
z_a\sim\left(
\begin{array}{l}
\cos\phi_1 \\
\sin\phi_1\cos\phi_2 \\
\sin\phi_1\sin\phi_2\cos\phi_3 \\
\sin\phi_1\sin\phi_2\sin\phi_3\cos\phi_4 \\
\sin\phi_1\sin\phi_2\sin\phi_3\sin\phi_4\cos\phi_5 \\
\sin\phi_1\sin\phi_2\sin\phi_3\sin\phi_4\sin\phi_5\cos\phi_6 \\
\sin\phi_1\sin\phi_2\sin\phi_3\sin\phi_4\sin\phi_5\sin\phi_6\cos\phi_7 \\
\sin\phi_1\sin\phi_2\sin\phi_3\sin\phi_4\sin\phi_5\sin\phi_6\sin\phi_7 \\
\end{array}
\right)
\eeq
The advantage is the gluon currents will be real instead of complex. The
disadvantage is we almost double the number of integration angles.



We will add the normalizations once we choose the integration variables and
perform a numerical validation of the construction.

\section{Color dressed currents and recursion}

The single gluon current is given by
\beq
J^a_\mu(1)=\eta^a(y_1,z_1) J_\mu(1)=\eta^a(y_1,z_1) e_\mu(\phi_1)
\eeq

We will see this generalizes for a multi-gluon ordered current to
a generic form
\beq
J^a_\mu(1\cdots n)=C(y_1,z_1\cdots y_n,z_n)\eta^a(y,z) J_\mu(1\cdots n)
\eeq
where the function $C$ is a scalar consisting of products to polarization
vectors.

The merger of two currents through the 3-gluon vertex is given by
\beqa
J^a_\mu(1,2)&=&{J_1}_{\mu_1}^{a_1}{J_2}_{\mu_2}^{a_2}\times f^{a_1a_2a}
V^{\mu_1\mu_2\mu}(K_1,K_2,-K_1-K_2)\nonumber\\
&=&(\eta^{a_1}(y_1,z_1)\eta^{a_2}(y_2,z_2)f^{a_1a_2a})
\times J_\mu(1,2)
\eeqa

For unordered currents we simply evaluate the color factor
numerically
\beqa\label{contractstruc}
\eta_a(1,2)&=&\eta_{a_1}(1)\eta_{a_2}(2)f^{aa_1a_2}
\nonumber\\ && \\
\eta^1(1,2)&=&f^{123}\Gamma_{23}+f^{147}\Gamma_{47}+f^{156}\Gamma_{56}
\nonumber \\
\eta^2(1,2)&=&f^{231}\Gamma_{31}+f^{246}\Gamma_{46}+f^{257}\Gamma_{57}
\nonumber \\
\eta^3(1,2)&=&f^{312}\Gamma_{12}+f^{345}\Gamma_{45}+f^{367}\Gamma_{67}
\nonumber \\
\eta^4(1,2)&=&f^{471}\Gamma_{71}+f^{453}\Gamma_{53}+f^{458}\Gamma_{58}
\nonumber \\
\eta^5(1,2)&=&f^{561}\Gamma_{61}+f^{572}\Gamma_{72}+f^{534}\Gamma_{34}+f^{584}\Gamma_{84}
\nonumber \\
\eta^6(1,2)&=&f^{615}\Gamma_{15}+f^{624}\Gamma_{24}+f^{673}\Gamma_{73}+f^{678}\Gamma_{78}
\nonumber \\
\eta^7(1,2)&=&f^{714}\Gamma_{14}+f^{725}\Gamma_{25}+f^{736}\Gamma_{36}+f^{786}\Gamma_{86}
\nonumber \\
\eta^8(1,2)&=&f^{845}\Gamma_{45}+f^{867}\Gamma_{67}
\nonumber \\
\eeqa
with
\beq
\Gamma_{ij}=\eta_i(1)\eta_j(2)-\eta_i(2)\eta_j(1)
\eeq
and (antisymmetric under permutations)
\beqa
f^{123}&=&1\nonumber\\
f^{147}&=&f^{156}=f^{257}=f^{345}=f^{367}=1/2\nonumber\\
f^{458}&=&f^{678}=\sqrt{3}/2
\eeqa
(all other combinations are zero).

For ordered currents we follow a different approach as the structure
constant is broken up $f^{aa_1a_2}=Tr(T^{a}T^{a_1}T^{a_2})-Tr(T^{a}T^{a_2}T^{a_1})$
and only one of the two contributes to a particular ordered amplitude.
The basic structure to work out is therefore
\beqa\lefteqn{
\eta^{a_1}(y_1,z_1)\eta^{a_2}(y_2,z_2) Tr(T^aT^{aa_1a_2})=}\nonumber\\ &&
 <y_1z_2>\eta^a(z_1,y_2)
-\frac{1}{2N}(<y_1z_1>\eta^a(z_2,y_2)+<y_2z_2>\eta^a(z_2,y_2))\nonumber \\
&&\rightarrow <y_1z_2>\eta^a(z_1,y_2)
\eeqa
where $<yz>=\sum_iy_iz_i$ and the last step uses the antsymmetry of the ordered
current $J(1,2)=-J(2,1)$ canceling the abelian color supressed contribution.

The 3-gluon vertex contribution to the $n$-gluon current is recursively 
given by
\beq
J_{\mu}^a(12\cdots n)=<y_1z_2><y_2z_3>\cdots <y_{n-1}z_n> \eta^a(z_1,y_n)\times
\sum_m [J(1\cdots m),J(m+1\cdots n)]_{\mu}
\eeq
where $[J_1,J_2]_{\mu}=J_1^{\mu_1}J_2^{\mu_2}V_{\mu\mu_1\mu_2}(-K_1-K_2,K_1,K_2)$.
Note that all elements on the RHS are real numbers, hence the gluonic ordered
current is a real 4-vector. Note that by mathematicat consistency the 4-gluon
contribution to the ordered current can be added to complete the definition
of the ordered current.

For unordered currents we have to work out the color factor. The 4-gluon
vertex contribution to the current is given by  
\beqa
J^a_{mu}&=&{J_1}^{\mu_1}_{a_1}{J_2}^{\mu_2}_{a_2}{J_3}^{\mu_3}_{a_3}
\sum_{C(123)} f^{a_1a_2b}f^{ba_3a} K_{\mu_1\mu_2;\,\mu_3\mu_4}\nonumber\\
&=&\sum_{C(123)}\left((\omega_1^{a_1}\omega_2^{a_2}\omega_3^{a_3}f^{a_1a_2b}f^{ba_3a}\right)
\times\left({J_1}^{\mu_1}{J_2}^{\mu_2}{J_3}^{\mu_3}K_{\mu_1\mu_2;\,\mu_3\mu}\right)
\eeqa
To evaluate the color factor for unordered currents we use 
eq.~\ref{contractstruc} twice
\beqa
\omega_1^{a_1}\omega_2^{a_2}\omega_3^{a_3}f^{a_1a_2b}f^{ba_3a}&=&
\left(\omega_1^{a_1}\omega_2^{a_2}f^{a_1a_2b}\right)
\times\left(\omega_3^{a_3}f^{ba_3a}\right)\nonumber\\
&=&\omega_{12}^b\omega_3^{a_3}f^{ba_3a}\nonumber\\
&=&\omega_{123}^a
\eeqa 

In principle all $(n-1)!/2$ ordered amplitudes have to be calculated, however
we can make use of the fact that the phase space integration will symmetrize
over the gluons also. For leading color we can integrate using just one 
squared ordered amplitude and let phase space do the symmetrization over the 
different orderings, that is
\beqa
{\cal A}_n&=&\int d\,\mbox{PS}_n \sum_P^{(n-1)!/2} |m(12\cdots n)|^2 \nonumber \\
&=&{\cal N}\times\int d\,\mbox{PS}_n |m(12\cdots n)|^2
\eeqa
avoiding the factorial growth and ${\cal N}=(n-1)!/2$.
We can repeat this for the color order amplitudes, except now we have 
to take into account the interferences between different orderings
\beqa
{\cal A}_n&=&\int d\,\mbox{PS}_n \left|\sum_P^{(n-1)!/2} m(12\cdots n)\right|^2 
\nonumber \\
&=& \cal{N}\times\int d\,\mbox{PS}_n\,m(12\cdots n)\times
\left(m(12\cdots n)+(\cal{N}-1)m(1P(2\cdots n)\right)^*
\eeqa
where $P(2\cdots n)$ is a random permutation of the gluons 2 through $n$.
Note that the ordered amplitudes are real so the complex conjugation can
be dropped.


\section{Numerical Validation}

We will perform a set of tests relevant for the gluonic scattering 
amplitudes. Specifically we will evaluate the following 5 color factors
numerically
\beqa
C_2&=&Tr(12)\times Tr(21)=\frac{1}{4}(N^2-1)=2 \\
C_3&=&Tr(123)\times Tr(321)=
\frac{1}{8}(N^3-3N+\frac{2}{N})=\frac{7}{3} \\
C_4&=&Tr(1234)\times Tr(4321)=
    \frac{1}{16}(N^4-4N^2+6-\frac{3}{N^2})=\frac{19}{6} \\
C_5&=&Tr(12345)\times Tr(53421)=
    \frac{1}{32}(-N^3+4N-\frac{7}{N}+\frac{4}{N^3})=-\frac{29}{54} \\
C_6&=&Tr(123456)\times Tr(625314)=
    \frac{5}{64}(\frac{1}{N^2}-\frac{1}{N^4})=\frac{5}{648}
\eeqa
where $Tr(12\cdots n)=Tr(T_1T_2\cdots T_n)$ and $T_i=T^{a_i}$.

For the numerical evaluation using the color polarization vectors of eq. 3.4
we use eqs. 3.2 and 3.3.

We start with the polarization expression representing a color string
$(T_1T_2\cdots T_n)_{ij}$ which is the colorfactor for
a quark line with $n$ gluons attached. Using the quark and anti-quark color
polarization vectors $y_q$ and $z_{\bar q}$ respectively we can derive
a reduction evaluation 
\beqa
y_1^i(T_2\cdots T_n)_{ij}z_1^j&=&\langle y_1|T_2\cdots T_n|z_1\rangle
\nonumber \\
&=&\frac{1}{2}\left(\langle y_1z_2\rangle \langle y_2|T_3\cdots T_n|z_1\rangle
-\frac{1}{N}\langle y_2z_2\rangle\langle y_1|T_3\cdots T_n|z_1\rangle\right)
\eeqa
where the gluon polarization for gluon $i$ is given by $\eta^{a_i}(y_i,z_i)$.

For the evaluation of $Tr(T_1T_2\cdots T_n)$ we use
\beqa
Tr(T_1T_2\cdots T_n)&\rightarrow&\frac{1}{2}\left( 
y_1^iz_1^j-\frac{1}{N}\langle y_1z_1\rangle\delta_{ij}\right)
(T_1T_2\cdots T_n)_{ij}\nonumber\\
&=&\frac{1}{2}\left(\langle y_1|T_2\cdots T_n|z_1\rangle
-\frac{1}{N}\langle y_1z_1\rangle Tr(T_2\cdots T_n)\right)
\eeqa
To be explicit
\beqa
Tr(1)&=&0
\nonumber \\
Tr(12)&=&\frac{1}{2}\left(\langle y_1|T_2|z_1\rangle
-\frac{1}{N}\langle y_1z_1\rangle Tr(2)\right)
\nonumber \\
Tr(123)&=&\frac{1}{2}\left(\langle y_1|T_2T_3|z_1\rangle
-\frac{1}{N}\langle y_1z_1\rangle Tr(23)\right)
\nonumber \\
Tr(1234)&=&\frac{1}{2}\left(\langle y_1|T_2T_3T_4|z_1\rangle
-\frac{1}{N}\langle y_1z_1\rangle Tr(234)\right)
\nonumber \\
Tr(12345)&=&\frac{1}{2}\left(\langle y_1|T_2T_3T_4T_5|z_1\rangle
-\frac{1}{N}\langle y_1z_1\rangle Tr(2345)\right)
\nonumber \\
Tr(123456)&=&\frac{1}{2}\left(\langle y_1|T_2T_3T_4T_5T_6|z_1\rangle
-\frac{1}{N}\langle y_1z_1\rangle Tr(23456)\right)
\eeqa 
and
\beqa
\langle y_1|T_2|z_1\rangle&=&
\frac{1}{2}\left(\langle y_1z_2\rangle \langle y_2z_1\rangle
-\frac{1}{N}\langle y_2z_2\rangle\langle y_1z_1\rangle\right)
\nonumber \\
\langle y_1|T_2T_3|z_1\rangle&=&
\frac{1}{2}\left(\langle y_1z_2\rangle \langle y_2|T_3|z_1\rangle
-\frac{1}{N}\langle y_2z_2\rangle\langle y_1|T_3|z_1\rangle\right)
\nonumber \\
\langle y_1|T_2T_3T_4|z_1\rangle&=&
\frac{1}{2}\left(\langle y_1z_2\rangle \langle y_2|T_3T_4|z_1\rangle
-\frac{1}{N}\langle y_2z_2\rangle\langle y_1|T_3T_4|z_1\rangle\right)
\nonumber \\
\langle y_1|T_2T_3T_4T_5|z_1\rangle&=&
\frac{1}{2}\left(\langle y_1z_2\rangle \langle y_2|T_3T_4T_5|z_1\rangle
-\frac{1}{N}\langle y_2z_2\rangle\langle y_1|T_3T_4T_5|z_1\rangle\right)
\nonumber \\
\langle y_1|T_2T_3T_4T_5T_6|z_1\rangle&=&
\frac{1}{2}\left(\langle y_1z_2\rangle \langle y_2|T_3T_4T_5T_6|z_1\rangle
-\frac{1}{N}\langle y_2z_2\rangle\langle y_1|T_3T_4T_5T_6|z_1\rangle\right)
\eeqa

The results of the numerical evaluation of eqs 5.1-5.5 using eq. 5.8
are given in table 1. As can be seen the results validate the choosen approach.

\begin{table}[h]
\begin{center}
\begin{tabular}{c|l|l|l|l|l|l|l}
& $10\times 10^3$ & $10\times 10^4$ & $10\times 10^5$ 
& $10\times 10^6$ & $10\times 10^7$ & $10\times 10^8$ & exact\\
\hline
$C_2$ & 2.06 (7) & 1.99 (2) & 1.990 (4) 
      & 2.000 (1) & 1.9992 (5) & 1.9997 (1) & 2.0000 \\
$C_3$ & 2.2 (1) & 2.30 (3) & 2.318 (7) 
      & 2.329 (4) & 2.332 (1) & 2.3330 (2) & 2.3333 \\
$C_4$ & 3.3 (3) & 3.11 (6) & 3.13 (1) 
      & 3.168 (6) & 3.167 (2) & 3.1663 (6) & 3.1666 \\
$C_5$ & -0.4 (1) & -0.45 (3) & -0.52 (2) 
      & -0.521 (6) & -0.5348 (3) & -0.5352 (6) &-0.5370 \\
$C_6$ & 0.2 (1) & 0.02 (8) & 0.02 (1) 
      & 0.012 (4) & 0.008 (1) & 0.0073 (5) & 0.0077 \\
\end{tabular}
\caption{The numerical results for the color factors using 10 runs of $N$ 
events. The central value is the average result of the 10 runs and the
standard deviation is calculated using the results of the 10 runs}
\end{center}
\end{table}
While the above translation is needed to evaluate
the color factors 5.1-5.5, when we use this in the gluonic amplitude the 
color polarization expression greatly simplifies due to the fact that
any term containing an abelian-ized gluon is zero. That is any color
factor containing $\langle y_iz_i\rangle$ can be set to zero. As a result
the color polarization expression for ordered gluon amplitudes is simply
\beq
\sum_P Tr(T_1T_2\cdots T_n)\,m(12\cdots n)\rightarrow\sum_P
\langle z_ny_1\rangle\langle z_1y_2 
\rangle\langle z_2y_3\rangle\cdots\langle z_{n-1}y_n\rangle\,m(12\cdots n)
\eeq
which for the PT helicity amplitudes translates to
\beq
{\cal M}_n^{++-\cdots -}\sim [12]^4\sum_{P/C}
\left(\frac{\langle z_1y_2\rangle}{[12]}\right)
\times\left(\frac{\langle z_2y_3\rangle}{[23]}\right)\times\cdots
\times\left(\frac{\langle z_ny_1\rangle}{[n1]}\right)
\eeq

\section{Squaring the color dressed amplitude}

We want to avoid the factorial growth caused by the summation over
the permutations of the ordered amplitudes. To do this we want to
MC over the permutations on an event-by-event basis.

If we make the approximation that we neglect interferences between different
orderings when squaring the amplitude it is pretty straightforward. 
These interference terms are color supressed and for pure gluons
this approximation gives the usual leading color result. When quarks are 
involved a single squared ordered amplitude contains color suppressed 
contributions. In this case the neglection of interference terms gives
an enhanced leading color prediction. The included color supressed terms
within a single squared ordered amplitude are non-neglectable and represent
abelian contributions
The interference terms arise when 6 or more partons are present
and from phenomelogical studies {refs?] one finds that neglecting the
interference terms change the cross section by around a few percent.
However, this will depend on the observable and one can construct 
special observables where these interference terms become important.

By neglecting the interference terms we get
\beqa
{\cal A}_{\mbox{\tiny LC}}&=&\int d k\left|\sum_{i=1}^Pm_i(k)\right|^2 \nn
&\sim&\sum_{i=1}^P\int d k\left|m_i(k)\right|^2\nn
&=&\frac{1}{N}\sum_{n=1}^N\sum_{i=1}^P\left|m_i(k^{(n)})\right|^2\nn
&=&\frac{P}{N}\sum_{n=1}^N\left|m_{p^{(n)}}(k^{(n)})\right|^2
\eeqa
where $k^{(n)}$ is a random phase space point and $p^{(n)}$ a random permutation
of the ordered amplitude. As can be seen the factorial growth is no longer 
present and replaced by a MC sampling over the permutations.

We want to repeat this when including interference terms using the 
minimal number of evaluated ordered amplitude by picking two random orderings.
$p_1^{(n)}$ and  $p_2^{(n)}$  
\beqa
{\cal A}&=&\int d k\left(\sum_{j>i=1}^P\left|m_i(k)+m_j(k)\right|^2
-(P-2)\times\sum_{i=1}^P\left|m_i(k)\right|^2\right) \nn
&=&\int d k\sum_{j>i=1}^P\left|m_i(k)+m_j(k)\right|^2
-(P-2)\times{\cal A}_{\mbox{\tiny LC}}\nn
&=&\frac{1}{N}\sum_{n=1}^N\sum_{j>i=1}^P\left|m_i(k^{(n)})+m_j(k^{(n)})\right|^2
-(P-2)\times{\cal A}_{\mbox{\tiny LC}} \nn
&=&\frac{1}{2}\frac{P(P-1)}{N}\sum_{n=1}^N
\left|m_{p_1^{(n)}}(k^{(n)})+m_{p_2^{(n)}}(k^{(n)})\right|^2
-(P-2)\times{\cal A}_{\mbox{\tiny LC}} \nn
&=&\frac{1}{N}\sum_{n=1}^N\left(\frac{1}{2}P(P-1)\times
W_{\mbox{\tiny LC}}^{(n)}\times W_{\mbox{\tiny CC}}^{(n)}-(P-2)\times W_{\mbox{\tiny LC}}^{(n)}
\right)
\eeqa
where
\beq
W_{\mbox{\tiny LC}}^{(n)}=\frac{1}{2}
\left(|m_{p_1^{(n)}}(k^{(n)})|^2+|m_{p_2^{(n)}}(k^{(n)})|^2\right)
\sim|m_{p_1^{(n)}}(k^{(n)})|^2
\eeq
and
\beq
W_{\mbox{\tiny CC}}^{(n)}=\frac{
\left|m_{p_1^{(n)}}(k^{(n)})+m_{p_2^{(n)}}(k^{(n)})\right|^2}
{|m_{p_1^{(n)}}(k^{(n)})|^2+|m_{p_2^{(n)}}(k^{(n)})|^2}
\eeq
Note that $0\leq W_{\mbox{\tiny CC}}\leq 2$, though the extremes
are difficult to reach ($m_1=\pm m_2$). 

Within a MC we can unweight the events according to the LC weight, 
$W_{\mbox{\tiny LC}}$, and 
apply the parton/shower/hadronization/detector simmulation corrections
to them. This will give the leading color prediction for the observable 
${\cal O}_{\mbox{\tiny LC}}$
Next we can reweight
each event with the color correction weight $W_{\mbox{\tiny CC}}$ giving us
${\cal O}_{\mbox{\tiny CC}}$.

Finally we construct the full color dependent prediction of the observable
by a linear sum between the two terms 
${\cal O}_{\mbox{\tiny FC}}=\frac{1}{2}P(P-1)\times{\cal O}_{\mbox{\tiny CC}}
-(P-2)\times{\cal O}_{\mbox{\tiny LC}}$. Any attempt to do the sum of the 
two contributions on an event-by-event basisi lead to negative weights.
\end{document} 



